public class PendingApprovalsController {

    public static final String PARAM_TYPE = 'type';
    
    public String          objectName {get; set;}
    
    public Map<String, String> params {get; private set;}

    List<SelectOption> optionList;
    
    public PendingApprovalsController() {
    	objectName = ''; getObjectSelectionList(); initParams();
    }
    
    private void debug(String s) {
        System.debug(LoggingLevel.INFO, 'PendingApprovalsController : ' + s);
    }

    private void initParams() {
        PageReference pr = System.currentPageReference(); String s;
        params = pr.getParameters(); debug('params = ' + params);
        s = params.get(PARAM_TYPE); if (!String.isBlank(s)) {objectName = s;}
    }
    
    public PageReference getPendingApprovals() {
        String nextPage = Custom_Approval_Functions.getObjectPageUrl(objectName);
        if (nextPage == null) {return null;}
        PageReference pr = new PageReference(nextPage);
        pr.setRedirect(true); return pr;
    }
    
    public Boolean renderObjectSelectionList {
    	get {
    		getObjectSelectionList();
            return (optionList != null && !optionList.isEmpty());
    	}
    }

    public List<SelectOption> getObjectSelectionList() {
    	if (optionList == null) {
            optionList = Custom_Approval_Functions.getObjectSelectionList();
    	}
        if (optionList != null && optionList.size() == 1) {
            objectName = optionList[0].getValue();
        }
        return optionList;
    }

    public PageReference pageAction() {
        return getPendingApprovals();
    }

}