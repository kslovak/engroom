public class Attachment_Functions {

    private static void debug(String s) {System.debug(LoggingLevel.INFO, s);}
    
    public static Map<Id, List<Attachment>> getAttachments(Set<Id> objectIds) {
        Map<Id, List<Attachment>> amap = new Map<Id, List<Attachment>>();
        List<Attachment> alist = [select BodyLength, ContentType, CreatedById,
                                         Id, LastModifiedDate, Name, ParentId
                                    from Attachment where ParentId in :objectIds];
        if (alist.isEmpty()) {return null;}
        List<Attachment> blist; Id objectId;
        for (Attachment a : alist) {
            objectId = a.ParentId;
            if (!amap.containsKey(objectId)) {
            	amap.put(objectId, new List<Attachment>());
            }
            blist = amap.get(objectId); blist.add(a);
            amap.put(objectId, blist);
        }
        return amap;
    }
    
    public static Map<Id, Set<Id>> getAttachmentsIds(Set<Id> objectIds) {
        Map<Id, Set<Id>> amap = new Map<Id, Set<Id>>();
        List<Attachment> alist = [select Id, ParentId from Attachment 
                                   where ParentId in :objectIds];
        if (alist.isEmpty()) {return amap;}
        Set<Id> aset; Id objectId;
        for (Attachment a : alist) {
            objectId = a.ParentId;
            if (!amap.containsKey(objectId)) {
                amap.put(objectId, new Set<Id>());
            }
            aset = amap.get(objectId); aset.add(a.Id);
            amap.put(objectId, aset);
        }
        return amap;
    }
    
    public static Map<Id, Set<Id>> getNotesIds(Set<Id> objectIds) {
        Map<Id, Set<Id>> amap = new Map<Id, Set<Id>>();
        List<Note> alist = [select Id, ParentId from Note 
                             where ParentId in :objectIds];
        if (alist.isEmpty()) {return amap;}
        Set<Id> aset; Id objectId;
        for (Note a : alist) {
            objectId = a.ParentId;
            if (!amap.containsKey(objectId)) {
                amap.put(objectId, new Set<Id>());
            }
            aset = amap.get(objectId); aset.add(a.Id);
            amap.put(objectId, aset);
        }
        return amap;
    }
    
    public static Map<Id, List<Note>> getNotes(Set<Id> objectIds) {
        Map<Id, List<Note>> amap = new Map<Id, List<Note>>();
        List<Note> alist = [select Id, ParentId, Title
                              from Note where ParentId in :objectIds];
        if (alist.isEmpty()) {return null;}
        List<Note> blist; Id objectId;
        for (Note a : alist) {
            objectId = a.ParentId;
            if (!amap.containsKey(objectId)) {
                amap.put(objectId, new List<Note>());
            }
            blist = amap.get(objectId); blist.add(a);
            amap.put(objectId, blist);
        }
        return amap;
    }
    
    private static void createCloneLog(Id oldParentId, Id oldRecordId, 
                                       Id newParentId, Id newRecordId) {
        if (oldParentId == null || oldRecordId == null ||
            newParentId == null || newRecordId == null) {return;}
        Record_Clone_Log__c a = new Record_Clone_Log__c();
        String k = oldParentId + ':' + oldRecordId + ':' + 
                   newParentId + ':' + newRecordId; 
        a.Name = k; a.Record_Key__c = k; 
        a.Old_Parent_Id__c = oldParentId; a.Old_Record_Id__c = oldRecordId;
        a.New_Parent_Id__c = newParentId; a.New_Record_Id__c = newRecordId;
        try {upsert a Record_Key__c;} catch(Exception e) {debug(e.getMessage());}
    }
    
    public static Record_Clone_Log__c getCloneLog(Id oldParentId, Id oldRecordId, 
                                                  Id newParentId) {
        if (oldParentId == null || oldRecordId == null ||
            newParentId == null) {return null;}
        String opid = oldParentId, orid = oldRecordId, npid = newParentId;
        List<Record_Clone_Log__c> alist = [
            select Id, Name, New_Record_Id__c from Record_Clone_Log__c 
             where Old_Parent_Id__c = :opid
               and Old_Record_Id__c = :orid
               and New_Parent_Id__c = :npid
             limit 1];
        if (alist == null || alist.isEmpty()) {return null;}
        return alist[0];
    }
    
    public static Boolean alreadyCloned(Id oldParentId, Id oldRecordId, 
                                        Id newParentId) {
        Record_Clone_Log__c r = getCloneLog(oldParentId, oldRecordId, newParentId);
        return (r != null && r.Id != null && r.New_Record_Id__c != null);      
    }
    
    public static void cloneAttachment(Id attachmentId, Id parentId) {
        Attachment b, a = [select Body, BodyLength, ContentType, Description, 
                                  Id, IsPrivate, Name, OwnerId, ParentId 
                             from Attachment where Id = :attachmentId];
        if (alreadyCloned(a.ParentId, a.Id, parentId)) {return;}
        try {b = a.clone(); b.ParentId = parentId;
            insert b; createCloneLog(a.ParentId, a.Id, b.ParentId, b.Id);
        } catch(Exception e) {debug(e.getMessage());}
    }

    public static void cloneNote(Id noteId, Id parentId) {
        Note b, a = [select Body, Id, IsPrivate, OwnerId, ParentId, Title 
                       from Note where Id = :noteId];
        if (alreadyCloned(a.ParentId, a.Id, parentId)) {return;}
        try {b = a.clone(); b.ParentId = parentId;
            insert b; createCloneLog(a.ParentId, a.Id, b.ParentId, b.Id);
        } catch(Exception e) {debug(e.getMessage());}
    }

}