@isTest
class RSSTest {

    @isTest
    static void getRSSDataTest() {
        RSS.channel testChannel = RSS.getRSSData('test', 1);
        System.assertNotEquals( null, testChannel );
        System.assertEquals( 'salesforce.com - Bing News', testChannel.title );
        System.assertEquals( 'Bing', testChannel.image.title );
        System.assertEquals( '2012-03-19T15:21:47Z', testChannel.items[0].pubDate );
    }
}