@isTest
class UserAccountControllerTest {
    
    @isTest
    static void getUserTest() {
        User newUser = new User(Id = null);
        ApexPages.StandardController sc = new ApexPages.StandardController(newUser);
        UserAccountController testClass = new UserAccountController(sc);
        
        System.assertEquals( newUser, testClass.getUser() );
        
        testClass = new UserAccountController();
        System.assertEquals( null, testClass.getUser().Id );
    }
    
    @isTest
    static void getHasEditPermissionTest() {
        User newUser = new User(Id = null);
        ApexPages.StandardController sc = new ApexPages.StandardController(newUser);
        UserAccountController testClass = new UserAccountController(sc);
        
        System.assertNotEquals( null, testClass.getHasEditPermission() );
    }
    
    @isTest
    static void saveUserTest() {
        User newUser = new User(Id = null);
        ApexPages.StandardController sc = new ApexPages.StandardController(newUser);
        UserAccountController testClass = new UserAccountController(sc);
        
        System.assertEquals( null, testClass.saveUser() );
    }
}