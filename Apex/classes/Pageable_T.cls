@isTest
private class Pageable_T {
/****************************************************************************
 * Test Class Pageable_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - Pageable Apex Class
 ****************************************************************************/
 
    //Test Data
    
    
    //Test Settings
    
    
    private static testMethod void unitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
        // No Test Data Needed
    }
  
    private static void executeTest01() {
        // Execute Tests

		ListCollection coll = new ListCollection();

		integer i;
		boolean b;
 
		i = coll.PageSize;
		i = coll.PageNumber;
		i = coll.PageIndex;
		i = coll.RecordCount;
		i = coll.PageCount;
		i = coll.Offset;
		i = coll.LNumber;
		i = coll.UNumber;
		b = coll.AllowMoveNext;
		b = coll.AllowMovePrev;	

		coll.getRecordCount();
		coll.movePrev();
		coll.moveNext();
		coll.moveLast();
		coll.moveFirst();
    }


    public class ListCollection extends Pageable {
        public List<sObject> sObjects {get;set;}

        public override Integer getRecordCount() {
            return (sObjects == null? 0 : sObjects.size());
      }
    }   
}