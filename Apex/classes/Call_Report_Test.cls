@isTest
private class Call_Report_Test {

    static Account acc;
    static Contact con;
    static Call_Report_Goodyear__c crg;
    
    static void setup1() {
    	acc = new Account(Name='test account');
    	insert acc;
    	
    	con = new Contact(LastName='test', email='test@example.com');
    	con.AccountId = acc.Id; con.Role__c = 'Manager';
    	insert con;
    	
    	crg = new Call_Report_Goodyear__c();
    	crg.Account__c = acc.Id;
    	insert crg;
    }
    
    static testMethod void test01() {
    	setup1(); Test.startTest();
    	ApexPages.StandardController sc = new ApexPages.StandardController(crg);
    	Call_Report_Goodyear c = new Call_Report_Goodyear(); 
    	c.initAction1(); c.getCallReportId(); c.setCallReportId(crg.Id);
    	c = new Call_Report_Goodyear(sc); c.getCallReport();
    	c.selectAllContacts(); c.sendCallReport();
    	Test.stopTest();
    }
}