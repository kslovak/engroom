public class CSV_Data_Controller {

    public Blob             csvFileBlob {get; set;}
    public String           csvFileName {get; set;}
    public String           csvConfigId {get; set;}
    
    public Boolean           renderBtns {get; private set;}
    public Boolean         renderPsopts {get; private set;}
    public String               pbTitle {get; private set;}
    public String          recordLayout {get; private set;}
    public CSV_Data_Config__c csvConfig {get; private set;}
    public List<SelectOption>    psopts {get; private set;}
    public Map<String, String>   params {get; private set;}
    
    public CSV_Data_Controller() {init1();}
    
    private void debug(String s) {System.debug(LoggingLevel.INFO, s);}
    
    @TestVisible
    private void addErrMsg(String s) {
        addMsg(ApexPages.Severity.ERROR,  '<b>' + s + '</b>');
    }

    @TestVisible
    private void addInfoMsg(String s) {
        addMsg(ApexPages.Severity.INFO,  '<b>' + s + '</b>');
    }

    private void addMsg(ApexPages.Severity mtype, String s) {
        ApexPages.Message msg = new ApexPages.Message(mtype, s);
        ApexPages.addMessage(msg);
    }

    private void init1() {
        PageReference pr = System.currentPageReference(); String s;
        params = new Map<String, String>();
        if (pr != null) {params = pr.getParameters();} 
        debug('params = ' + params);
        s = params.get('ck'); csvConfigId = CSV_Data_Methods.getCsvConfigId(s);

        changeProcess(); initPsopts(); 
    }
    
    private void initPsopts() {
        pbTitle = 'Upload CSV Data File'; renderPsopts = false;
        if (!String.isBlank(csvConfigId)) {pbTitle = 'Upload ' + csvConfig.Name; return;}
        if (!CSV_Data_Methods.isCsvDataAdmin()) {pbTitle = ''; return;}
        try {psopts = CSV_Data_Methods.getCsvProcessNames();} catch(Exception e) {} 
        renderPsopts = true;
    }
    
    public void changeProcess() {
        renderBtns = false; if (String.isBlank(csvConfigId)) {return;}
        csvConfig = CSV_Data_Methods.getCsvDataConfig(csvConfigId);
        setRecordLayout(); renderBtns = true;
    }
    
    private void setRecordLayout() {
        recordLayout = '';
        List<String> slist = CSV_Data_Methods.getFieldLabels(csvConfig);
        if (slist.isEmpty()) {return;}
        for (String s : slist) {recordLayout += '<li>' + s + '</li>';}
    }
    
    public void processData() {
        if (csvFileBlob == null) {addInfoMsg('No File Selected'); return;}

        String s = csvFileBlob.toString(); csvFileBlob = null;
        List<String> slist = String_Functions.getLines(s); s = null;
        if (slist.isEmpty()) {
            addInfoMsg('File is either empty or too large to process'); return;
        }
        addInfoMsg('CSV File Name : ' + csvFileName);
        addInfoMsg('Number of Lines read : ' + (slist.size()-1));
        try {
	        CSV_Data_Batch b = new CSV_Data_Batch(slist, csvConfigId);
	        Database.executeBatch(b);
            addInfoMsg('Process has been started and an email will be sent to you when it completes.');
        } catch(Exception e) {addErrMsg(e.getMessage());}
    }
    
}