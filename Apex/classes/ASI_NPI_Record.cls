public class ASI_NPI_Record {

    public String materialName {get; private set;}
    
    public Integer      recnum {get; set;}
    public Boolean    selected {get; set;}

    public ASI_NPI_Record() {this(1);}
    
    public ASI_NPI_Record(Integer n) {this('', n);}
    
    public ASI_NPI_Record(String matName, Integer n) {
    	materialName = matName; recnum = n; selected = false;
    }
}