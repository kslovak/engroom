public with sharing class APPDocumentTable {

    private List<String> documentTags;
    private List<ContentVersionWrapper> documents;
    
    public APPDocumentTable(List<String> docTags) {
        this.documentTags = docTags;
    }
    
    public List<ContentVersionWrapper> getDocuments() {
        try {
            refreshDocuments();
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
        return this.documents;
    }
    
    /*
        PRIVATE
    */
    
    private void refreshDocuments() {
        this.documents = getContentByTags(this.documentTags);
    }
    
    private List<ContentVersionWrapper> getContentByTags(String[] tags) {
        List<ContentVersionWrapper> result = new List<ContentVersionWrapper>();
        Set<Id> subscriptionIds = new Set<Id>();
        Set<Id> contentIds = new Set<Id>();
        
        List<ContentVersion> content = [select Id, ContentDocumentId, Title, TagCsv, ContentModifiedDate, ContentUrl, ContentSize from ContentVersion where isLatest = true];
        
        for (ContentVersion c : content) {
            contentIds.add(c.ContentDocumentId);
        }
        
        if (contentIds.size() > 0) {
            Set<EntitySubscription> subscriptions = new Set<EntitySubscription>([SELECT Id, ParentId FROM EntitySubscription WHERE ParentId IN : contentIds AND SubscriberId =: UserInfo.getUserId() LIMIT 1000]);
            for (EntitySubscription subscription : subscriptions) { 
                subscriptionIds.add(subscription.ParentId);
            }
        }
        
        for(ContentVersion c : content) {
            boolean canAdd = true;
            
            for(String t : tags) {
                if(t == null || c.TagCsv == null) {
                    canAdd = false;
                    break;
                }
                canAdd = canAdd && (c.TagCsv.contains(t));
            }
            
            if(canAdd) {
                Boolean subscribed = subscriptionIds.contains(c.ContentDocumentId);
                ContentVersionWrapper w = new ContentVersionWrapper(c);
                w.Id = c.Id;
                w.ContentDocumentId = c.ContentDocumentId;
                w.Title = c.Title;
                w.TagCsv = c.TagCsv;
                w.ContentModifiedDate = c.ContentModifiedDate;
                w.ContentUrl = c.ContentUrl;
                w.ContentSize = c.ContentSize;
                w.IsSubscribed = subscribed;
                result.add(w);
            }
        }
        
        return result;
    }

}