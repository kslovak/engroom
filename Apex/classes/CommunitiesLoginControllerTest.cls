@isTest
class CommunitiesLoginControllerTest {

    @isTest
    static void authForwardToLandingTest() {
        CommunitiesLoginController testClass = new CommunitiesLoginController();
        
        if(UserInfo.getUserType() != 'Guest') {
            System.assertEquals( Page.Home.getUrl(), testClass.authForwardToLanding().getUrl() );
        } else {
            System.assertEquals( null, testClass.authForwardToLanding() );
        }
    }
    
    @isTest 
    static void loginTest() {
        CommunitiesLoginController testClass = new CommunitiesLoginController();
        
        System.assertEquals( null, testClass.login() );
    }
}