public class ASI_Innova_Record {

    public String projectName {get; private set;}
    
    public Integer     recnum {get; set;}
    public Boolean   selected {get; set;}

    public ASI_Innova_Record() {this(1);}
    
    public ASI_Innova_Record(Integer n) {this('', n);}
    
    public ASI_Innova_Record(String projName, Integer n) {
        projectName = projName; recnum = n; selected = false;
    }
}