@isTest
private class EmailUtilTest {
    
    static testMethod void  testSendEmail(){
        ContentVersion testContentInsert = new ContentVersion();
         
        testContentInsert.Title = 'TestDoc'; 
        testContentInsert.VersionData = Blob.valueOf('Unit Test Attachment Body'); 
        testContentInsert.pathOnClient = 'Test.pdf';
        
        insert testContentInsert;
        
        ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id][0];
        System.assertNotEquals( null, testContent );
        test.startTest();
        EmailUtil.SendEmail('test email', 'abc@abc.com', testContent.ContentDocumentId);
        System.debug('Inside test');
        test.stopTest();
    }

}