public class VIEnrollmentLocationMaterial {

    public VI_Enrollment_Location_Material__c elm {get; private set;}
    
    public VI_Promotion__c                  promo {get; set;}
    public Double                       promoCost {get; set;}
    public Integer                   recordNumber {get; set;}
    public Boolean                    imageExists {get; set;}
    public Boolean                         isEven {get; set;}
    public Boolean                        isFirst {get; set;}
    public Boolean                         isLast {get; set;}
    public Boolean                       selected {get; set;}
    
    public VIEnrollmentLocationMaterial(VI_Enrollment_Location_Material__c m) {
        selected = false; imageExists = false; elm = m;
    }

}