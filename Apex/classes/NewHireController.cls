public with sharing class NewHireController extends PageController {
    public string subscribeContentId { get; set; }
    public boolean subscribeSelected { get; set; }
    public string emailResponse { get; set; }
    
    public List<ContentVersionWrapper> videos { get; set; }
    public List<ContentVersionWrapper> literature { get; set; }
    public List<ContentVersionWrapper> presentations { get; set; }
    
    public NewHireController() {
        getDocumentation();
    }
    
    public void getDocumentation() {
        String newHireTag = UserUtil.groupListContains( 'APP-Internal-Group', this.allUserGroups ) ? 
            'APP-Training-Internal' : 'APP-Training-Distributor';
            
        this.videos = this.getContentByTags(new String[]{'APP-Collateral-Video',newHireTag});
        this.presentations = this.getContentByTags(new String[]{'APP-Collateral-Presentation',newHireTag});
        this.literature = this.getContentByTags(new String[]{'APP-Collateral-Literature',newHireTag});
    }

    // PRIVATE METHODS
    
    private List<ContentVersionWrapper> getContentByTags(String[] tags) {
        List<ContentVersionWrapper> result = new List<ContentVersionWrapper>();
        Set<Id> subscriptionIds = new Set<Id>();
        Set<Id> contentIds = new Set<Id>();

        List<ContentVersion> content = [select Id, ContentDocumentId, Title, TagCsv, ContentModifiedDate, ContentUrl, ContentSize from ContentVersion where isLatest = true];
        
        for (ContentVersion c : content) {
            contentIds.add(c.ContentDocumentId);
        }
        
        if (contentIds.size() > 0) {
            Set<EntitySubscription> subscriptions = new Set<EntitySubscription>([SELECT Id, ParentId FROM EntitySubscription WHERE ParentId IN : contentIds AND SubscriberId =: UserInfo.getUserId() LIMIT 1000]);
            for (EntitySubscription subscription : subscriptions) { 
                subscriptionIds.add(subscription.ParentId);
            }
        }
        
        for(ContentVersion c : content) {
            boolean canAdd = true;
            
            for(String t : tags) {
                if(t == null || c.TagCsv == null) {
                    canAdd = false;
                    break;
                }
                canAdd = canAdd && (c.TagCsv.contains(t));
            }
            
            if(canAdd) {
                Boolean subscribed = subscriptionIds.contains(c.ContentDocumentId);
                ContentVersionWrapper w = new ContentVersionWrapper(c);
                w.Id = c.Id;
                w.ContentDocumentId = c.ContentDocumentId;
                w.Title = c.Title;
                w.TagCsv = c.TagCsv;
                w.ContentModifiedDate = c.ContentModifiedDate;
                w.ContentUrl = c.ContentUrl;
                w.ContentSize = c.ContentSize;
                w.IsSubscribed = subscribed;
                result.add(w);
            }
        }
        
        return result;
    }
    
    public PageReference setSubscription() {
        List<EntitySubscription> subscription = [SELECT Id FROM EntitySubscription WHERE ParentId =: this.subscribeContentId AND SubscriberId =: UserInfo.getUserId() LIMIT 1000];
        
        if(this.subscribeSelected != null) {
            if (this.subscribeSelected && subscription.size() == 0) {
                EntitySubscription es = new EntitySubscription();
                es.ParentId = this.subscribeContentId;
                es.SubscriberId = UserInfo.getUserId();
                insert es;
            } else if (!this.subscribeSelected && subscription.size() > 0) {
                delete subscription;
            }
        }
        
        getDocumentation();
        return null;
    }
    
    public PageReference sendEmail() {
        emailResponse ='';
        String emailMsg = Apexpages.currentPage().getParameters().get('emailMsg');
        String emailid = Apexpages.currentPage().getParameters().get('emailid');
        String contentid = Apexpages.currentPage().getParameters().get('contentid');
        emailResponse = EmailUtil.SendEmail(emailMsg, emailid, contentid);
        return null;
    }
}