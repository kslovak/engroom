public  class EmailUtil {
    
    public static String SendEmail(String content, String emailid, id contentid){
        string response = 'There was an error trying to send email. Please retry';
        // Define the email
        try{
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            // Reference the attachment page and pass in the account ID
            List<ContentVersion> lstCD = [Select id, ContentSize, FileExtension, Title, VersionData from ContentVersion 
                                    Where Id =:contentid and  isLatest = true limit 1];
            
            if(lstCD.Size() >0){
                ContentVersion cd = lstCD[0];
                // Take the PDF content
                Blob b = cd.VersionData ;
                // Create the email attachment
                
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName(cd.Title +'.' + cd.FileExtension);
                efa.setBody(b);
                
        
                // Sets the paramaters of the email
                email.setSubject( cd.Title);
                email.setToAddresses( new String[]{emailid});
                email.setPlainTextBody( content);
                System.debug('Content:' + content);
                email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
               // Sends the email
                Messaging.SendEmailResult [] r =
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}); 
                response = 'Email was sent successfully.';
            } 
        }Catch(Exception e){
            response = 'There was an error trying to send email. Please retry.';
        }
            
        return response;    
    }
}