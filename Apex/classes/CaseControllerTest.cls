@isTest
class CaseControllerTest {
    
    @isTest
    static void deleteAllTest() {
        List<Case> cases = [select Id, Description, createdDate, closedDate from Case];
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(cases);
        CaseController testClass = new CaseController(sc);
        
        System.assertEquals( null, testClass.deleteAll() );
    }
    
    @isTest
    static void getCustomCasesTest() {
        List<Case> cases = [select Id, Description, createdDate, closedDate from Case];
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(cases);
        CaseController testClass = new CaseController(sc);
        
        testClass.getRecords();
        System.assertNotEquals( null, testClass.getCustomCases() );
    }
    
    @isTest
    static void getMarketTagsTest() {
        List<Case> cases = [select Id, Description, createdDate, closedDate from Case];
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(cases);
        CaseController testClass = new CaseController(sc);
        
        System.assertNotEquals( null, testClass.getMarketTags() );
    }
    
    @isTest
    static void getProductTagsTest() {
        List<Case> cases = [select Id, Description, createdDate, closedDate from Case];
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(cases);
        CaseController testClass = new CaseController(sc);
        
        System.assertNotEquals( null, testClass.getProductTags() );
    }
    
    @isTest
    static void submitCaseTest() {
        List<Case> cases = [select Id, Description, createdDate, closedDate from Case];
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(cases);
        CaseController testClass = new CaseController(sc);
        
        PageReference pr = testClass.submitCase();
        System.assertEquals( null, pr );
        System.assertNotEquals( '', testClass.caseMessage );
    }
}