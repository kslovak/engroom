@IsTest
private class VIFacilityEditControllerTest {
    static testmethod void newFacility() {
        Account distributor = new Account(), facility;
        distributor.Name = 'Test Distributor';
        distributor.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_DISTRIBUTOR;
        insert distributor;

        Contact contact = new Contact();
        contact.AccountId = distributor.Id;
        contact.LastName = 'Test';
        insert contact;

        Profile p;
        try {
            p = [SELECT Id FROM Profile WHERE Name = :VIUtils.PARTNER_PORTAL_NAME];
        } catch(Exception e) {return;}
        
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing',
                LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                ProfileId = p.Id, ContactId = contact.Id, Ashland_Employee_Number__c = 'zzzTest1',
                TimeZoneSidKey = 'America/Los_Angeles', Username = 'VIFacilityEditControllerTest1@testorg.com');
                
        System.runAs(u) {

            VIFacilityEditController controller = new VIFacilityEditController();
            controller.init(); facility = controller.getFacility();

            controller.save();
            
            controller.getName().setValue('Test Facility');
            controller.getBillingStreet().setValue('123 Some St');
            controller.getBillingCity().setValue('Dallas');
            controller.getBillingState().setValue('TX');
            controller.getBillingPostalCode().setValue('75243');
            controller.getPhone().setValue('8005551234');
            controller.getFax().setValue('8005551234');
            controller.getWebsite().setValue('http://www.google.com');
            controller.getActive().setValue('Yes');
            //controller.primaryIndustry.setValue('TestIndustry');
            facility.Primary_Industry__c = 'TestIndustry';

            controller.save();

            System.debug(ApexPages.getMessages());
        }
    }

    static testmethod void editFacility() {
        Account distributor = new Account();
        distributor.Name = 'Test Distributor';
        distributor.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_DISTRIBUTOR;
        insert distributor;

        Account facility = new Account();
        facility.VI_Parent_Account__c = distributor.Id;
        facility.Name = 'Test Facility';
        facility.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_FACILITY;
        facility.SAP_Street__c = facility.BillingStreet = '123 Some St';
        facility.SAP_City__c = facility.BillingCity = 'Dallas';
        facility.SAP_State__c = facility.BillingState = 'TX';
        facility.SAP_Zip_Code__c = facility.BillingPostalCode = '75243';
        facility.Phone = '8005551234';
        facility.Fax = '8005551234';
        facility.Website = 'http://www.google.com';
        facility.Inactive_Account__c = false;
        facility.Primary_Industry__c = 'TestIndustry';
        insert facility;

        Contact contact = new Contact();
        contact.AccountId = distributor.Id;
        contact.LastName = 'Test';
        insert contact;

        Map<String, String> params = System.currentPageReference().getParameters();
        params.put(VIFacilityEditController.PARAM_FACILITY_ID, facility.Id);
        params.put(VIFacilityEditController.PARAM_RETURN_URL, '/home/home.jsp');

        VIFacilityEditController controller = new VIFacilityEditController();
        controller.init();
		controller.getAviLocatorName();
		controller.getDistributor();
		controller.getFacility();
		controller.getFacilityId();
		controller.getReadOnly();
		controller.getStoreHourOptions();
		
        controller.save();
        controller.availablePromotions();
        controller.cancel();
    }
}