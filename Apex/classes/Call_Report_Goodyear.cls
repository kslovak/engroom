public class Call_Report_Goodyear {

    public static final String DMGR = 'District Manager';
    public static final String GMGR = 'General Manager';
    public static final String SMGR = 'Manager';
    public static final String TEMPLATE_NAME = 'V-Goodyear-CallReport';
    
    public class StoreContact {
    	public Boolean selctd {get; set;}
    	public Integer recNum {get; set;}
    	public Contact contct {get; set;}
    	
    	public StoreContact(Contact c, Integer n) {
    		contct = c; selctd = false; recNum = n;
    	} 
    }
    
    public List<StoreContact> contacts {get; private set;}
    
    public Boolean renderMsgs          {get; private set;}
    public Boolean allContacts         {get; set;}

    public String  msg                 {get; private set;}
    
    String                  accountId;
    String                  callReportId;
    String                  templateId;
    Call_Report_Goodyear__c callReport;
    Map<String, String>     params;
    
    private void debug(String s) {System.debug(LoggingLevel.INFO, s);}
    
    public Call_Report_Goodyear() {
        callReport = new Call_Report_Goodyear__c(); init1();
    }
    
    public Call_Report_Goodyear(ApexPages.StandardController sc) {
    	callReport = (Call_Report_Goodyear__c)sc.getRecord(); init1();
    }
    
    private void init1() {
        params = System.currentPageReference().getParameters();
        if (callReport != null) {callReportId = callReport.Id;} 
        if (String.isBlank(callReportId)) {callReportId = params.get('id');}
        contacts = new List<StoreContact>(); templateId = '';  
        setCallReport(); setTemplateId(); init2();
    }
    
    public void init2() {
        renderMsgs = false; allContacts = false; msg = '';
        for (StoreContact c : contacts) {c.selctd = false;}
    }

    public void initAction1() {
    	
    }

    public String getCallReportId() {return callReportId;}

    public void setCallReportId(String s) {callReportId = s; setCallReport();}
    
    public Call_Report_Goodyear__c getCallReport() {return callReport;}

    private void setCallReport() {
    	if (String.isBlank(callReportId)) {return;}
    	try {
    		callReport = [
                select  Id, Name, Account__c, Account__r.Name,  
                        Action_Items__c, Additional_Comments__c, 
						Assistant_Manager__c, Avg_Car_P_Day_Var_P_Year__c, Avg_Cars_P_Day__c, 
						Avg_LOF_Dollar_Goal__c, Avg_LOF_Dollar_MTD__c, Avg_LOF_Dollar_YTD__c, 
						Date_Time__c, Equipment__c, GVMP__c, Improvement_Comments__c, Inventory_Management__c, 
						LOF_P_Day_Goal__c, LOF_P_Day_MTD__c, LOF_P_Day_YTD__c, 
						Machine_Tool_1_Working__c, Machine_Tool_2_Working__c, Machine_Tool_3_Working__c, 
						Machine_Tool_4_Working__c, Machine_Tool_5_Working__c, Machine_Tool_Other_Working__c, 
						Machine_Tools_1__c, Machine_Tools_2__c, Machine_Tools_3__c, Machine_Tools_4__c, 
						Machine_Tools_5__c, Machine_Tools_Other__c, Oil_Change__c, Opportunities_Comments__c, 
						PM_Dollars_P_Car_Goal__c, PM_Dollars_P_Car_MTD__c, PM_Dollars_P_Car_YTD__c, 
						PM_Dollars_Per_Car_wo_OC__c, 
						PM_Fluid_Percent_Cars_Goal__c, PM_Fluid_Percent_Cars_MTD__c, PM_Fluid_Percent_Cars_YTD__c, 
						PM_Fluid_Services_Goal__c, PM_Fluid_Services_MTD__c, PM_Fluid_Services_YTD__c, 
						PM_Service__c, 
						Percent_Cars_w_LOF_Goal__c, Percent_Cars_w_LOF_MTD__c, Percent_Cars_w_LOF_YTD__c, 
						Percent_Cars_w_Wipers_Goal__c, Percent_Cars_w_Wipers_MTD__c, Percent_Cars_w_Wipers_YTD__c, 
						Performance_Goals_FollowUp__c, Power_Steering__c, 
						Prem_LOF_Goal__c, Prem_LOF_MTD__c, Prem_LOF_YTD__c, 
						Prior_Yr_Avg_Car_Day__c, SAP_Customer_Number__c, Service_Manager__c, Shift_Manager__c, 
						State_City_Country__c, Store_Number__c, Successes_Comments__c, 
						Title__c, Trainee_1__c, Trainee_2__c, Trainee_3__c, Trainee_4__c, 
						Training_recommendations__c, VCR_Attachment_Rate__c,
						Wiper_Blade_Units_Goal__c, Wiper_Blade_Units_MTD__c, Wiper_Blade_Units_YTD__c 
                  from  Call_Report_Goodyear__c
                 where  Id = :callReportId
            ];
    	} catch(Exception e) {return;}
    	accountId = callReport.Account__c;
    	contacts = new List<StoreContact>(); Integer n = 0;
    	for (Contact c : [select Id, Name, Email, Role__c 
    	     from Contact where AccountId = :accountId
              and Role__c includes (:DMGR, :GMGR, :SMGR)
            order by Name]) {contacts.add(new StoreContact(c, ++n));}
    	debug('contacts : ' + contacts);
    }
    
    private void setTemplateId() {
    	templateId = '';
    	try {
    		templateId = [select Id from emailtemplate where name = :TEMPLATE_NAME limit 1].Id;
    	} catch(Exception e) {}
    }
    
    @TestVisible private void selectAllContacts() {
        for (StoreContact c : contacts) {c.selctd = true;}
    }
    
    public void sendCallReport() {
        msg = ''; renderMsgs = false; 
        if (String.isBlank(templateId)) {msg = 'Email Template not found'; return;}
    	Id contctId = null; Set<String> sset = new Set<String>();
        for (StoreContact c : contacts) {
        	if (c.selctd) {
        		if (contctId == null) {contctId = c.contct.Id;}
        		else {sset.add(c.contct.Email);}
        	}
        }
        if (contctId == null) {msg = 'No Contact selected'; return;}
        String replyTo = UserInfo.getUserEmail();
        List<String> tlist = new List<String>(), blist = new List<String>{replyTo};
        Messaging.SingleEmailMessage m = new Messaging.SingleEmailMessage();
        m.setReplyTo(replyTo); m.setSaveAsActivity(false);
        if (!sset.isEmpty()) {tlist.addAll(sset); m.setToAddresses(tlist);} 
        m.setBccAddresses(blist); m.setWhatId(callReportId);
        m.setTargetObjectId(contctId); m.setTemplateId(templateId);
        List<Messaging.SingleEmailMessage> mlist = new List<Messaging.SingleEmailMessage>{m};
        List<Messaging.SendEmailResult> rs;
        try {
        	if (!Test.isRunningTest()) {rs = Messaging.sendEmail(mlist);}
            msg = 'Call Report sent to the following selected Contacts';
            renderMsgs = true;
        } catch(Exception e) {msg = e.getMessage();}
    }
    
}