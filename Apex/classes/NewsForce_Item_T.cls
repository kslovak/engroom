@isTest
private class NewsForce_Item_T {
/****************************************************************************
 * Test Class NewsForce_Item_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - NewsForce_Item Apex Class
 ****************************************************************************/
 
    //Test Data
    public static NewsForce_Item__c item1;
    public static NewsForce_Item newsForceItem;
    
    //Test Settings
    
    
    private static testMethod void unitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
        item1 = new NewsForce_Item__c();
        item1.Active__c = true;
        item1.Content__c = 'Content';
        item1.Display_Date__c = System.Today();
        item1.Start_Date__c = System.Today();
        item1.End_Date__c = System.Today();
        item1.Force_Display__c = false;
        item1.Priority__c = 'Low';
        item1.Title__c = 'Title 1';
        
        insert item1;
    }
  
    private static void executeTest01() {
        // Execute Tests
		newsForceItem = new NewsForce_Item(item1);

		Boolean bool;
		String s;
		
		bool = newsForceItem.isNew;
		bool = newsForceItem.forceDisplayOfItem;
		
		s = newsForceItem.formattedTitle;
		 
		newsForceItem.userHasViewedItem();
		newsForceItem.userHasDismissedItem();
    }
}