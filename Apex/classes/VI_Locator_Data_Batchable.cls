global class VI_Locator_Data_Batchable implements Database.Batchable<SObject>, Schedulable {

    public static final String PROC_STEP_CREATE = 'CREATE';
    public static final String PROC_STEP_DELETE = 'DELETE';

    public static final String userEmail = User_Functions.getLoggedInUserEmail();
    
    private final String procStep;
    
    @TestVisible private void debug(String s) {
        System.debug(LoggingLevel.INFO, 'VI_Locator_Data_Batchable : ' + s);
    }
    
    global VI_Locator_Data_Batchable(String pstep) {
        procStep = pstep;
    }
    
    global Database.Querylocator start(Database.BatchableContext bc) {
        Database.Querylocator ql = null;
        if (procStep.equals(PROC_STEP_DELETE)) {ql = VI_Locator_Data_Methods.getLocatorDataQL();} else
        if (procStep.equals(PROC_STEP_CREATE)) {ql = VI_Locator_Data_Methods.getAccountQL();}
        return ql;
    }

    global void execute(Database.BatchableContext bc, List<SObject> alist){
        if (procStep.equals(PROC_STEP_DELETE)) {
            VI_Locator_Data_Methods.deleteLocatorData((List<VI_Locator_Data__c>)alist);
        } else if (procStep.equals(PROC_STEP_CREATE)) {
            VI_Locator_Data_Methods.upsertLocatorData((List<Account>)alist);
        }
    }
    
    global void execute(SchedulableContext sc) {
        submitBatchJob(procStep);
    }
    
    global void finish(Database.BatchableContext bc){
        sendEmail(bc); debug('procStep = ' + procStep);
        if (procStep.equals(PROC_STEP_DELETE)) {submitBatchJob(PROC_STEP_CREATE);}
    }
    
    private void sendEmail(Database.BatchableContext bc) {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                 TotalJobItems, CreatedBy.Email
                            from AsyncApexJob 
                           where Id =:bc.getJobId()];
        String s = 'Apex Batch Job - Valvoline Locator Data - ' + procStep + ' - ' + 
                         a.Status+' - '+a.TotalJobItems+' batches';
        String b = 'Job Id : '+a.Id+' processed ' + a.TotalJobItems +
                              ' batches with '+ a.NumberOfErrors + ' failures.';
        sendEmail(s, b);
    }
    
    private void sendEmail(String s, String b) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {userEmail});
        mail.setReplyTo(userEmail);
        mail.setSenderDisplayName('SysAdmin');
        mail.setSubject(s);
        mail.setPlainTextBody(b);
        MessagingMethods.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    Webservice static void submitBatchJob(String pstep) {
        VI_Locator_Data_Batchable b = new VI_Locator_Data_Batchable(pstep);
        try {Database.executeBatch(b);} catch(Exception e) {
            b.sendEmail('VI Locator Data Batch Job Error', e.getMessage());
        }
    }

}