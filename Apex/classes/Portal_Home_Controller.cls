public class Portal_Home_Controller {

    public static final String BASEURL = URL.getSalesforceBaseUrl().toExternalForm();

    public static final String PARAM_DEBUG = 'debug';
    public static final String PARAM_ID    = 'id';

    public static final String PORTAL_TYPE_PARTNER  = 'Partner';
    public static final String PORTAL_TYPE_CUSTOMER = 'CustomerPortal';

    public static final String URL_HOME   = '/home/home.jsp';
    public static final String URL_LOGOUT = '/secur/logout.jsp';

    public static final ApexPages.Severity INFO  = ApexPages.Severity.INFO;

    public String  accountId     {get; private set;}
    public String  homePageUrl   {get; private set;}
    public String  portalType    {get; private set;}

    public Boolean partnerPortal {get; private set;}
    public Boolean showDebugMsgs {get; private set;}

    transient List<String> debugMsgs = new List<String>();

    Portal_Site_Settings__c pss = Portal_Site_Settings__c.getInstance();
    UserRole usrRole;
    
    public Portal_Home_Controller() {init1();}

    private void debug(String s) {
        if (debugMsgs == null) {debugMsgs = new List<String>();}
        s = 'Portal_Home_Controller : ' + (debugMsgs.size() + 1) + ' : ' + s;
        System.debug(LoggingLevel.INFO, s); addDebugMsg(s);
    }

    private void addDebugMsg(String s) {
        if (showDebugMsgs != null && showDebugMsgs) {debugMsgs.add(s);}
    }

    private void addDebugMsgs() {
        if (debugMsgs == null) {return;}
        for (String s : debugMsgs) {addInfoMsg(s);}
    }

    @TestVisible
    private void addInfoMsg(String s) {addMsg(INFO,  s);}

    private void addMsg(ApexPages.Severity mtype, String s) {
        ApexPages.Message msg = new ApexPages.Message(mtype, s);
        ApexPages.addMessage(msg);
    }

    private void init1() {
        Map<String, String> params = ApexPages.currentPage().getParameters();
        Boolean b = pss != null && pss.Show_Debug_Msgs__c;
        showDebugMsgs = b && params != null && params.containsKey(PARAM_DEBUG);
        List<UserRole> userRoles = [SELECT PortalAccountId, PortalType
                                      FROM UserRole
                                     WHERE Id = :UserInfo.getUserRoleId()];
        if (!userRoles.isEmpty()) {usrRole = userRoles.get(0);}
        if (usrRole != null) {accountId = usrRole.PortalAccountId;}
        if (accountId == null) {accountId = params.get(PARAM_ID);}
        debug('init1 : accountId = ' + accountId);
        portalType = 'None';
        if (usrRole != null) {portalType = usrRole.PortalType;}
        partnerPortal = PORTAL_TYPE_PARTNER.equalsIgnoreCase(portalType);
        homePageUrl = pss != null ? pss.Home_Page__c : URL_HOME;
        debug('init1 : homePageUrl = ' + homePageUrl);
    }

    public PageReference initAction() {
        addDebugMsgs(); 
        PageReference pr = homePageUrl != null ? new PageReference(homePageUrl) : null;
        return pr;
    }

}