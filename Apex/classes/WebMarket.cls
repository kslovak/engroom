public with sharing class WebMarket {

    private Market__c market;
    
    public WebMarket(Market__c m) {
        this.market = m;        
    }
    
    public Market__c getMarket() {
        return this.market;
    }
}