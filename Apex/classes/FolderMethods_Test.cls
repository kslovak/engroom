@isTest(SeeAllData=true)
private class FolderMethods_Test {

    static final String TYPE_DBF = FolderMethods.TYPE_DASHBOARD;
    static final String TYPE_DCF = FolderMethods.TYPE_DOCUMENT;
    static final String TYPE_EMF = FolderMethods.TYPE_EMAIL;
    static final String TYPE_RPF = FolderMethods.TYPE_REPORT;
     
    static Folder dbf, dcf, emf, rpf;
    
    static void setup() {
    	dbf = getFolder(TYPE_DBF);
        dcf = getFolder(TYPE_DCF);
        emf = getFolder(TYPE_EMF);
        rpf = getFolder(TYPE_RPF);
    }
    
    static Folder getFolder(String ftype) {
    	List<Folder> fs = [select Id, Name from Folder where Type = :ftype limit 1];
    	if (fs == null || fs.isEmpty()) {return null;} return fs[0];
    }
    
    static testMethod void test01() {
    	setup(); Test.startTest();
    	if (dbf != null) {FolderMethods.getDashboardFolder(dbf.Name);}
        if (dcf != null) {FolderMethods.getDocumentFolder(dcf.Name);}
        if (emf != null) {FolderMethods.getEmailFolder(emf.Name);}
        if (rpf != null) {FolderMethods.getReportFolder(rpf.Name);}
    	Test.stopTest();
    }
}