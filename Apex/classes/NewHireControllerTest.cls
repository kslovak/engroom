@isTest
class NewHireControllerTest {
    
    @isTest(SeeAllData=true)
    static void getDocumentationTest() {
        NewHireController testClass = new NewHireController();
        
        testClass.getDocumentation();
        System.assertNotEquals( null, testClass.videos );
        System.assertNotEquals( null, testClass.literature );
        System.assertNotEquals( null, testClass.presentations );
        
        // run with real content
        ContentVersion testContent = new ContentVersion(Title='testContentTitle',contentUrl='url',TagCsv='test;APP-Collateral-Video;test;APP-Training-Distributor;test');
        insert testContent;
        
        testClass = new NewHireController();
        testClass.getDocumentation();
        System.assertNotEquals( null, testClass.videos );
    }
    
    @isTest
    static void setSubscriptionTest() {
        NewHireController testClass = new NewHireController();
        
        System.assertEquals( null, testClass.setSubscription() );
    }
    
    @isTest
    static void sendEmailTest() {
        NewHireController testClass = new NewHireController();
        
        System.assertEquals( null, testClass.sendEmail() );
    }
}