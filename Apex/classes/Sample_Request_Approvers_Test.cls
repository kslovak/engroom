@isTest
private class Sample_Request_Approvers_Test {

    static Account acc;
    static Sample_Request__c sreq;
    static Sample_Material__c smat;
    
    static void setup() {
        acc = new Account(Name='test account');
        insert acc;
        
        sreq = new Sample_Request__c();
        sreq.Account__c = acc.Id;
        insert sreq;
        
        smat = new Sample_Material__c();
        smat.Sample_Request__c = sreq.Id;
        smat.Sample_UOM__c = 'Drum';
        smat.Sample_Quantity__c = 1;
        insert smat;
    }
    
    static testmethod void test01() {
        setup(); List<Sample_Request> srpa;
        Set<String> srids = new Set<String>(); srids.add(sreq.Id);

        Set<String> aset = new Set<String>(); aset.add('Approver 1');
        Map<Id, Set<String>> amap = new Map<Id, Set<String>>(); amap.put(sreq.Id, aset);

        Set<String> eset = new Set<String>(); eset.add('Approver1@example.com');
        Map<Id, Set<String>> emap = new Map<Id, Set<String>>(); emap.put(sreq.Id, eset);

        Map<Id, Datetime> tmap = new Map<Id, Datetime>(); 
        tmap.put(sreq.Id, Datetime.now().addDays(-5));
        
        Sample_Request_Approvers_Methods.sendNotifications(srids);
        srpa = Sample_Request_Approvers_Methods.getSubmittedSampleRequests(srids, amap, emap, tmap);
        Sample_Request_Approvers_Methods.sendNotifications(srpa);
    }
}