public with sharing class DocumentsTableController {

    public List<String> documentTags {get; set; }
    private static Integer numTables = 0;
    
    public String emailResponse {get; set;}
    
    public string subscribeContentId { get; set; }
    public boolean subscribeSelected { get; set; }
    
    public DocumentsTableController() {
        numTables++;
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, '#'+getNumDocTables()));
    }
    
    public static Integer getNumDocTables() {
        return numTables;
    }
    
    public PageReference setSubscription() {
        try {
            List<EntitySubscription> subscription = [SELECT Id FROM EntitySubscription WHERE ParentId =: this.subscribeContentId AND SubscriberId =: UserInfo.getUserId()];
            
            if(this.subscribeSelected != null) {
                if (this.subscribeSelected && subscription.size() == 0) {
                    EntitySubscription es = new EntitySubscription();
                    es.ParentId = this.subscribeContentId;
                    es.SubscriberId = UserInfo.getUserId();
                    insert es;
                } else if (!this.subscribeSelected && subscription.size() > 0) {
                    delete subscription;
                }
            }
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
        
        return null;
    }
       
    public PageReference sendEmail() {
        this.emailResponse ='';
        String emailMsg = Apexpages.currentPage().getParameters().get('emailMsg');
        String emailid = Apexpages.currentPage().getParameters().get('emailid');
        String contentid = Apexpages.currentPage().getParameters().get('contentid');
        System.debug('emailMsg:' + emailMsg);
        this.emailResponse = EmailUtil.SendEmail(emailMsg, emailid, contentid);
        return null;
    }
}