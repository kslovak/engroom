@isTest
private class Cummins_Portal_Test {

    static String username = 'testuser@example.ashland.com';
    static String password = 'testpasswd';
    static Cummins_Portal_Controller cpc;
    static Account testAcc;
    static Contact testCon;
    static Lead testLead;
    
    static void createUser() {
        testAcc = new Account(Name='testAcc');
        insert testAcc;
        
        testCon = new Contact(LastName='testCon');
        testCon.AccountId = testAcc.Id;
        insert testCon;

        String profileName = Cummins_Portal_Controller.PROFILE_NAME;
        Profile p;
        try {p = [SELECT Id FROM Profile WHERE Name = :profileName];}
        catch(Exception e) {return;}
        
        User u = new User(Alias = 'testuser', Email = username,
                            EmailEncodingKey = 'UTF-8', LastName = 'testuser',
                            LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                            ProfileId = p.Id, ContactId = testCon.Id, 
                            Ashland_Employee_Number__c = username,
                            TimeZoneSidKey = 'America/Los_Angeles', 
                            Username = username);
        try {insert u;} catch(Exception e) {return;}
    }
    
    static void setup() {
        createUser();
        
        testLead = new Lead();
        testLead.Company = 'Company';
        testLead.Location_Name__c = 'Location';
        testLead.Street = 'Street';
        testLead.City = 'City';
        testLead.State = 'ST';
        testLead.PostalCode = '99999';
        testLead.Country = 'US';
        testLead.Phone = '999-999-9999';
        testLead.Primary_Industry__c = 'PrimaryIndustry';
        
        cpc = new Cummins_Portal_Controller();
        cpc.username = username; cpc.password = password;
        cpc.countryCode = 'CC'; cpc.stateCode = 'SC';
        cpc.newLead = testLead;
        
    }
    
    static testMethod void test01() {
        setup(); Test.startTest();
        cpc.forgotPassword(); cpc.resetPassword(); 
        cpc.showPageBlock1(); cpc.login(); cpc.saveLead();
        cpc.debug('test'); cpc.addReqMsg('test'); cpc.addWarnMsg('test');
    }
}