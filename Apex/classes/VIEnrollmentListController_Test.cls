@isTest
private class VIEnrollmentListController_Test {

    static Account acc;
    
    static void debug(String s) {
        System.debug(LoggingLevel.INFO, '>>>>>>>>>> ' + s);
    }
    
    static void createTestData() {
        acc = new Account(Name='Test Account', AccountNumber='TestAcct');
        insert acc;
        VI_Promotion__c p = new VI_Promotion__c();
        p.Program_Code__c = 'PC1001';
        p.Channel__c = 'Test Channel';
        p.Program_Name__c = 'Test Promotion';
        insert p;
        VI_Enrollment__c e = new VI_Enrollment__c();
        e.Facility__c = acc.Id;
        e.Promotion__c = p.Id;
        insert e;
        VI_Enrollment_Location__c el = new VI_Enrollment_Location__c();
        el.Approval_Status__c = VIUtils.ENROLLMENT_STATUS_SUBMITTED;
        el.Enrollment__c = e.Id;
        el.Facility__c = acc.Id;
        el.Location__c = acc.Id;
        el.Promotion__c = p.Id;
        insert el;
    }
    
    static testMethod void test01() {
        Test.startTest();
        createTestData();
        VIEnrollmentListController c = new VIEnrollmentListController();
        c.accountId = acc.Id; c.status = null;
        c.changeStatus(); c.getEnrollments();
    }
}