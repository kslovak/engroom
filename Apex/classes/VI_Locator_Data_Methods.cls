public class VI_Locator_Data_Methods {

    public static final String DELIM = '-';
    public static final String YES   = 'Yes';

    public static final Set<String> ACCOUNT_TYPES = new Set<String>{
        VIUtils.ACCOUNT_TYPE_FACILITY, 
        VIUtils.ACCOUNT_TYPE_LOCATION
    };

    public static final Set<String> ENROLL_STATS = new Set<String>{
        VIUtils.ENROLLMENT_STATUS_APPROVED, 
        VIUtils.ENROLLMENT_STATUS_APPROVED_WITH_EXCEPTIONS
    };
    
    private static final String ACC_QRY1 = ''
        + 'select Id, AVI_Certified__c, Inactive_Account__c'
        + '  from Account where VI_Account_Type__c in :ACCOUNT_TYPES'
        + '   and AVI_Certified__c = true and Inactive_Account__c = false';
    
    private static final String VLD_QRY1 = ''
        + 'select Id from VI_Locator_Data__c';
    
    private static void debug(String s) {
        System.debug(LoggingLevel.INFO, 'VI_Locator_Data_Methods' + s);
    }
    
    public static Database.Querylocator getAccountQL() {
        return Database.getQueryLocator(ACC_QRY1);
    }
    
    public static Database.Querylocator getLocatorDataQL() {
        return Database.getQueryLocator(VLD_QRY1);
    }

    public static void deleteLocatorData(List<VI_Locator_Data__c> dlist) {
        if (!dlist.isEmpty()) {
            try {delete dlist;} catch (Exception e) {debug(e.getMessage());}
        }
    }
        
    public static void upsertLocatorData(List<Account> alist) {
        Set<Id> accIds = new Set<Id>();
        for (Account a : alist) {
            if (a.AVI_Certified__c && !a.Inactive_Account__c) {accIds.add(a.Id);}
        }
        if (accIds.isEmpty()) {return;}
        upsertLocatorData(accIds);
    }
    
    public static void upsertLocatorData(Set<Id> accIds) {
        Map<String, VI_Locator_Data__c> amap = new Map<String, VI_Locator_Data__c>();
        Date tdate = Date.today();
        for (Account acc : [
            select Id, AccountNumber, BillingStreet,
                  (select Id, Promotion__r.Id,
                          Promotion__r.Program_Code__c
                     from Enrollment_Locations__r
                    where Promotion__r.End_Date__c >= :tdate
                      and Approval_Status__c in :ENROLL_STATS
                      and Promotion__r.Promotion_Appear_on_Locator__c = :YES)
              from Account where Id in :accIds]) {
            addLocatorData(acc, amap);
        }
        if (amap == null || amap.isEmpty()) {return;}
        try {upsert amap.values() Record_Key__c;}
        catch (Exception e) {debug(e.getMessage());}
    }
    
    private static void addLocatorData(Account acc, Map<String, VI_Locator_Data__c> amap) {
        List<VI_Enrollment_Location__c> els = acc.Enrollment_Locations__r; VI_Locator_Data__c v;
        if (els == null || els.isEmpty()) {
                v = getLocatorData(acc, null); 
                if (v != null) {amap.put(v.Record_Key__c, v);}
        } else {
            for (VI_Enrollment_Location__c el : els) {
                v = getLocatorData(acc, el); 
                if (v != null) {amap.put(v.Record_Key__c, v);}
            }
        }
    } 

    private static VI_Locator_Data__c getLocatorData(Account acc, VI_Enrollment_Location__c el) {
        VI_Locator_Data__c v = new VI_Locator_Data__c(); String k = acc.Id;
        v.Location__c = acc.Id; if (el != null) {v.Enrollment__c = el.Id; k += DELIM + el.Id;}
        v.Record_Key__c = k; v.Name = getRecordName(acc, el); setAddressLines(acc, v);
        debug('LocatorData : v = ' + v); return v;
    } 

    private static String getRecordName(Account acc, VI_Enrollment_Location__c el) {
        String s = acc.AccountNumber; if (el == null) {return s;}
        VI_Promotion__c p = el.Promotion__r;
        if (p != null) {s += (DELIM + p.Program_Code__c);}
        return s;
    }
    
    private static void setAddressLines(Account acc, VI_Locator_Data__c v) {
        List<String> sa; String s = acc.BillingStreet;
        if (s != null) {sa = s.split('\n');}
        if (sa != null) {
            if (sa.size() > 0) {
                v.Location_Address1__c = sa[0];
                if (sa.size() > 1) {v.Location_Address2__c = sa[1];}
            }
        }
    }
    
}