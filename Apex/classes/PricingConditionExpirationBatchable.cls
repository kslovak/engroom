global class PricingConditionExpirationBatchable implements Database.Batchable<sObject> {

    private static final String ACTIVE = PricingConditionFunctions.ACTIVE;
    private static final String APPROVED = PricingConditionFunctions.APPROVED;
    private static final String EXPIRED = PricingConditionFunctions.EXPIRED;
    private static final Set<String> REC_STATS = new Set<String>{ACTIVE, APPROVED};

    global PricingConditionExpirationBatchable() {}

    global Database.Querylocator start(Database.BatchableContext bc) {
        Database.Querylocator ql;
        Date currentDate = System.today();
        ql = Database.getQueryLocator([
                select Id, CreatedDate, Name, Active__c, Current_State__c, 
                       Record_Status__c, Valid_To_Date__c
                  from Pricing_Condition__c
                 where Record_Status__c in :REC_STATS
                   and Valid_To_Date__c < :currentDate
              order by CreatedDate]);
        return ql;
    }

    global void execute(Database.BatchableContext bc, List<SObject> alist){
        List<Pricing_Condition__c> pcs = (List<Pricing_Condition__c>)alist;
        for (Pricing_Condition__c pc : pcs) {
            pc.Active__c = false; pc.Current_State__c = EXPIRED; pc.Record_Status__c = EXPIRED;
        }
        PricingConditionFunctions.setUserComments(pcs, 'Expired by Date');
        try {update pcs;} catch(Exception e) {System.debug(e.getMessage());}
    }

    global void finish(Database.BatchableContext bc){
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                 TotalJobItems, CreatedBy.Email
                            from AsyncApexJob
                           where Id =:bc.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {a.CreatedBy.Email});
        mail.setReplyTo(a.CreatedBy.Email);
        mail.setSenderDisplayName('PricingCondition Expiration Batch Processing');
        mail.setSubject('Apex Batch Job - PricingConditionExpiration - ' +
                         a.Status+' - '+a.TotalJobItems+' batches');
        mail.setPlainTextBody('Job Id : '+a.Id+' processed ' + a.TotalJobItems +
                              ' batches with '+ a.NumberOfErrors + ' failures.');
        if (!Test.isRunningTest()) {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }

    Webservice static String submitBatchJob() {
        PricingConditionExpirationBatchable b = new PricingConditionExpirationBatchable();
        String msg;
        try {msg = Database.executeBatch(b);} catch(Exception e) {
            msg = e.getMessage(); System.debug(e);
        }
        return msg;
    }

}