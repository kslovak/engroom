@IsTest
private class VIAvailablePromotionsControllerTest {
    private static Account distributor;
    private static Account facility;
    private static List<Account> locations;
    private static Integer thisYear;
    private static Integer nextYear;
    private static VI_Promotion__c thisYearPromoGroupA;
    private static VI_Promotion__c thisYearPromoGroupB;
    private static VI_Promotion__c nextYearPromoGroupA;
    private static VI_Promotion__c nextYearPromoGroupB;
    
    private static String PROMO_CATEGORY = 'VPS'; 

	private static void debug(String s) {
		System.debug(LoggingLevel.INFO, 'VIAvailablePromotionsControllerTest : ' + s);
	}
	
    private static void setUp() {
        distributor = new Account();
        distributor.Name = 'Test Distributor';
        distributor.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_DISTRIBUTOR;
        insert distributor;

        facility = new Account();
        facility.VI_Parent_Account__c = distributor.Id;
        facility.Name = 'Test Facility';
        facility.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_FACILITY;
        facility.VI_Channel__c = VIUtils.CHANNEL_QUICKLUBE;
        facility.VI_Ineligible_for_Promotions__c = true;
        facility.AVI_Certified__c = true;
        facility.SAP_Customer_Group_5_Desc__c = VIUtils.SAP_CUST_GROUP_5_EXPRESSCARE;
        insert facility; debug('facility : ' + facility);

        locations = new List<Account>();
        for (Integer i = 0; i < 2; i++) {
            Account location = new Account();
            location.VI_Parent_Account__c = facility.Id;
            location.Name = 'Test Location ' + String.valueOf(i + 1);
            location.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_LOCATION;
	        location.VI_Channel__c = VIUtils.CHANNEL_QUICKLUBE;
	        location.SAP_Customer_Group_5_Desc__c = VIUtils.SAP_CUST_GROUP_5_EXPRESSCARE;

            locations.add(location);
        }
        insert locations;
/*
        List<Group> groups = [SELECT Id
                FROM Group
                WHERE RelatedId = :UserInfo.getUserRoleId()
                AND Type = 'RoleAndSubordinates'];
        if (groups.isEmpty()) {
                groups = [SELECT Id
                        FROM Group
                        WHERE RelatedId = :UserInfo.getUserRoleId()
                        AND Type = 'Role'];
        }
        if (groups.isEmpty()) {return;}

        List<AccountShare> accountShares = new List<AccountShare>();
        for (Account location : locations) {
            accountShares.add(new AccountShare(AccountId = location.Id,
                    UserOrGroupId = groups.get(0).Id,
                    AccountAccessLevel = 'Edit',
                    OpportunityAccessLevel = 'None',
                    CaseAccessLevel = 'None'));
        }
        insert accountShares;
*/
        thisYear = Date.today().year();
        nextYear = thisYear + 1;

        thisYearPromoGroupA = getPromotion(thisYear, 'A', 'TMP001', 'Test Promotion 1');
        insert thisYearPromoGroupA;

        thisYearPromoGroupB = getPromotion(thisYear, 'B', 'TMP002', 'Test Promotion 2');
        insert thisYearPromoGroupB;

        nextYearPromoGroupA = getPromotion(nextYear, 'A', 'TMP003', 'Test Promotion 3');
        insert nextYearPromoGroupA;

        nextYearPromoGroupB = getPromotion(nextYear, 'B', 'TMP004', 'Test Promotion 4');
        insert nextYearPromoGroupB;

        VI_Enrollment__c enrollment = new VI_Enrollment__c();
        enrollment.Promotion__c = thisYearPromoGroupA.Id;
        enrollment.Facility__c = facility.Id;
        insert enrollment;

        VI_Enrollment_Location__c enrollmentLocation = new VI_Enrollment_Location__c();
        enrollmentLocation.Enrollment__c = enrollment.Id;
        enrollmentLocation.Location__c = locations.get(0).Id;
        enrollmentLocation.Approval_Status__c = VIUtils.ENROLLMENT_STATUS_APPROVED;
        enrollmentLocation.Approval_Date__c = Date.today().addDays(-1);
        insert enrollmentLocation;
    }
    
    private static VI_Promotion__c getPromotion(Integer year, String pgrp, String code, String name) {
        Date startDate = Date.newInstance(year, 1, 1);
        Date endDate = Date.newInstance(year + 1, 1, 1).addDays(-1);
        VI_Promotion__c p = new VI_Promotion__c();
        p.Name = name; p.Active__c = true; p.Program_Name__c = name; p.Program_Code__c = code;
        p.Start_Date__c = startDate; p.End_Date__c = endDate; p.Redemption_Date__c = endDate;
        p.Fiscal_Year__c = String.valueOf(year); p.Promotional_Group__c = pgrp; p.Promotional_Category__c = PROMO_CATEGORY;
        p.Channel__c = VIUtils.CHANNEL_QUICKLUBE_EXPRESSCARE;
        p.SalesOrg_Code__c = VIUtils.DEFAULT_SALESORG;
        debug('p = ' + p); return p;
    }

    static testmethod void availablePromotions() {
        setUp();

        facility.VI_Promotional_Group__c = String.valueOf(thisYear) + 'A';
        facility.VI_Ineligible_for_Promotions__c = false;
        update facility;

        Map<String, String> params = System.currentPageReference().getParameters();
        params.put(VIAvailablePromotionsController.PARAM_ID, locations.get(0).Id);
        params.put(VIAvailablePromotionsController.PARAM_RETURN_URL, '/home/home.jsp');

        Test.startTest();

        VIAvailablePromotionsController controller = new VIAvailablePromotionsController();
		
        controller.getPortalUser();
        controller.getShowCost();
        controller.getInternalUser();
        controller.getFacility();
        controller.getCurrentStep();
        controller.getLocationFax();
        controller.getLocationWebsite();
        controller.getShipTo();
		controller.getCanDeleteEnrollment();
		controller.getCanEnterEnrollment();
		controller.getIsCustomerPortalUser();
		controller.getAccountId();
		controller.getFacilityName();
		controller.getAVICertified();
		controller.getThisYear();
		controller.getNextYear();
		controller.getNextYearPromotions();
		controller.getReturnURL();
		controller.getThisURL();
		controller.getLocationName();
		controller.getLocationStreet();
		controller.getLocationCity();
		controller.getLocationState();
		controller.getLocationPostalCode();
		controller.getLocationPhone();
		controller.getLocations();
		controller.getEnrollmentLocations();
		
		controller.ctgry = PROMO_CATEGORY;
		controller.setPromotions();
        List<VIPromotion> thisYearPromotions = controller.getThisYearPromotions();

        for (VIPromotion p : thisYearPromotions) {p.selected = true;}
		controller.getSelectedPromotions();
        controller.getDefaultQuantities();
        controller.getTotalCost();
		controller.enrollSelectedPromotion();
        controller.enrollSelectedPromotions();

        controller.step2();
        controller.step3();
        controller.step4();
        controller.submit();
        controller.step99();
        controller.gotoAvailablePromotionsPage();
		controller.viewMaterialImage();
        controller.cancel();

        Test.stopTest();
    }

    static testmethod void availablePromotionsForCustomerPortalUserAtFacility() {
        setUp();

        Contact contact = new Contact();
        contact.AccountId = facility.Id;
        contact.LastName = 'Test';
        insert contact;

        /*Profile p;
        try {
            p = [SELECT Id FROM Profile WHERE Name = :VIUtils.CUSTOMER_PORTAL_NAME];
        } catch(Exception e) {return;}
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing',
                LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                ProfileId = p.Id, ContactId = contact.Id, Ashland_Employee_Number__c = 'zzzTest17',
                TimeZoneSidKey = 'America/Los_Angeles', Username = 'standarduser17@testorg.com');
        System.runAs(u) {*/
            Test.startTest();

            VIAvailablePromotionsController controller = new VIAvailablePromotionsController();

            Test.stopTest();
        //}
    }

    static testmethod void availablePromotionsForCustomerPortalUserAtLocation() {
        setUp();

        Contact contact = new Contact();
        contact.AccountId = locations.get(0).Id;
        contact.LastName = 'Test';
        insert contact;

        /*Profile p;
        try {
            p = [SELECT Id FROM Profile WHERE Name = :VIUtils.CUSTOMER_PORTAL_NAME];
        } catch(Exception e) {return;}
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing',
                LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                ProfileId = p.Id, ContactId = contact.Id, Ashland_Employee_Number__c = 'zzzTest18',
                TimeZoneSidKey = 'America/Los_Angeles', Username = 'standarduser18@testorg.com');
        System.runAs(u) {*/
            Test.startTest();

            VIAvailablePromotionsController controller = new VIAvailablePromotionsController();

            Test.stopTest();
        //}
    }
}