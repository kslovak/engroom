@isTest
private class AssetManagementFunctions_Test {

    static Account plant1, plant2, acc1;
    static Asset asset1;
    static List<Asset> assets;
    static Product2 equip1;
    static List<AssetManagementController.AssetClass> aclist; 
    
    static testMethod void test01() {
    	createTestData();
    	ApexPages.currentPage().getParameters().put('pid', plant1.Id);
        AssetManagementController c = new AssetManagementController();
        c.newAsset = new AssetManagementController.AssetClass(asset1);
        c.newAsset.assignedAccName = 'Test Asset Account1'; 
        c.newAsset.equipmentName = 'Test Asset Equipment1';
        c.newAsset.plantName = 'Test Asset Plant1'; 
        c.getStatSelOps(); c.setAccounts(); c.setEquipments(); c.setPlants();
        c.newAsset.quantity = 2; c.newAsset.plantId = plant1.Id; c.newAsset.equipmentId = equip1.Id;
        c.createAssets(); c.initAssetList(); c.searchAssets(); 
        aclist = c.getAssetList(); aclist[0].selected = true;
        c.getAssetsToAssign(); 
        c.getAssetsToDelete(); 
        c.getAssetsToRetire();
        c.getAssetsToReturn();
        c.getAssetsToTransfer();
    }
    
    static void createTestData() {
    	plant1 = new Account(Name='Test Asset Plant1');
        plant2 = new Account(Name='Test Asset Plant2');
        acc1   = new Account(Name='Test Asset Account1');
        List<Account> accs = new List<Account>{acc1, plant1, plant2};
        insert accs;
        
        equip1 = new Product2(Name='Test Asset Equipment1');
        insert equip1;
        
        asset1 = new Asset(Name='Test Asset 1', AccountId=plant1.Id, Product2Id=equip1.Id);
        insert asset1;
        
        assets = new List<Asset>();
        for (Integer i = 0; i < 100; i++) {
        	asset1 = new Asset(Name='Asset '+i, AccountId=plant1.Id, Product2Id=equip1.Id);
        	assets.add(asset1);
        }
        //insert assets;
    }

/*    
    static  List<AssetClass> getTestAssets() {
        List<AssetClass> alist = new List<AssetClass>();
        Product2 eqp = [select Id, Name from Product2
                         where Name = 'Valv Equipment 1' 
                         limit 1];
        for (Integer i = 0; i < 10; i++) {
            Asset a = new Asset();
            a.Product2 = eqp;
            a.Name = 'Asset Name ' + i;
            a.Quantity = i;
            alist.add(new AssetClass(a));
        }
        return alist;
    }
*/    

}