@isTest
class PageControllerTest {

    @isTest
    static void getPageTest() {
        PageController pc = new PageController();
        System.debug( '***' + pc.getPage() );
        System.assertNotEquals(null, pc.getPage());
    }
    
    @isTest
    static void getASCPagesMapTest() {
        PageController pc = new PageController();
        System.assertNotEquals(null, pc.getASCPagesMap());
    }
    
    @isTest
    static void getNavProductListTest() {
        PageController pc = new PageController();
        System.assertNotEquals(null, pc.getNavProductList());
        System.assert(pc.getNavProductList().size() >= 0);
    }
    
    @isTest
    static void getNavMarketListTest() {
        PageController pc = new PageController();
        System.assertNotEquals(null, pc.getNavMarketList());
        System.assert(pc.getNavMarketList().size() >= 0);
    }
    
    @isTest(SeeAllData=true)
    static void getUnreadConversationCountTest() {
        PageController pc = new PageController();
        Integer unreadCount = pc.getUnreadConversationCount().unreadCount;
        System.assertNotEquals(null, unreadCount);
        System.assert(unreadCount >= 0);
    }
    
    @isTest
    static void pageAuthHandlerTest() {
        PageController pc = new PageController();
        String type = UserInfo.getUserType();
        if(type == 'Guest') {
            System.assertNotEquals(null, pc.pageAuthHandler().getParameters().get('startURL'));
        } else {
            System.assertEquals(null, pc.pageAuthHandler());
        }
    }
    
    @isTest
    static void navProductTest() {
        PageController.NavProduct newNavProduct1 = new PageController.NavProduct('testName', 'testId');
        PageController.NavProduct newNavProduct2 = new PageController.NavProduct('aTestName', 'testId');
        
        System.assertEquals( 'testName', newNavProduct1.getName() );
        System.assertEquals( 'testId', newNavProduct1.getId() );
        System.assert( 0 < newNavProduct1.compareTo(newNavProduct2) );
    }
    
    @isTest
    static void navMarketTest() {
        PageController.navMarket newNavMarket1 = new PageController.NavMarket('testName', 'testId');
        PageController.navMarket newNavMarket2 = new PageController.NavMarket('aTestName', 'testId');
        
        System.assertEquals( 'testName', newNavMarket1.getName() );
        System.assertEquals( 'testId', newNavMarket2.getId() );
        System.assert( 0 < newNavMarket1.compareTo(newNavMarket2) );
    }
    
    /*@isTest
    static void a1TargetTest() {
        PageController testClass = new PageController();
        testClass.a1_target = 'test';
        System.assertEquals( 'test', testClass.a1_target );
    }*/
    
    @isTest
    static void userCanViewTest() {
        PageController testClass = new PageController();
        
        // null article
        System.assertEquals( null, testClass.getArticle1() );
        System.assertEquals( null, testClass.getArticle2() );
        System.assertEquals( null, testClass.getArticle3() );
        
        
        Community_Article__c testArticle = new Community_Article__c(Name='testArticleName');
    }
    
    public static testMethod void testgetAllMarkets() {
        Profile prof = [Select Id, Name From Profile Where Name = 'System Administrator'];

        User u = new User();
        u.FirstName = 'TestFest';
        u.LastName = 'User123';
        u.alias = 'tuser123';
        u.Username = 'Test@testengineroom.com.test';
        u.Email = 'Test@testengineroom.com.test';
        u.emailencodingkey = 'UTF-8';
        u.languagelocalekey = 'en_US';
        u.localesidkey = 'en_US';
        u.profileId = prof.Id;
        u.timezonesidkey = 'America/Los_Angeles';
        insert u;
        
       System.runAs(u){
        
            Group grp = new Group();
            grp.Name = 'Market_TestMarket_Group';
            insert grp;
            
            GroupMember gm = new GroupMember();
            gm.UserOrGroupId = u.id;
            gm.GroupId =grp.id;
            insert gm;
            //add market
            Market__C mkt = new Market__C();
            mkt.name = 'Test Market';
            mkt.Alias__c = 'Test Market';
            mkt.Tag__c = 'Market-TestMarket';
            insert mkt;

            test.startTest();
            PageController pc = new MarketController();
            List<PageController.NavMarket> m = pc.getNavMarketList();
            System.assertEquals(m.size(), 1);
            test.stopTest();
            
        }
    }
    
     public static testMethod void testretreiveUserProducts() {
        Profile prof = [Select Id, Name From Profile Where Name = 'System Administrator'];
        User u = new User();
        u.FirstName = 'TestFest';
        u.LastName = 'User123';
        u.alias = 'tuser123';
        u.Username = 'Test@testengineroom.com.test';
        u.Email = 'Test@testengineroom.com.test';
        u.emailencodingkey = 'UTF-8';
        u.languagelocalekey = 'en_US';
        u.localesidkey = 'en_US';
        u.profileId = prof.Id;
        u.timezonesidkey = 'America/Los_Angeles';
        insert u;
        System.runAs(u){
            //add Product
            Product__C prd = new Product__C();
            prd.name = 'Test Product';
            prd.Alias__c ='Test Product';
            prd.Tag__c = 'Product-TestProduct';
            insert prd;
            
            Group grp = new Group();
            grp.Name = 'Product_TestProduct_Group';
            insert grp;
            
            GroupMember gm = new GroupMember();
            gm.UserOrGroupId = u.id;
            gm.GroupId =grp.id;
            insert gm;
        
            test.startTest();
            PageController pc = new PageController();
            List<PageController.NavProduct> p = pc.getNavProductList();
            System.assertEquals(p.size(), 1);
            test.stopTest();
            
        }
      
     }

      
}