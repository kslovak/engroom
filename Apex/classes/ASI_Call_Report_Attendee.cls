public class ASI_Call_Report_Attendee {

    public User         usr {get; private set;}

    public Integer   recnum {get; set;}
	public Boolean selected {get; set;}
    public String     acrId {get; set;}
	
    public ASI_Call_Report_Attendee(User o, Integer n) {this(null, o, n);}
    
    public ASI_Call_Report_Attendee(String crId, User o, Integer n) {
    	acrId = crId; usr = o; selected = false; recnum = n;
    }

    public ASI_Call_Report_Attendee(ApexPages.StandardController sc) {
        debug('Constructor(sc)');
        init1(sc.getRecord());
    }
    
    private void debug(String s) {
        System.debug(LoggingLevel.INFO, 'ASI_Call_Report_Attendee : ' + s);
    }

    private void init1(SObject sobj) {
        PageReference pr = System.currentPageReference(); String s;
        Map<String, String> params = pr.getParameters(); 
        debug('params = ' + params);
    }
    
    public PageReference initAction1() {
        debug('initAction1');
        return null;
    }
    
}