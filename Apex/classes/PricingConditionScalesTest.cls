@isTest
private class PricingConditionScalesTest {

    static Account                 acc;
    static Material_Sales_Data2__c mat;
    static Pricing_Condition__c    pc;

    static PricingConditionScales  c;
    
    static void setup() {
        acc = new Account(Name='testAcc');
        insert acc;
        
        mat = Material_Sales_Data_Functions_Test.getTestMsd2();
        
        pc = new Pricing_Condition__c();
        pc.Account__c = acc.Id; pc.Material2__c = mat.Id;
        pc.Record_Status__c = PricingConditionScales.ACTIVE;
        pc.Used_Scaled_Pricing__c = true;
        insert pc;
        
        c = new PricingConditionScales(); c.accountId = acc.Id;
    }
    
    static testMethod void test01() {
        setup(); c.init3(); 
        for (PricingConditionScales.Rec r : c.recs) {r.selected = true;}
        c.deleteScales1(); c.deleteScales2(); 
        c.cancel2(); c.cancel1();
        c.start(null); c.execute(null, null); c.finish(null);
    }
}