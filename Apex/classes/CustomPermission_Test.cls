@isTest(SeeAllData=true)
private class CustomPermission_Test {

	static testmethod void test01() {
		Test.startTest();
		
		String cpname = 'PCN_Send_to_SAP';
		CustomPermission_Methods.isPermitted(cpname);
		CustomPermission_Methods.getPermittedUserIds(cpname);
		
		Test.stopTest();
	}
}