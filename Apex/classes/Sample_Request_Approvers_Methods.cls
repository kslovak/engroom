public class Sample_Request_Approvers_Methods {

    public static final Boolean TESTING_MODE;
    public static final Integer APPROVAL_PENDING_DAYS;
    public static final Date    CREATED_DATE_FROM;
    public static final List<String> EMAIL_IDS_BCC, EMAIL_IDS_CC;
    
    private static final String CRNL = '\r\n';
    private static final String SERVER_URL = System.URL.getSalesforceBaseUrl().toExternalForm();
    private static final String VFPAGE_URL = SERVER_URL + '/apex/PendingApprovals';
    private static final String SYSADMIN_EMAIL  = User_Functions.getSysAdminEmail();
    private static final Map<String, Approval_Reminder_Settings__c> settingsMap;
    private static final Approval_Reminder_Settings__c settings1, settings2;
    
    private static final String HTML_HEAD = '<head><style>\n' +
        '.tblcaption {font-family: Verdana, Arial, Geneva, sans-serif; font-weight: bold; font-size: 9pt; text-align: center;}\n' +
        'TABLE.tbl {border: black 1px solid; cell-spacing: 0px; padding: 0px; margin: 0px; border-collapse: collapse; width: 100%}\n' +
        'TABLE.tbl TR.orow {background-color: #f3f3ec;}\n' +
        'TABLE.tbl TH {font-family: Verdana, Arial, Geneva, sans-serif; font-weight: bold; font-size: 8pt; text-align: left; vertical-align: top; background-color: #eeecd1; padding: 5px; border: black 1px solid; border-collapse: collapse;}\n' +
        'TABLE.tbl TD {font-family: Verdana, Arial, Geneva, sans-serif; font-weight: normal; font-size: 8pt; border: black 1px; border-collapse: collapse; align: left; text-align: left; vertical-align: top; padding: 5px; margin: 0px; white-space: nowrap;}\n' +
        'TABLE.tbl TH.right {align: right; text-align: right;}\n' +
        'TABLE.tbl TD.right {align: right; text-align: right;}\n' +
        'TABLE.approvers TD {border: black 0px solid; padding: 0px;}\n' +
        '</style></head>';
        
    static {
        settingsMap = Approval_Reminder_Settings__c.getAll();
        settings1 = settingsMap.get('Default');
        settings2 = settingsMap.get('Samples');
        APPROVAL_PENDING_DAYS = getApprovalPendingDays();
        CREATED_DATE_FROM = getCreatedDateFrom();
        EMAIL_IDS_BCC = getEmailIdsBCC();
        EMAIL_IDS_CC = getEmailIdsCC();
        TESTING_MODE = getTestingMode();
    }
    
    private static void debug(String s) {System.debug(LoggingLevel.INFO, s);}
            
    private static String getString(String s) {return s == null ? '' : s.trim();}
    
    private static List<String> getList(String s) {
        if (String.isBlank(s)) {return null;} return s.trim().split(CRNL);
    }
    
    private static Boolean getTestingMode() {
        Boolean b = false;
        if (!b && settings2 != null) {b = settings2.Testing_Mode__c;}
        if (!b && settings1 != null) {b = settings1.Testing_Mode__c;}
        return b;
    }

    private static Integer getApprovalPendingDays() {
        Decimal d = null;
        if (d == null && settings2 != null) {d = settings2.Approval_Pending_Days__c;}
        if (d == null && settings1 != null) {d = settings1.Approval_Pending_Days__c;}
        if (d == null) {d = 2;} return d.intValue();
    }

    private static Date getCreatedDateFrom() {
        Decimal d = null; Integer n = -30;
        if (d == null && settings2 != null) {d = settings2.Days_since_Created__c;}
        if (d == null && settings1 != null) {d = settings1.Days_since_Created__c;}
        if (d != null && d > 0) {n = -1 * d.intValue();}
        return Date.today().addDays(n);
    }

    private static List<String> getEmailIdsCC() {
        List<String> alist = null;
        if (alist == null && settings2 != null) {alist = getList(settings2.Email_Ids_CC__c);}
        if (alist == null && settings1 != null) {alist = getList(settings1.Email_Ids_CC__c);}
        return alist;
    }

    private static List<String> getEmailIdsBCC() {
        List<String> alist = null;
        if (alist == null && settings2 != null) {alist = getList(settings2.Email_Ids_BCC__c);}
        if (alist == null && settings1 != null) {alist = getList(settings1.Email_Ids_BCC__c);}
        return alist;
    }

    private static String getLink(Sample_Request__c p) {
        String s = '<a href="'+SERVER_URL+'/'+p.Id+'">'+p.Name+'</a>';
        return s;
    }

    public static void sendNotifications(Set<String> srids) {
        Map<Id, List<ProcessInstanceWorkItem>> pmap = Custom_Approval_Functions.getProcessInstanceWorkItems(srids);
        Map<Id, Set<String>> amap = Custom_Approval_Functions.getApproverNames(pmap);
        Map<Id, Set<String>> emap = Custom_Approval_Functions.getApproverEmails(pmap);
        Map<Id, Datetime> tmap = Custom_Approval_Functions.getProcessInstanceTimes(pmap);
        List<Sample_Request> srpa = getSubmittedSampleRequests(srids, amap, emap, tmap);
        sendNotifications(srpa);
    }
    
    public static void sendNotifications(List<Sample_Request> srpa) {
        if (srpa == null || srpa.isEmpty()) {return;}
        List<Messaging.SingleEmailMessage> mlist = new List<Messaging.SingleEmailMessage>();
        Map<String, List<String>> amap = getApproverSampleRequests(srpa);
        if (amap == null || amap.isEmpty()) {return;}
        getEmailMessages(amap, mlist);
        if (!mlist.isEmpty()) {
            List<Messaging.SendEmailResult> sers;
            try {
                if (!Test.isRunningTest()) {sers = Messaging.sendEmail(mlist);}
            } catch(Exception e) {
                debug(e.getMessage());
            }
        }
    }
    
    private static void getEmailMessages(Map<String, List<String>> amap,
                                         List<Messaging.SingleEmailMessage> mlist) {
        if (TESTING_MODE && (EMAIL_IDS_CC == null || EMAIL_IDS_CC.isEmpty())) {return;}
        Messaging.SingleEmailMessage m; 
        Set<String> es = amap.keySet(); List<String> sa;
        for (String e : es) {
            sa = amap.get(e);
            m = new Messaging.SingleEmailMessage();
            m.setHtmlBody(getHtmlBody(sa));
            m.setSaveAsActivity(false); m.setReplyTo(e);
            m.setSubject(getSubject(sa.size(), e));
            if (!TESTING_MODE) {
                m.setToAddresses(new List<String>{e});
            }
            if (EMAIL_IDS_CC != null && !EMAIL_IDS_CC.isEmpty()) {
                if (TESTING_MODE) {
                    m.setToAddresses(EMAIL_IDS_CC);
                } else {
                    m.setCcAddresses(EMAIL_IDS_CC);
                }
            }
            if (EMAIL_IDS_BCC != null && !EMAIL_IDS_BCC.isEmpty()) {
                m.setBccAddresses(EMAIL_IDS_BCC);
            }
            mlist.add(m);
        }
        //System.debug(LoggingLevel.INFO, mlist);
    }
    
    private static String getSubject(Integer recs, String emailId) {
        String s = 'Sample Requests Pending Approval'; 
        Integer n = emailId.indexOf('=ashland.com@example.com');
        if (n < 0) {n = emailId.indexOf('@');}
        if (n > 0) {s += ' - ' + emailId.substring(0, n);}
        return s + ' - ' + recs;
    }
    
    private static String getHtmlBody(List<String> sa) {
        String h = '<html>'+ HTML_HEAD + '<body>';
        h += getTableHdr(); 
        Boolean isEvenRow = false;
        for (String s : sa) {
            if (isEvenRow) {
                h += '<tr class="erow">';
            } else {
                h += '<tr class="orow">';
            }
            h += s + '</tr>';
            isEvenRow = !isEvenRow;
        }
        h += '</table></body></html>';
        return h;
    }
    
    public static List<Sample_Request> getSubmittedSampleRequests(Set<String> srids,
        Map<Id, Set<String>> amap, Map<Id, Set<String>> emap, Map<Id, Datetime> tmap) {
        List<Sample_Request> rlist = new List<Sample_Request>(); Sample_Request r;
        for (Sample_Request__c sr : [
            select Id, Name, Account__r.Name, Account__r.AccountNumber, 
                   Account__r.Owner_Name__c, Number_of_Items__c, VCP_Rating__c 
              from Sample_Request__c where Id in :srids order by Account__r.Name]) {
            if (amap != null && amap.containsKey(sr.Id)) {
                r = new Sample_Request(sr);
                r.setApprovers(amap);
                if (tmap != null && tmap.containsKey(sr.Id)) {
                    r.submittedDateTime = tmap.get(sr.Id);
                    r.submittedTime = tmap.get(sr.Id).format();
                }
                if (emap != null && emap.containsKey(sr.Id)) {
                    r.approverEmails = emap.get(sr.Id);
                }
                rlist.add(r);
            }
        }
        return getSampleRequestsPendingApproval(rlist);
    }
        
    private static List<Sample_Request> getSampleRequestsPendingApproval(List<Sample_Request> submittedRecs) {
        List<Sample_Request> plist = new List<Sample_Request>();
        if (submittedRecs == null || submittedRecs.isEmpty()) {return plist;}
        Date d2 = Custom_Approval_Functions.getLatestDateForNotifications(APPROVAL_PENDING_DAYS);
        Date d1;
        for (Sample_Request p : submittedRecs) {
            if (p.submittedDateTime != null) {
                d1 = p.submittedDateTime.date();
                if (d1.daysBetween(d2) >= 0) {
                    plist.add(p);
                }
            }
        }
        return plist;
    }
    
    private static Map<String, List<String>> getApproverSampleRequests(List<Sample_Request> srs) {
        Map<String, List<String>> amap = new Map<String, List<String>>();
        List<String> plist; Integer n;
        for (Sample_Request p : srs) {
            for (String e : p.approverEmails) {
                if (amap.containsKey(e)) {
                    plist = amap.get(e);
                } else {
                    plist = new List<String>();
                }
                n = plist.size() + 1;
                plist.add(getSampleRequestString(p, n));
                amap.put(e, plist);
            }
        } 
        return amap;
    }
    
    private static String getTableHdr() {
        String s = '';
        s += '<div class="tblcaption">'; 
        s += 'Following Sample Requests are pending for your approval for more than ' + APPROVAL_PENDING_DAYS + ' days.<br><br>';
        s += '<a href="' + VFPAGE_URL + '">Please login to Salesforce and review the following Sample Requests.</a>';
        s += '</div><br>';
        s += '<table class="tbl">';
        s += '<tr>';
        s += '<th width="20px" class="right">#</th>';
        s += '<th width="100px">Sample Request#</th>';
        s += '<th width="180px">Account Name<br>and Number</th>';
        s += '<th width="180px">Account Owner</th>';
        s += '<th width="50px">VCP<br>Rating</th>';
        s += '<th width="50px"># of<br>Materials</th>';
        s += '<th width="120px">Submitted<br>Time</th>';
        s += '<th width="100px">&nbsp;Approvers</th>';
        s += '</tr>';
        return s;
    }
    
    private static String getSampleRequestString(Sample_Request sr, Integer rowNum) {
        Sample_Request__c r = sr.sampleRequest;
        String s = '';
        s += '<td width="20px" class="right">' + rowNum + '</td>';
        s += '<td width="100px">' + getLink(r) + '</td>';
        s += '<td width="180px">' + r.Account__r.Name + '<br>' + sr.accountNumber + '</td>';
        s += '<td width="180px">' + r.Account__r.Owner_Name__c + '</td>';
        s += '<td width="50px">' + getString(r.VCP_Rating__c) + '</td>';
        s += '<td width="50px">' + r.Number_of_Items__c + '</td>';
        s += '<td width="120px">' + sr.submittedTime + '</td>';
        s += '<td width="100px">' + sr.approvers + '</td>';
        return s;
    }
    
}