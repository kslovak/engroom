@isTest
private class NewsForceBannerController_T {
/****************************************************************************
 * Test Class NewsForceBannerController_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - NewsForceBannerController Apex Class
 ****************************************************************************/
 
    //Test Data
    public static NewsForce_Item__c item1;
    
    //Test Settings
    
    
    private static testMethod void unitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
        item1 = new NewsForce_Item__c();
        item1.Active__c = true;
        item1.Content__c = 'Content';
        item1.Display_Date__c = System.Today();
        item1.Start_Date__c = System.Today();
        item1.End_Date__c = System.Today();
        item1.Force_Display__c = false;
        item1.Priority__c = 'Low';
        item1.Title__c = 'Title 1';
        
        insert item1;
    }
  
    private static void executeTest01() {
        // Execute Tests
        NewsForceBannerController c = new NewsForceBannerController();    

		Boolean bool;
		String id;
		Integer i;

		bool = c.hasNoItems;
		bool = c.hasItems;
		i = c.totalItems;

		id = c.itemIdToForceDisplay;
		
		item1.Force_Display__c = true;
		update item1;
		c.newsItems.refresh();
		id = c.itemIdToForceDisplay;
		
		id = c.currentItemId;

		c.currentItemId = item1.Id;

		c.userHasViewedItem();
		c.userHasDismissedItem();
    }
}