public class TSR_Marketing_Segment_Config {
	static String COMMA_SEPARATOR = ',';
    
	public static Map<String,List<String>> retrieveValuesMap(String type) {
      	Map<String,List<String>> results = new Map<String,List<String>>();
        Map<String,TSR_Marketing_Segment_Config__c> settings = TSR_Marketing_Segment_Config__c.getAll();
        
        for (String key : settings.keySet()) {
            TSR_Marketing_Segment_Config__c item = settings.get(key);
            
            if (item.Type__c == type) {
                String availableValues = item.Available_Values__c;
                List<String> availableValuesList = availableValues.split(COMMA_SEPARATOR);
                List<String> cleanAvailableValuesList = new List<String>();
                for (String availableValue : availableValuesList) {availableValue = availableValue.trim();cleanAvailableValuesList.add(availableValue);}
                results.put(key,cleanAvailableValuesList);
            }
        }
        
        return results;
	}

	public static Map<String,List<SelectOption>> retrieveSelectOptionMap(String type) {
        Map<String,List<String>> valuesMap = retrieveValuesMap(type);
		
        Map<String,List<SelectOption>> results = new Map<String,List<SelectOption>>();
        
        for (String key : valuesMap.keySet()) {
            List<SelectOption> selectOptions = new List<SelectOption>();
            selectOptions.add(new SelectOption('','--None--'));
            List<String> values = valuesMap.get(key);
            for (String value : values) {
				selectOptions.add(new SelectOption(value,value));			                
            }
            results.put(key,selectOptions);
        }
        
        return results;
	}
    
    public static List<String> retrieveKeysForType(String type) {
      	List<String> results = new List<String>();
        
        Map<String, TSR_Marketing_Segment_Config__c> settings = TSR_Marketing_Segment_Config__c.getAll();
        
        for (String key : settings.keySet()) {
            TSR_Marketing_Segment_Config__c item = settings.get(key);
            
            if (item.Type__c == type) {
                results.add(key);
            }
        }
        
        return results;
    }
}