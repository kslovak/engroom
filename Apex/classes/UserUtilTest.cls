@isTest
class UserUtilTest {


    @isTest
    static void userHasAccessTest() {
        List<String> roles = new List<String>{''};
        
        System.assertEquals( false, UserUtil.userHasAccess( Userinfo.getUserId(), roles ) );
        
    }
    
    @isTest
    static void groupListContainsTest() {
        List<Group> testGroupList = new List<Group>{
            new Group(Name='testGroup1',DeveloperName='tg1'),
            new Group(Name='testGroup2',DeveloperName='tg2'),
            new Group(Name='testGroup3',DeveloperName='tg3')
        };
        System.assertEquals( false, UserUtil.groupListContains('notListed',testGroupList) );
        System.assertEquals( true, UserUtil.groupListContains('testGroup1',testGroupList) );
        System.assertEquals( true, UserUtil.groupListContains('testGroup2',testGroupList) );
        System.assertEquals( true, UserUtil.groupListContains('testGroup3',testGroupList) );
    }
}