global with sharing class Chatter_Feed_Batchable implements Database.Batchable<SObject> {

    public static final Integer BATCH_SIZE = 200;

    public static String userEmail = User_Functions.getLoggedInUserEmail();
    
    global final String objName; global final Boolean delFeed;
    
    global Chatter_Feed_Batchable(String obj, Boolean del) {
    	this.objName = String.isBlank(obj) ? '' : obj;
    	this.delFeed = (del == null) ? false : del;
    }
    
    global Database.Querylocator start(Database.BatchableContext bc) {
    	String q = 'select Id, ParentId from FeedItem';
    	if (!String.isBlank(objName)) {q = 'select Id from ' + objName;}
        return Database.getQueryLocator(q);
    }

    global void execute(Database.BatchableContext bc, List<SObject> alist){
    	if (String.isBlank(objName)) {processAllFeed(alist);} else {processObjFeed(alist);}
    }
    
    private void processAllFeed(List<SObject> alist) {
    	List<FeedItem> feeds = (List<FeedItem>)alist, 
    	               dlist = new List<FeedItem>();
		Set<String> objectNames = new Set<String>(), 
		            keyPrefixes = new Set<String>(); String pid, kp;
		Schema_Functions.getFeedEnabled(objectNames, keyPrefixes);
    	for (FeedItem f : feeds) {
    		if (f.ParentId != null) {
	    		pid = f.ParentId; kp = pid.subString(0,3);
	    		if (!keyPrefixes.contains(kp)) {dlist.add(f);}
    		}
    	}
    	if (!dlist.isEmpty()) {
    		if (delFeed) {delete dlist;} else {sendEmail(dlist);}
    	}
    }
    
    private void processObjFeed(List<SObject> alist) {
    	Set<Id> aset = new Set<Id>(); for (SObject a : alist) {aset.add(a.Id);}
    	for (List<FeedItem> flist : [select Id from FeedItem where ParentId in :aset]) {
    		if (delFeed) {delete flist;} else {sendEmail(flist);}
    	} 
    }
    
    global void finish(Database.BatchableContext bc){
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                 TotalJobItems, CreatedBy.Email
                            from AsyncApexJob where Id =:bc.getJobId()];
        String s = 'Apex Batch Job - Chatter Feed - ' + objName + ' - ' +  
                   a.Status + ' - ' + a.TotalJobItems + ' batches - ' +
                   a.NumberOfErrors + ' failures';
        String b = 'Job Id : ' + a.Id + ' : ' + s;
        sendEmail(s, b);
    }
    
    private void sendEmail(List<FeedItem> flist) {
    	String b = '', s = 'Apex Batch Job - Chatter Feed';
    	for (FeedItem f : flist) {
    		b += f.Id + ',' + f.ParentId + '\n';
    	}
    	sendEmail(s, b);
    }
    
    private void sendEmail(String s, String b) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {userEmail});
        mail.setReplyTo(userEmail);
        mail.setSenderDisplayName('SysAdmin');
        mail.setSubject(s);
        mail.setPlainTextBody(b);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    Webservice static String submitBatchJob(String objName, Boolean delFeed) {
        Chatter_Feed_Batchable b = new Chatter_Feed_Batchable(objName, delFeed);
        String msg;
        try {msg = Database.executeBatch(b, BATCH_SIZE);}
        catch(Exception e) {msg = e.getMessage(); System.debug(e);}
        return msg;
    }

}