@IsTest
private class VILocationEditControllerTest {
    static testmethod void newLocation() {
        Account distributor = new Account();
        distributor.Name = 'Test Distributor';
        distributor.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_DISTRIBUTOR;
        insert distributor;

        Account facility = new Account(), location;
        facility.VI_Parent_Account__c = distributor.Id;
        facility.Name = 'Test Facility';
        facility.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_FACILITY;
        insert facility;

        Contact contact = new Contact();
        contact.AccountId = facility.Id;
        contact.LastName = 'Test';
        insert contact;

        Profile p;
        try {
            p = [SELECT Id FROM Profile WHERE Name = :VIUtils.CUSTOMER_PORTAL_NAME];
        } catch(Exception e) {return;}
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing',
                LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                ProfileId = p.Id, ContactId = contact.Id, Ashland_Employee_Number__c = 'zzzTest4',
                TimeZoneSidKey = 'America/Los_Angeles', Username = 'standarduser4@testorg.com');

        System.runAs(u) {
            Map<String, String> params = System.currentPageReference().getParameters();
            params.put(VILocationEditController.PARAM_FACILITY_ID, facility.Id);

            VILocationEditController controller = new VILocationEditController();
            controller.init(); location = controller.getLocation();

            controller.save();

            controller.getName().setValue('Test Location');
            controller.getBillingStreet().setValue('123 Some St');
            controller.getBillingCity().setValue('Dallas');
            controller.getBillingState().setValue('TX');
            controller.getBillingPostalCode().setValue('75243');
            controller.getPhone().setValue('8005551234');
            controller.getFax().setValue('8005551234');
            controller.getWebsite().setValue('http://www.google.com');
            controller.getActive().setValue('Yes');
            //controller.primaryIndustry.setValue('TestIndustry');
            location.Primary_Industry__c = 'TestIndustry';

            controller.save();
        }
    }

    static testmethod void editLocation() {
        Account distributor = new Account();
        distributor.Name = 'Test Distributor';
        distributor.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_DISTRIBUTOR;
        insert distributor;

        Account facility = new Account();
        facility.VI_Parent_Account__c = distributor.Id;
        facility.Name = 'Test Facility';
        facility.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_FACILITY;
        insert facility;

        Account location = new Account();
        location.VI_Parent_Account__c = facility.Id;
        location.Name = 'Test Location';
        location.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_LOCATION;
        location.SAP_Street__c = facility.BillingStreet = '123 Some St';
        location.SAP_City__c = facility.BillingCity = 'Dallas';
        location.SAP_State__c = facility.BillingState = 'TX';
        location.SAP_Zip_Code__c = facility.BillingPostalCode = '75243';
        location.Phone = '8005551234';
        location.Fax = '8005551234';
        location.Website = 'http://www.google.com';
        location.Inactive_Account__c = false;
        location.Primary_Industry__c = 'TestIndustry';
        insert location;

        List<Group> groups = [SELECT Id
                FROM Group
                WHERE RelatedId = :UserInfo.getUserRoleId()
                AND Type = 'RoleAndSubordinates'];
        if (groups.isEmpty()) {
                groups = [SELECT Id
                        FROM Group
                        WHERE RelatedId = :UserInfo.getUserRoleId()
                        AND Type = 'Role'];
        }
        if (groups.isEmpty()) {return;}

        insert new AccountShare(AccountId = location.Id,
                UserOrGroupId = groups.get(0).Id,
                AccountAccessLevel = 'Edit',
                OpportunityAccessLevel = 'None',
                CaseAccessLevel = 'None');

        Contact contact = new Contact();
        contact.AccountId = facility.Id;
        contact.LastName = 'Test';
        insert contact;

        /*
        Profile p;
        try {
            p = [SELECT Id FROM Profile WHERE Name = :VIUtils.CUSTOMER_PORTAL_NAME];
        } catch(Exception e) {return;}
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing',
                LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                ProfileId = p.Id, ContactId = contact.Id, Ashland_Employee_Number__c = 'zzzTest5',
                TimeZoneSidKey = 'America/Los_Angeles', Username = 'standarduser5@testorg.com',
                ManagerId = UserInfo.getUserId());
        */
        //System.runAs(u) {
            Map<String, String> params = System.currentPageReference().getParameters();
            params.put(VILocationEditController.PARAM_FACILITY_ID, facility.Id);
            params.put(VILocationEditController.PARAM_LOCATION_ID, location.Id);
            params.put(VILocationEditController.PARAM_RETURN_URL, '/home/home.jsp');

            VILocationEditController controller = new VILocationEditController();
            controller.init();

            controller.save();
            controller.cancel();
        //}
    }

    static testmethod void viewLocation() {
        Account distributor = new Account();
        distributor.Name = 'Test Distributor';
        distributor.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_DISTRIBUTOR;
        insert distributor;

        Account facility = new Account();
        facility.VI_Parent_Account__c = distributor.Id;
        facility.Name = 'Test Facility';
        facility.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_FACILITY;
        insert facility;

        Account location = new Account();
        location.VI_Parent_Account__c = facility.Id;
        location.Name = 'Test Location';
        location.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_LOCATION;
        insert location;

        Contact contact = new Contact();
        contact.AccountId = location.Id;
        contact.LastName = 'Test';
        insert contact;

        /*
        Profile p;
        try {
            p = [SELECT Id FROM Profile WHERE Name = :VIUtils.CUSTOMER_PORTAL_NAME];
        } catch(Exception e) {return;}
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing',
                LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                ProfileId = p.Id, ContactId = contact.Id, Ashland_Employee_Number__c = 'zzzTest6',
                TimeZoneSidKey = 'America/Los_Angeles', Username = 'standarduser6@testorg.com',
                ManagerId = UserInfo.getUserId());
        */
        //System.runAs(u) {
            Map<String, String> params = System.currentPageReference().getParameters();
            params.put(VILocationEditController.PARAM_FACILITY_ID, facility.Id);
            params.put(VILocationEditController.PARAM_LOCATION_ID, location.Id);

            VILocationEditController controller = new VILocationEditController();
            controller.init();
            controller.getAviLocatorName();
			controller.getFacilityId();
			controller.getFacilityName();
			controller.getLocationId();
			controller.getLocation();
			controller.getReadOnly();
			controller.getStoreHourOptions();
			
        //}
    }
}