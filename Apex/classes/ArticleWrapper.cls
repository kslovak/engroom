global class ArticleWrapper {
    private Community_Article__c article;
    private String ctaLink;
    
    global ArticleWrapper(Community_Article__c art) {
        this.article = art;
        this.ctaLink = this.lookupCtaLink();
    }
    
    global Community_Article__c getArticle() {
        return this.article;
    }
    
    global String getCtaLink() {
        return this.ctaLink;
    }
    
    private String lookupCtaLink() {
        String result = '';
        if(this.article.Content_Link_Id__c != null && this.article.Content_Link_Id__c != '') {
            try {
                List<ContentVersion> content = [select Id, ContentDocumentId from ContentVersion where ContentDocumentId = :this.article.Content_Link_Id__c];
                if(!content.isEmpty()) {
                    result = (String)content.get(0).Id;
                    result = '/sfc/servlet.shepherd/version/download/' + result;
                }
            } catch (DMLException e) {
            
            }
        } else {
            result = this.article.Button_Path__c;
        }
        return result;
    }
}