@isTest
private class CustomLookupController_T {
/****************************************************************************
 * Test Class CustomLookupController_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - CustomLookupController Apex Class
 ****************************************************************************/
 
    //Test Data
	public static CustomLookupController c;
    
    
    //Test Settings
    
    
    private static testMethod void unitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
    }
  
    private static void executeTest01() {
        // Execute Tests
		c = new CustomLookupController();

		c.objectName = '';
		c.columnsToShow = '';
		c.allowEmptySearch = '';
		c.additionalSearchFields = '';
		c.runFirst = '';
		c.orderBy = '';
		c.recordLimit = '';
		c.minSearchBoxLength = '';
		c.linkFieldName = '';

		c.filter1 = '';
		c.filter1AsEntered = '';
		c.filter1Label = '';
		c.filter1OffByDefault = '';
		c.filter1Optional = '';
		c.filter1Disabled = '';
		c.filter1ColumnsToAppendIfRemoved = '';
		c.filter1ForceSearchBoxIfRemoved = '';
		c.filter1AdditionalSearchFieldsIfRemoved = '';
		c.filter1OrderByPrefixIfRemoved = '';
		c.filter1OrderBySuffixIfRemoved = '';
		c.filter1MinSearchBoxLengthIfRemoved = '';

		c.filter2 = '';
		c.filter2AsEntered = '';
		c.filter2Label = '';
		c.filter2OffByDefault = '';
		c.filter2Optional = '';
		c.filter2Disabled = '';
		c.filter2ColumnsToAppendIfRemoved = '';
		c.filter2ForceSearchBoxIfRemoved = '';
		c.filter2AdditionalSearchFieldsIfRemoved = '';
		c.filter2OrderByPrefixIfRemoved = '';
		c.filter2OrderBySuffixIfRemoved = '';
		c.filter2MinSearchBoxLengthIfRemoved = '';

		c.filter3 = '';
		c.filter3AsEntered = '';
		c.filter3Label = '';
		c.filter3OffByDefault = '';
		c.filter3Optional = '';
		c.filter3Disabled = '';
		c.filter3ColumnsToAppendIfRemoved = '';
		c.filter3ForceSearchBoxIfRemoved = '';
		c.filter3AdditionalSearchFieldsIfRemoved = '';
		c.filter3OrderByPrefixIfRemoved = '';
		c.filter3OrderBySuffixIfRemoved = '';
		c.filter3MinSearchBoxLengthIfRemoved = '';

		c.searchPanelVisible = true;

		c.UpdateSettings();    
    }
}