@isTest
private class VI_Redirect_Controller_Test {

	static void setup() {
    	VI_Portal_Settings__c vps = new VI_Portal_Settings__c();
    	vps.Adwizard_Login_URL__c = 'http://valvolineadwizard.saepio.com/ams/urlauthlogin.do';
    	vps.Adwizard_User_Group__c = 'Users/Installers';
    	vps.PFC_CryptoKey__c = 'abcdef1234567890';
    	vps.Learnshare_CryptoKey__c = 'abcdef1234567890';
    	vps.Learnshare_URL__c = 'http://lms3.learnshare.com/lsapientry.aspx?cid=50&Z=';
    	vps.QuickSquare_IV__c = 'Jzwnbb8xWcOw8eYB7gfjOg==';
    	vps.QuickSquare_Key__c = 'toi4dPoEewm7iIdz7yWb9YBPqEGWPJZz9Bl3SzlX0iY=';
    	vps.QuickSquare_URL__c = 'https://valvolinemedia.com/login?ssoString=';
    	insert vps;
	}
	
    static testMethod void test01() {
    	setup(); Test.startTest();
    	VI_Redirect_Controller c = new VI_Redirect_Controller();
    	c.gotoLearnshare();
		c.gotoPfc();
		c.gotoValvolineMedia();
    }
}