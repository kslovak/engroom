global class CSV_Data_Batch implements Database.Batchable<String>, Database.Stateful {
    
    private static final String USER_EMAIL = User_Functions.getLoggedInUserEmail();
    private static final String ADMN_EMAIL = User_Functions.getSysAdminEmail();

    private final List<String> csvData;
    private final String csvConfigId;
    
    global CSV_Data_Batch(List<String> slist, String cnfgId) {
        csvData = slist; csvConfigId = cnfgId;
    }
    
    global Iterable<String> start(Database.BatchableContext bc) {
        return csvData;
    }

    global void execute(Database.BatchableContext bc, List<String> slist){
        CSV_Data_Methods.processCsvData(slist, csvConfigId);
    }

    global void finish(Database.BatchableContext bc){
        sendEmail(bc);
    }
    
    private void debug(String s) {System.debug(LoggingLevel.INFO, s);}
    
    private void sendEmail(Database.BatchableContext bc) {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                 TotalJobItems, CreatedBy.Email
                            from AsyncApexJob where Id =:bc.getJobId()];
        String s = 'Apex Batch Job - CSV Data Batch ' + 
                   ' - ' + a.Status + ' - ' + a.TotalJobItems + ' batches' +
                   ' - ' + a.NumberOfErrors + ' failures';
        String b = 'Job Id : ' + a.Id + ' processed ' + a.TotalJobItems +
                   ' batches with ' + a.NumberOfErrors + ' failures.' +
                   ' CSV Config Id : ' + csvConfigId;
        Messaging.SingleEmailMessage m = new Messaging.SingleEmailMessage();
        m.setToAddresses(new String[] {USER_EMAIL});
        if (USER_EMAIL != ADMN_EMAIL) {m.setBccAddresses(new String[]{ADMN_EMAIL});}
        m.setReplyTo(USER_EMAIL); m.setSenderDisplayName('SysAdmin');
        m.setSubject(s); m.setPlainTextBody(b);
        if (Test.isRunningTest()) {return;}
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {m});
    }
    
}