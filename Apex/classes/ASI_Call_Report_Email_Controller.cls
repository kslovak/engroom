public class ASI_Call_Report_Email_Controller {

    public String callReportId {get; set;}

    private static final String CHECKMARK = '&#x2714;';
    private static final String EMPTYBOX = '&#x2610;';

    private void debug(String s) {
        System.debug(LoggingLevel.INFO, 'ASI_Call_Report_Email_Controller : ' + s);
    }
    
    public ASI_Call_Report__c callReport {
        private set; get {
	        if (callReport == null || callReport.Id == null) {
	            callReport = ASI_Call_Report_Methods.getCallReport(callReportId);
	        } return callReport;
        } 
    }
    
    public Account acrAccount {
        private set; get {
            if (acrAccount == null || acrAccount.Id == null) {
                acrAccount = ASI_Call_Report_Methods.getAccount(callReport.Account__c);
            } return acrAccount;
        }
    }
    
    public Account acrDistributor {
        private set; get {
            if (acrDistributor == null || acrDistributor.Id == null) {
                acrDistributor = ASI_Call_Report_Methods.getAccount(callReport.Distributor__c);
            } return acrDistributor;
        }
    }
    
    public Account acrEndUseCust {
        private set; get {
            if (acrEndUseCust == null || acrEndUseCust.Id == null) {
                acrEndUseCust = ASI_Call_Report_Methods.getAccount(callReport.End_Use_Customer__c);
            } return acrEndUseCust;
        }
    }
    
    public User ashlandRep {
        private set; get {
            if (ashlandRep == null || ashlandRep.Id == null) {
                ashlandRep = ASI_Call_Report_Methods.getUser(callReport.Ashland_Rep__c);
            } return ashlandRep;
        }
    }

    public Case acrCase {
        private set; get {
            if (acrCase == null || acrCase.Id == null) {
                acrCase = ASI_Call_Report_Methods.getCase(callReport.Case__c);
            } return acrCase;
        }
    }

    public Competitor__c acrCompetitor {
        private set; get {
            if (acrCompetitor == null || acrCompetitor.Id == null) {
                acrCompetitor = ASI_Call_Report_Methods.getCompetitor(callReport.Competitor__c);
            } return acrCompetitor;
        }
    }

    public Opportunity acrOpportunity {
        private set; get {
            if (acrOpportunity == null || acrOpportunity.Id == null) {
                acrOpportunity = ASI_Call_Report_Methods.getOpportunity(callReport.Opportunity__c);
            } return acrOpportunity;
        }
    }

    public Risk__c acrRisk {
        private set; get {
            if (acrRisk == null || acrRisk.Id == null) {
                acrRisk = ASI_Call_Report_Methods.getRisk(callReport.Risk__c);
            } return acrRisk;
        }
    }

    public Sample_Request__c acrSampleRequest {
        private set; get {
            if (acrSampleRequest == null || acrSampleRequest.Id == null) {
                acrSampleRequest = ASI_Call_Report_Methods.getSampleRequest(callReport.Sample_Request__c);
            } return acrSampleRequest;
        }
    }

    public List<ASI_Call_Report_Attendee> attendees1 {
        private set; get {
            attendees1 = ASI_Call_Report_Methods.getAttendees(callReportId);
            return attendees1;
        }
    }
    public Boolean attendees1Empty {get {return attendees1.isEmpty();}}

    public List<ASI_Call_Report_Contact> contacts1 {
        private set; get {
            contacts1 = ASI_Call_Report_Methods.getContacts(callReportId);
            return contacts1;
        }
    }

    public Boolean contacts1Empty {get {return contacts1.isEmpty();}}
    
    public List<ASI_Call_Report_Distribution> distributions1 {
        private set; get {
            distributions1 = ASI_Call_Report_Methods.getEmailReceivers(callReportId);
            return distributions1;
        }
    }

    public Boolean distributions1Empty {get {return distributions1.isEmpty();}}
    
    public List<ASI_NPI_Record> npis1 {
        private set; get {
            npis1 = ASI_Call_Report_Methods.getNpis(callReport);
            return npis1;
        }
    }

    public List<ASI_Innova_Record> innovas1 {
        private set; get {
            innovas1 = ASI_Call_Report_Methods.getInnovas(callReport);
            return innovas1;
        }
    }
    
    public String confidential {
        private set; get {
            confidential = callReport != null && callReport.Confidential__c ? CHECKMARK : EMPTYBOX;
            return confidential;
        }
    }
    
    public String publish {
        private set; get {
            publish = callReport != null && callReport.Publish__c ? CHECKMARK : EMPTYBOX;
            return publish;
        }
    }
    
    public String russiaCis {
        private set; get {
            russiaCis = callReport != null && callReport.Russia_CIS__c ? CHECKMARK : EMPTYBOX;
            return russiaCis;
        }
    }
}