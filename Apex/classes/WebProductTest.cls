@isTest
class WebProductTest {

    @isTest
    static void getProductTest() {
        Product__c testProduct = new Product__c();
        WebProduct testClass = new WebProduct(testProduct);
        
        System.assertEquals( testProduct, testClass.getProduct() );
    }
    
    @isTest
    static void assertAliasMethods() {
        Product__c testProduct = new Product__c();
        WebProduct testClass = new WebProduct(testProduct);
        
        System.assertEquals( testClass.getProduct(), testClass.getProd() );
    }
    
    @isTest
    static void getBenefitsListTest() {
        Product__c testProduct = new Product__c();
        WebProduct testClass = new WebProduct(testProduct);
        
        if( testClass.getProduct().Product_Benefits__c == null || testClass.getProduct().Product_Benefits__c == '') {
            System.assertEquals( null, testClass.getBenefitsList() );
        } else {
            System.assertNotEquals( null, testClass.getBenefitsList() );
        }
    }
    
    @isTest
    static void getBenefitsPreviewListTest() {
        Product__c testProduct = new Product__c();
        WebProduct testClass = new WebProduct(testProduct);
        
        if( testClass.getProduct().Benefits_Preview__c == null || testClass.getProduct().Benefits_Preview__c == '') {
            System.assertEquals( null, testClass.getBenefitsPreviewList() );
        } else {
            System.assertNotEquals( null, testClass.getBenefitsPreviewList() );
        }
    }
    
    @isTest
    static void getMarketsTest() {
        Product__c testProduct = new Product__c();
        WebProduct testClass = new WebProduct(testProduct);
        
        if( testClass.getProduct().Market_Applications__c == null || testClass.getProduct().Market_Applications__c == '') {
            System.assertEquals( null, testClass.getMarkets() );
        }
        
        Market__c newMarket = new Market__c(Name='newMarketName',Alias__c='newMarketAlias');
        insert newMarket;
        testProduct.Market_Applications__c = 'test;newMarketName;test';
        testClass = new WebProduct(testProduct);
        System.assertEquals( newMarket.Id, testClass.getMarkets()[0].Id );
    }
}