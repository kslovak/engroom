@isTest
private class VI_Locator_Data_Test {

    static Account acc;
    
    static void debug(String s) {
        System.debug(LoggingLevel.INFO, 'VI_Locator_Data_Test : ' + s);
    }
    
    static void setup() {
        acc = new Account(Name='Test Account', AccountNumber='TestAcct');
        insert acc;
        VI_Promotion__c p = new VI_Promotion__c();
        p.Program_Code__c = 'PC1001';
        p.Channel__c = 'Test Channel';
        p.Program_Name__c = 'Test Promotion';
        p.Promotion_Appear_on_Locator__c = 'Yes';
        p.End_Date__c = Date.today() + 30;
        insert p;
        VI_Enrollment__c e = new VI_Enrollment__c();
        e.Facility__c = acc.Id;
        e.Promotion__c = p.Id;
        insert e;
        VI_Enrollment_Location__c el = new VI_Enrollment_Location__c();
        el.Approval_Status__c = VIUtils.ENROLLMENT_STATUS_APPROVED;
        el.Enrollment__c = e.Id;
        el.Facility__c = acc.Id;
        el.Location__c = acc.Id;
        el.Promotion__c = p.Id;
        insert el;
    }
    
    static testMethod void test1() {
        setup(); Test.startTest();
        VI_Locator_Data_Methods.getAccountQL();
        List<Account> accs = new List<Account>{acc};
        VI_Locator_Data_Methods.upsertLocatorData(accs); 
        Database.Querylocator ql = VI_Locator_Data_Methods.getLocatorDataQL();
        List<VI_Locator_Data__c> dlist = Database.query(ql.getQuery());
        VI_Locator_Data_Methods.deleteLocatorData(dlist);
    }
}