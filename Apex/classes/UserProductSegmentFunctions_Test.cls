@isTest
private class UserProductSegmentFunctions_Test {

    static String langcode = '!';
    static Id profileId = [Select Id from Profile where Name = 'Standard User'].Id;
    static String usrname1 = 'testuser1@testuser.testuser';
    static String usrname2 = 'testuser2@testuser.testuser';
    static String empnum1 = 'empnum1';
    static String empnum2 = 'empnum2';
    
    static User usr;
    static User_Product_Segment__c userProductSegment;
    
    static testMethod void test01() {
    	Test.startTest();
        createTestData();
        List<User_Product_Segment__c> alist;
        alist = UserProductSegmentFunctions.getUserProductSegments(usr.Id);
        System.debug(alist);
        createUser(usrname2, empnum2);
        alist = UserProductSegmentFunctions.getUserProductSegments(usr.Id);
        System.debug(alist);
        Test.stopTest();
    }
    
    static void createTestData() {
        createUser(usrname1, empnum1);
        createUserProductSegment();
    }

    static void createUser(String usrname, String empnum) {
        usr = new User();
        usr.Alias = 'tstusr';
        usr.Email = 'testuser@testuser.testuser';
        usr.EmailEncodingKey = 'ISO-8859-1';
        usr.LanguageLocaleKey = 'en_US';
        usr.LastName = 'testusr';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/New_York';
        usr.ProfileId = profileId;
        usr.Username = usrname;
        usr.Ashland_Employee_Number__c = empnum;
        insert usr;
    }   
     
    static void createUserProductSegment() {
        userProductSegment =  new User_Product_Segment__c();
        userProductSegment.User__c = usr.Id;
        userProductSegment.Product_Segment_Code__c = '016';
        insert userProductSegment;
        System.debug(userProductSegment);
    }
    
}