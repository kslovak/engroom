global class PricingConditionListBatchable implements Database.Batchable<sObject>, Schedulable {

    private static final String SYSADMIN_EMAIL = User_Functions.getSysAdminEmail();

    private static final String MULTI_LEVEL_INDUSTRY = SAP_LISD_Functions.MULTI_LEVEL_INDUSTRY;
    private static final Set<String> UPD_SELLER_STATS = PricingConditionFunctions.getStatsToUpdateSellers();

    public static final String EXTEND_PCNS    = 'Extend';
    public static final String UPDATE_SELLERS = 'UPDATE_SELLERS';

	private final String INVALID_ID = '000000000000000';
	private final String DEFAULT_QRY = 'select Id from Pricing_Condition__c where Id = :INVALID_ID';
	
    final static Map<String, Integer> BATCH_SIZES = new Map<String, Integer>{
        EXTEND_PCNS    => 10,
        UPDATE_SELLERS => 10 
    };
    
    public Date extendToDate      {get; set;}
    public Boolean extendAtSoldTo {get; set;}
    public String accountId       {get; set;}
    public String userComments    {get; set;}
    public Set<String> pcIds      {get; set;}
    
    String procStep, queryStr; Integer maxLimit;

    private void debug(String s) {System.debug(LoggingLevel.INFO, 'PCListBatchable : ' + s);}

    global PricingConditionListBatchable(String ps) {this(ps, null);}
    
    global PricingConditionListBatchable(String ps, Integer ml) {
    	procStep = ps; queryStr = DEFAULT_QRY; maxLimit = ml != null ? ml : 0;
        if (UPDATE_SELLERS.equals(procStep)) {
            queryStr = getUpdateSellersQuery();
    	}
    }
    
    global Database.Querylocator start(Database.BatchableContext bc) {
    	Database.Querylocator ql;
    	if (EXTEND_PCNS.equals(procStep)) {
            ql = PricingConditionListFunctions.getPricingConditionQueryLocator(pcIds);
    	} else 
        if (UPDATE_SELLERS.equals(procStep)) {
            ql = Database.getQueryLocator(queryStr);
    	}
        return ql;
    }

    private String getUpdateSellersQuery() {
        String q = PricingConditionFunctions.getQueryToUpdateSellers();
        q += ' where Record_Status__c in :UPD_SELLER_STATS';
        q += '   and Account__r.Inactive_Account__c = false';
        //q += '   and Account__r.Multi_Level_Industry__c = :MULTI_LEVEL_INDUSTRY';
        if (maxLimit != null && maxLimit > 0) {q += ' limit :maxLimit';}
        debug('UpdateSellersQuery : ' + q); return q;
    }
    
    global void execute(SchedulableContext sc) {
        Id processId; Integer batchSize = 200;
        if (BATCH_SIZES.containsKey(procStep)) {batchSize = BATCH_SIZES.get(procStep);}
        try {processId = Database.executeBatch(this, batchSize);}
        catch(Exception e) {debug(e.getMessage());}
    }

    global void execute(Database.BatchableContext bc, List<SObject> alist){
        List<Pricing_Condition__c> pcs = (List<Pricing_Condition__c>)alist;
        if (EXTEND_PCNS.equals(procStep)) {
            PricingConditionListFunctions.extendAndSubmitForApproval(
                accountId, extendAtSoldTo, extendToDate, userComments, pcs);
        } else
        if (UPDATE_SELLERS.equals(procStep)) {
            PricingConditionFunctions.updateLisdSellers(pcs);
        } 
    }
    
    global void finish(Database.BatchableContext bc){
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                 TotalJobItems, CreatedBy.Email
                            from AsyncApexJob where Id =:bc.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[]{a.CreatedBy.Email});
        if (SYSADMIN_EMAIL != null && SYSADMIN_EMAIL != a.CreatedBy.Email) {
        	List<String> bccList = new List<String>{SYSADMIN_EMAIL};
        	mail.setBccAddresses(bccList);
        }
        mail.setReplyTo(a.CreatedBy.Email);
        mail.setSenderDisplayName('PricingConditions ' + procStep + ' Batch Job');
        mail.setSubject('Apex Batch Job - PricingConditions ' + procStep + ' - ' + 
                         a.Status+' - '+a.TotalJobItems+' batches - ' +
                         a.NumberOfErrors + ' failures');
        mail.setPlainTextBody('Job Id : '+a.Id+' processed ' + a.TotalJobItems +
                              ' batches with '+ a.NumberOfErrors + ' failures');
                              /* + '\n\nQuery String : ' + queryStr + '\n\n' +
                              'UPD_SELLER_STATS : ' + UPD_SELLER_STATS + '\n\n' +
                              'maxLimit : ' + maxLimit); */
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    Webservice static String submitBatchJob2(String stepName, Integer maxLimit) {
        if (String.isBlank(stepName)) {return '';}
        PricingConditionListBatchable b = new PricingConditionListBatchable(stepName, maxLimit);
        String msg; Integer batchSize = 200;
        if (BATCH_SIZES.containsKey(stepName)) {batchSize = BATCH_SIZES.get(stepName);}
        if (!Test.isRunningTest()) {
            try {msg = Database.executeBatch(b, batchSize);}
            catch(Exception e) {msg = e.getMessage(); b.debug(msg);}
        }
        return msg;
    }

    Webservice static String submitBatchJob(String stepName) {
        return submitBatchJob2(stepName, null);
    }

    Webservice static String updateSellers(Integer maxLimit) {
    	return submitBatchJob2(UPDATE_SELLERS, maxLimit);
    }
    
}