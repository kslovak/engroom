public with sharing class NewsForce_Items {

	public NewsForce_Items() {
		refresh();
	}
 
 	public void refresh() {
		List<NewsForce_Item__c> newsItemsForUser = retrieveNewsItems(UserInfo.getUserId());
		
		this.recs = new List<NewsForce_Item>();
		for (NewsForce_Item__c rec : newsItemsForUser) {
			this.recs.add(new NewsForce_Item(rec));
		}
 	}
 
	public List<NewsForce_Item> recs {get;private set;}

	public void userHasViewedItem(Id itemId) {
		NewsForce_Item item = locateItem(itemId);
		item.userHasViewedItem();
	}

	public void userHasDismissedItem(Id itemId) {
		NewsForce_Item item = locateItem(itemId);
		item.userHasDismissedItem();
	}

	public Id itemIdToForceDisplay {
		get {
			Id result;
			for (NewsForce_Item item : recs) {
				if (item.forceDisplayOfItem) {
					result = item.rec.id;
					break;
				}
			}
			return result;
		}
	}

	private List<NewsForce_Item__c> retrieveNewsItems(Id userId) {
		List<NewsForce_Item__c> items;
		try {
			items = [ 
				SELECT Id, 
	                   Name, 
	                   Title__c, 
	                   Display_Date__c,
	                   Content__c, 
	                   Custom_Content_Style__c,
	                   Start_Date__c, 
	                   End_Date__c, 
	                   Priority__c, 
	                   Force_Display__c,
	                   Active__c,
	                   (
	                   SELECT NewsForce_Item__c, Viewed__c, Dismissed__c, View_Count__c, Last_Viewed_On__c, Dismissed_On__c 
	                     FROM NewsForce_Items_Usage__r
	                    WHERE User__c = :userId
	                   LIMIT 1
	                   )  
	              FROM NewsForce_Item__c 
	             WHERE Active__c = True
	               AND (Start_Date__c = NULL OR Start_Date__c <= TODAY)
	               AND (End_Date__c = NULL OR End_Date__c >= TODAY)
	          ORDER BY Priority__c DESC, Display_Date__c DESC
	        ];
		}
		catch (Exception e) {
			items = new List<NewsForce_Item__c>();
		}
        return items;
	}

	private NewsForce_Item locateItem(Id itemId) {
		NewsForce_Item locatedRec;
		for (NewsForce_Item item : recs) {
			if (item.rec.Id == itemId) {
				locatedRec = item;
				break;
			} 
		}
		return locatedRec;
	}
}