@isTest
private class TSR_Marketing_Segment_Config_T {
/****************************************************************************
 * Test Class TSR_Marketing_Segment_Config_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - TSR_Marketing_Segment_Config Apex Class
 ****************************************************************************/
 
    //Test Data
    
    
    //Test Settings
    
    
    private static testMethod void unitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
		TSR_Marketing_Segment_Config__c settings1;
	    settings1 = new TSR_Marketing_Segment_Config__c(Name='01');
		settings1.Description__c = 'Desc';
		settings1.Type__c = 'Type';
		settings1.Available_Values__c = 'value1,value2, value 3, value 4 , value5';
		insert settings1;    

        TSR_Marketing_Segment_Config__c settings2;
	    settings2 = new TSR_Marketing_Segment_Config__c(Name='02');
		settings2.Description__c = 'Desc2';
		settings2.Type__c = 'Type';
		settings2.Available_Values__c = 'value6,value7, value 8, value 9 , value10';
		insert settings2;    

        TSR_Marketing_Segment_Config__c settings3;
	    settings3 = new TSR_Marketing_Segment_Config__c(Name='03');
		settings3.Description__c = 'Desc3';
		settings3.Type__c = 'Type2';
		settings3.Available_Values__c = 'value6,value7, value 8, value 9 , value10';
		insert settings3;    
    }
  
    private static void executeTest01() {
        // Execute Tests
		Map<String,List<String>> valuesMap = TSR_Marketing_Segment_Config.retrieveValuesMap('Type');
	    List<String> keys = TSR_Marketing_Segment_Config.retrieveKeysForType('Type');
		Map<String,List<SelectOption>> selectOptionsMap = TSR_Marketing_Segment_Config.retrieveSelectOptionMap('Type');
            
        System.assertEquals(2, keys.size());
        System.assertEquals(2, selectOptionsMap.size());
        System.assertEquals(5, valuesMap.get('01').size());
        System.assertEquals('value1',valuesMap.get('01').get(0));
        System.assertEquals('value2',valuesMap.get('01').get(1));
        System.assertEquals('value 3',valuesMap.get('01').get(2));
        System.assertEquals('value 4',valuesMap.get('01').get(3));
        System.assertEquals('value5',valuesMap.get('01').get(4));
    }
}