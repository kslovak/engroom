public with sharing class GenericSearchComponentCntlr {
    static String COMMA_SEPARATOR = ',';
    static String SEMICOLON_SEPARATOR = ';';
    static String PERIOD_SEPARATOR = '\\.';
    static Integer INITIAL_LIMIT = 10;
    static String VIEW_SEARCHRESULTS = 'VIEW_SEARCHRESULTS';
    static String VIEW_RECENTITEMS = 'VIEW_RECENTITEMS';
    static String VIEW_ERRORS = 'VIEW_ERRORS';
    static String VIEW_INIT = 'VIEW_INIT';

	// -----------
	// Constructor
	// -----------

    public GenericSearchComponentCntlr(){ //Component Parameters are not available here...
        SearchString = system.currentPageReference().getParameters().get('lksrch');
		ShowAdminPanel = false;
		viewMode = VIEW_INIT;
    }

    public void checkIfInitialSearchNeeded() { //Component Parameters ARE available here!!!


	    InitCalled = true;
 
        Filter1On = Filter1Active && Filter1OnByDefault;
        Filter2On = Filter2Active && Filter2OnByDefault;
        Filter3On = Filter3Active && Filter3OnByDefault;
    
        if (RunFirst || String.isNotBlank(SearchString) ) {
            configViewModeSearchResults();
        }
		else {
			configViewModeRecentItems();
		}
		initiateQuery();
    }
    
	// --------------------
    // Component Parameters
	// --------------------
   
    public String ObjectName {get;set;}
    public String ColumnsToShow {get;set;}
    public String AdditionalSearchFields {get;set;}
    public Boolean RunFirst {get;set;}
    public String OrderBy {get;set;}
    public Integer RecordLimit {get;set;}
    public Integer MinSearchBoxLength {get;set;}
    public String LinkFieldName {get;set;}

    public String Filter1 {get;set;}
    public String Filter1AsEntered {get;set;}
    public String Filter1Label {get;set;}
    public Boolean Filter1OffByDefault {get;set;}
    public Boolean Filter1Optional {get;set;}
    public Boolean Filter1Disabled {get;set;}
    public String Filter1ColumnsToAppendIfRemoved {get;set;}
    public Boolean Filter1ForceSearchBoxIfRemoved {get;set;}
    public String Filter1AdditionalSearchFieldsIfRemoved {get;set;}
    public String Filter1OrderByPrefixIfRemoved {get;set;}
    public String Filter1OrderBySuffixIfRemoved {get;set;}
    public Integer Filter1MinSearchBoxLengthIfRemoved {get;set;}

    public String Filter2 {get;set;}
    public String Filter2AsEntered {get;set;}
    public String Filter2Label {get;set;}
    public Boolean Filter2OffByDefault {get;set;}
    public Boolean Filter2Optional {get;set;}
    public Boolean Filter2Disabled {get;set;}
    public String Filter2ColumnsToAppendIfRemoved {get;set;}
    public Boolean Filter2ForceSearchBoxIfRemoved {get;set;}
    public String Filter2AdditionalSearchFieldsIfRemoved {get;set;}
    public String Filter2OrderByPrefixIfRemoved {get;set;}
    public String Filter2OrderBySuffixIfRemoved {get;set;}
    public Integer Filter2MinSearchBoxLengthIfRemoved {get;set;}

    public String Filter3 {get;set;}
    public String Filter3AsEntered {get;set;}
    public String Filter3Label {get;set;}
    public Boolean Filter3OffByDefault {get;set;}
    public Boolean Filter3Optional {get;set;}
    public Boolean Filter3Disabled {get;set;}
    public String Filter3ColumnsToAppendIfRemoved {get;set;}
    public Boolean Filter3ForceSearchBoxIfRemoved {get;set;}
    public String Filter3AdditionalSearchFieldsIfRemoved {get;set;}
    public String Filter3OrderByPrefixIfRemoved {get;set;}
    public String Filter3OrderBySuffixIfRemoved {get;set;}
    public Integer Filter3MinSearchBoxLengthIfRemoved {get;set;}

	// -------------------------------------------------------------
    // Items Needed for Custom Lookup to Link with Salesforce Lookup
	// -------------------------------------------------------------

    // Used to send the link to the right dom element
    public String getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
 
    // Used to send the link to the right dom element for the text box
    public String getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }

	// ---------------
    // Page Properties
	// ---------------

    public ListCollection SearchResults {get;set;}
    public List<ColumnDetail> SearchColumnDetails {get;set;}
    
    public String SearchString {get;set;}
    public Boolean InitCalled {get;set;}
    
	public Boolean ViewModeIsSearchResults {get {return (viewMode==VIEW_SEARCHRESULTS);}}
	public Boolean ViewModeIsRecentItems {get {return (viewMode==VIEW_RECENTITEMS);}}
	public Boolean ViewModeIsErrors {get {return (viewMode==VIEW_ERRORS);}}

	public String SearchResultsTitle {get;set;}
	public String SearchResultsMessage {get;set;}
	public String SearchResultsTableHeaderMessage {get;set;}

    public Boolean Filter1On {get;set;}
    public Boolean Filter2On {get;set;}
    public Boolean Filter3On {get;set;}

    public String Filter1ToggleButtonLabel {get{return FilterToggleButtonLabel(Filter1,Filter1Label,Filter1On);}}
    public String Filter2ToggleButtonLabel {get{return FilterToggleButtonLabel(Filter2,Filter2Label,Filter2On);}}
    public String Filter3ToggleButtonLabel {get{return FilterToggleButtonLabel(Filter3,Filter3Label,Filter3On);}}

    public String Filter1ToggleButtonHoverLabel {get{return fixFilter(Filter1AsEntered);}}
    public String Filter2ToggleButtonHoverLabel {get{return fixFilter(Filter2AsEntered);}}
    public String Filter3ToggleButtonHoverLabel {get{return fixFilter(Filter3AsEntered);}}

    public Boolean Filter1Active {get {return filterActive(Filter1, Filter1Disabled);}}
    public Boolean Filter2Active {get {return filterActive(Filter2, Filter2Disabled);}}
    public Boolean Filter3Active {get {return filterActive(Filter3, Filter3Disabled);}}
    
    public Boolean Filter1OnByDefault {get {return filterOnByDefault(Filter1OffByDefault, Filter1Optional);}}
    public Boolean Filter2OnByDefault {get {return filterOnByDefault(Filter2OffByDefault, Filter2Optional);}}
    public Boolean Filter3OnByDefault {get {return filterOnByDefault(Filter3OffByDefault, Filter3Optional);}}
    
    public Boolean Filter1CanBeToggled {get {return filterCanBeToggled(Filter1Active, Filter1Optional);}}
    public Boolean Filter2CanBeToggled {get {return filterCanBeToggled(Filter2Active, Filter2Optional);}}
    public Boolean Filter3CanBeToggled {get {return filterCanBeToggled(Filter3Active, Filter3Optional);}}

    public Boolean HasData { 
		get {
			Boolean retVal = false;
			if(SearchResults!=null){
				if(SearchResults.getRecordCount()>0){
					retVal = true;
				}
			}
			return retVal;
		}
    }

    public Boolean ShowingRecentItemInSearchResults {
		get {
			Boolean result = false;
			if(SearchResults!=null){
				if (SearchResults.sObjects != null) {
					if (SearchResults.sObjects.size() > 0) {
						System.debug('SearchResults.offset = ' + SearchResults.offset);
						System.debug('SearchResults.PageSize = ' + SearchResults.PageSize);
						for (integer i=SearchResults.offset;i<(SearchResults.Offset + SearchResults.PageSize);i++) {
							if (i>=SearchResults.sObjects.size()) {break;}
							SObject sobj = SearchResults.sObjects.get(i);
							if (sobj.get('LastViewedDate') != null) {
								result = true;
								break;
							}
						}
					}
				}
			}
			return result;
		}
    }

	public Integer currentMinSearchBoxLength { 
		get {
			Integer result = MinSearchBoxLength;
			if (Filter1Active && !Filter1On && Filter1MinSearchBoxLengthIfRemoved > result) {result = Filter1MinSearchBoxLengthIfRemoved;}
			if (Filter2Active && !Filter2On && Filter2MinSearchBoxLengthIfRemoved > result) {result = Filter2MinSearchBoxLengthIfRemoved;}
			if (Filter3Active && !Filter3On && Filter3MinSearchBoxLengthIfRemoved > result) {result = Filter3MinSearchBoxLengthIfRemoved;}
			return result;
		}
	}

    public Boolean ShowAdminPanel {get;set;}
    public Boolean getAdminUser() {return (User_Functions.isSysAdmin() || User_Functions.isSupport());}
	public String Soql {get;set;}

	// --------------------
	// Public Page Commands
	// --------------------
            
    public void clearSearch() {
        clearSearchBoxAndResults();
		configViewModeRecentItems();
        initiateQuery();
    }
    
    public void clearSearchAndRequery() {
        clearSearchBoxAndResults();
		configViewModeSearchResults();
        initiateQuery();
    }
    
    public void toggleFilter1() {
		Filter1On = !Filter1On;
		validateViewMode();
		initiateQuery();
	}

    public void toggleFilter2() {
		Filter2On = !Filter2On;
		validateViewMode();
		initiateQuery();
	}
    
    public void toggleFilter3() {
		Filter3On = !Filter3On;
		validateViewMode();
		initiateQuery();
	}
   
    public void initiateSearch() {
		if (searchStringValid) {
			configViewModeSearchResults();
			initiateQuery();
		}
		else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The search string needs to be at least ' + currentMinSearchBoxLength + ' characters in length!');
            ApexPages.addMessage(myMsg);
		}
	}

	public void toggleAdminPanel() {ShowAdminPanel = !ShowAdminPanel;}

	public void adminApplyChanges() {checkIfInitialSearchNeeded();}

	// ------------
    // Private Vars
	// ------------

    private Schema.SObjectType ObjType;
    private Schema.SObjectField ObjQuickCreateParentField;
   	private String viewMode {get;set;}
	private String currentSearchIs = '';
	private Boolean searchStringValid {get {return (SearchString.length() >= currentMinSearchBoxLength);}}
	private Set<String> additionalFieldsToSearch {
		get {
			Set<String> results = new Set<String>();

			if (AdditionalSearchFields.length() > 0) {results.addAll(ParseDelimitedString(AdditionalSearchFields));}
			if (Filter1Active && !Filter1On && Filter1AdditionalSearchFieldsIfRemoved.length() > 0) {results.addAll(ParseDelimitedString(Filter1AdditionalSearchFieldsIfRemoved));}
			if (Filter2Active && !Filter2On && Filter2AdditionalSearchFieldsIfRemoved.length() > 0) {results.addAll(ParseDelimitedString(Filter2AdditionalSearchFieldsIfRemoved));}
			if (Filter3Active && !Filter3On && Filter3AdditionalSearchFieldsIfRemoved.length() > 0) {results.addAll(ParseDelimitedString(Filter3AdditionalSearchFieldsIfRemoved));}

			return results;
		}
	}

	// ----------------------------
	// Private Methods - Query Data
	// ----------------------------

	private void validateViewMode() {
		if (currentSearchIs == VIEW_SEARCHRESULTS && !searchStringValid) {
			configViewModeRecentItems();
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Only Recent Items are showing because at least ' + currentMinSearchBoxLength + ' characters are required to search!');
            ApexPages.addMessage(myMsg);
		}
	}

 	private void clearSearchBoxAndResults() {
        SearchString = '';
        if (SearchResults.sObjects != null) {
            SearchResults.sObjects.clear();
        }
	}

	private String whereClauseSearchForString(String fieldName, String searchString) {
		return fieldName + ' like \'%' + searchString + '%\'';
	}

    private void initiateQuery() {

		if (viewMode == VIEW_ERRORS) {
			viewMode = currentSearchIs;
		}

		currentSearchIs = viewMode;

        String columnsToShow = ColumnsToShow;
        if (Filter1Active && !Filter1On && String.isNotEmpty(Filter1ColumnsToAppendIfRemoved)) {columnsToShow += ', ' + Filter1ColumnsToAppendIfRemoved;}
        if (Filter2Active && !Filter2On && String.isNotEmpty(Filter2ColumnsToAppendIfRemoved)) {columnsToShow += ', ' + Filter2ColumnsToAppendIfRemoved;}
        if (Filter3Active && !Filter3On && String.isNotEmpty(Filter3ColumnsToAppendIfRemoved)) {columnsToShow += ', ' + Filter3ColumnsToAppendIfRemoved;}
        
        createColumnDetails(ObjectName, columnsToShow);
        
        Soql = 'SELECT Id, ' + columnsToShow + ', LastViewedDate ';
        Soql += ' FROM ' + ObjectName;

		// Build Where Clause        
		String whereClause = '';

		List<String> whereParts = new List<String>();
		whereParts.add(whereClauseSearchForString(LinkFieldName, searchString));
		if (additionalFieldsToSearch.size() > 0) {
			for (String additionalFieldToSearch : additionalFieldsToSearch) {
				whereParts.add(whereClauseSearchForString(additionalFieldToSearch, searchString));
			}
		}

		whereClause += '(' + String.join(whereParts,' OR ') + ')';

        // Process Filter 1
        if (!Filter1Disabled) {
            if (Filter1On) {
                if(String.isNotEmpty(Filter1)) {
                    whereClause += ' AND (' + fixFilter(Filter1) + ') ';
                }
            }
        }

        // Process Filter 2
        if (!Filter2Disabled) {
            if (Filter2On) {
                if(String.isNotEmpty(Filter2)) {
                    whereClause += ' AND (' + fixFilter(Filter2) + ') ';
                }
            }
        }

        // Process Filter 3
        if (!Filter3Disabled) {
            if (Filter3On) {
                if(String.isNotEmpty(Filter3)) {
                    whereClause += ' AND (' + fixFilter(Filter3) + ') ';
                }
            }
        }

		// Handle VIEW_RECENTITEMS
		if (viewMode == VIEW_RECENTITEMS) {
			whereClause += ' AND LastViewedDate != null ';
		}

		// Append Where Clause to Query
        Soql += ' WHERE ' + whereClause;
       
        // Process OrderBy Parameter
        Soql += ' ORDER BY LastViewedDate DESC NULLS LAST ';
		if (Filter1Active && !Filter1On && Filter1OrderByPrefixIfRemoved != '') {Soql += ', ' + Filter1OrderByPrefixIfRemoved;}
		if (Filter2Active && !Filter2On && Filter2OrderByPrefixIfRemoved != '') {Soql += ', ' + Filter2OrderByPrefixIfRemoved;}
		if (Filter3Active && !Filter3On && Filter3OrderByPrefixIfRemoved != '') {Soql += ', ' + Filter3OrderByPrefixIfRemoved;}

        if (String.isNotEmpty(OrderBy)) {
            Soql += ', ' + OrderBy;
        }
        else {
            Soql += ', ' + LinkFieldName;
        }

		if (Filter1Active && !Filter1On && Filter1OrderBySuffixIfRemoved != '') {Soql += ', ' + Filter1OrderBySuffixIfRemoved;}
		if (Filter2Active && !Filter2On && Filter2OrderBySuffixIfRemoved != '') {Soql += ', ' + Filter2OrderBySuffixIfRemoved;}
		if (Filter3Active && !Filter3On && Filter3OrderBySuffixIfRemoved != '') {Soql += ', ' + Filter3OrderBySuffixIfRemoved;}

        
        // Process RecordLimit Parameter
        if (RecordLimit != null) {
            Soql += ' LIMIT ' + RecordLimit;
        }
        else {
            Soql += ' LIMIT 200';
        }
        
        System.debug('Search SOQL: ' + Soql);
        
        try {
            SearchResults = new ListCollection();
            SearchResults.PageSize = 10;
			//database.setTimeout(30000);
            SearchResults.sObjects = database.query(Soql);
        }
        catch(Exception e) {
            system.debug('ERROR: ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage() + '  Query was: ' + Soql);
            ApexPages.addMessage(myMsg);
			viewMode = VIEW_ERRORS;
        }

    }
    
	// -----------------------------
	// Private Methods - View Config
	// -----------------------------

	private void configViewModeSearchResults() {
		viewMode = VIEW_SEARCHRESULTS;
		SearchResultsTitle = 'Search Results';
		SearchResultsMessage = '';
		if (ShowingRecentItemInSearchResults) {
			SearchResultsTableHeaderMessage = '<b>Recent Items Are <span class="highlightedTableRow">Highlighted</span> And Shown First</b>';
		}
		else {
			SearchResultsTableHeaderMessage = '';
		}
	}

	private void configViewModeRecentItems() {
		viewMode = VIEW_RECENTITEMS;
		SearchResultsTitle = 'Recent Items';
		SearchResultsMessage = 'Enter a search filter above and Click \'Go!\'';
		SearchResultsTableHeaderMessage = '';
	}

	private void configViewModeErrors() {
		viewMode = VIEW_ERRORS;
		SearchResultsTitle = '';
		SearchResultsMessage = '';
		SearchResultsTableHeaderMessage = '';
	}

	// -------------------------------------------
	// Private Methods - Filter Processing Methods
	// -------------------------------------------

    private Boolean filterActive(String filterText, Boolean filterDisabled) {
        Boolean result = false;
        try {
            result = (filterText != '' && !filterDisabled);
        }
        catch (Exception e) {
            System.debug('filterActive error');
            System.debug('filterText = ' + filterText);
            System.debug('filterDisabled = ' + filterDisabled);
        }
        return result;
    }
    
    private Boolean filterOnByDefault(Boolean filterOffByDefault, Boolean filterOptional) {
        Boolean result = true;
        if (filterOptional && filterOffByDefault) {
            result = false;
        }
        return result;
    }
    
    private Boolean filterCanBeToggled(Boolean filterActive, Boolean filterOptional) {
        return filterOptional && filterActive;
    }
 
    private String FilterToggleButtonLabel(String filterText, String filterLabel, Boolean filterOn) {
        String customText;
        String prefix;
        String suffix = 'Filter';
        if (String.isEmpty(filterLabel)) {customText = filterText;} else {customText = filterLabel;}
        if (filterOn != null) {
            if (filterOn) {prefix = 'Remove';} else {prefix = 'Apply';}
        }
        return prefix + ' ' + customText + ' ' + suffix;
    }

	// ----------------------------------------
	// Private Methods - Generate Table Columns
	// ----------------------------------------

    private void createColumnDetails(String primaryObject, String columnsToShow){
        // Process Search Fields only when quick create is enable.
        SearchColumnDetails = new list<ColumnDetail>();
        List<String> columnsToShowList = columnsToShow.split(COMMA_SEPARATOR);
        
        for(String f : columnsToShowList){
            String fullFieldName = f.trim();
            
            List<String> fieldParts = fullFieldName.split(PERIOD_SEPARATOR);
            
            String objectName;
            String fieldName;
            
            if (fieldParts.size() == 1) {
                objectName = primaryObject;
                fieldName = fieldParts.get(0);
            }               

            if (fieldParts.size() == 2) {
                objectName = RetrieveTargetObjectNameForSourceRelationshipName(primaryObject, fieldParts.get(0));
                fieldName = fieldParts.get(1);
            }

            if (fieldParts.size() > 2) {
                String errorMessage = 'ERROR! Columns currently can only be one level up - ' + fullFieldName;
                throw new BadColumnException(errorMessage);
            }

            Schema.DescribeFieldResult fieldDesc = RetrieveDescribeFieldResult(objectName, fieldName);
            
            if (fieldDesc != null) {
                String fieldLabel = fieldDesc.getLabel();
                Schema.DisplayType fieldType = fieldDesc.getType();
                if (objectName != primaryObject) {fieldLabel = fieldLabel + ' (' + objectName + ')';}
                ColumnDetail newColumn = new ColumnDetail(fullFieldName,fieldLabel,fieldType);
				if (fieldName == LinkFieldName) {newColumn.clickableLink = true;}
                SearchColumnDetails.add(newColumn);    
            }
        }
    }

    private String RetrieveTargetObjectNameForSourceRelationshipName(String sourceObjectName, String relationshipName) {
        Map<String,Schema.SObjectField> objFields = RetrieveFieldsForObject(sourceObjectName);
        
        for (Schema.SObjectField field : objFields.values()) {
            Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
            
            if (fieldDescribe.getRelationshipName() == relationshipName) {
                if (!fieldDescribe.isNamePointing()) {
                    return fieldDescribe.getReferenceTo().get(0).getDescribe().getName();
                }
                else {
                    String errorMessage = 'ERROR! Column can refer to multiple parent object types.  This is not supported.  Recommend creating formula field on primary object to resolve. - ' + sourceObjectName + '-' + relationshipName;
                    throw new BadColumnException(errorMessage);
                }
            }
        }
        
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'RetrieveTargetObjectNameForSourceRelationshipName Error - Target object name can not be found for: sourceObjectName = ' + sourceObjectName + ' - relationshipName = ' + relationshipName );
        ApexPages.addMessage(myMsg);
        
        return null;
    }
    
    private Map<String,Map<String,Schema.SObjectField>> ObjFieldsByObject = new Map<String,Map<String,Schema.SObjectField>>();
    private Map<String,Schema.SObjectType> globalDesc;

    private Map<String,Schema.SObjectField> RetrieveFieldsForObject(String objectName) {
        try {
            if (globalDesc == null) {globalDesc = Schema.getGlobalDescribe();}
            Map<String,Schema.SObjectField> objFields;
            
            if (ObjFieldsByObject.containsKey(objectName)) {
                objFields = ObjFieldsByObject.get(objectName);
            }
            else {
                Schema.SObjectType ObjType = globalDesc.get(objectName);
                Schema.DescribeSObjectResult descSObjResult = ObjType.getDescribe();
                objFields = descSObjResult.fields.getMap();
                
                ObjFieldsByObject.put(objectName, objFields);
            }
            return objFields;        
        }
        catch (Exception e) {
            system.debug('ERROR: ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'RetrieveFieldsForObject Error - ' + e.getMessage() + ' objectName = ' + objectName );
            ApexPages.addMessage(myMsg);
            return null;        
        }
    }
    
    private Schema.DescribeFieldResult RetrieveDescribeFieldResult(String objectName, String fieldName) {
        Map<String,Schema.SObjectField> objFields = RetrieveFieldsForObject(objectName);

        if (objFields != null) {
            if(objFields.containsKey(fieldName)){
                Schema.SObjectField field = objFields.get(fieldName);
                Schema.DescribeFieldResult fieldDesc = field.getDescribe();
                return fieldDesc;
            }
            else {
                return null;
            }        
        }
        else {
            return null;
        }
    }

	// ------------------------------
	// Private Methods - General Util
	// ------------------------------

    private String fixFilter(String filterIn) {
        String result;
        result = filterIn.replace('\\', '');
        return result;
    }

	private List<String> ParseDelimitedString(String stringIn) {return ParseDelimitedString(stringIn,COMMA_SEPARATOR);}
	private List<String> ParseDelimitedString(String stringIn, String delimiter) {
		List<String> results = new List<String>();
        results = stringIn.split(delimiter);
		for(String result : results) {result = result.trim();}
		return results;
	}


	// ------------------------------------------
    // Internal Classes for Search Result Columns
	// ------------------------------------------

    public class BadColumnException extends Exception{}

    private class ColumnDetail{
        public String apiName {get;set;}
        public String dispName {get;set;}   
        public Schema.DisplayType fieldType {get;set;}
        public Boolean isRequired {get;set;}
		public Boolean clickableLink {get;set;}
        
        public String displayStyle {
            get {
                if (clickableLink) {
                    return 'Link';
                }
                
                if (fieldType.name() == 'DateTime' || fieldType.name() == 'Date' || fieldType.name() == 'Currency') {
                    return 'Field';
                }
                
                return 'Text';
            }
        }
        
        public Boolean DisplayAsField {get {return displayStyle == 'Field';}}
        public Boolean DisplayAsText {get {return displayStyle == 'Text';}}
        public Boolean DisplayAsLink {get {return displayStyle == 'Link';}}
        
        public ColumnDetail(String apiName, String dispName, Schema.DisplayType fieldType){
            this.apiName = apiName;
            this.dispName = dispName;
            this.fieldType = fieldType;
            this.isRequired = false;
			this.clickableLink = false;
        }
    }
    
    //Internal Class for Paging

    public class ListCollection extends Pageable {
        public List<sObject> sObjects {get;set;}

        public override Integer getRecordCount() {
            return (sObjects == null? 0 : sObjects.size());
      }
    }    
}