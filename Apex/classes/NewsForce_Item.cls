public with sharing class NewsForce_Item {

	public NewsForce_Item(NewsForce_Item__c rec) {
		this.rec = rec;
		userId = UserInfo.getUserId();
		populateItemUsage();
	}

	public NewsForce_Item__c rec {get;private set;}
	public NewsForce_Item_Usage itemUsage {get; private set;}

	public Boolean isNew {
		get {
			return (!itemUsage.rec.Viewed__c);
		}
	}

	public Boolean forceDisplayOfItem {
		get {
			return (rec.Force_Display__c && !itemUsage.rec.Dismissed__c);
		}
	}

	public String formattedTitle {
		get {
			
			String formattedDate = '';
			
			try {
				Datetime dateTimeIn = DateTime.newInstance(rec.Display_Date__c, Time.newInstance(0,0,0,0));
				formattedDate = dateTimeIn.format('MMMM d, yyyy');
			}
			catch (Exception e) {}
/*
			return '<span class="titleDate">' + formattedDate + '</span>' + 
			       '<BR>' + 
			       '<span class="titleText">' + rec.Title__c + '</span>';
*/

/*
			return '<span class="titleDate">' + formattedDate + '</span>' + 
			       '<span class="titleText">' + rec.Title__c + '</span>';
*/
/*			       
			return '<div><div class="titleDate">' + formattedDate + '</div>' + 
			       '<p>' + 
			       '<div class="titleText">' + rec.Title__c + '</div></div>';
*/
			return '<div class="titleDate">' + formattedDate + '</div>' + 
			       '<p>' + 
			       '<div class="titleText">' + rec.Title__c + '</div>';

		}
	}

	private Id userId;

	public void userHasViewedItem() {
		itemUsage.userHasViewedItem();
	}

	public void userHasDismissedItem() {
		itemUsage.userHasDismissedItem();
	}

	private void populateItemUsage() {
		if (rec.NewsForce_Items_Usage__r.size() > 0) {
			itemUsage = new NewsForce_Item_Usage(rec.NewsForce_Items_Usage__r);
		}
		else {
			itemUsage = new NewsForce_Item_Usage(rec.Id,userId);
		}
	}
}