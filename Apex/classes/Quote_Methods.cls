public class Quote_Methods {

    public static final String STD_PRICE_BOOK = 'Standard Price Book';
    public static final String QRY1 = ''
        + 'select Id from Product2' 
        + ' where DistChan_Code__c = :distChan'
        + '   and SalesOrg_Code__c = :salesOrg'
        + '   and Active_Material__c = true';
    
    public static void doBeforeUpsert(List<Quote> quotes) {
        for (Quote q : quotes) {q.Account__c = q.AccountId;}
    }
    
    public static PriceBook2 getPriceBook2(String pbName) {
        if (String.isBlank(pbName)) {return null;} PriceBook2 pb = null;
        List<PriceBook2> pbs = [select Id, Name from PriceBook2 where Name = :pbName];
        if (pbs != null && !pbs.isEmpty()) {pb = pbs[0];} return pb;
    }

    public static Id getPriceBookId(String pbName) {
        PriceBook2 pb = getPriceBook2(pbName); if (pb == null) {return null;}
        return pb.Id;
    }
    
    public static Id getStandardPriceBookId() {
        return getPriceBookId(STD_PRICE_BOOK);
    }
    
    public static void updatePriceBook(String distChan, String salesOrg, String pbName) {
        PriceBook2 pb = getPriceBook2(STD_PRICE_BOOK); if (pb == null) {return;}
        updatePriceBook(pb.Id, distChan, salesOrg, pbName); 
    }
        
    public static void updatePriceBook(Id stdPbId, String distChan, String salesOrg, String pbName) {
        PriceBook2 pb = getPriceBook2(pbName); if (pb == null) {return;}
        for (List<Product2> plist : Database.query(QRY1)) {
            createPriceBookEntries(stdPbId, pb.Id, plist);
        } 
    }

    public static void createPriceBookEntries(Id stdPbId, Id pbId, List<Product2> plist) {
        createPriceBookEntries(stdPbId, plist);
        createPriceBookEntries(pbId, plist);
    }
        
    public static void createPriceBookEntries(Id pbId, List<Product2> plist) {
        Set<Id> set1 = new Set<Id>(), set2 = new Set<Id>(); 
        for (Product2 p : plist) {set1.add(p.Id);}
        for (PriceBookEntry pbe : [select Product2Id from PriceBookEntry
                                    where PriceBook2Id = :pbId 
                                      and Product2Id in :set1]) {
            set2.add(pbe.Product2Id);
        }
        List<PriceBookEntry> pbes = new List<PriceBookEntry>(); PriceBookEntry pbe;
        for (Id pid : set1) {
            if (set2.contains(pid)) {continue;}
            pbe = new PriceBookEntry(IsActive=true, PriceBook2Id=pbId, Product2Id=pid, 
                                     UnitPrice=0, UseStandardPrice=false);
            pbes.add(pbe);
        }
        if (!pbes.isEmpty()) {
            try {insert pbes;} catch(Exception e) {}
        }
    }
}