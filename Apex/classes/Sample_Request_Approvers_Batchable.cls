global class Sample_Request_Approvers_Batchable implements Database.Batchable<sObject>, Schedulable {

    final String userEmail = User_Functions.getLoggedInUserEmail();

    final String SUBMITTED = 'In Approval Process';
    final Set<String> REC_STATUS_SET = new Set<String>{SUBMITTED};

    Date created_Date_From;
    final String QRY1 = ''
        + 'select Id from Sample_Request__c'
        + ' where CreatedDate >= :created_Date_From'
        + '   and Status__c in :REC_STATUS_SET';
        
    final String qryString;
    final Set<String> recIds; final Integer recLimit;
    final String REC_FILTR = ' and Id in :recIds', REC_LIMIT = ' limit :recLimit';
    
    private void debug(String s) {System.debug(LoggingLevel.INFO, s);}

    global Sample_Request_Approvers_Batchable() {this(null, 0);}
    
    global Sample_Request_Approvers_Batchable(Set<String> recIds, Integer recLimit) {
        this.recIds = recIds; this.recLimit = recLimit;
        qryString = getQueryString();
    }
    
    global Database.Querylocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(qryString);
    }

    global void execute(Database.BatchableContext bc, List<SObject> alist){
        List<Sample_Request__c> srs = (List<Sample_Request__c>) alist;
        Set<String> srids = new Set<String>(); for (Sample_Request__c sr : srs) {srids.add(sr.Id);}
        Sample_Request_Approvers_Methods.sendNotifications(srids);
    }
    
    global void execute(SchedulableContext sc) {sendNotifications(null, 0);}
    
    global void finish(Database.BatchableContext bc) {sendEmail(bc);}        

    private String getQueryString() {
        created_Date_From = Sample_Request_Approvers_Methods.CREATED_DATE_FROM;
        String q = QRY1; 
        if (recIds != null && !recIds.isEmpty()) {q += REC_FILTR;}
        if (recLimit != null && recLimit > 0)    {q += REC_LIMIT;}
        debug('q = ' + q); return q;
    }
    
    public void sendEmail(Database.BatchableContext bc) {
        String s = 'Apex Batch Job - Sample Requests Pending Approval - '; String b = s;
        if (bc != null) {
            AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                     TotalJobItems, CreatedBy.Email
                                from AsyncApexJob where Id = :bc.getJobId()];
            s += a.Status + ' - ' + a.TotalJobItems + ' batches - ' + a.NumberOfErrors + ' failures';
            b = s + ' - Job Id - ' + a.Id;
        }
        b += '\n\n' + 'Query : ' + qryString;
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {userEmail});
        mail.setReplyTo(userEmail); mail.setSenderDisplayName('SysAdmin');
        mail.setSubject(s); mail.setPlainTextBody(b);
        Messaging.SingleEmailMessage[] mails = new Messaging.SingleEmailMessage[]{mail};
        if (!Test.isRunningTest()) {Messaging.sendEmail(mails);}
    }
    
    Webservice static String sendNotifications(List<String> recIdList, Integer recLimit) {
        Set<String> aset = new Set<String>(); 
        if (recIdList != null && !recIdList.isEmpty()) {aset = new Set<String>(recIdList);}
        Sample_Request_Approvers_Batchable b = new 
        Sample_Request_Approvers_Batchable(aset, recLimit);
        String msg; Integer batchSize = 200;
        if (!Test.isRunningTest()) {
            try {msg = Database.executeBatch(b, batchSize);} 
            catch(Exception e) {msg = e.getMessage(); b.debug(msg);}
        }
        return msg;
    }

}