@isTest
class ArticleWrapperTest {

    @isTest
    static void getArticleTest() {
        // null content
        ArticleWrapper testClass = new ArticleWrapper(null);
        System.assertEquals( null, testClass.getArticle() );
        
        // real content
        Community_Article__c testArticle = new Community_Article__c(Name='testName');
        testClass = new ArticleWrapper( testArticle );
        System.assertEquals( testArticle, testClass.getArticle() );
    }
    
    @isTest
    static void getCtaLinkTest() {
        // null content
        ArticleWrapper testClass = new ArticleWrapper(null);
        System.assertEquals( null, testClass.getCtaLink() );
        
        // real content, no content link, no external link
        Community_Article__c testArticle = new Community_Article__c(Name='testName');
        testClass = new ArticleWrapper( testArticle );
        System.assertEquals( null, testClass.getCtaLink() );
        
        // real content, no content link, with external link
        testArticle = new Community_Article__c(Name='testName',Button_Path__c='testUrl');
        testClass = new ArticleWrapper( testArticle );
        System.assertEquals( 'testUrl', testClass.getCtaLink() );
        
        // real content, with content link, with external link
        ContentVersion testContent = new ContentVersion(Title='testTitle',PathOnClient='',ContentUrl='testContentUrl');
        insert testContent;
        testContent = [select Title,Id,ContentDocumentId from ContentVersion where Id = :testContent.Id][0];
        ContentDocument testDocument = [select Id from ContentDocument][0];
        System.assertNotEquals(null,testContent.ContentDocumentId);
        testArticle = new Community_Article__c(Name='testName',Button_Path__c='testUrl',Content_Link_Id__c=testDocument.Id);
        testClass = new ArticleWrapper( testArticle );
        System.assertEquals( '/sfc/servlet.shepherd/version/download/' + testContent.Id, testClass.getCtaLink() );
    }
}