public class Role_Detail_Methods {
	
	private static final String COLON = ':';
	
	public static Database.Querylocator getUserRoleQueryLocator() {
		return Database.getQuerylocator([
			select Id, Name, ParentRoleId, PortalAccountId, 
			       PortalRole, PortalType
			  from UserRole
		]);
	}
	
	public static void createRoleDetails(List<UserRole> roles) {
		List<Role_Detail__c> rds = new List<Role_Detail__c>();
		for (UserRole r : roles) {
			rds = new List<Role_Detail__c>();
			for (List<User> users : [
				select Id from User where UserRoleId = :r.Id]) {
				for (User u : users) {
					addRoleDetail(rds, r, u);
				}
			}
			if (rds.isEmpty()) {
				addRoleDetail(rds, r, null);
			}
			if (!rds.isEmpty()) {upsert rds Record_Key__c;}
		}
	}
	
	private static void addRoleDetail(List<Role_Detail__c> rds, 
	                                  UserRole r, User u) {
		Role_Detail__c rd = new Role_Detail__c();
		rd.Name = r.Name; rd.Role_Id__c = r.Id;
		rd.Parent_Role_Id__c = r.ParentRoleId;
		rd.Portal_Account_Id__c = r.PortalAccountId;
		rd.Portal_Role__c = r.PortalRole;
		rd.Portal_Type__c = r.PortalType;
		String k = r.Id;
		if (u != null) {
			rd.User__c = u.Id;
			k += COLON + u.Id;
		}
		rd.Record_Key__c = k;
		rds.add(rd);
	}

}