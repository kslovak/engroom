@isTest
private class SAP_Ultimate_Parent_Refresh_Active_UPs_T {

/****************************************************************************
 * Test Class for SAP_Ultimate_Parent_Refresh_Active_UPs_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - SAP_Ultimate_Parent_Refresh_Active_UPs Apex Class
 ****************************************************************************/

    //Test Data
    private static Id accountId;
    private static Account shipTo, soldTo, cHerky, invalidAcc;
 
    private static testMethod void myUnitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
		Account a = TestObjects.NewAccount();
		List<Account> accounts = new List<Account>();
		accounts.add(a);
		Account_Relationship.createAccountRelationshipRecords(accounts);
    }
  
    private static void executeTest01() {
        // Execute Tests
		SAP_Ultimate_Parent_Refresh_Active_UPs.startSchedule();
		SAP_Ultimate_Parent_Refresh_Active_UPs.startSchedule('');
		SAP_Ultimate_Parent_Refresh_Active_UPs.submitBatchJob();
		SAP_Ultimate_Parent_Refresh_Active_UPs.submitBatchJob(5);
   }
}