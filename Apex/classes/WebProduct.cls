public with sharing class WebProduct {

    private Product__c product;
    
    public WebProduct(Product__c p) {
        this.product = p;        
    }
    
    public Product__c getProduct() {
        return this.product;
    }
        // alias method
    public Product__c getProd() {
        return getProduct();
    }
    
    public List<String> getBenefitsList() {
        if(this.product.Product_Benefits__c == null || this.product.Product_Benefits__c == '') {
            return null;
        }
        return this.product.Product_Benefits__c.split('\\s*;\\s*');
    }
    
    public List<String> getBenefitsPreviewList() {
        if(this.product.Benefits_Preview__c == null || this.product.Benefits_Preview__c == '') {
            return null;
        }
        return this.product.Benefits_Preview__c.split('\\s*;\\s*');
    }    
    
    public List<Market__c> getMarkets() {
        if(this.product.Market_Applications__c == null || this.product.Market_Applications__c == '') {
            return null;
        }
        List<String> markets = this.product.Market_Applications__c.split('\\s*;\\s*');
        List<Market__c> market_objects = new List<Market__c>();
        
        
        for(String m : markets){
            Market__c[] market = [select Name, Id from Market__c where Name = :m limit 1];
            if(market.size() > 0) {    // make sure not null
                market_objects.add(market.get(0));
            }
        }
        
        return market_objects;
    }

}