public class CustomPermission_Methods {
	
	public static final String SE_TYPE_CP = 'CustomPermission'; // SetupEntityType
	
	private static void debug(String s) {
		System.debug(LoggingLevel.INFO, 'CustomPermission_Methods : ' + s);
	}
	
	public static CustomPermission getCustomPermission(String cpname) {
		CustomPermission cp = null; 
		debug('getCustomPermission : cpname = ' + cpname);
		if (String.isBlank(cpname)) {return cp;}
		List<CustomPermission> cplist = [
    		select Id from CustomPermission
    		 where DeveloperName = :cpname];
        if (cplist == null || cplist.isEmpty()) {return cp;}
        cp = cplist[0]; debug('getCustomPermission : cp = ' + cp);
        return cp;
	}
	
	public static Set<Id> getPermissionSetIds(String cpname) {
		Set<Id> pset = new Set<Id>();
    	CustomPermission cp = getCustomPermission(cpname);
    	if (cp == null) {return pset;}
    	for (SetupEntityAccess a : [
	    	select ParentId from SetupEntityAccess
	    	 where SetupEntityType = :SE_TYPE_CP
	    	   and SetupEntityId = :cp.Id]) {pset.add(a.ParentId);}
    	debug('getPermissionSetIds : pset = ' + pset); return pset;
	}

    public static Boolean isPermitted(String userid, String cpname) {
    	debug('isPermitted : userid = ' + userid + ' : cpname = ' + cpname);
    	Set<Id> pset = getPermissionSetIds(cpname); Boolean b = false;
    	if (String.isBlank(userid) || pset.isEmpty()) {return b;}
    	List<PermissionSetAssignment> alist = [
	    	select Id from PermissionSetAssignment
	    	 where PermissionSetId in :pset
	    	   and AssigneeId = :userid];
	    b = (alist != null && !alist.isEmpty());
    	debug('isPermitted : b = ' + b); return b;
    }

    public static Boolean isPermitted(String cpname) {
    	return isPermitted(UserInfo.getUserId(), cpname);
    }

	public static Set<Id> getPermittedUserIds(String cpname) {
    	debug('getPermittedUserIds : cpname = ' + cpname);
    	Set<Id> pset = getPermissionSetIds(cpname), uset = new Set<Id>(); 
    	if (pset.isEmpty()) {return uset;}
    	for (PermissionSetAssignment a : [
	    	select AssigneeId from PermissionSetAssignment
	    	 where PermissionSetId in :pset]) {uset.add(a.AssigneeId);}
    	debug('uset.size = ' + uset.size()); return uset;
	}
}