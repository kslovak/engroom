global class TempBatchable implements Database.Batchable<sObject> {

    final static Integer BATCH_SIZE = 200;
    
    private static final String userEmail = User_Functions.getLoggedInUserEmail();
    
    private void debug(String s) {System.debug(LoggingLevel.INFO, '>>>>>>>>>>> ' + s);}
    
    global TempBatchable() {}
    
    global Database.Querylocator start(Database.BatchableContext bc) {
        //return Database.getQueryLocator([select Id, Pricing_Condition__c from Temp_Batch__c]);
        return Database.getQueryLocator([select Id, OwnerId, Inactive_Account__c from Account]);
    }

    global void execute(Database.BatchableContext bc, List<SObject> alist){
    	updateContactOwnerId(alist);
    }

    global void finish(Database.BatchableContext bc){
    	if (!Test.isRunningTest()) {sendEmail(bc);}
    }
    
    private void updateContactOwnerId(List<Account> accs) {
        Map<Id, Id> accOwners = new Map<Id, Id>();
        for (Account a : accs) {
            if (!a.Inactive_Account__c) {accOwners.put(a.Id, a.OwnerId);}
        }
        Contact_Methods.updateOwnerId(accOwners);
    }
    
    private void sendEmail(Database.BatchableContext bc) {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                 TotalJobItems, CreatedBy.Email
                            from AsyncApexJob where Id =:bc.getJobId()];
        String s = 'Apex Batch Job - TempBatchable - ' + 
                   ' - ' + a.Status + ' - ' + a.TotalJobItems + ' batches' +
                   ' - ' + a.NumberOfErrors + ' failures';
        String b = 'Job Id : ' + a.Id + ' processed ' + a.TotalJobItems +
                   ' batches with ' + a.NumberOfErrors + ' failures.';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {userEmail});
        mail.setReplyTo(userEmail);
        mail.setSenderDisplayName('SysAdmin');
        mail.setSubject(s);
        mail.setPlainTextBody(b);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    Webservice static String submitBatchJob(Integer batchSize) {
        TempBatchable b = new TempBatchable();  String msg;
        if (batchSize == null || batchSize < 1) {batchSize = BATCH_SIZE;}
        try {msg = Database.executeBatch(b, batchSize);}
        catch(Exception e) {msg = e.getMessage(); b.debug(msg);}
        return msg;
    }

}