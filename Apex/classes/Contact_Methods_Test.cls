@isTest
private class Contact_Methods_Test {

    static Account acc;
    static Contact con;
    static User    usr;
    
    static void setup() {
        acc = new Account(Name='test account');
        insert acc;
        
        con = new Contact(AccountId=acc.Id, Email='test@example.com', LastName='test');
        insert con;
        
        Profile p;
        try {
            p = [SELECT Id FROM Profile WHERE Name = 'Standard User' limit 1];
        } catch(Exception e) {return;}
        String s = 'x' + p.id + '@example.com';
        usr = new User(Alias = 'standt', Email = s,
                EmailEncodingKey = 'UTF-8', LastName = 'Testing',
                LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                ProfileId = p.Id, Ashland_Employee_Number__c = 'zzzTest10',
                TimeZoneSidKey = 'America/Los_Angeles', Username = s);
        insert usr;
    }
    
    static testMethod void test1() {
        setup(); Test.startTest();
        acc.OwnerId = usr.Id;
        update acc;                       
    }
}