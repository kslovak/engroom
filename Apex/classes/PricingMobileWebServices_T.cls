@isTest
private class PricingMobileWebServices_T {
/****************************************************************************
 * Test Class PricingMobileWebServices_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - PricingMobileWebServices Apex Class
 ****************************************************************************/
 
    //Test Data
    private static Account shipTo, soldTo, soldTo2, cHerky, invalidAcc;
    private static Material_Sales_Data2__c msd2;
    private static Pricing_Condition__c priceCond1, priceCond2, priceCond3;
    private static List<Pricing_Condition__c> pcs;
    private static Active_Pricing__c activePrice;
    
    //Test Settings
    
    
    private static testMethod void myUnitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
    	Map<String,SObject> testData = TestScenarios.createAccountsWithRelationshipMaterialandPCObjects();
        cHerky = (Account)testData.get('cHerky');
        soldTo = (Account)testData.get('soldTo');
        shipTo = (Account)testData.get('shipTo');
        invalidAcc = (Account)testData.get('invalidAcc');
        msd2 = (Material_Sales_Data2__c)testData.get('msd2');
        priceCond1 = (Pricing_Condition__c)testData.get('priceCond1');
        priceCond2 = (Pricing_Condition__c)testData.get('priceCond2');
        priceCond3 = (Pricing_Condition__c)testData.get('priceCond3');
    }
  
    private static void executeTest01() {
        // Execute Tests
//        Id ownerId = [SELECT Id FROM User LIMIT 1].Id;
        
//        Account account = new Account();
//        account.Name = 'Test Account';
//        insert account;
        
//        Pricing_Condition__c pc = [SELECT Id FROM Pricing_Condition__c LIMIT 1];
        
        Date dt = Date.Today();
        
        PricingMobileWebServices.retrieveAccounts(shipTo.ownerId);
        PricingMobileWebServices.retrieveAccountDetails(shipTo.Id);
        PricingMobileWebServices.retrievePricingConditions(shipTo.Id);
        PricingMobileWebServices.retrievePricingConditionDetails(priceCond1.Id);
        PricingMobileWebServices.expirePricingCondition(priceCond1.Id,''); 
        PricingMobileWebServices.extendPricingCondition(priceCond1.Id,'',dt); 
    }
}