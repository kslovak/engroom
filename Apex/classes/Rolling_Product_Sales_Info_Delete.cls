global with sharing class Rolling_Product_Sales_Info_Delete implements Database.Batchable<SObject> {

    public final static String PROC_STEP_ASF = 'ASF'; // Account Sales Figures
    public final static String PROC_STEP_ASP = 'ASP'; // Account Sales Plans
    public final static String PROC_STEP_PSF = 'PSF'; // Product Sales Figures
    public final static String PROC_STEP_PSP = 'PSP'; // Product Sales Plans
    public final static String PROC_STEP_RPS = 'RPS'; // Rolling Product Sales Info
    
    private final static Integer BATCH_SIZE = 200;
    
    private final static String userEmail = User_Functions.getLoggedInUserEmail();

    private final String procStep;

    private final String INVALID_ID = '000000000000000', 
                         QRY1 = 'select Account__r.SAP_DistChannel__c from ';
    
    private final String QRY_ASF = QRY1 + 'Account_Sales_Figure__c';
    private final String QRY_ASP = QRY1 + 'Account_Sales_Plan__c';

    private final String QRY_DEF = QRY1 + 'Contact where Id = :INVALID_ID';

    private final String QRY_PSF = QRY1 + 'Product_Sales_Figure__c';
    private final String QRY_PSP = QRY1 + 'Product_Sales_Plan__c';

    private final String QRY_RPS = QRY1 + 'Rolling_Product_Sales_Info__c';
        
    private void debug(String s) {System.debug(LoggingLevel.INFO, 'RPSI_Delete : ' + s);}
    
    global Rolling_Product_Sales_Info_Delete(String ps) {
        if (String.isBlank(ps)) {return;} procStep = ps;
    }

    global Database.Querylocator start(Database.BatchableContext bc) {
        Database.Querylocator ql; String q = QRY_DEF;
        if (String.isBlank(procStep))       {q = QRY_DEF;} else
        if (procStep.equals(PROC_STEP_ASF)) {q = QRY_ASF;} else
        if (procStep.equals(PROC_STEP_ASP)) {q = QRY_ASP;} else
        if (procStep.equals(PROC_STEP_PSF)) {q = QRY_PSF;} else
        if (procStep.equals(PROC_STEP_PSP)) {q = QRY_PSP;} else
        if (procStep.equals(PROC_STEP_RPS)) {q = QRY_RPS;}
        debug(q); return Database.getQueryLocator(q);
    }

    global void execute(Database.BatchableContext bc, List<SObject> alist){
        deleteSObjects(alist);
    }
    
    global void finish(Database.BatchableContext bc){
        sendEmail(bc);
        if (Rolling_Product_Sales_Info.RUN_BATCH_JOBS_IN_SEQUENCE) {
            if (procStep == PROC_STEP_RPS) {submitBatchJob1(PROC_STEP_ASF);} else
            if (procStep == PROC_STEP_ASF) {submitBatchJob1(PROC_STEP_PSF);} else
            if (procStep == PROC_STEP_PSF) {submitBatchJob1(PROC_STEP_ASP);} else
            if (procStep == PROC_STEP_ASP) {submitBatchJob1(PROC_STEP_PSP);}
        }
    }
    
    private void deleteSObjects(List<SObject> alist) {
    	List<SObject> blist = new List<SObject>(); String dc, distChan = '30';
    	Account_Sales_Figure__c asf; Account_Sales_Plan__c asp;
    	Product_Sales_Figure__c psf; Product_Sales_Plan__c psp;
    	Rolling_Product_Sales_Info__c rps;
    	for (SObject a : alist) {
	               if (procStep.equals(PROC_STEP_ASF)) {
	        	asf = (Account_Sales_Figure__c)a;       dc = asf.Account__r.SAP_DistChannel__c;
	        } else if (procStep.equals(PROC_STEP_ASP)) {
                asp = (Account_Sales_Plan__c)a;         dc = asp.Account__r.SAP_DistChannel__c;
	        } else if (procStep.equals(PROC_STEP_PSF)) {
                psf = (Product_Sales_Figure__c)a;       dc = psf.Account__r.SAP_DistChannel__c;
	        } else if (procStep.equals(PROC_STEP_PSP)) {
                psp = (Product_Sales_Plan__c)a;         dc = psp.Account__r.SAP_DistChannel__c;
	        } else if (procStep.equals(PROC_STEP_RPS)) {
                rps = (Rolling_Product_Sales_Info__c)a; dc = rps.Account__r.SAP_DistChannel__c;
	        }
    		if (dc != null && dc == distChan) {blist.add(a);}
    	}
    	if (!blist.isEmpty()) {delete blist;}
    }
    
    private void sendEmail(Database.BatchableContext bc) {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                 TotalJobItems, CreatedBy.Email
                            from AsyncApexJob 
                           where Id = :bc.getJobId()];
        String s = 'Apex Batch Job - RPSI Delete - ' + 
                             procStep + ' - ' + 
                             a.Status + ' - ' + 
                      a.TotalJobItems + ' batches - ' + 
                     a.NumberOfErrors + ' failures';
        String b = s + ' - Job Id - ' + a.Id;
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {userEmail});
        mail.setReplyTo(userEmail);
        mail.setSenderDisplayName('SysAdmin');
        mail.setSubject(s);
        mail.setPlainTextBody(b);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    Webservice static String submitBatchJob1(String ps) {
        if (String.isBlank(ps)) {return 'Process Step is required';}
        Rolling_Product_Sales_Info_Delete b = new Rolling_Product_Sales_Info_Delete(ps);
        String msg; Integer batchSize = BATCH_SIZE;
        try {msg = Database.executeBatch(b, batchSize);} 
        catch(Exception e) {msg = e.getMessage(); System.debug(LoggingLevel.INFO, e);}
        return msg;
    }

}