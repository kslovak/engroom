public class SAP_Seller {

    public Integer       recNum {get; set;}
    public SAP_Seller__c seller {get; set;}

    public SAP_Seller(SAP_Seller__c s, Integer n) {seller = s; recNum = n;}

}