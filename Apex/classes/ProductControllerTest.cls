@isTest 
class ProductControllerTest {

    @isTest
    static void ProductControllerTest() {
        System.assertNotEquals(null, new ProductControllerTest());
    }
    
    @isTest
    static void getProductTest() {
        ProductController pc = new ProductController();
        System.assertEquals(null, pc.getProduct());
    }
    
    @isTest
    static void getWebProductTest() {
        ProductController pc = new ProductController();
        System.assertEquals(null, pc.getWebProduct());
    }
    
   /* @isTest
    static void getAllProductsTest() {
        ProductController pc = new ProductController();
        insert new Product__c(Name='test',Alias__c='test');
        System.assert(pc.getAllProducts().size() > 0);
    }
    
    @isTest
    static void getAllWebProductsTest() {
        ProductController pc = new ProductController();
        insert new Product__c(Name='test',Alias__c='test');
        System.assert(pc.getAllWebProducts().size() > 0);
    }
    */
    @isTest
    static void getDocumentationTest() {
        ProductController pc = new ProductController();
        pc.getDocumentation();
        System.assertNotEquals(null, pc.getLiterature());
        System.assertNotEquals(null, pc.getDataSheets());
        
        ApexPages.StandardController sc = new ApexPages.StandardController(new Product__c(Name='testName',Alias__c='testAlias',Tag__c='testTag'));
        pc = new ProductController(sc);
        insert new ContentVersion(Title='Title',TagCsv='1,2,APP-Product-Literature,testTag',ContentModifiedDate=Date.today(),ContentUrl='ContentUrl');
        
        pc.getDocumentation();
        System.assertNotEquals(null, pc.getLiterature());
        //System.assert(pc.getLiterature().size() == 1);
        System.assertNotEquals(null, pc.getDataSheets());
    }
    
    @isTest
    static void saveProductTest() {
        ProductController pc = new ProductController();
        System.assertEquals(null, pc.saveProduct());
        
        ApexPages.StandardController sc = new ApexPages.StandardController(new Product__c(Name='testName',Alias__c='testAlias',Tag__c='testTag'));
        pc = new ProductController(sc);
        System.assertEquals(null, pc.saveProduct());
        
    }
    
    @isTest
    static void setSubscriptionTest() {
        ProductController testClass = new ProductController();
        System.assertEquals( null, testClass.setSubscription() );
       
        /*
        Product__c newProduct = new Product__c(Name='testName',Alias__c='testAlias',Tag__c='testTag');
        ContentVersion newContent = new ContentVersion(Title='Title',TagCsv='1,2,Product-Literature,testTag',ContentModifiedDate=Date.today(),ContentUrl='ContentUrl');
        insert newContent;
        ApexPages.StandardController sc = new ApexPages.StandardController(newProduct);
        
        testClass = new ProductController(sc);
        testClass.subscribeContentId = newContent.Id;
        testClass.subscribeSelected = true;
        System.assertEquals( null, testClass.setSubscription() );
        */
    }
    
    @isTest
    static void sendEmailTest() {
        ProductController pc = new ProductController();
       System.assertEquals( null, pc.sendEmail() );
    }
    
    @isTest
    static void emailContentIdTest() {
        ProductController testClass = new ProductController();
        System.assertEquals( null, testClass.emailContentId );
        testClass.emailContentId = 'testEmail@test.com';
        System.assertEquals( 'testEmail@test.com', testClass.emailContentId );
    }
    
    public static testMethod void testgetAllWedProducts() {
        Profile prof = [Select Id, Name From Profile Where Name = 'System Administrator'];
        User u = new User();
        u.FirstName = 'TestFest';
        u.LastName = 'User123';
        u.alias = 'tuser123';
        u.Username = 'Test@testengineroom.com.test';
        u.Email = 'Test@testengineroom.com.test';
        u.emailencodingkey = 'UTF-8';
        u.languagelocalekey = 'en_US';
        u.localesidkey = 'en_US';
        u.profileId = prof.Id;
        u.timezonesidkey = 'America/Los_Angeles';
        insert u;
        System.runAs(u){
            //add Product
            Product__C prd = new Product__C();
            prd.name = 'Test Product';
            prd.Alias__c ='Test Product';
            prd.Tag__c = 'Product-TestProduct';
            insert prd;
            
            Group grp = new Group();
            grp.Name = 'Product_TestProduct_Group';
            insert grp;
            
            GroupMember gm = new GroupMember();
            gm.UserOrGroupId = u.id;
            gm.GroupId =grp.id;
            insert gm;
        
            test.startTest();
            ProductController pc = new ProductController();
            List<WebProduct> wp = pc.getAllWebProducts();
            System.assertEquals(wp.size(), 1);
            test.stopTest();
            
        }
      
     }
}