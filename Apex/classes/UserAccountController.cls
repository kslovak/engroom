public with sharing class UserAccountController extends PageController {

    private ApexPages.StandardController controller;
    private User user{get;set;}
    private Map<String,String> params;
    
    public UserAccountController() {
        //UserAccountController(null);
        this.user = new User(Id=null);
    }
    
    public UserAccountController(ApexPages.StandardController c) {
        this.controller = c;
        this.user = (User)this.controller.getRecord();
        this.params = System.currentPageReference().getParameters();
    }
    
    public User getUser(){
        return this.user;
    }
    
    public boolean getHasEditPermission() {
        // this user is the currently logged-in user
        return UserInfo.getUserId() == this.user.Id;
    }
    
    // pull current user info
    public PageReference saveUser(){
        try {
            update user;
        } catch (DMLException e) {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error saving user info.'));
        }
        return null;
    }
}