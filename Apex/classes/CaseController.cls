public with sharing class CaseController extends PageController {
    
    private ApexPages.StandardSetController controller;
    public Case newCase { get; set; }
    public String caseMessage { get; set; }
    
    public CaseController(ApexPages.StandardSetController c) {
        this.newCase = new Case();
        //this.controller = c;
        //this.controller.setFilterId('00B30000002XJDXEA4');
        this.caseMessage = '';
        getRecords();
    }
    
    public PageReference deleteAll() {
        try {
            delete this.controller.getRecords();
        } catch (Exception e) {
        
        }
        return null;
    }
    
    public void getRecords() {
        List<Case> cases = [SELECT Id, Description, createdDate, closedDate FROM Case WHERE CreatedById=:Userinfo.getUserId()];
        this.controller = new ApexPages.StandardSetController(cases);
    }
    
    public List<Case> getCases() {
        return this.controller.getRecords();
    }
    
    public List<CaseWrapper> getCustomCases() {
        List<CaseWrapper> result = new List<CaseWrapper>();
        for(Case standardCase : this.getCases()) {
            result.add( new CaseWrapper(standardCase) );
        }
        result.sort();
        return result;
    }
    
    public List<SelectOption> getMarketTags() {
      return this.marketList2SelectOptions(retrieveUserMarkets(),'Market');
    }
    
    public List<SelectOption> getProductTags() {
      return this.productList2SelectOptions(retrieveUserProducts(),'Product');
    }
    
    public PageReference submitCase() {
        try {
            this.newCase = new Case(Description = newCase.Description, Market_Tag__c = newCase.Market_Tag__c, Product_Tag__c = newCase.Product_Tag__c);

            List<RecordType> recordTypes = [SELECT Id, Name FROM RecordType WHERE sObjectType='Case' AND Name='AskAnExpert' AND isActive=true ];
            
            if (recordTypes.size() >0){
                newCase.recordTypeId = recordTypes[0].Id; // ask an expert
            }
            
            User user = [Select ContactId from User where id =: Userinfo.getUserId()];
            
            if (user.ContactId != null) {
                Contact contact = [SELECT Id, AccountId FROM Contact WHERE id =: user.ContactId];
                newCase.accountId = contact.AccountId;
                newCase.contactId = contact.Id;
            } else {
                newCase.accountId = '0013B000003SkspQAC'; // Need a way to choose a default account
            }

            newCase.ownerId = Userinfo.getUserId();
            newCase.status = 'new';

            // Insert the case
            upsert newCase;
            this.caseMessage = 'Your question has been submitted.';
        } catch (Exception e) {
            //ApexPages.addMessages(e);
            this.caseMessage = 'Error adding your question.';
        }
        getRecords();
        return null;
    }
    
    private List<SelectOption> productList2SelectOptions(List<Product__c> productList, String placeholder) {
      List<SelectOption> result= new List<SelectOption>();
       
       result.add(new SelectOption('', placeholder));
       for( Product__c product : productList) {
          result.add(new SelectOption(product.Tag__c, product.Alias__c));
       }
       return result;
    }
    
    private List<SelectOption> marketList2SelectOptions(List<Market__c> marketList, String placeholder) {
      List<SelectOption> result= new List<SelectOption>();
       
       result.add(new SelectOption('', placeholder));
       for( Market__c market: marketList) {
          result.add(new SelectOption(market.Tag__c, market.Alias__c));
       }
       return result;
    }
    
    /*
        UTILITY CLASSES
    */
    
    public class CaseWrapper implements Comparable {
        public Case unwrappedCase;
        
        public CaseWrapper(Case c) {
            this.unwrappedCase = c;
        }
        
        public Case getCase() {
            return this.unwrappedCase;
        }
        
        public Integer compareTo(Object case2) {
            return (Integer)( ((CaseWrapper)case2).getLatestActivityTime().getTime() - this.getLatestActivityTime().getTime() );

        }
        
        public DateTime getLatestActivityTime() {
            DateTime result = this.unwrappedCase.createdDate;
            if( this.unwrappedCase.closedDate != null ) {
                if( this.unwrappedCase.closedDate.getTime() > result.getTime() ) {
                    result = this.unwrappedCase.closedDate;
                }
            }
            return result;
        }
        
        public Boolean getIsNew() {
            return this.getLatestActivityTime().date().daysBetween( Date.today() ) < 7;
        }
    }
}