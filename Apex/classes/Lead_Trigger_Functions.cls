public class Lead_Trigger_Functions {

	private static List<Lead_Configuration__c> leadConfigList;
	private static Map<String, String>         queueNameIdMap;
	
	private static void debug(String s) {
		System.debug(LoggingLevel.INFO, 'Lead_Trigger_Functions : ' + s);
	}
    
    private static Map<String, Schema.RecordTypeInfo> getLeadRTMap(){
        return Lead.sObjectType.getDescribe().getRecordTypeInfosByName();
    }
    
    public static void processAfterUpdate(List<Lead> alist, Map<Id, Lead> oldmap)  {
        updateLeadConvertedObjects(alist);
    }
    
    public static void processBeforeInsert(List<Lead> alist) {
        setAddressFields(alist, true); setLeadOwner(alist);
        Map<String, Schema.RecordTypeInfo> RTMap = getLeadRTMap();
        Lead_Process_Carbrite_WebtoLead(alist, RTMap);    
    }
    
    public static void processBeforeUpdate(List<Lead> alist, Map<Id, Lead> oldmap) {
        setAddressFields(alist, false); 
        List<Lead> blist = new List<Lead>(); Lead oldLead;
        for (Lead newLead : alist) {
        	oldLead = oldMap.get(newLead.Id); if (oldLead == null) {continue;}
        	if (!isOwnerChanged(newLead, oldLead)) {blist.add(newLead);}
        }
        if (!blist.isEmpty()) {setLeadOwner(blist);}
    }
    
    private static Boolean isOwnerChanged(Lead newLead, Lead oldLead) {
        Id nid = newLead.OwnerId, oid = oldLead.OwnerId;
        return (nid == null && oid != null) ||
               (nid != null && oid == null) || (nid != oid);
    }
    
    private static void Lead_Process_Carbrite_WebtoLead(List<Lead> alist, Map<String, Schema.RecordTypeInfo> RTMap){
		String carbriteLeadRecordType = RTMap.get('Car Brite Web to Lead').getRecordTypeID();
		if (carbriteLeadRecordType == null || carbriteLeadRecordType == '') {return;}

		Map<String, Schema.RecordTypeInfo> AccountRTMap = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
		String valvolineCustomerRecordType = AccountRTMap.get('Valvoline Customer').getRecordTypeID();
		if (valvolineCustomerRecordType == null || valvolineCustomerRecordType == '') {return;}

    	Set<String> distributorAccountNumbers = new Set<String>();
    	Set<String> carbriteTBMs = new Set<String>();
    	Set<String> employeeIds = new Set<String>();
    	for(Lead l : alist) {
	        if (l.RecordTypeId == carbriteLeadRecordType) {
				distributorAccountNumbers.add(String_Functions.Right('0000000000' + l.Distributor_Account_Number__c,10));
				carbriteTBMs.add(String_Functions.Right('0000000000' + l.car_brite_tbm__c,10));
				if (l.car_brite_tbm__c != null) {employeeIds.add(l.car_brite_tbm__c.toUpperCase());}
			}
		}

    	List<Account> distributorAccounts = [SELECT Id, SAP_Customer_Number__c FROM Account 
                                              WHERE SAP_Customer_Number__c IN :distributorAccountNumbers
                                                AND RecordTypeId = :valvolineCustomerRecordType];

    	List<User> TBMUsers = [SELECT Id, SAP_Cust_Num__c FROM User 
                                WHERE SAP_Cust_Num__c  IN :carbriteTBMs AND IsActive = true];

    	List<User> TBMUsers_EmployeeId = [SELECT Id, Ashland_Employee_Number__c FROM User 
                                           WHERE Ashland_Employee_Number__c IN :employeeIds AND IsActive = true];

		//debug('distributorAccountNumbers = ' + distributorAccountNumbers);                                        
		//debug('distributorAccounts = ' + distributorAccounts);                                        
		//debug('carbriteTBMs = ' + carbriteTBMs);                                        
		//debug('TBMUsers = ' + TBMUsers);                                        
		//debug('TBMUsers_EmployeeId = ' + TBMUsers_EmployeeId);                                        
                                        
    	Map<String,Id> distributorAccountMap = new Map<String,Id>();
    	for(Account a: distributorAccounts) {distributorAccountMap.put(a.SAP_Customer_Number__c,a.Id);}
		//debug('distributorAccountMap = ' + distributorAccountMap);                                        

    	Map<String,Id> TBMUserMap = new Map<String,Id>();
    	for(User u: TBMUsers) {TBMUserMap.put(u.SAP_Cust_Num__c,u.Id);}
		//debug('TBMUserMap = ' + TBMUserMap);                                        

    	Map<String,Id> TBMUserMap_EmployeeId = new Map<String,Id>();
    	for(User u: TBMUsers_EmployeeId) {TBMUserMap_EmployeeId.put(u.Ashland_Employee_Number__c.toUpperCase(),u.Id);}
		//debug('TBMUserMap_EmployeeId = ' + TBMUserMap_EmployeeId);                                        

    	for(Lead l : alist) {
			if (l.RecordTypeId != carbriteLeadRecordType) {continue;}
      
			try {
				l.Distributor_Account__c = distributorAccountMap.get(String_Functions.Right('0000000000' + l.Distributor_Account_Number__c,10));           
        	} catch (Exception e) {debug('An Error occurred assigning the account: ' + e);}

			try {
				Id TBMUserId = TBMUserMap.get(String_Functions.Right('0000000000' + l.car_brite_tbm__c,10));

				//debug('Searching TBMUserMap for /' + String_Functions.Right('0000000000' + l.car_brite_tbm__c,10) + '/ = ' + TBMUserId);

				if (TBMUserId == null) {
					TBMUserId = TBMUserMap_EmployeeId.get(l.car_brite_tbm__c.toUpperCase());
					//debug('Searching TBMUserMap_EmployeeId for /' + l.car_brite_tbm__c + '/ = ' + TBMUserId);
				}            
	
				if(TBMUserId != null) {
					l.OwnerId = TBMUserId; //debug('Lead OwnerId : ' + l.OwnerId);
				}            
        	} catch (Exception e) {debug('An Error occurred assigning the lead: ' + e);}
     		debug('Lead Owner Id = ' + l.OwnerId);                                        
		}  
	}
	
	private static Id getRecordTypeId(Map<String, Schema.RecordTypeInfo> rtmap, String rtype) {
        Schema.RecordTypeInfo rti = rtmap.get(rtype); 
        if (rti != null) {return rti.getRecordTypeId();}
        return null; 
	}
    
    public static void updateLeadConvertedObjects(List<Lead> leads){
	    Set<Id> accountIds = new set<Id>(), contactIds     = new set<Id>(), 
	            leadIds    = new set<Id>(), opportunityIds = new Set<Id>();
	    
        for (Lead l : leads) {
            if (l.isConverted) {
            	leadIds.add(l.Id);
                accountIds.add(l.ConvertedAccountId);
                opportunityIds.add(l.ConvertedOpportunityId);
            }
        }
        
        if (leadIds.isEmpty()) {return;}
    
	    Map<String, Schema.RecordTypeInfo> caseRTMap = Case.sObjectType.getDescribe().getRecordTypeInfosByName(),
	                                       leadRTMap = Lead.sObjectType.getDescribe().getRecordTypeInfosByName();

        Id ashWebLeadRTId = getRecordTypeId(leadRTMap, 'Ashland Website Lead'); 
        Id ashWebCaseRTId = getRecordTypeId(caseRTMap, 'Ashland Website Case with Account'); 
        
	    Map<Id, Account> accountMap = new Map<Id, Account>();
        Map<Id, Case>       caseMap = new Map<Id, Case>();
	
	    for (Account a : [select Id, lead_created_Date__c 
	                        from Account where Id in :accountIds]) {accountMap.put(a.Id, a);}
	    
	    for (Case c : [select Id, AccountId, ContactId, RecordTypeId, Related_Lead__c 
	                     from Case where Related_Lead__c in :leadIds]) {caseMap.put(c.Related_Lead__c, c);}
	    
	    List<Account> accs = new List<Account>(); List<Case> cases = new List<Case>();
	    
	    for (Lead l : leads) {
			Account a = accountMap.get(l.convertedAccountId);
			if (a != null) {
    	        a.lead_created_Date__c = l.createddate.date(); accs.add(a);
			}      
	       
            Case c = caseMap.get(l.Id);
			if (c != null && ashWebLeadRTId != null && l.RecordTypeId == ashWebLeadRTId) {
                c.AccountId    = l.ConvertedAccountId;
                c.ContactId    = l.ConvertedContactId;
                c.RecordTypeId = ashWebCaseRTId;
                cases.add(c);
			}
		}
		
		if (!accs.isEmpty()) {
			try {update accs;}  catch(Exception e) {}
		}
		
		if (!cases.isEmpty()) {
			try {update cases;} catch(Exception e) {}
		}
	}
    
    private static void setAddressFields(List<Lead> alist, Boolean isInsert){
	    List<String> addressLines;
	    String oneStreetVal = '';
	    List<String> street = new List<String>();
	    for (Lead l : alist) {      
			if((l.Lead_Street__c != null || 
				l.Lead_Street_1__c != null || 
	            l.Lead_Street_2__c != null || 
	            l.Lead_Street_3__c != null || 
	            l.Lead_City__c != null || 
	            l.Lead_State__c != null || 
	            l.Lead_Zip_Code__c != null || 
	            l.Lead_Country__c != null) && isInsert) {
	            addressLines = new List<String>();
	            addressLines.add(l.Lead_Street__c);
	            addressLines.add(l.Lead_Street_1__c);
	            addressLines.add(l.Lead_Street_2__c);
	            addressLines.add(l.Lead_Street_3__c);
	            oneStreetVal = Address_Functions.convert4LineAddressto1(addressLines);
	            
	            l.Street     = oneStreetVal;
	            l.City       = l.Lead_City__c;
	            l.State      = l.Lead_State__c;
	            l.PostalCode = l.Lead_Zip_Code__c;
	            l.Country    = l.Lead_Country__c;
	            
	        } else {
	            street = Address_Functions.convert1LineAddressto4(l.Street);
	            if (street.size() > 0) {l.Lead_Street__c   = street[0];} else {l.Lead_Street__c   = '';}
	            if (street.size() > 1) {l.Lead_Street_1__c = street[1];} else {l.Lead_Street_1__c = '';}
	            if (street.size() > 2) {l.Lead_Street_2__c = street[2];} else {l.Lead_Street_2__c = '';}
	            if (street.size() > 3) {l.Lead_Street_3__c = street[3];} else {l.Lead_Street_3__c = '';}
	            
	            l.Lead_City__c     = l.City;
	            l.Lead_State__c    = l.State;
	            l.Lead_Zip_Code__c = l.PostalCode;
	            l.Lead_Country__c  = l.Country;            
	        }     
		}
	}
	
	private static Map<String, String> getCountryRegionMap(Set<String> countries) {
		Map<String, String> rmap = new Map<String, String>();
		if (countries.isEmpty()) {return rmap;}
		for (SAP_Region__c r : [select Country_Code__c, Country_Name__c, Name_of_Super_Region__c
		                          from SAP_Region__c where Country_Code__c in :countries or
		                                                   Country_Name__c in :countries]) {
		    if (String.isBlank(r.Name_of_Super_Region__c)) {continue;}
		    if (!String.isBlank(r.Country_Code__c)) {rmap.put(r.Country_Code__c.toLowerCase(), r.Name_of_Super_Region__c);}
		    if (!String.isBlank(r.Country_Name__c)) {rmap.put(r.Country_Name__c.toLowerCase(), r.Name_of_Super_Region__c);}
		}
		return rmap;
	}
	
	private static Boolean matched(String s1, String s2) {
		return String.isBlank(s1) || (!String.isBlank(s2) && s1.equalsIgnoreCase(s2));
	}
	
	private static void setLeadConfigs() {
		queueNameIdMap = new Map<String, String>();
		leadConfigList = [select Commercial_Unit__c, Lead_Market__c, Lead_Owner__c, Lead_Queue__c, Reason__c, Region__c
		                    from Lead_Configuration__c order by Record_Key__c];
		Set<String> queues = new Set<String>();
		for (Lead_Configuration__c lc : leadConfigList) {
			if (!String.isBlank(lc.Lead_Queue__c)) {queues.add(lc.Lead_Queue__c);}
		}
		if (queues.isEmpty()) {return;}
		queueNameIdMap = Public_Group_Functions.getQueueNameIdMap(queues);
	}
	
	private static Id getQueueId(String queueName) {
		if (!String.isBlank(queueName) && queueNameIdMap.containsKey(queueName)) {
			return queueNameIdMap.get(queueName); 
		}
		return null; 
	}
	
	private static Id getLeadOwnerId(Lead l) {
		Id ownerId = null; Boolean b1, b2, b3, b4;
		for (Lead_Configuration__c lc : leadConfigList) {
			b1 = matched(lc.Commercial_Unit__c, l.Commercial_Unit__c);
			b2 = matched(lc.Lead_Market__c,     l.Lead_Market__c);
			b3 = matched(lc.Region__c,          l.Region__c);
			b4 = matched(lc.Reason__c,          l.Reason__c);
			if (b1 && b2 && b3 && b4) {
				ownerId = lc.Lead_Owner__c;             if (ownerId != null) {break;}
				ownerId = getQueueId(lc.Lead_Queue__c); if (ownerId != null) {break;}
			}
		}
		return ownerId;
	}
	
	private static Boolean isTradeShowLead(Lead l) {
		return (l.Lead_Type__c != null && l.Lead_Type__c.trim() == TradeShowController.LEAD_TYPE);
	}
	
	private static void setLeadOwner(List<Lead> leads) {
		setLeadConfigs();
		Set<String> countries = new Set<String>();
		for (Lead l : leads) {
			if (!String.isBlank(l.Country)) {countries.add(l.Country);}
		}
		Map<String, String> crmap = getCountryRegionMap(countries); 
		String country; Id ownerId;
		for (Lead l : leads) {
			if (isTradeShowLead(l)) {continue;}
			if (!String.isBlank(l.Country)) {
				country = l.Country.toLowerCase();
				if (crmap.containsKey(country)) {
					l.Region__c = crmap.get(country);
				}
			}
			ownerId = getLeadOwnerId(l);
			if (ownerId != null) {l.OwnerId = ownerId;}
		}
	}
}