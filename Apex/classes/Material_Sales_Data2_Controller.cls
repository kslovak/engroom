public with sharing class Material_Sales_Data2_Controller {
	
	public class Plant {
		public Material_Plant2__c mp {get; set;}
        public Integer        recnum {get; set;} 
        public String        country {get; set;} 
		
		public Plant(Material_Plant2__c p, Integer n) {
			mp = p; recnum = n; country = '';
		}
	}
	
	private static final String XX = 'X';
	
    private final Set<String> adminProfiles = new Set<String>{
        'CA Team - Tier 3 Support',
        'System Administrator',
        'System Administrator - SSO Enabled'
    };

    private final Set<String> baseCodeViewProfiles = new Set<String>{};
    
    private final Set<String> exclPlantCodes = new Set<String>{'QM01'};

    public Material_Sales_Data2__c msd {get; private set;}
    public Boolean      sampleMaterial {get; private set;}
    public Boolean        showBaseCode {get; private set;}
    public Boolean   showDeletedPlants {get; private set;}
    public Boolean           plantsMFD {get; private set;}
    public String      reqLeadTimeHelp {get; private set;}
    public String          deletedHelp {get; private set;}
    public String       materialNumber {get; private set;}

    public transient List<Material_UOM2__c> altUOMs {get; private set;}
    public transient List<Plant>             plants {get; private set;}
    
    
    List<Material_Plant2__c> allPlants = new List<Material_Plant2__c>();
    Map<String, String> countryCodeNames = new Map<String, String>(); 
    
    public Material_Sales_Data2_Controller(ApexPages.StandardController stdController) {
        msd = (Material_Sales_Data2__c)stdController.getRecord(); init1();
    }
    
    private void debug(String s) {System.debug(LoggingLevel.INFO, s);}
    
    private void init1() {
        String userProfileName = User_Functions.getLoggedInUserProfileName();
        baseCodeViewProfiles.addAll(adminProfiles);
        showBaseCode = (baseCodeViewProfiles.contains(userProfileName));
        debug(userProfileName + ':' + showBaseCode);
        reqLeadTimeHelp = Material_Plant2__c.Required_Lead_Time__c.getDescribe().getInlineHelpText();
        deletedHelp     = Material_Plant2__c.Deletion_Flag__c.getDescribe().getInlineHelpText();
    	if (msd == null || msd.Id == null) {return;}
    	msd = [select Id, Name, 
					Deletion_Flag__c,
					DistChannel_Desc__c,
					Dist_Channel_Code__c,
					Dist_Channel_Material_Status__c,
					Division_Code__c,
					Gross_Weight__c,
					Gross_Weight_in_LBs__c,
					Material_Desc__c,
					Material_General_Data__r.Base_UOM_Desc__c,
					Material_General_Data__r.Cross_Dist_Material_Status__c,
					Material_General_Data__r.Division_Desc__c,
					Material_General_Data__r.Marked_for_deletion__c,
					Material_General_Data__r.Material_Base_Code_Desc__c,
					Material_General_Data__r.Material_Container_Desc__c,
					Material_General_Data__r.Material_Group_Desc__c,
					Material_General_Data__r.Material_Type_Desc__c,
					Material_General_Data__r.Weight_UOM_Desc__c,
					Material_General_Data__r.id,
					Material_Group1_Desc__c,
					Material_Group2_Desc__c,
					Material_Number__c,
					Minimum_Order_Qty__c,
					Net_Weight__c,
					Net_Weight_in_LBs__c,
					Product_Hierarchy__r.PH1_Division__c,
					Product_Hierarchy__r.PH1__c,
					Product_Hierarchy__r.PH2_Line__c,
					Product_Hierarchy__r.PH2__c,
					Product_Hierarchy__r.PH3_Group__c,
					Product_Hierarchy__r.PH3__c,
					Product_Hierarchy__r.PH4_Supplier__c,
					Product_Hierarchy__r.PH4__c,
					Product_Hierarchy__r.PH5_Family__c,
					Product_Hierarchy__r.PH5__c,
					Product_Hierarchy__r.PH6_Segment__c,
					Product_Hierarchy__r.PH6__c,
					Product_Hierarchy__r.PH7_SubSegment__c,
					Product_Hierarchy__r.PH7__c,
					SalesOrg_Desc__c,
					Sales_Org_Code__c,
					Sales_UOM_Desc__c,
					Sample_Material__c
               from Material_Sales_Data2__c where Id = :msd.Id];
        materialNumber = String_Functions.removeLeadingZeros(msd.Material_Number__c);
        sampleMaterial = XX.equalsIgnoreCase(msd.Sample_Material__c);
		allPlants = Material_Sales_Data_Functions.getPlants(msd);
		altUOMs   = Material_Sales_Data_Functions.getAltUOMs(msd);
		setCountryCodeMap(); showDeletedPlants = true; setPlants();
    }
    
    private void setCountryCodeMap() {
        countryCodeNames = new Map<String, String>();
        if (allPlants == null || allPlants.isEmpty()) {return;}
        Map<String, String> cmap = Country_Code_Functions.getCountryCodeNameMap();
        String moc;
        for (Material_Plant2__c p : allPlants) {
        	moc = p.Material_Origin_Country__c;
        	if (!String.isBlank(moc) && cmap.containsKey(moc)) {
        		countryCodeNames.put(moc, cmap.get(moc));
        	}
        } 
    }
    
    private Boolean excludePlant(Material_Plant2__c p) {
    	Boolean b; String s; 
    	s = p.Plant_Code__c;
    	b = !String.isBlank(s) && exclPlantCodes.contains(s);
    	if (b) {return b;} 
    	
    	s = p.Deletion_Flag__c;
        b = !String.isBlank(s) && (s == XX);
        if (b) {
        	plantsMFD = b; if (!showDeletedPlants) {return b;}
        } 
        
        return false;
    }

    public void setPlants() {
        plantsMFD = false; showDeletedPlants = !showDeletedPlants;
        plants = new List<Plant>(); if (allPlants == null) {return;}
        String moc; Integer n = 0; Plant p;
        for (Material_Plant2__c mp : allPlants) {
        	if (excludePlant(mp)) {continue;}
        	p = new Plant(mp, ++n); plants.add(p);
        	moc = mp.Material_Origin_Country__c;
        	if (!String.isBlank(moc) && countryCodeNames.containsKey(moc)) {
        		p.country = countryCodeNames.get(moc);
        	}
        }        
    }
}