public with sharing class NewsForceBannerController {

    public NewsForceBannerController() {
      init();
    }

	private void init() {
		newsItems = new NewsForce_Items();
	}

	public NewsForce_Items newsItems {get; private set;}
 
	public Boolean hasNoItems {
		get {
			return newsItems.recs.size() == 0;
		}
	}

	public Boolean hasItems {
		get {
			return newsItems.recs.size() > 0;
		}
	}

	public Integer totalItems {
		get {
			return newsItems.recs.size();
		}
	}

	public String itemIdToForceDisplay {
		get {
			Id result = newsItems.itemIdToForceDisplay;
			if (result == null) {
				return ''; 
			} 
			else {
				return result;
			}
		}
	}

	public Id currentItemId {get;set;}

	public void userHasViewedItem() {
		newsItems.userHasViewedItem(currentItemId);
	}

	public void userHasDismissedItem() {
		newsItems.userHasDismissedItem(currentItemId);
	}
}