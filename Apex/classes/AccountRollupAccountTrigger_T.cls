@isTest
private class AccountRollupAccountTrigger_T {
/****************************************************************************
 * Test Class AccountRollupAccountTrigger_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - AccountRollupAccountTrigger Trigger
 ****************************************************************************/
 
    //Test Data
    private static Id accountId;
    private static Account shipTo, soldTo, cHerky, invalidAcc;
    
    private static testMethod void myUnitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
      	Map<String,SObject> testData = TestScenarios.createAccountsWithRelationshipObjects();
		cHerky = (Account)testData.get('cHerky');
		soldTo = (Account)testData.get('soldTo');
		shipTo = (Account)testData.get('shipTo');
		invalidAcc = (Account)testData.get('invalidAcc');

        Account_Rollup_Settings__c settings = Account_Rollup_Settings.defaultSettings();
        settings.Triggers_Enabled__c = true;
        update settings;
    }
  
    private static void executeTest01() {
        // Execute Tests
        update shipTo;
        //delete shipTo;
    }
}