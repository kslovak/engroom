global with sharing class Apex_Job_Schedule_Controller {

    public static final String SORT_ASC_IMG = '/img/colTitle_uparrow.gif'; 
    public static final String SORT_DESC_IMG = '/img/colTitle_downarrow.gif'; 

    public static final ApexPages.Severity ERROR = ApexPages.Severity.ERROR;
    public static final ApexPages.Severity INFO = ApexPages.Severity.INFO;

    public enum SortOrder {A, D} // Ascending, Descending
    public SortOrder nrtso {get; private set;} // next run time sort order
    
    // Map of <Job Name, Schedulable Class>
    private Map<String, Schedulable> scmap;	

    public static void debug(String s) {
        System.debug(LoggingLevel.INFO, 'Apex_Job_Schedule_Controller : ' + s);
    }
    
    private void addErrMsg(String s) {addMsg(ERROR, s);}
    
    private void addInfoMsg(String s) {addMsg(INFO, s);}
    
    private void addMsg(ApexPages.Severity mtype, String s) {
        ApexPages.Message msg = new ApexPages.Message(mtype, s);
        ApexPages.addMessage(msg);
    }
    
    global class JobSchedule implements Comparable {
    	public Apex_Job_Schedule__c ajs {get; private set;}
    	public Boolean selected {get; set;}
    	public CronTrigger ct {get; set;} 
        public Integer recNum {get; private set;}
        public String styleClass {get; private set;}
        public SortOrder nrtso {get; set;}
    	public JobSchedule(Apex_Job_Schedule__c a, Integer n) {
    		ajs = a; nrtso = null; init1(n);
    	}
    	
    	public void init1(Integer n) {
            recNum = n; styleClass = 'row1';
            if (Math.mod(n, 2) == 0) {styleClass = 'row2';}
        }
    	
    	global Integer compareTo(Object obj) {
    		Integer i = 0; JobSchedule js = (JobSchedule) obj;
    		if (nrtso != null && ct != null && ct.NextFireTime != null && 
    		    js.ct != null && js.ct.NextFireTime != null) {
                //debug('ct.NextFireTime = ' + recNum + ' : ' + ct.NextFireTime);
                //debug('js.ct.NextFireTime = ' + js.recNum + ' : ' + js.ct.NextFireTime);
    			if (nrtso == SortOrder.A) if (ct.NextFireTime > js.ct.NextFireTime) {i = 1;} else {i = -1;}
                if (nrtso == SortOrder.D) if (ct.NextFireTime > js.ct.NextFireTime) {i = -1;} else {i = 1;}
    		}
    		//debug('i = ' + i + ' : nrtso = ' + nrtso);
    		return i;
    	}
    }
    
    public Boolean allSelected1 {get; set;}
    public Boolean allSelected2 {get; set;}
    public Boolean renderlist1  {get; set;}
    public Boolean renderlist2  {get; set;}
    public List<JobSchedule> alist {get; private set;} // Scheduled
    public List<JobSchedule> blist {get; private set;} // Not Scheduled
    public List<JobSchedule> slist {get; private set;} // Selected

    public String sortImage {get; private set;}
    public boolean renderNRTSortImage {get; private set;}
    
    public Apex_Job_Schedule_Controller(ApexPages.StandardController sc) {}
    
    private Boolean isNull(String s) {
    	return (s == null || s.trim().length() == 0);
    }
    
    public void initAction() {
    	allSelected1 = false; allSelected2 = false; 
    	nrtso = null; renderNRTSortImage = false;
    	scmap = new Map<String, Schedulable>(); Schedulable sc;
    	alist = new List<JobSchedule>(); blist = new List<JobSchedule>();
    	Integer n1 = 0, n2 = 0; JobSchedule j; Set<String> ctids = new Set<String>();
    	for (Apex_Job_Schedule__c a : [
    	    select Id, Name, Cron_Expression__c, CronTrigger_ID__c, 
    	           Day_of_Month__c, Day_of_Week__c, Hours__c, 
    	           Job_Name__c, Job_Scheduler__c,
    	           Minutes__c, Month__c, Scheduled__c, Seconds__c
    	      from Apex_Job_Schedule__c where Active__c = true order by Job_Name__c]) {
    	    sc = getSchedulable(a.Job_Scheduler__c);
    	    if (sc == null) {continue;}
    	    scmap.put(a.Job_Name__c, sc);
            if (!isNull(a.CronTrigger_ID__c)) {
            	j = new JobSchedule(a, ++n1);
            	alist.add(j);
            	ctids.add(a.CronTrigger_ID__c);
            } else {
                j = new JobSchedule(a, ++n2);
                j.ajs.Scheduled__c = false;
            	blist.add(j);
            }
        }
        Map<String, CronTrigger> ctmap = getCronTriggers(ctids);
        for (JobSchedule a : alist) {
        	a.ajs.Scheduled__c = false;
        	if (ctmap.containsKey(a.ajs.CronTrigger_ID__c)) {
        		a.ct = ctmap.get(a.ajs.CronTrigger_ID__c);
                a.ajs.Scheduled__c = true;
        	}
        }
        sortOnNextRunTime();
        renderlist1 = alist.size() > 0;
        renderlist2 = blist.size() > 0;
    }
    
    private Schedulable getSchedulable(String s) {
        if (String.isBlank(s)) {return null;}
        s = 'Apex_Job_Schedule.' + s;
        Apex_Job_Schedule.JobScheduler j; Type t; 
        try {
            t = Type.forName(s);
            j = (Apex_Job_Schedule.JobScheduler)t.newInstance();
            return j.getSchedulable();
        } catch(Exception e) {
            addErrMsg('Error getting Schedulable : ' + e.getMessage());
        }
        return null;
    }
    
    private Map<String, CronTrigger> getCronTriggers(Set<String> ctids) {
    	Map<String, CronTrigger> amap = new Map<String, CronTrigger>();
    	if (ctids == null || ctids.isEmpty()) {return amap;}
    	List<CronTrigger> cts = [select Id, CronExpression, EndTime, NextFireTime,
            OwnerId, PreviousFireTime, StartTime, State, TimesTriggered, TimeZoneSidKey
            from CronTrigger where Id in :ctids];
        if (cts == null || cts.isEmpty()) {return amap;}
        for (CronTrigger ct : cts) {amap.put(ct.Id, ct);}
        return amap;
    }
    
    private void setSelected(List<JobSchedule> tlist) {
    	slist = new List<JobSchedule>();
        for (JobSchedule t : tlist) {if (t.selected) {slist.add(t);}}
    }
    
    private void scheduleSelected() {
    	String jn, ce; Schedulable sc;
    	List<Apex_Job_Schedule__c> ulist = new List<Apex_Job_Schedule__c>();
    	Apex_Job_Schedule__c u; Id sid;
    	for (JobSchedule s : slist) {
    		sc = null; u = s.ajs; jn = u.Job_Name__c; ce = u.Cron_Expression__c;
            if (scmap.containsKey(jn)) {sc = scmap.get(jn);}
    		if (sc != null) {
	    		try {
                    sid = System.schedule(jn, ce, sc);
	        		u.CronTrigger_ID__c = sid;
	        		u.Scheduled__c = true;
	        		ulist.add(u);
	    		} catch(Exception e) {
	    			
	    		}
    		}
    	}
    	if (!ulist.isEmpty()) {update ulist;}
    }
    
    private void deleteSelected() {
        List<Apex_Job_Schedule__c> ulist = new List<Apex_Job_Schedule__c>();
        Apex_Job_Schedule__c u;
        for (JobSchedule s : slist) {
            u = s.ajs;
            if (s.ct != null && s.ct.Id != null) {
                try {
                	System.abortJob(s.ct.Id);
                    u.CronTrigger_ID__c = null;
                    u.Scheduled__c = false;
                    ulist.add(u);
                } catch(Exception e) {
                    
                }
            }
        }
        if (!ulist.isEmpty()) {update ulist;}
    }
    
    public void startSchedulableJobs() {
    	setSelected(blist);
    	scheduleSelected();
    	initAction();
    }
    
    public void deleteScheduleJobs() {
        setSelected(alist);
        deleteSelected();
        initAction();
    }
    
    private void setNextRunTimeSO(SortOrder so) {
        for (JobSchedule a : alist) {a.nrtso = nrtso;}
    }
    
    public void sortOnNextRunTime() {
    	if (alist.isEmpty()) {return;}
    	//debug('sortOnNextRunTime : nrtso = ' + nrtso);
    	renderNRTSortImage = true;
    	if (nrtso == SortOrder.A) {
    		nrtso = SortOrder.D; sortImage = SORT_DESC_IMG;
    	} else {
    		nrtso = SortOrder.A; sortImage = SORT_ASC_IMG;
    	}
    	setNextRunTimeSO(nrtso);
    	alist.sort(); Integer n = 0;
    	for (JobSchedule a : alist) {++n; a.init1(n);}
    }
}