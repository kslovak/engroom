global class Role_Detail_Batchable implements Database.Batchable<SObject>{

    public final static String PROC_STEP_CRT = 'CREATE'; // Create Role Detail Records
    public final static String PROC_STEP_DEL = 'DELETE'; // Delete Role Detail Records

    private final static Integer BATCH_SIZE = 200;
    
    private final static String userEmail = User_Functions.getLoggedInUserEmail();
    
    final static Map<String, Integer> BATCH_SIZES = new Map<String, Integer>{
        PROC_STEP_CRT => 20, 
        PROC_STEP_DEL => 200 
    };
    
    private final String procStep;
    
    global Role_Detail_Batchable(String ps) {
    	procStep = ps;
    }

    global Database.Querylocator start(Database.BatchableContext bc) {
    	Database.Querylocator ql = Database.getQuerylocator([
    		select Id from Contact where Id = '000000000000000'
    	]);
        if (procStep.equals(PROC_STEP_CRT)) {
        	ql = Role_Detail_Methods.getUserRoleQueryLocator();
        } else
        if (procStep.equals(PROC_STEP_DEL)) {
        	ql = Database.getQuerylocator([
        		select Id from Role_Detail__c
        	]);
		}
		return ql;
    }

    global void execute(Database.BatchableContext bc, List<SObject> alist){
        if (procStep.equals(PROC_STEP_DEL)) {
        	List<Role_Detail__c> rds = (List<Role_Detail__c>)alist; 
            delete rds;
        } else
        
        if (procStep.equals(PROC_STEP_CRT)) {
        	List<UserRole> roles = (List<UserRole>)alist;   
            Role_Detail_Methods.createRoleDetails(roles); 
        }
    }

    global void finish(Database.BatchableContext bc){
        sendEmail(bc);
        if (procStep == PROC_STEP_DEL) {submitBatchJob1(PROC_STEP_CRT);}
    }

    private void sendEmail(Database.BatchableContext bc) {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                 TotalJobItems, CreatedBy.Email
                            from AsyncApexJob 
                           where Id = :bc.getJobId()];
        String s = 'Apex Batch Job - Role Details - ' + 
                             procStep + ' - ' + 
                             a.Status + ' - ' + 
                      a.TotalJobItems + ' batches - ' + 
                     a.NumberOfErrors + ' failures';
        String b = s + ' - Job Id - ' + a.Id;
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {userEmail});
        mail.setReplyTo(userEmail);
        mail.setSenderDisplayName('SysAdmin');
        mail.setSubject(s);
        mail.setPlainTextBody(b);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    Webservice static String submitBatchJob1(String ps) {
    	if (String.isBlank(ps)) {return 'Process Step is required';}
        Role_Detail_Batchable b = new Role_Detail_Batchable(ps);
        String msg; Integer batchSize = BATCH_SIZE;
        if (BATCH_SIZES.containsKey(ps)) {batchSize = BATCH_SIZES.get(ps);}
        try {msg = Database.executeBatch(b, batchSize);} 
        catch(Exception e) {msg = e.getMessage(); System.debug(LoggingLevel.INFO, e);}
        return msg;
    }

    Webservice static String createRoleDetails() {
    	return submitBatchJob1(PROC_STEP_DEL);
    }
    
}