@isTest
class ContentVersionWrapperTest {
    
    @isTest
    static void isNewTest() {
        ContentVersionWrapper testClass = new ContentVersionWrapper(new ContentVersion(ContentModifiedDate = Date.today()));
        
        System.assertEquals( true, testClass.getIsNew() ); // should be new
    }
    
    // getters/setters
    
    @isTest
    static void idTest() {
        ContentVersionWrapper testClass = new ContentVersionWrapper(new ContentVersion());
        
        System.assertEquals( null, testClass.Id );
        testClass.Id = '123';
        System.assertEquals( '123', testClass.Id );
    }
    
    @isTest
    static void contentDocumentIdTest() {
        ContentVersionWrapper testClass = new ContentVersionWrapper(new ContentVersion());
        
        System.assertEquals( null, testClass.ContentDocumentId );
        testClass.ContentDocumentId = '123';
        System.assertEquals( '123', testClass.ContentDocumentId );
    }
    
    @isTest
    static void TitleTest() {
        ContentVersionWrapper testClass = new ContentVersionWrapper(new ContentVersion());
        
        System.assertEquals( null, testClass.Title );
        testClass.Title = 'test';
        System.assertEquals( 'test', testClass.Title );
    }
    @isTest
    static void TagCsvTest() {
        ContentVersionWrapper testClass = new ContentVersionWrapper(new ContentVersion());
        
        System.assertEquals( null, testClass.TagCsv );
        testClass.TagCsv = '1,2,3';
        System.assertEquals( '1,2,3', testClass.TagCsv );
    }
    
    @isTest
    static void ContentModifiedDateTest() {
        ContentVersionWrapper testClass = new ContentVersionWrapper(new ContentVersion());
        
        System.assertEquals( null, testClass.ContentModifiedDate );
        testClass.ContentModifiedDate = Date.today().addDays(1);
        System.assertEquals( Date.today().addDays(1), testClass.ContentModifiedDate );
    }
    
    @isTest
    static void ContentUrlTest() {
        ContentVersionWrapper testClass = new ContentVersionWrapper(new ContentVersion());
        
        System.assertEquals( null, testClass.ContentUrl );
        testClass.ContentUrl = 'testUrl';
        System.assertEquals( 'testUrl', testClass.ContentUrl );
    }
    @isTest
    static void ContentSizeTest() {
        ContentVersionWrapper testClass = new ContentVersionWrapper(new ContentVersion());
        
        System.assertEquals( null, testClass.ContentSize );
    }
    
    @isTest
    static void IsSubscribedTest() {
        ContentVersionWrapper testClass = new ContentVersionWrapper(new ContentVersion());
        
        testClass.IsSubscribed = true;
        System.assertEquals( true, testClass.IsSubscribed );
        testClass.IsSubscribed = false;
        System.assertEquals( false, testClass.IsSubscribed );
    }
}