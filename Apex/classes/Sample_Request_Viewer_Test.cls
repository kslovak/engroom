@isTest
private class Sample_Request_Viewer_Test {

    static Account testAccount;
    static Contact testContact;
    static Sample_Material__c testMaterial1, testMaterial2, testMaterial3;
    static Sample_Request__c testSampleRequest;
    static List<Sample_Material__c> sampleMaterials;

    static ApexPages.StandardController sc;
    static Sample_Request_Viewer srv; 
    static PageReference pr1 = Page.Sample_Request_Invoice;
    static PageReference pr2 = Page.Sample_Request_DeliveryNote;
    
    static Map<String, String> params;

    static void debug(String s) {System.debug(LoggingLevel.INFO, s);}
    
    @testSetup static void setup() {
        testAccount = new Account();
        testAccount.Name = 'Test Account'; 
        testAccount.SAP_DistChannel__c = '50';
        testAccount.SAP_Sales_Org__c = '1021';
        insert testAccount;
        
        testContact = new Contact();
        testContact.FirstName = 'Test FName';
        testContact.LastName = 'Test LName';
        testContact.AccountId = TestAccount.Id;
        insert testContact;
    
        testSampleRequest = new Sample_Request__c();
        setContactInfo(testSampleRequest);
        insert testSampleRequest;
    
        sampleMaterials = new List<Sample_Material__c>();   
        testMaterial1 = getSampleMaterial('Sample Material 1');
        testMaterial2 = getSampleMaterial('Sample Material 2');
        testMaterial3 = getSampleMaterial('Sample Material 3');
        sampleMaterials.add(testMaterial1);
        sampleMaterials.add(testMaterial2);
        sampleMaterials.add(testMaterial3);
        insert sampleMaterials;
    }
    
    static void setContactInfo(Sample_Request__c sr) {
        sr.Account__c = TestAccount.Id;
        sr.Contact_First_Name__c = 'fname';
        sr.Contact_Last_Name__c = 'lname';
        sr.Contact_Phone_Number__c = '999-999-9999';
        sr.Contact_Email__c = 'a@b.c';
        sr.Country__c = 'TH';
    }
    
    static Sample_Material__c getSampleMaterial(String s) {
        Sample_Material__c m = new Sample_Material__c();
        m.name = s;
        m.Sample_Quantity__c = 1;
        m.Sample_UOM__c = 'Drum';
        m.Date_Required_By__c = System.today();
        m.Estimated_Sample_Value__c = 1;
        m.Potential_Annual_Value__c = 1;
        m.Sample_Request__c = testSampleRequest.Id;
        return m;
    }
    
    static void init1() {
        testSampleRequest = [select Id, Name, Account__c from Sample_Request__c limit 1]; 
        debug('testSampleRequest = ' + testSampleRequest);
        sc = new ApexPages.StandardController(testSampleRequest);
        params = ApexPages.currentPage().getParameters();
        params.put('id', testSampleRequest.Id);
        params.put('view', 'pdf');
        params.put('pagesize', '1');
    }

    static testMethod void test01() {
    	Test.setCurrentPage(pr1); init1();
    	srv = new Sample_Request_Viewer(sc);
    }
}