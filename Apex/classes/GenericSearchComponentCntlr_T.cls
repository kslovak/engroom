@isTest
private class GenericSearchComponentCntlr_T {
/****************************************************************************
 * Test Class GenericSearchComponentCntlr_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - GenericSearchComponentCntlr Apex Class
 ****************************************************************************/
 
    //Test Data
    public static Account account1;
	public static GenericSearchComponentCntlr c;
    
    //Test Settings
    
    
    private static testMethod void unitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
	    account1 = TestObjects.NewAccount();
		account1.Name = 'test1';
		update account1;

		c = new GenericSearchComponentCntlr();    
		c.SearchString = '';
		c.ObjectName = 'Account';
		c.ColumnsToShow = 'Name, Type';
		c.RunFirst = true;
		c.AdditionalSearchFields = '';
		c.OrderBy = 'Name';
		c.RecordLimit = 200;
		c.MinSearchBoxLength = 0;
		c.LinkFieldName = 'Name';

		c.Filter1 = 'Name = \'test1\'';
		c.Filter1AsEntered = c.Filter1;
		c.Filter1Label = 'test1';
		c.Filter1OffByDefault = false;
		c.Filter1Optional = false;
		c.Filter1Disabled = false;
		c.Filter1ColumnsToAppendIfRemoved = '';
		c.Filter1ForceSearchBoxIfRemoved = false;
		c.Filter1AdditionalSearchFieldsIfRemoved = '';
		c.Filter1OrderByPrefixIfRemoved = 'Type';
		c.Filter1OrderBySuffixIfRemoved = 'AccountNumber';
		c.Filter1MinSearchBoxLengthIfRemoved = 2;

		c.Filter2 = 'Name = \'test1\'';
		c.Filter2AsEntered = c.Filter2;
		c.Filter2Label = 'test1';
		c.Filter2OffByDefault = false;
		c.Filter2Optional = false;
		c.Filter2Disabled = false;
		c.Filter2ColumnsToAppendIfRemoved = '';
		c.Filter2ForceSearchBoxIfRemoved = false;
		c.Filter2AdditionalSearchFieldsIfRemoved = '';
		c.Filter2OrderByPrefixIfRemoved = '';
		c.Filter2OrderBySuffixIfRemoved = 'AccountNumber';
		c.Filter2MinSearchBoxLengthIfRemoved = 2;

		c.Filter3 = 'Name = \'test1\'';
		c.Filter3AsEntered = c.Filter3;
		c.Filter3Label = 'test1';
		c.Filter3OffByDefault = false;
		c.Filter3Optional = false;
		c.Filter3Disabled = false;
		c.Filter3ColumnsToAppendIfRemoved = '';
		c.Filter3ForceSearchBoxIfRemoved = false;
		c.Filter3AdditionalSearchFieldsIfRemoved = 'Type';
		c.Filter3OrderByPrefixIfRemoved = '';
		c.Filter3OrderBySuffixIfRemoved = 'AccountNumber';
		c.Filter3MinSearchBoxLengthIfRemoved = 2;
    }
  
    private static void executeTest01() {
        // Execute Tests

		// Test Public Properties

		Boolean bool;

		bool = c.ViewModeIsSearchResults;
		bool = c.ViewModeIsRecentItems;
		bool = c.ViewModeIsErrors;

		String s;

		s = c.Filter1ToggleButtonLabel;
		s = c.Filter2ToggleButtonLabel;
		s = c.Filter3ToggleButtonLabel;

		bool = c.Filter1CanBeToggled;
		bool = c.Filter2CanBeToggled;
		bool = c.Filter3CanBeToggled;

		bool = c.HasData;

		// Test Public Methods

		c.RunFirst = false;
		c.checkIfInitialSearchNeeded();

		c.RunFirst = true;
		c.checkIfInitialSearchNeeded();

		c.getFormTag();

		c.getTextBox();

		c.clearSearch();
		
		c.clearSearchAndRequery();

		c.toggleFilter1();

		c.toggleFilter2();

		c.toggleFilter3();
        
		c.initiateSearch();

		c.toggleAdminPanel();

		c.adminApplyChanges();

		c.Filter1Disabled = true;
		c.Filter2Disabled = true;
		c.Filter3Disabled = true;
		c.initiateSearch();

		c.getAdminUser();

		c.SearchString = 'test';
		c.initiateSearch();

		c.clearSearch();

		c.RecordLimit = null;
		c.SearchString = 'test';
		c.initiateSearch();

		c.ColumnsToShow += ', badFieldName';
		c.initiateSearch();
    }
}