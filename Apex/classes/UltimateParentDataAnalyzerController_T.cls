@isTest
private class UltimateParentDataAnalyzerController_T {
/****************************************************************************
* Test Class UltimateParentDataAnalyzerController_T
* --------------------------------------------------------------------------
* Responsible for Testing:
*   - UltimateParentDataAnalyzerController Apex Class
****************************************************************************/
 
    //Test Data


    //Test Settings 


    private static testMethod void myUnitTest() {
        // Create Test Data
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }

    private static void createTestData() {
        // No Test Data Needed
    }

    private static void executeTest01() {
        // Execute Tests
        Account a = TestObjects.NewAccount();
        Account_Relationship__c ar = TestObjects.NewAccount_Relationship(new Map<String,String>{'Child_Account__c' =>a.Id});
        ApexPages.currentpage().getparameters().put( 'id' , a.Id);
        UltimateParentDataAnalyzerController c = new UltimateParentDataAnalyzerController();
        c.toggleShowAll();  
        c.rebuildAccountRelationshipsforAccount();
        c.deleteAccountRelationshipRecord();
        c.rebuildAccountRollupRecord();
        c.reprocessECOMCustRecords();
        
        UltimateParentDataAnalyzerController.AcctCompare acctCompare;
        String msg;  
        acctCompare = new UltimateParentDataAnalyzerController.AcctCompare(a.Id,a,ar);
        msg = acctCompare.message;
        acctCompare = new UltimateParentDataAnalyzerController.AcctCompare(a.Id,null,ar);
        msg = acctCompare.message;
        acctCompare = new UltimateParentDataAnalyzerController.AcctCompare(a.Id,a,null);
        msg = acctCompare.message;
    }
}