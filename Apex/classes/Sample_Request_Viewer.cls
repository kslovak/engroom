public class Sample_Request_Viewer {
	
	public static final Integer MAX_PAGE_SIZE = 20;
	
	public static final String INVOICE_PAGE1 = '/apex/Sample_Request_Invoice'; 

    public static final Map<String, String> COUNTRY1_MAP = new Map<String, String>{
        'FR' => 'France'
    }; 
    
    public static final Map<String, String> CITY1_MAP = new Map<String, String>{
        'FR' => 'Sophia Antipolis'
    }; 
    
    // Map<Country, Text>
    public static final Map<String, String> FOR_USE_MAP = new Map<String, String>{
        'TH' => 'SAMPLE FOR RAW MATERIAL FOR COSMETIC USE / NO COMMERCIAL VALUE'
    }; 
    
    public static final Map<String, PageReference> HEADER1_MAP = new Map<String, PageReference>{
        'FR' => Page.Letterhead_Header_ASI_France
    }; 
    
    public static final Map<String, PageReference> FOOTER1_MAP = new Map<String, PageReference>{
        'FR' => Page.Letterhead_Footer_ASI_France
    };
    
    public static final Set<String> EXCLUDE_STATS1 = new Set<String>{
        Sample_Material_Functions.STAT_CANCELLED,
        Sample_Material_Functions.STAT_CMC_BKORD
    };
    
    public static final Set<String> COUNTRIES_LA = Custom_Config_Functions.getCCSet('Countries_LA__c');
    
	public class SampleMaterial {
		public Integer            snum {get; set;}
		public String            style {get; set;}  
        public String        unitPrice {get; set;}  
        public String        totlPrice {get; set;}  
		public Sample_Material__c smat {get; set;}
		
		public SampleMaterial(Sample_Material__c m, Integer n, Boolean b) {
			smat = m; snum = n;
			style = b ? 'even' : 'odd';
		}
	}

    public class SampleRequestPage {
        public Boolean         firstPage {get; set;}
        public Boolean          lastPage {get; set;}
        public Integer        pageNumber {get; set;}
        
        public List<SampleMaterial> mats {get; set;}
        
        public SampleRequestPage(Integer pn) {
        	pageNumber = pn; mats = new List<SampleMaterial>(); 
        }
    }
    
    public Id                 sampleRequestId {get; private set;}
    public Sample_Request__c    sampleRequest {get; private set;}
    public List<SampleRequestPage>    srpages {get; private set;}
    public SampleRequestPage           srpage {get; private set;}
    
    public Integer          numberOfMaterials {get; private set;}
    public Integer              numberOfPages {get; private set;}
    public Integer                   pageSize {get; private set;}
    public Decimal                   totalQty {get; private set;}
    public String                sampleCenter {get; private set;}
    public String                currencyCode {get; private set;}
    public String                 currentDate {get; private set;}
    public String                addressText1 {get; private set;}
    public String                contactText1 {get; private set;}
    public String                invoiceText1 {get; private set;}

    public String                        view {get; private set;}

    public PageReference           pageHeader {get; private set;}
    public PageReference           pageFooter {get; private set;}

    Map<String, String> params;
    Boolean isInvoicePage;
    String city1, country1;
    
    public Sample_Request_Viewer(ApexPages.StandardController sc) {
    	sampleRequestId = sc.getId(); init1();
    }
    
    private void debug(String s) {
    	System.debug(LoggingLevel.INFO, 'Sample_Request_Viewer : ' + s);
    }
    
    private String getStr(String s) {
        return (String.isBlank(s) ? '' : s.trim());
    }
    
    private void initParams() {
    	PageReference pr = System.currentPageReference();
        params = pr.getParameters();
        String url = pr.getUrl(); debug('url = ' + url);
        isInvoicePage = (url != null && url.startsWithIgnoreCase(INVOICE_PAGE1)); 
        setSampleCenter(); setPageSize(); setView();
    }
    
    private void setSampleCenter() {
        sampleCenter = 'FR'; city1 = ''; country1 = ''; pageHeader = null; pageFooter = null;
        String s = params.get('sc'); if (!String.isBlank(s)) {sampleCenter = s;}
        if (CITY1_MAP.containsKey(sampleCenter))    {city1 = CITY1_MAP.get(sampleCenter);}
        if (COUNTRY1_MAP.containsKey(sampleCenter)) {country1 = COUNTRY1_MAP.get(sampleCenter);}
        if (HEADER1_MAP.containsKey(sampleCenter))  {pageHeader = HEADER1_MAP.get(sampleCenter);}
        if (FOOTER1_MAP.containsKey(sampleCenter))  {pageFooter = FOOTER1_MAP.get(sampleCenter);}
    }
    
    private void setView() {
        view = params.get('view');
        if (String.isBlank(view)) {return;}
        if (view != 'pdf') {view = '';}
    }
    
    private void setPageSize() {
    	pageSize = MAX_PAGE_SIZE;
        String s = params.get('pagesize');
        if (String.isBlank(s)) {return;}
        try {
        	Integer n = Integer.valueof(s);
        	if (n > 0 && n < MAX_PAGE_SIZE) {pageSize = n;}
        } catch(Exception e) {}
    }
    
    private void init1() {
    	debug('init1 : sampleRequestId = ' + sampleRequestId);
    	currentDate = Datetime.now().format('MMMMM dd yyyy');
    	initParams(); setSampleRequest(); setSrpages();
    	setAddressText1(); setInvoiceText1();
    }
    
    private void setSampleRequest() {
    	if (String.isBlank(sampleRequestId)) {return;}
    	sampleRequest = [
            select Id, Name, CurrencyIsoCode, Account__r.Name, Account_Owner_Name__c,
                   Address_Line_1__c, Address_Line_2__c, City__c,
                   Contact_Email__c, Contact_First_Name__c, Contact_Last_Name__c,
                   Contact_Phone_Number__c, Country__c, Postal_Zip_Code__c, State__c
    	      from Sample_Request__c 
    	     where Id = :sampleRequestId]; 
        currencyCode = sampleRequest.CurrencyIsoCode;
    }
    
    private void setAddressText1() {
        String s = '';
        s += '<span class="italic">Attn: ';
        s +=           getStr(sampleRequest.Contact_First_Name__c);
        s += ' '     + getStr(sampleRequest.Contact_Last_Name__c);
        s += '</span><br/>';
        s += '<br/>' + getStr(sampleRequest.Address_Line_1__c);
        s += '<br/>' + getStr(sampleRequest.Address_Line_2__c);
        s += '<br/>' + getStr(sampleRequest.City__c);  
        s += ', '    + getStr(sampleRequest.State__c);
        s += ' '     + getStr(sampleRequest.Postal_Zip_Code__c);
        s += ', '    + getStr(sampleRequest.Country__c);
        s += '<br/>';             
        s += '<br/>' + getStr(sampleRequest.Contact_Phone_Number__c);
        s += '<br/>' + getStr(sampleRequest.Contact_Email__c);
        s += '<br/><br/>';             
        s += city1 + ', ';
        s += currentDate + '<br/>';             
        
        addressText1 = s;
    }
    
    private String getForUseText() {
    	String s = 'SAMPLE FOR COSMETIC USE / NO COMMERCIAL VALUE',
    	       c = sampleRequest.Country__c;
    	if (String.isBlank(c)) {return s;} else
    	if (c == 'TR' || COUNTRIES_LA.contains(c)) {s = '';}
    	if (FOR_USE_MAP.containsKey(c)) {s = FOR_USE_MAP.get(c);}
    	return s;
    }
    
    private void setInvoiceText1() {
    	String s = ''; setContactText1();
        s += '<br/>ORIGIN OF SAMPLES: ' + country1;
    	s += '<br/>' + getForUseText();
        s += '<br/>NOT FLAMABLE / NOT HAZARDOUS';
        s += '<br/>' + contactText1;
        
        invoiceText1 = s;
    }
    
    private void setContactText1() {
        String s = '<br/>';
        s += 'Person to Contact:';
        s += ' ' + getStr(sampleRequest.Contact_First_Name__c);
        s += ' ' + getStr(sampleRequest.Contact_Last_Name__c);
        s += ' ' + getStr(sampleRequest.Contact_Phone_Number__c);
        
        contactText1 = s;
    }
    
    private void setSrpages() {
        srpages = new List<SampleRequestPage>();
        Integer n = 0; numberOfPages = 0; 
        numberOfMaterials = 0; totalQty = 0;
        srpage = new SampleRequestPage(0);
        SampleMaterial mat; Boolean even = true;
        for (Sample_Material__c sm : [
            select Id, Name, Lot_Numbers__c, Record_Status__c,
                   Sample_Quantity__c, Sample_Size__c, Sample_UOM__c
              from Sample_Material__c 
             where Sample_Request__c = :sampleRequestId]) {
            if (String.isBlank(sm.Record_Status__c) || 
                EXCLUDE_STATS1.contains(sm.Record_Status__c)) {continue;}
            numberOfMaterials++; even = !even;
            totalQty += sm.Sample_Quantity__c;
        	n++; if (n > pageSize) {n = 1;}
        	if (n == 1) {
        		numberOfPages++; 
        		srpage = new SampleRequestPage(numberOfPages);
        		srpage.firstPage = (numberOfPages == 1);
        		srpages.add(srpage);
        	}
        	mat = new SampleMaterial(sm, numberOfMaterials, even);
        	mat.unitPrice = '1.00 ' + currencyCode;
        	mat.totlPrice = sm.Sample_Quantity__c + ' ' + currencyCode; 
        	srpage.mats.add(mat);
        }
        srpage.lastPage = (numberOfPages > 0);
        if (isInvoicePage) {
            addHandlingCharges(); addTotalPrice();
        }
    }
    
    private SampleMaterial getSampleMaterial(String name, Boolean even) {
        Sample_Material__c sm = new Sample_Material__c(Name=name);
        SampleMaterial mat = new SampleMaterial(sm, null, even);
        return mat;
    }
    
    private void addHandlingCharges() {
    	Decimal shc = 10.00; totalQty += shc; 
    	SampleMaterial mat = getSampleMaterial('Sample Handling Charges', false);
        mat.unitPrice = shc + ' ' + currencyCode;
        mat.totlPrice = mat.unitPrice; 
    	srpage.mats.add(mat);
    }
    
    private void addTotalPrice() {
        SampleMaterial mat = getSampleMaterial('', true); 
        mat.style = 'totl'; mat.unitPrice = 'Total';
        mat.totlPrice = totalQty + ' ' + currencyCode; 
        srpage.mats.add(mat);
    }
    
}