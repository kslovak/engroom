public with sharing class AccountController {

    ApexPages.StandardController controller;
    
    public AccountController(ApexPages.StandardController c) {
        this.controller = c;
    }
    
    public Account getAccount() {
        String id = ApexPages.currentPage().getParameters().get('id');
        return [select name
            from Account
            where id = :id
            limit 1];
    }
}