/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class CommunitiesLoginController extends PageController {
    
    global String username{get;set;}
    global String password{get;set;}

    global CommunitiesLoginController () {}
    
    global PageReference authForwardToLanding() {
        return UserInfo.getUserType() != 'Guest' ? Page.Home : null;
    }
    
    global PageReference login() {
        return Site.login(username, password, null);
    }
}