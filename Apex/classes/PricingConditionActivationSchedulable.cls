global with sharing class PricingConditionActivationSchedulable implements Schedulable {

    /* Run this code to schedule to run this job at every hour
    System.schedule('Pricing Condition Activations', '0 0 * * * ?',
                     new PricingConditionActivationSchedulable());
    */
    
    global void execute(SchedulableContext sc) {
        PricingConditionActivationBatchable.submitBatchJob();
    }
}