global class PricingConditionScales implements Database.Batchable<sObject> {
    
    public class Rec {
        public Pricing_Condition__c pc {get; set;}
        public Boolean        selected {get; set;}
        public Integer          recnum {get; set;}
        
        public Rec(Pricing_Condition__c p, Integer n) {
            pc = p; recnum = n; selected = false;
        }
    }
    
    public static final String PARAM_ACCID = 'ac';
    public static final String PARAM_RSTAT = 'rs';

    public static final String ACTV     = PricingConditionListController.ACTV;
    public static final String ACTIVE   = PricingConditionListController.ACTIVE;
    public static final String APPROVED = PricingConditionListController.APPROVED;

    public static final Set<String> stats1 = new Set<String>{ACTIVE, APPROVED};
    
    public static final Map<String, String>      RSVN_MAP = PricingConditionListController.RSVN_MAP;
    public static final Map<String, Set<String>> RECS_MAP = PricingConditionListController.RECS_MAP;

    public Account                   acc {get; private set;}

    public List<Rec>                recs {get; private set;}
    public Map<String, String>    params {get; private set;}

    public Boolean             renderPB1 {get; private set;}
    public Boolean             renderPB2 {get; private set;}
    public Boolean             renderPB3 {get; private set;}

    public Boolean         disableChkbx1 {get; private set;}
    public Boolean      renderCancel1Btn {get; private set;}
    public Boolean      renderCancel2Btn {get; private set;}
    public Boolean      renderDelete1Btn {get; private set;}
    public Boolean      renderDelete2Btn {get; private set;}

    public String              recStatus {get; private set;}
    public String            accountDesc {get; private set;}

    public Boolean               allRecs {get; set;}
    public String              accountId {get; set;}
    public String                rsparam {get; set;}
    public Set<String>          recStats {get; set;}

    public PricingConditionScales() {init1();}
    
    @TestVisible
    private void debug(String s) {
        System.debug(LoggingLevel.INFO, '*********************** ' + s);
    }

    @TestVisible
    private void addInfoMsg(String s) {
        addMsg(ApexPages.Severity.INFO,  '<b>' + s + '</b>');
    }

    private void addMsg(ApexPages.Severity mtype, String s) {
        ApexPages.Message msg = new ApexPages.Message(mtype, s);
        ApexPages.addMessage(msg);
    }

    private void init1() {
        recStats = new Set<String>();
        initParams(); init2(); init3();
    }
    
    private void initParams() {
        PageReference pr = System.currentPageReference(); String s;
        params = new Map<String, String>();
        if (pr != null) {params = pr.getParameters();} 
        debug('params = ' + params);

        accountId = params.get(PARAM_ACCID);
        
        rsparam = params.get(PARAM_RSTAT); if (rsparam   == null) {rsparam   = ACTV;}
        recStatus = RSVN_MAP.get(rsparam); if (recStatus == null) {recStatus = ACTIVE;}
        if (stats1.contains(recStatus)) {recStats = RECS_MAP.get(recStatus);}  
    }
    
    private void init2() {
        renderPB1 = false; renderPB2 = false; renderPB3 = false; disableChkbx1 = false; 
        renderCancel1Btn = false; renderCancel2Btn = false;
        renderDelete1Btn = false; renderDelete2Btn = false;
    }
    
    public void init3() {
        if (String.isBlank(accountId)) {return;}
        setAccount(); getPricingConditions(); showPB1();
    }
    
    private void showPB1() {
        init2(); renderPB1 = true; renderCancel1Btn = true; renderDelete1Btn = true;
    }
/*    
    private void showPB2() {init2(); renderPB2 = true;}
    private void showPB3() {init2(); renderPB3 = true;}
*/    
    public PageReference cancel1() {
        String s1 = PricingConditionListController.URL1,
               s2 = PricingConditionListController.URL2;
        String s = s1 + accountId + s2 + rsparam;
        return new PageReference(s);
    }
    
    public void cancel2() {
        getPricingConditions(); showPB1();
    }
    
    private void setAccount() {
        acc = [select Id, Name, AccountNumber, SAP_Sales_Org__c 
                 from Account where Id = :accountId];
        accountDesc = acc.Name 
            + ' - ' + String_Functions.removeLeadingZeros(acc.AccountNumber) 
            + ' - ' + acc.SAP_Sales_Org__c;
    }
    
    public void getPricingConditions() {
        recs = new List<Rec>(); allRecs = false; Integer n = 0;
        if (String.isBlank(accountId) || 
            recStats == null || recStats.isEmpty()) {return;}
        for (Pricing_Condition__c pc : [
            select Id, Name, EndUse_Customer_Desc__c, 
                   Material2__r.Name, PAC_Industry_Segment_Name__c, 
                   Record_Status__c, SAP_Pricing_Table__c,
                   Valid_From_Date__c, Valid_To_Date__c
              from Pricing_Condition__c
             where Account__c = :accountId
               and Used_Scaled_Pricing__c = true
               and Record_Status__c in :recStats
          order by PAC_Industry_Segment_Name__c, 
                   Material2__r.Name, Valid_From_Date__c
             limit 1000]) {recs.add(new Rec(pc, ++n));}
    }
    
    public void deleteScales1() {
        List<Rec> slist = new List<Rec>(); Integer n = 0;
        for (Rec r : recs) {
            if (!r.selected) {continue;}
            r.recnum = ++n; r.selected = false; slist.add(r);
        }
        if (!slist.isEmpty()) {
            recs = slist; init2(); allRecs = false;
            renderPB1 = true; disableChkbx1 = true;
            renderCancel2Btn = true; renderDelete2Btn = true;
        } else {showPB1();}
    }
    
    public void deleteScales2() {
        if (recs == null || recs.isEmpty()) {cancel2(); return;}
        List<Id> alist = new List<Id>(); for (Rec r : recs) {alist.add(r.pc.Id);}
        submitBatchJob(DELETE_PCN_SCALES, alist, null);
        recs = new List<Rec>(); init2(); renderPB1 = true; renderCancel1Btn = true;
        addInfoMsg('A batch job has been submitted and an email will be sent to you when it completes.');
    }
    
// ============================================= Batch Process ============================================================

    public static final String DELETE_PCN_SCALES  = 'DELETE_PCN_SCALES';

    public static final String QRY_DEF = 'select Id from Contact limit 1';
    
    static final Set<String> PROCESS_STEPS = new Set<String>{
        DELETE_PCN_SCALES
    };
    
    static final Map<String, Integer> BATCH_SIZES = new Map<String, Integer>{
        DELETE_PCN_SCALES => 10 
    };
    
    static final Map<String, String> QRY_STRINGS = new Map<String, String>{
        DELETE_PCN_SCALES => PricingConditionFunctions.SELECT_QRY_1
    };
    
    final String userEmail = User_Functions.getLoggedInUserEmail();

    final String procStep, qryString; final Set<Id> recIds; final Integer recLimit;
    final String REC_FILTR = ' where Id in :recIds', REC_LIMIT = ' limit :recLimit';
    
    public PricingConditionScales(String pstep, Set<Id> recIds, Integer recLimit) {
        if (String.isBlank(pstep)) {pstep = '';}
        this.procStep = pstep; this.recIds = recIds; this.recLimit = recLimit;
        qryString = getQueryString();
    } 
    
    private Boolean procStepUnknown() {
        return (String.isBlank(procStep) || !PROCESS_STEPS.contains(procStep));
    }
    
    global Database.Querylocator start(Database.BatchableContext bc) {
        String q = qryString; if (String.isBlank(q)) {q = QRY_DEF;} 
        return Database.getQueryLocator(q);
    }

    global void execute(Database.BatchableContext bc, List<SObject> alist){
        if (procStepUnknown() || alist == null) {return;}
        if (DELETE_PCN_SCALES.equals(procStep)) {
            PricingConditionFunctions.deleteScales((List<Pricing_Condition__c>)alist);
        }
    }
    
    global void finish(Database.BatchableContext bc) {sendEmail(bc);}        

    private String getQueryString() {
        String q = QRY_STRINGS.get(procStep); 
        if (String.isBlank(q)) {q = QRY_DEF; return q;}
        if (recIds != null && !recIds.isEmpty()) {q += REC_FILTR;}
        if (recLimit != null && recLimit > 0)    {q += REC_LIMIT;}
        debug('q = ' + q); return q;
    }
    
    public void sendEmail(Database.BatchableContext bc) {
        String s = 'Apex Batch Job - PCN Scales - ' + procStep + ' - '; String b = s;
        if (bc != null) {
            AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                     TotalJobItems, CreatedBy.Email
                                from AsyncApexJob where Id = :bc.getJobId()];
            s += a.Status + ' - ' + a.TotalJobItems + ' batches - ' + a.NumberOfErrors + ' failures';
            b = s + ' - Job Id - ' + a.Id;
        }
        //b += '\n\n' + 'Query : ' + qryString;
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {userEmail});
        mail.setReplyTo(userEmail); mail.setSenderDisplayName('SysAdmin');
        mail.setSubject(s); mail.setPlainTextBody(b);
        Messaging.SingleEmailMessage[] mails = new Messaging.SingleEmailMessage[]{mail};
        if (!Test.isRunningTest()) {Messaging.sendEmail(mails);}
    }
    
    public String submitBatchJob(String procStep, List<Id> alist, Integer recLimit) {
        Set<Id> aset = new Set<Id>(); if (alist != null && !alist.isEmpty()) {aset = new Set<Id>(alist);}
        PricingConditionScales b = new PricingConditionScales(procStep, aset, recLimit);
        String msg; Integer batchSize = 1;
        if (BATCH_SIZES.containsKey(procStep)) {batchSize = BATCH_SIZES.get(procStep);}
        if (!Test.isRunningTest()) {
            try {msg = Database.executeBatch(b, batchSize);} 
            catch(Exception e) {msg = e.getMessage(); b.debug(msg);}
        }
        return msg;
    }

// =========================================================================================================

}