public class DocumentMethods {
	
	private static final String FILE_URL = '/servlet/servlet.FileDownload?file=';
	
	public class Rec {
		public Document   doc {get; set;}
		public Integer recNum {get; set;}
        public String    size {get; set;}
        public String viewUrl {get; set;}
		
		public Rec(Document d, Integer n) {
			doc = d; recNum = n;
            viewUrl = FILE_URL + doc.Id;
            size = getFileSize(doc.BodyLength);
		}
		
	}
	
	private static String getFileSize(Integer n) {
		String s = ''; if (n == null) {return s;}
        Decimal d, d1 = 1024, d2 = d1 * d1, d3 = d2 * d1, d4 = d3 * d1; 
        if (n < d2) {
            s = 'KB'; d = n / d1;
        } else 
        if (n < d3) {
            s = 'MB'; d = n / d2;
        } else 
        if (n < d4) {
            s = 'GB'; d = n / d3;
        } 
        d = round(d, 2); s = d + ' ' + s;
        return s; 
	}
	
    private static Decimal round(Decimal d, Integer n) {
        if (d == null || d == 0) {return 0;}
        Decimal d1 = 10;
        Decimal d2 = d1.pow(n);
        Decimal d3 = (d * d2).round(RoundingMode.HALF_UP);
        Decimal d4 = d3.divide(d2, n);
        return  d4;
    }

    public static final String SORTBY_NAME       = 'Name'; 
	public static final String SORTBY_NEW_TO_OLD = 'LastModifiedDate desc'; 
	
	private static final Integer MAX_LIMIT = 1000;
    private static final String QRY1 = ''
        + 'select Id, Name, Author.Name, BodyLength, Description,'
        + '       IsInternalUseOnly, LastModifiedDate, Type'
        + '  from Document where FolderId = :folderId';

    private static final String QRY2 = ' and IsInternalUseOnly = :internalUseOnly';
    
    private static final String QRY3 = ' order by ';
	
    private static final String QRY4 = ' limit :MAX_LIMIT';
    
    private static void debug(String s) {
        System.debug(LoggingLevel.INFO, 'DocumentMethods : ' + s);
    }
    
    private static String getQuery(Boolean internalUseOnly, String sortBy) {
        String q = QRY1;
        if (internalUseOnly != null) {q += QRY2;}
        if (!String.isBlank(sortBy)) {q += (QRY3 + sortBy);}
        q += QRY4; debug(q);
        return q; 
    }

    public static List<Rec> getDocuments(String folderName) {
    	Boolean internalUseOnly = null;
    	return getDocuments(folderName, internalUseOnly);
    }
        
    public static List<Rec> getDocuments(String folderName, Boolean internalUseOnly) {
        String sortBy = SORTBY_NAME; sortBy = SORTBY_NEW_TO_OLD;
        return getDocuments(folderName, internalUseOnly, sortBy);
    }
        
	public static List<Rec> getDocuments(String folderName, Boolean internalUseOnly, String sortBy) {
		List<Rec> recs = new List<Rec>(); Rec r; Integer n = 0;
		Folder f = FolderMethods.getDocumentFolder(folderName);
		if (f == null) {return recs;}
		String folderId = f.Id, q = getQuery(internalUseOnly, sortBy);
		List<Document> docs = Database.query(q);
        if (docs == null || docs.isEmpty()) {return recs;}
        for (Document d : docs) {
        	r = new Rec(d, ++n); recs.add(r);
        }
		return recs;
	}
	
}