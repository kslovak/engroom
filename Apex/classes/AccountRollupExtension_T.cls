@isTest
private class AccountRollupExtension_T {
/****************************************************************************
 * Test Class AccountRollupExtension_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - AccountRollupExtension Apex Class
 ****************************************************************************/
 
    //Test Data
    
    
    //Test Settings
    
    
    private static testMethod void myUnitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
        // No Test Data Needed
    }
  
    private static void executeTest01() {
        // Execute Tests
        Account a = TestObjects.NewAccount();
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        AccountRollupExtension ext = new AccountRollupExtension(sc);    
    }
}