global class Quote_Batchable implements Database.Batchable<sObject> {

    final String userEmail = User_Functions.getLoggedInUserEmail();

    final String distChan, salesOrg, pbName, qryString;
    final Id stdPbId, pbId; 
    final Set<Id> recIds; final Integer recLimit;
    final String REC_FILTR = ' and Id in :recIds', REC_LIMIT = ' limit :recLimit';
    
    private void debug(String s) {System.debug(LoggingLevel.INFO, s);}

    global Quote_Batchable(String distChan, String salesOrg, String pbName) {this(distChan, salesOrg, pbName, null, 0);}
    
    global Quote_Batchable(String distChan, String salesOrg, String pbName, Set<Id> recIds, Integer recLimit) {
        this.distChan = distChan; this.salesOrg = salesOrg; this.pbName = pbName; 
        this.recIds = recIds; this.recLimit = recLimit;
        qryString = getQueryString(); stdPbId = Quote_Methods.getStandardPriceBookId();
        pbId = Quote_Methods.getPriceBookId(pbName);
    }
    
    global Database.Querylocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(qryString);
    }

    global void execute(Database.BatchableContext bc, List<SObject> alist){
        if (stdPbId == null || pbId == null) {return;}
        Quote_Methods.createPriceBookEntries(stdPbId, pbId, (List<Product2>)alist);
    }
    
    global void finish(Database.BatchableContext bc) {sendEmail(bc);}        

    private String getQueryString() {
        String q = Quote_Methods.QRY1; 
        if (recIds != null && !recIds.isEmpty()) {q += REC_FILTR;}
        if (recLimit != null && recLimit > 0)    {q += REC_LIMIT;}
        debug('q = ' + q); return q;
    }
    
    public void sendEmail(Database.BatchableContext bc) {
        String s = 'Apex Batch Job - Quotes - '; String b = s;
        if (bc != null) {
            AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                     TotalJobItems, CreatedBy.Email
                                from AsyncApexJob where Id = :bc.getJobId()];
            s += a.Status + ' - ' + a.TotalJobItems + ' batches - ' + a.NumberOfErrors + ' failures';
            b = s + ' - Job Id - ' + a.Id;
        }
        b += '\n\n' + 'Query : ' + qryString;
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {userEmail});
        mail.setReplyTo(userEmail); mail.setSenderDisplayName('SysAdmin');
        mail.setSubject(s); mail.setPlainTextBody(b);
        Messaging.SingleEmailMessage[] mails = new Messaging.SingleEmailMessage[]{mail};
        if (!Test.isRunningTest()) {Messaging.sendEmail(mails);}
    }
    
    Webservice static String createPriceBookEntries(String distChan, String salesOrg, String pbName, 
                                                    List<Id> recIdList, Integer recLimit) {
        Set<Id> aset = new Set<Id>(); 
        if (recIdList != null && !recIdList.isEmpty()) {aset = new Set<Id>(recIdList);}
        Quote_Batchable b = new Quote_Batchable(distChan, salesOrg, pbName, aset, recLimit);
        String msg; Integer batchSize = 200;
        if (!Test.isRunningTest()) {
            try {msg = Database.executeBatch(b, batchSize);} 
            catch(Exception e) {msg = e.getMessage(); b.debug(msg);}
        }
        return msg;
    }

}