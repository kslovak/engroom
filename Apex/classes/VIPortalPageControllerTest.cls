@isTest
private class VIPortalPageControllerTest {

    static Account acc;

    static void setup() {
        acc = new Account(Name='TestAcc');
        acc.VI_Channel__c = VIUtils.CHANNEL_ASC;
        insert acc;
    }

    static testMethod void test01() {
        setup(); Test.startTest();

        VIPortalPageController c1 = new VIPortalPageController();
        c1.setAccountId(acc.Id); c1.addInfoMsg('Test');
        c1.getAccountId(); c1.gotoHomePage(); c1.gotoLocations();
        c1.gotoLogout(); c1.gotoPromotions();

        VIPortalHomePageController c2 = new VIPortalHomePageController();
        c2.initAction(); c2.addInfoMsg('Test');

        Test.stopTest();
    }
}