@isTest
public class Sample_Request_Functions_Test {

    static Account acc1;
    static Material_Sales_Data2__c mat1;
    static Sample_Request__c sr1;
    static Sample_Material__c sm1;
    static List<Sample_Request__c> srs1;
    static List<Sample_Material__c> sms1;
    static Sample_Material_Catalog__c smc;
    static List<Sample_Material_Catalog__c> smclist;
    static String sampleCenter = 'Test-SC';
    
    static void addSMC(String ssize) {
        smc = new Sample_Material_Catalog__c();
        smc.Product_Code__c = 'Test1234';
        smc.Product_Name__c = 'TestProduct';
        smc.Sample_Center__c = sampleCenter;
        smc.Sample_Size__c = ssize;
        smclist.add(smc);
    }
        
    static void createTestData() {
    	acc1 = new Account(Name='Test Account');
    	acc1.SAP_DistChannel__c = '50'; acc1.SAP_Sales_Org__c = '1021'; 
    	insert acc1;
    	mat1 = Material_Sales_Data_Functions_Test.getTestMsd2();
    	sr1 = new Sample_Request__c();
    	sr1.Account__c = acc1.Id;
    	insert sr1;
    	sm1 = new Sample_Material__c();
    	sm1.Sample_Request__c = sr1.Id;
    	sm1.SAP_Material_MSD2__c = mat1.Id;
    	insert sm1;
    	srs1 = new List<Sample_Request__c>(); srs1.add(sr1);
        sms1 = new List<Sample_Material__c>(); sms1.add(sm1);
        
        smclist = new List<Sample_Material_Catalog__c>();
        addSMC('TestSize1'); addSMC('TestSize2'); addSMC('TestSize3');
        insert smclist;
        User usr = User_Functions.getLoggedInUser();
        usr.Sample_Center__c = sampleCenter;
        try {update usr;} catch(Exception e){}
    }
    
    public static Sample_Request__c getSampleRequest() {
    	createTestData(); Id srid = sr1.Id;
    	String q = Sample_Request_Functions.QRY1 + ' where Id = :srid';
    	List<Sample_Request__c> srs = Database.query(q);
    	sr1 = srs[0];
    	return sr1;
    }
     
    public static Sample_Material__c getSampleMaterial() {
    	if (sr1 == null) {createTestData();}
    	return sm1;
    }
    
    static testMethod void test01() {
    	createTestData();
        Sample_Request_Functions.Validate_SAP_Order_Number('', false);
        Sample_Request_Functions.Validate_SAP_Order_Number('', true);
        Sample_Request_Functions.AssignMyselfasCoordinator(sr1.Id);
        Sample_Request_Functions.RemoveRequestfromHold(sr1.Id);
        Sample_Request_Functions.Update_Approval_Process(sr1.Id);
        Sample_Request_Functions.setRouteForApprovalFlags(acc1, sr1, sms1);
        Sample_Request_Functions.getMatCatalogPC(sr1, acc1);
        sr1.No_Charge_Order__c = true;
        Sample_Request_Functions.getMatCatalogPC(sr1, acc1);
        Sample_Request_Functions.getMatCatalogPC(sr1, acc1, 'matName');
    }

    
    static testMethod void test02() {
        createTestData();
        acc1.SAP_DistChannel__c = '10'; acc1.SAP_Sales_Org__c = '1020';
        Sample_Request_Functions.isACM(acc1); 
        Sample_Request_Functions.isAPM(acc1); 
        Sample_Request_Functions.setRouteForApprovalFlags(acc1, sr1, sms1);
    }
}