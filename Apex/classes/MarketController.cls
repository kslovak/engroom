public with sharing class MarketController extends PageController {
    
    private ApexPages.StandardController controller;
    private Market__c market;    // regular object
    private WebMarket webMarket;         // web-customized object wrapper
    private Integer numNewsItems;
    
    public string subscribeContentId { get; set; }
    public boolean subscribeSelected { get; set; }
    public string emailResponse { get; set; }
    
    public List<ContentVersionWrapper> literature { get; set; }
    public List<ContentVersionWrapper> training { get; set; }
    public List<ContentVersionWrapper> presentations { get; set; }
    
    public MarketController(ApexPages.StandardController sc) {
        this.controller = sc;
        
        if(!Test.isRunningTest()) {
            this.controller.addFields(new List<String>{'Alias__c','RSS_Feeds__c',
                'Article1__c', 'Article1_Default__c', 'Article1_Access__c',
                'Article2__c', 'Article2_Default__c', 'Article2_Access__c',
                'Product_Applications__c','Tag__c', 'Main_Text__c', 'Highlight_Description__c' });
        }
        
        this.market = (Market__c)this.controller.getRecord();
        this.webMarket = new WebMarket(this.market);
        this.numNewsItems = 5;
        
        getDocumentation();
    }
    
    public MarketController() {
        this.controller = null;
        this.market = null;
        this.webMarket = null;
    }
    
    public Market__c getMarket() {
        return this.market;
    }
    
    public WebMarket getWebMarket() {
        return this.webMarket;
    }
    
    public List<Market__c> getAllMarkets(){
        //List<Market__c> results = Database.query('Select id, Name, Alias__c, Image__c, Image_URL__c From Market__c ORDER By Name ASC');
        List<Market__c> results = this.userViewableMarkets;
        return results;
    }
    
    public List<NewsController.RSSItemWrapper> getNewsItems() {
        return this.market != null ? NewsController.getRSSItemsByMarket(this.market, this.numNewsItems) : null;
    }
    
    /* this field has been removed
    public List<Market__c> getFeaturedMarkets(){
        List<Market__c> results = Database.query('Select id, Name, Alias__c, Image__c, Image_URL__c From Market__c WHERE Featured_Market__c = true ORDER By Name ASC');
        
        return results;
    }
    */
    public void getDocumentation() {
        this.literature = this.getContentByTags(new String[]{'APP-Collateral-Literature', this.market.Tag__c});
        this.training = this.getContentByTags(new String[]{'APP-Collateral-Training', this.market.Tag__c});
        this.presentations = this.getContentByTags(new String[]{'APP-Collateral-Presentation', this.market.Tag__c});
    }
    
    public List<Product__c> getProductApplications() {
        if(this.market == null || this.market.Product_Applications__c == null) {
            return null;
        }
        
        List<String> products = this.market.Product_Applications__c.split('\\s*;\\s*');
        List<Product__c> productObjects = new List<Product__c>();
        
        for(String p : products){
            try {
                Product__c[] productQuery = [select Name, Id, Alias__c from Product__c where Name = :p limit 1];
                if(productQuery.size() > 0) {    // make sure not empty
                    productObjects.add(productQuery.get(0));
                }
            } catch (Exception e) {
                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
            }
        }
        
        return productObjects;
    }

    public List<WebMarket> getAllWebMarkets() {
        List<WebMarket> result = new List<WebMarket>();
        for(Market__c m : getAllMarkets()) {
            result.add(new WebMarket(m));
        }
        return result;
    }
    
    public List<Market_Event__c> getMarketEvents() {
        try {
            List<Market_Event__c> allEvents = [SELECT Related_Markets__c, Name, Link__c, Description__c, DateTime__c, Ending_DateTime__c, Location__c FROM Market_Event__c];
            if(!allEvents.isEmpty()) {
                List<Market_Event__c> result = new List<Market_Event__c>();
                for(Market_Event__c event : allEvents) {
                    if( marketTagIsInEvent( this.market.Tag__c, event ) ) {
                        result.add( event );
                    }
                }
                return result;
            }
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
        }
        return null;
    }
    
    private Boolean marketTagIsInEvent( String marketTag, Market_Event__c event ) {
        if(event.Related_Markets__c != null) {
            List<String> relatedMarkets = event.Related_Markets__c.split('\\s*;\\s*');
            for( String tag : relatedMarkets) {
                if( tag == marketTag) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public ArticleWrapper getMarketArticle1() {
        try {
        return retrieveArticle( this.market.Article1__c, this.market.Article1_Default__c, this.market.Article1_Access__c );
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
    }
    public ArticleWrapper getMarketArticle2() {
        try {
        return retrieveArticle( this.market.Article2__c, this.market.Article2_Default__c, this.market.Article2_Access__c );
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
    }
    
    /*
        PRIVATE METHODS
    */
    
    private List<ContentVersionWrapper> getContentByTags(String[] tags) {
        List<ContentVersionWrapper> result = new List<ContentVersionWrapper>();
        Set<Id> subscriptionIds = new Set<Id>();
        Set<Id> contentIds = new Set<Id>();

        List<ContentVersion> content = [select Id, ContentDocumentId, Title, TagCsv, ContentModifiedDate, ContentUrl, ContentSize from ContentVersion where isLatest = true];
        
        for (ContentVersion c : content) {
            contentIds.add(c.ContentDocumentId);
        }
        system.debug('###'+contentIds.size());
        if (contentIds.size() > 0) {
            Set<EntitySubscription> subscriptions = new Set<EntitySubscription>([SELECT Id, ParentId FROM EntitySubscription WHERE ParentId IN : contentIds AND SubscriberId =: UserInfo.getUserId() LIMIT 1000]);
            for (EntitySubscription subscription : subscriptions) { 
                subscriptionIds.add(subscription.ParentId);
            }
        }
        
        for(ContentVersion c : content) {
            
            boolean canAdd = true;
            
            for(String t : tags) {
                if(t == null || c.TagCsv == null) {
                    canAdd = false;
                    break;
                }
                canAdd = canAdd && (c.TagCsv.contains(t));
            }
            if(canAdd) {
                Boolean subscribed = subscriptionIds.contains(c.ContentDocumentId);
                ContentVersionWrapper w = new ContentVersionWrapper(c);
                w.Id = c.Id;
                w.ContentDocumentId = c.ContentDocumentId;
                w.Title = c.Title;
                w.TagCsv = c.TagCsv;
                w.ContentModifiedDate = c.ContentModifiedDate;
                w.ContentUrl = c.ContentUrl;
                w.ContentSize = c.ContentSize;
                w.IsSubscribed = subscribed;
                result.add(w);
            }
        }
        
        return result;
    }
    
    public String getMarketsDetailPage() {
        List<ASC_Page__c> ASCPages = [select Id from ASC_Page__c where name='MarketsDetail'];
        return ASCPages.size() > 0 ? ASCPages[0].Id : null;
    }
    
    public PageReference setSubscription() {
        List<EntitySubscription> subscription = [SELECT Id FROM EntitySubscription WHERE ParentId =: this.subscribeContentId AND SubscriberId =: UserInfo.getUserId() LIMIT 1000];
        
        if(this.subscribeSelected != null) {
            if (this.subscribeSelected && subscription.isEmpty()) {
                EntitySubscription es = new EntitySubscription();
                es.ParentId = this.subscribeContentId;
                es.SubscriberId = UserInfo.getUserId();
                insert es;
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '!!!' + [SELECT Id FROM EntitySubscription WHERE ParentId =: this.subscribeContentId AND SubscriberId =: UserInfo.getUserId() LIMIT 1000]));
            } else if (!this.subscribeSelected && subscription.size() > 0) {
                delete subscription;
            }
        }
        
        getDocumentation();
        return null;
    }
    
    public PageReference sendEmail() {
        emailResponse ='';
        String emailMsg = Apexpages.currentPage().getParameters().get('emailMsg');
        String emailid = Apexpages.currentPage().getParameters().get('emailid');
        String contentid = Apexpages.currentPage().getParameters().get('contentid');
        emailResponse = EmailUtil.SendEmail(emailMsg, emailid, contentid);
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, resp));
        return null;
    }
}