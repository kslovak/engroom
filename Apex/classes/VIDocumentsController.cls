public class VIDocumentsController {

    public static final String PARAM_LOCATION_ID = 'id';
    public static final String PARAM_RETURN_URL  = 'retURL';

    public static final String NONE              = '-- None --';
    public static final String TILDA             = '~';
    public static final String FOLDERS_CNFG_FLD  = 'VI_Document_Folders__c';
    
    private Account location;
    private Boolean portalUser = VIUtils.isPortalUser();
    private String returnURL;

    public List<DocumentMethods.Rec> docs {get; private set;}
    public List<SelectOption>    docTypes {get; private set;}
    public Boolean             renderDocs {get; private set;}
    public String              locationId {get; private set;}

    public String                 docType {get; set;}
    
    public VIDocumentsController() {init();}
    
    public Boolean getPortalUser() {return portalUser;}

    private void debug(String s) {
    	System.debug(LoggingLevel.INFO, 'VIDocumentsController : ' + s);
    }
    
    private void addErrMsg(String s) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, s));
    }

    private void init() {
        Map<String, String> params = ApexPages.currentPage().getParameters();
        debug('init : params : ' + params);
        returnURL = params.get(PARAM_RETURN_URL); locationId = params.get(PARAM_LOCATION_ID);
        initDocTypes(); setDocs(); //loadLocation(locationId); 
    }
/*    
    private void loadLocation(String locationId) {
        List<Account> alist = [
            SELECT Id, OwnerId, Name, AccountNumber,
                   BillingStreet, BillingCity, BillingState, BillingPostalCode, VI_Parent_Account__c,
                   AVI_Certified__c, AVI_Locator_Name__c, SAP_Customer_Group_5_Desc__c,
                   VI_Account_Type__c, VI_Channel__c
              FROM Account WHERE Id = :locationId];
        if (!alist.isEmpty()) {location = alist.get(0);} else {
            addErrMsg('Invalid or missing location ID.');}
    }
*/
    private void initDocTypes() {
    	docType = NONE; docTypes = new List<SelectOption>(); SelectOption so;
    	so = new SelectOption(NONE, NONE); docTypes.add(so);
        List<String> slist = Custom_Config_Functions.getCCList(FOLDERS_CNFG_FLD), sa;
        if (slist == null) {return;}
        for (String s : slist) {
            sa = s.split(TILDA); 
            if ((sa.size() < 2) || String.isBlank(sa[0]) || String.isBlank(sa[0])) {continue;}
            so = new SelectOption(sa[1].trim(), sa[0].trim()); docTypes.add(so);
        }
    }
    
    public void setDocs() {
    	docs = new List<DocumentMethods.Rec>(); renderDocs = false;
    	if (docType == NONE) {return;}
    	Boolean allDocs = portalUser ? false : null;
    	docs = DocumentMethods.getDocuments(docType, allDocs);
    	renderDocs = (docs != null && !docs.isEmpty()); 
    }
}