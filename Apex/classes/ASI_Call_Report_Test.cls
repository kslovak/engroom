@isTest
private class ASI_Call_Report_Test {

    static final String testAcc = 'test account';
    static final String testCas = 'test case';
    static final String testCon = 'test contact';
    static final String testInd = 'test industry';
    static final String testMat = 'test material';
    static final String testOpp = 'test opportunity';
    static final String testPrj = 'test project';

    static final String ocrRecTypeId = RecordType_Functions.LookupRecordTypeId('AQ Call Report Record Type', 'Call_Report__c');

    static final PageReference pr1 = Page.ASI_Call_Report_Page_01; 
    static final PageReference pr2 = Page.ASI_Call_Report_Email; 

    static ApexPages.StandardController sc;
    static ASI_Call_Report_Controller acrc;
    static ASI_Call_Report_Email_Controller acrec;
    
    static Map<String, String> params;

    static              ASI_Call_Report__c acr;
    static       ASI_Call_Report_Config__c crc;
    static ASI_Call_Report_Distribution__c crd;
    static            ASI_Innova_Record__c inv;
    static               ASI_NPI_Record__c npi;
    static                         Account acc;
    static                            Case cas;
    static                  Call_Report__c ocr;
    static                         Contact con;
    static                     Opportunity opp;
    static                         Risk__c rsk;
    static               Sample_Request__c smp;
    
    static void debug(String s) {
    	System.debug(LoggingLevel.INFO, 'ASI_Call_Report_Test : ' + s);
    }
    
    @testSetup static void setup() {
        Id usrId = UserInfo.getUserId();
        
    	acc = new Account(Name=testAcc, SAP_DistChannel__c='50', SAP_Sales_Org__c='1021');
    	try {insert acc;} catch(Exception e) {}
    	
    	cas = new Case(Subject=testCas, AccountId=acc.Id);
    	try {insert cas;} catch(Exception e) {}
    	
        con = new Contact(AccountId=acc.Id, Email='a@b.c', LastName=testCon);
        try {insert con;} catch(Exception e) {}
        
    	opp = new Opportunity(Name=testOpp, AccountId=acc.Id);
    	opp.StageName = 'stage1'; opp.CloseDate = Date.today().addDays(30);
    	try {insert opp;} catch(Exception e) {}
    	
        rsk = new Risk__c(Account__c=acc.Id);
        try {insert rsk;} catch(Exception e) {}
        
    	smp = new Sample_Request__c(Account__c=acc.Id);
    	try {insert smp;} catch(Exception e) {}
    	
    	inv = new ASI_Innova_Record__c(Industry_Name__c=testInd, Project_Name__c=testPrj);
        try {insert inv;} catch(Exception e) {}
    	
        npi = new ASI_NPI_Record__c(Industry_Name__c=testInd, Material_Name__c=testMat);
        try {insert npi;} catch(Exception e) {}
        
        crc = new ASI_Call_Report_Config__c(Industry_Name__c=testInd, Receiver__c=usrId, Sender__c=usrId);
        try {insert crc;} catch(Exception e) {}
        
        ocr = new Call_Report__c();
        ocr.RecordTypeId = ocrRecTypeId;
        ocr.Account__c = acc.Id; ocr.Case__c = cas.Id; ocr.Opportunity__c = opp.Id;
        ocr.Title__c = 'Test Call Report'; ocr.Date__c = Date.today();
        ocr.PAC_Industry_Segment__c = 'Test Industry'; ocr.SBU__c = 'Test SBU';
        ocr.Ashland_Rep_Performing_Call__c = UserInfo.getUserId(); 
        ocr.AQ_Attendee__c = usrId; ocr.Contact__c = con.Id; insert ocr;

        acr = new ASI_Call_Report__c();
        acr.Account__c = acc.Id; acr.Case__c = cas.Id; acr.Opportunity__c = opp.Id;
        acr.Risk__c = rsk.Id; acr.Sample_Request__c = smp.Id; acr.Old_Call_Report__c = ocr.Id;
        acr.Title__c = 'Test Call Report'; acr.Date__c = Date.today();
        acr.Industry_Name__c = 'Test Industry'; acr.SBU__c = 'Test SBU';
        acr.Ashland_Rep__c = usrId; insert acr;
        
        crd = new ASI_Call_Report_Distribution__c();
        crd.ASI_Call_Report__c = acr.Id;
        crd.ASI_Call_Report_Config__c = crc.Id;
        crd.Receiver__c = usrId; crd.Record_Key__c = acr.Id + ':' + usrId;
        insert crd;
    }
    
    static void init1() {
    	acc = [select Id, Name, SAP_DistChannel__c, SAP_Sales_Org__c 
    	         from Account                         limit 1]; debug('acc = ' + acc);

        cas = [select Id, AccountId, Subject                         
                 from Case                            limit 1]; debug('cas = ' + cas);

        con = [select Id, AccountId, Email, LastName                 
                 from Contact                         limit 1]; debug('con = ' + con);

        opp = [select Id, AccountId, Name                            
                 from Opportunity                     limit 1]; debug('opp = ' + opp);

        rsk = [select Id, Name, Account__c                           
                 from Risk__c                         limit 1]; debug('rsk = ' + rsk);

        smp = [select Id, Name, Account__c                           
                 from Sample_Request__c               limit 1]; debug('smp = ' + smp);

        inv = [select Id, Name, Industry_Name__c, Project_Name__c    
                 from ASI_Innova_Record__c            limit 1]; debug('inv = ' + inv);

        npi = [select Id, Name, Industry_Name__c, Material_Name__c   
                 from ASI_NPI_Record__c               limit 1]; debug('npi = ' + npi);

        crc = [select Id, Name                                       
                 from ASI_Call_Report_Config__c       limit 1]; debug('crc = ' + crc);

        crd = [select Id, Name, Confidential__c                      
                 from ASI_Call_Report_Distribution__c limit 1]; debug('crd = ' + crd);

        acr = [select Id, Name                                       
                 from ASI_Call_Report__c              limit 1]; debug('acr = ' + acr);

        ocr = [select Id, Name                                       
                 from Call_Report__c                  limit 1]; debug('ocr = ' + ocr);

        params = ApexPages.currentPage().getParameters(); debug('params : ' + params);
    }
    
    static void init2() {
        acr = new ASI_Call_Report__c();
        sc = new ApexPages.StandardController(acr);
        acrc = new ASI_Call_Report_Controller(sc);
        acrc.userSrchStr = UserInfo.getName();
        acrc.roleSrchStr = ''; acrc.profileSrchStr = '';
        acr = acrc.callReport; acr.Date__c = Date.today(); 
        acr.Industry_Name__c = testInd;
        acr.Ashland_Rep__c = UserInfo.getUserId();
        acr.Title__c = 'test title'; acr.SBU__c = 'test sbu';
    }
    
    static void selectAttendees() {
        for (ASI_Call_Report_Attendee a : acrc.attendees2) {a.selected = true;}
    }
    
    static void selectContacts() {
        for (ASI_Call_Report_Contact a : acrc.contacts2) {a.selected = true;}
    }
    
    static void selectReceivers() {
        for (ASI_Call_Report_Distribution a : acrc.rcvrs2) {a.selected = true;}
    }
    
    static void selectInnovas() {
        for (ASI_Innova_Record a : acrc.innovas2) {a.selected = true;}
    }
    
    static void selectNpis() {
        for (ASI_NPI_Record a : acrc.npis2) {a.selected = true;}
    }

    static testmethod void test01() {}

//*    
    static testmethod void test02() {
        acrc = new ASI_Call_Report_Controller();
    	Test.setCurrentPage(pr1); init1(); 
    	init2(); // to test controller with no parameters
        params.put('casid', cas.Id); init2(); params.remove('casid');
        params.put('conid', con.Id); init2(); params.remove('conid');
        String msg = 'test msg'; List<String> msgs = new List<String>{msg};
        acrc.addErrMsgs(msgs); acrc.addReqMsg(msg);
        acrc.addInfoMsg(msg); acrc.addWarnMsg(msg);
    } 
    
    static testmethod void test03() {
        Test.setCurrentPage(pr1); init1(); 
        params.put('oppid', opp.Id); init2(); params.remove('oppid');
        params.put('rskid', rsk.Id); init2(); params.remove('rskid');
    } 
    
    static testmethod void test04() {
        Test.setCurrentPage(pr1); init1(); 
        params.put('smpid', smp.Id); init2(); params.remove('smpid');
    } 
    
    static testmethod void test05() {
        Test.setCurrentPage(pr1); init1(); 
        params.put('accid', acc.Id); init2();
        acrc.initAction1(); acrc.getStdPageRef(); acrc.gotoRetUrl();

        acrc.searchAttendees(); selectAttendees();
        acrc.selectAttendees(); acrc.deleteAttendees(); 
        acrc.searchAttendees(); selectAttendees();

        acrc.searchContacts(); selectContacts();
        acrc.selectContacts(); acrc.deleteContacts(); 
        acrc.searchContacts(); selectContacts();
        
        acrc.searchReceivers(); selectReceivers();
        //acrc.selectReceivers(); acrc.deleteReceivers(); 
        //acrc.searchReceivers(); selectReceivers();
        acrc.selectReceivers(); acrc.onConfidentialChange();
        
        acrc.searchInnovas(); selectInnovas();
        acrc.selectInnovas(); acrc.deleteInnovas(); 
        acrc.searchInnovas(); selectInnovas();
        
        acrc.searchNpis(); selectNpis();
        acrc.selectNpis(); acrc.deleteNpis(); 
        acrc.searchNpis(); selectNpis();
        
        acrc.showNewContactPB(); 
        acrc.newContact.Email = con.Email;
        acrc.newContact.LastName = con.LastName;
        acrc.addNewContact();
        
        for (ASI_Call_Report_Distribution a : acrc.rcvrs1) {a.senderDefault = true;}

        acrc.saveCallReport();
        
        ASI_Call_Report_Methods.getAccount(acc.Id);
        ASI_Call_Report_Methods.getCallReport(acr.Id);
        ASI_Call_Report_Methods.getAttendees(acr.Id);
        ASI_Call_Report_Methods.getCallReportReceivers(acr.Id);
        ASI_Call_Report_Methods.getContacts(acr.Id);
        ASI_Call_Report_Methods.getInnovas(acr);
        ASI_Call_Report_Methods.getNpis(acr);
        ASI_Call_Report_Methods.getReceivers(acr);
        ASI_Call_Report_Methods.clone(acr, acrc.attendees1, acrc.contacts1, acrc.rcvrs1);

        acrc.onIndustryChange(); acrc.sendEmailAction1(); acrc.sendEmailAction2();
    }
    
    static testmethod void test06() {
        Test.setCurrentPage(pr2); init1(); 
        acrec = new ASI_Call_Report_Email_Controller();
        acrec.callReportId = acr.Id;
        debug('' + acrec.callReport);
        debug('' + acrec.acrAccount);
        debug('' + acrec.acrDistributor);
        debug('' + acrec.acrEndUseCust);
        debug('' + acrec.ashlandRep);
        debug('' + acrec.acrCase);
        debug('' + acrec.acrCompetitor);
        debug('' + acrec.acrOpportunity);
        debug('' + acrec.acrRisk);
        debug('' + acrec.acrSampleRequest);
        debug('' + acrec.attendees1);
        debug('' + acrec.contacts1);
        debug('' + acrec.distributions1);
        debug('' + acrec.npis1);
        debug('' + acrec.innovas1);
        debug('' + acrec.confidential);
        debug('' + acrec.russiaCis);
        debug('' + acrec.publish);
        debug('' + acrec.attendees1Empty);
        debug('' + acrec.contacts1Empty);
        debug('' + acrec.distributions1Empty);
    }

    static testmethod void test07() {
        init1(); String procStep = ''; Integer recLimit = 1;
        Database.BatchableContext bc = null; List<Id> recIds = null;
        ASI_Call_Report_Batchable b = new ASI_Call_Report_Batchable(); 
        b.start(bc); b.execute(bc, null); b.finish(bc); 
        ASI_Call_Report_Batchable.createASICallReports(recIds, recLimit);
        ASI_Call_Report_Batchable.createAttachments(recIds, recLimit);

        List<Call_Report__c> alist = new List<Call_Report__c>{ocr};
        procStep = ASI_Call_Report_Batchable.CREATE_ASI_CALL_REPORTS;
        b = new ASI_Call_Report_Batchable(procStep); b.execute(bc, alist);
        
        List<ASI_Call_Report__c> blist = new List<ASI_Call_Report__c>{acr};
        procStep = ASI_Call_Report_Batchable.CREATE_NOTES_ATTACHMENTS;
        b = new ASI_Call_Report_Batchable(procStep); b.execute(bc, blist);
    } 
//*/
    
    static testmethod void test08() {
        init1(); 
        acr.Confidential__c = true;  update acr;
        crd.Confidential__c = true;  update crd;
        crd.Confidential__c = false; update crd;
        acr.Confidential__c = false; update acr;
    }
}