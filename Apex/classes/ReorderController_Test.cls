@isTest
private class ReorderController_Test {

    static SAP_Sales_Org__c so;
    static SAP_Distribution_Channel__c dc;
    static Account shipTo, soldTo, cHerky, invalidAcc;
    static Set<String> accountIds;
    static Material_Sales_Data2__c msd2;
    static Open_Orders__c openOrder;
    static String matnum = '010000000123456789';
    static String salesOrg = '1234';
    static String distChan = '12';
    static String salesArea = '00000000';
    static String plantCode = '1234';
    static String langCode = 'E';
    static String salesOrderNumber = 'TestOrder1';
    static String lineItemNumber = '000';

    static void createTestData() {
        createAccounts();
        createMaterial();
        createOpenOrder();
    }

    static void createAccounts() {

        so = new SAP_Sales_Org__c();
        so.Sales_Org_Code__c = salesOrg;
        so.Language_Code__c = langCode;
        insert so;

        dc = new SAP_Distribution_Channel__c();
        dc.Distribution_Channel_Code__c = distChan;
        dc.Language_Code__c = langCode;
        insert dc;

        cHerky = new Account(Name = 'CHerky');
        cHerky.Account_Number__c = 'CHerky';
        insert cHerky;

        soldTo = new Account(Name = 'SoldTo');
        soldTo.Account_Number__c = 'SoldTo';
        soldTo.ParentId = cHerky.Id;
        insert soldTo;

        shipTo = new Account(Name = 'ShipTo');
        shipTo.Account_Number__c = 'ShipTo';
        shipTo.SAP_DistChannel__c = dc.Distribution_Channel_Code__c;
        shipTo.SAP_Sales_Org__c = so.Sales_Org_Code__c;
        insert shipTo;

        invalidAcc = new Account(Name = 'invalidAcc');
        invalidAcc.Account_Number__c = 'invalidAcc';
        insert invalidAcc;

        accountIds = new Set<String>{cHerky.Id, soldTo.Id, shipTo.Id};

    }

    static void createMaterial() {
        SAP_Plant__c sp = new SAP_Plant__c();
        sp.Plant_Code__c = plantCode;
        insert sp;

        Material_General_Data2__c mgd2 = new Material_General_Data2__c();
        mgd2.Material_Number__c = matnum;
        insert mgd2;
        msd2 = getTestMsd2(mgd2, sp);
    }

    static Material_Sales_Data2__c getTestMsd2(Material_General_Data2__c mgd2,
                                               SAP_Plant__c sp) {

        Material_Plant2__c mp2 = getTestMp2(mgd2, sp);
        Product_Hierarchy2__c ph2 = getTestProdHerky2();

        msd2 = new Material_Sales_Data2__c();
        msd2.Material_Number__c = matnum;
        msd2.Sales_Org_Code__c = salesOrg;
        msd2.Dist_Channel_Code__c = distChan;
        msd2.Record_Key__c = matnum + salesOrg + distChan;
        msd2.SalesOrg_Code__r = so;
        msd2.DistChannel_Code__r = dc;
        msd2.Material_General_Data__r = mgd2;
        msd2.Product_Hierarchy__r = ph2;
        insert msd2;
        return msd2;
    }

    static Material_Plant2__c getTestMp2(Material_General_Data2__c mgd2,
                                         SAP_Plant__c sp) {
        Material_Plant2__c mp2 = new Material_Plant2__c();
        mp2.Material_Number__c = matnum;
        mp2.Plant_Code__c = plantCode;
        mp2.SalesOrg_Code__c = salesOrg;
        mp2.Record_Key__c = matnum + plantCode + salesOrg;
        mp2.Material_General_Data__r = mgd2;
        mp2.SAP_Plant__r = sp;
        mp2.SAP_Sales_Org__r = so;
        insert mp2;
        return mp2;
    }

    static Product_Hierarchy2__c getTestProdHerky2() {
        Product_Hierarchy__c ph1 = new Product_Hierarchy__c();
        ph1.Language_Code__c = 'E';
        ph1.Product_Hierarchy_Code__c = '!';
        insert ph1;

        Product_Hierarchy__c ph2 = new Product_Hierarchy__c();
        ph2.Language_Code__c = 'E';
        ph2.Product_Hierarchy_Code__c = '!@#';
        insert ph2;

        Product_Hierarchy__c ph3 = new Product_Hierarchy__c();
        ph3.Language_Code__c = 'E';
        ph3.Product_Hierarchy_Code__c = '!@#$%^';
        insert ph3;

        Product_Hierarchy__c ph4 = new Product_Hierarchy__c();
        ph4.Language_Code__c = 'E';
        ph4.Product_Hierarchy_Code__c = '!@#$%^&*(';
        insert ph4;

        Product_Hierarchy__c ph5 = new Product_Hierarchy__c();
        ph5.Language_Code__c = 'E';
        ph5.Product_Hierarchy_Code__c = '!@#$%^&*()!@';
        insert ph5;

        Product_Hierarchy__c ph6 = new Product_Hierarchy__c();
        ph6.Language_Code__c = 'E';
        ph6.Product_Hierarchy_Code__c = '!@#$%^&*()!@#$%';
        insert ph6;

        Product_Hierarchy__c ph7 = new Product_Hierarchy__c();
        ph7.Language_Code__c = 'E';
        ph7.Product_Hierarchy_Code__c = '!@#$%^&*()!@#$%^&*';
        insert ph7;

        Product_Hierarchy2__c pherky2 = new Product_Hierarchy2__c();
        pherky2.Product_Hierarchy__c = ph7.Product_Hierarchy_Code__c;
        pherky2.Product_Hierarchy1__r = ph1;
        pherky2.Product_Hierarchy2__r = ph2;
        pherky2.Product_Hierarchy3__r = ph3;
        pherky2.Product_Hierarchy4__r = ph4;
        pherky2.Product_Hierarchy5__r = ph5;
        pherky2.Product_Hierarchy6__r = ph6;
        pherky2.Product_Hierarchy7__r = ph7;
        insert pherky2;
        return pherky2;
    }

    static void createOpenOrder() {
        openOrder = new Open_Orders__c();
        openOrder.ShipTo__c = shipTo.Id;
        openOrder.SoldTo__c = soldTo.Id;
        openOrder.Sales_Order_Number__c = salesOrderNumber;
        openOrder.Line_Item_Number__c = lineItemNumber;
        openOrder.Key__c = salesOrderNumber + lineItemNumber;
        openOrder.Material__r = msd2;
        insert openOrder;
    }

    static void debug(String s) {
        System.debug(LoggingLevel.INFO, '>>>>>>>>> ' + s);
    }

    static testMethod void test01() {
        Test.startTest();
        createTestData();

        ApexPages.StandardController sc;
        ApexPages.currentPage().getParameters().put('Id', openOrder.Id);

        ReorderController c = new ReorderController(sc);

        Reorder_Line_Item__c li = new Reorder_Line_Item__c(Quantity__c = 11);
        li.Material2__r = msd2;
        insert li;
        c.newRecMat = li;
        c.addMat();

        //c.OnPlantChange();

        c.delMat();
        c.delReorder();
        c.saveReorders();

    }

    static testMethod void test02() {
        Test.startTest();
        createTestData();

        //Test if accessed from account page
        ApexPages.StandardController sc;
        ApexPages.currentPage().getParameters().put('AccountId', openOrder.ShipTo__c);
        ApexPages.currentPage().getParameters().put('hasOrders', 'false');

        ReorderController c = new ReorderController(sc);
        PageReference pg1 = c.callReorderPage();
        PageReference pg2 = c.callIntermediatePage();
        c.SortField = 'Id'; c.SortMasterList(); c.saveReorders();

        Reorder_Line_Item__c li = new Reorder_Line_Item__c(Quantity__c = 11);
        c.accountId = shipTo.Id; c.materialName = msd2.Material_Number__c;
        c.newRecMat = li; c.addMat(); c.addReorder(); c.saveReorders();
    }

}