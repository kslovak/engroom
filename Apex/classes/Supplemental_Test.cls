@isTest
private class Supplemental_Test {
    
    static Account acc, dst;
    static Material_Sales_Data2__c mat;
    static List<String> csvRates, csvClaims;
    
    static void setup() {
        String s, matNum = '000000000000001234', matKey = matNum + '100040'; 
        dst = new Account(Name='Test Distributor'); insert dst;
        dst = [select Id, AccountNumber from Account where Id = :dst.Id];

        acc = new Account(Name='Test Account',ParentId=dst.Id); insert acc;
        acc = [select Id, AccountNumber from Account where Id = :acc.Id];
        
        mat = new Material_Sales_Data2__c(Dist_Channel_Code__c='40',
                                          Material_Number__c=matNum,
                                          Record_Key__c=matKey,
                                          Sales_Org_Code__c='1000');
        insert mat;
        
        s = dst.AccountNumber + ',' + acc.AccountNumber + ',' + matNum + ',,1/1/2015,12/31/2015,1,.1';
        csvRates = new List<String>{s};

        s = dst.AccountNumber + ',' + acc.AccountNumber + ',' + matNum + ',,6/1/2015,1234,10,1,.1';
        csvClaims = new List<String>{s};
    }

    static testMethod void test01() {
        setup(); Test.startTest();
        Supplemental_Methods.upsertRates(csvRates, '');
        Supplemental_Methods.upsertClaims(csvClaims, '');
    }
}