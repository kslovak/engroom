public with sharing class NewsController extends PageController {
    
    private Set<Market__c> selectedMarkets;
    private List<RSSItemWrapper> RSSItems;
    
    private static final Integer numItemsToDisplay = 16;
    
    // for method setMarketFilter()
    public Id filterMarketId {get; set;}
    public Boolean addNewMarketFilters {get; set;}
    
    public NewsController() {
        List<Market__c> marketFilterList = getMarketFilterOptions();
        if( marketFilterList != null ) {
            this.selectedMarkets = new Set<Market__c>( marketFilterList );
        } else {
            this.selectedMarkets = new Set<Market__c>();
        }
        this.RSSItems = new List<RSSItemWrapper>();
        this.concatenateRSSItems();
    }
    
    public List<RSSItemWrapper> getRSSItems() {
        return this.RSSItems;
    }
    
    public List<Market__c> getMarketFilterOptions() {
        List<Market__c> result = retrieveUserMarkets();
            // can't filter SOQL on textarea fields, so do it here:
        result = filteredMarketFilterList(result);
        return !result.isEmpty() ? result : null;
    }
    
    public PageReference setMarketFilter() {
        
        if(this.filterMarketId != null && this.addNewMarketFilters != null) {
            if(this.addNewMarketFilters) {
                this.selectedMarkets.add( getMarketById(this.filterMarketId) );
            } else {

                for(Market__c selectedMarket : this.selectedMarkets) {
                    if(selectedMarket.Id == this.filterMarketId) {
                        this.selectedMarkets.remove( selectedMarket );
                    }
                }
            }
        }
        this.concatenateRSSItems();
        return null;
    }
    
    public static List<RSSItemWrapper> getRSSItemsByMarket(Market__c market, Integer numItems) {
        List<RSSItemWrapper> result = new List<RSSItemWrapper>();
        
        if(market.RSS_Feeds__c != null && market.RSS_Feeds__c != '') {
            for(String feed : market.RSS_Feeds__c.split('\\s*;\\s*')) {
                RSS.channel channel = RSS.getRSSData(feed, numItems);
                
                for(RSS.item item : channel.items) {
                    RSSItemWrapper newItem = new RSSItemWrapper(item);
                    newItem.image = channel.image;
                    result.add( newItem );
                    
                }
            }
            result.sort();
            result = trimList( result, numItems );
        }
        
        return result;
    }
    
    /*
        PRIVATE METHODS
    */
    
    private List<Market__c> filteredMarketFilterList(List<Market__c> listToBeFiltered) {
        List<Market__c> result = new List<Market__c>();
        
        if(listToBeFiltered != null && !listToBeFiltered.isEmpty()) {
            for(Integer i = 0; i < listToBeFiltered.size(); i++) {
            
                Market__c market = listToBeFiltered.get(i);
                if(market.RSS_Feeds__c != '' && market.RSS_Feeds__c != null) {
                    result.add(market);
                }
                
            }
        }
        
        return result;
    }
    
    private Market__c getMarketById(String Id) {
        Market__c result = null;
        try {
            result = [select Id, Alias__c, RSS_Feeds__c from Market__c where Id = :Id];
        } catch(DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
        }
        
        return result;
    }
    
    private void concatenateRSSItems() {
        
        if(this.selectedMarkets != null && !this.selectedMarkets.isEmpty()) {
            Integer numItemsPerMarket = numItemsToDisplay / this.selectedMarkets.size();
            List<RSSItemWrapper> masterList = new List<RSSItemWrapper>();
            
            for(Market__c marketFilter : this.selectedMarkets) {
                masterList.addAll( getRSSItemsByMarket(marketFilter, numItemsPerMarket) );
            }
            
            masterList.sort();    // sorts all by date
            this.RSSItems = masterList; // overwrites old list.
           
        } else {
            // no filters selected; empty the list
            this.RSSItems = null;
        }
    }
    
    private static List<RSSItemWrapper> trimList(List<RSSItemWrapper> listToTrim, Integer numItems) {        
        List<RSSItemWrapper> trimmedResults = new List<RSSItemWrapper>();
        if(listToTrim != null && !listToTrim.isEmpty()) {
            for(Integer i = 0; i < numItems && i < listToTrim.size(); i++) {
                trimmedResults.add( listToTrim.get(i) );
            }
        }
        return trimmedResults;
    }
    
    /*
        UTILITY CLASSES
    */
    
    public class RSSItemWrapper implements Comparable {
        
        private RSS.item RSSItem;
        public RSS.image image {get; set;}
        
        public RSSItemWrapper(RSS.item item) {
            this.RSSItem = item;
        }
        
        public RSS.item getRSSItem() {
            return this.RSSItem;
        }
        
        public Integer compareTo(Object RSSItem2) {
            return this.RSSItem.compareTo( ((RSSItemWrapper)RSSItem2).RSSItem );
        }
    }
}