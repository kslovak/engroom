@isTest
private class Account_Functions_Test {

  private static void debug(String s) {System.debug(LoggingLevel.INFO, 'Account_Functions_Test : ' + s);}

  static testmethod void test01() {
    Test.startTest();
    testAccountAgeInDays();
    testAccountDelete('System Administrator');
    testAccountDelete('Aqualon - Standard User');
    Test.stopTest();
  }
  
/*    
    static testMethod void test02() {
        Account a1 = new Account(Name='A1'), a2 = new Account(Name='A2');
        List<Account> alist = new List<Account>{a1, a2}; insert alist;
        List<Pricing_Condition> plist = PricingConditionFunctions_Test.getPclist();
        Pricing_Condition__c p1 = plist[0].pricingCondition; 
        p1.Account__c = a2.Id; update p1;
        merge a1 a2;
        for (Pricing_Condition__c p : [
            select Id, Name from Pricing_Condition__c
             where Account__c = :a1.Id]) {debug('p = ' + p);}
    }
*/

  private static void testAccountAgeInDays() {

    Account[] accts = new Account[]{}; 
    Account acct1 = new Account();
    Account acct2 = new Account();
    
    acct1.Name = 'Test 1';
    acct2.Name = 'Test 2';
    
    acct1.RecordTypeId = RecordType_Functions.LookupRecordTypeId('Distribution Prospect','Account');
    acct1.Type = 'Prospect';
    acct2.RecordTypeId = RecordType_Functions.LookupRecordTypeId('Distribution Customer','Account');
    acct2.Type = 'Customer';
    
    accts.add(acct1);
    accts.add(acct2);
    
    insert accts;
    

    Double age = Account_Functions.Calculate_Account_Age_in_Days(acct1.id);
    Boolean isnew1 = Account_Functions.isNewProspect(acct1.id);
    Boolean isnew2 = Account_Functions.isNewProspect(acct2.id);
  
    debug('isnew1 = ' + isnew1);
    debug('isnew2 = ' + isnew2);
    
  }
  
  private static void testAccountDelete(String profileName) {
    
    Id prospectRecordTypeId = [select Id from RecordType 
                                where Name = 'Aqualon Prospect' 
                                  and sObjectType = 'Account' limit 1].Id;
    
    Id accountRecordTypeId  = [select Id from RecordType 
                                where Name = 'Aqualon Customer' 
                                  and sObjectType = 'Account' limit 1].Id;
    
    Account prospectAccount = new Account();
    prospectAccount.Name = 'Prospect';
    prospectAccount.RecordTypeId = prospectRecordTypeId;
    
    Account regularAccount = new Account();
    regularAccount.Name = 'Account';
    regularAccount.RecordTypeId = accountRecordTypeId;
    
    Account[] aa = new Account[] {prospectAccount, regularAccount};
    insert aa;
    Account_Functions.printAccounts(aa);
    
    Profile p = [select id from profile 
                  where name=:profileName];
                  
	User u;

    User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
	System.runAs ( thisUser ) {
		u = TestObjects.newUser(new Map<String,String>{'ProfileId'=>p.Id}); 
	}
  
	/*                  
    User u = [select id from user 
               where profileid = :p.Id
                 and isActive = true 
               limit 1];
    */
    
    if (u != null) {
        System.runAs(u) {
            try {
                delete prospectAccount;
                // merge regularAccount prospectAccount;
            } catch(Exception e) {
                debug(e.getMessage());
                //validateAndLogForDelete(aa);
            }
        }
    }
    
  }

}