public with sharing class ContentViewController {

    public ContentVersion contentToUpload {get;set;}
    Id memberId;
    public Blob fileContent {get;set;}
//ApexPages.StandardController controller
    public ContentViewController () {
        //memberId = controller.getId();
        contentToUpload = new ContentVersion();
    }
    

    // list crm content
    public List<ContentVersion> getContentVersions() {
    // TODO: get published date
        return [select id, Title, ContentSize, Owner.Name, ContentModifiedDate, TagCsv
        from ContentVersion
        Where IsLatest = true];
    }
    
    // attach user info
    public User getCurrentUser() {
        return null; //[select Name, Email, UserRole from User where User.Username = 'rcerami']; //database.query(sql);
    }
    
    // upload crm content
    
    public PageReference uploadContents() {
        List<ContentWorkSpace> CWList = [SELECT Id, Name From ContentWorkspace WHERE Name = 'Trellist Test'];
        contentToUpload.VersionData = fileContent;
        insert contentToUpload;
        
        contentToUpload = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentToUpload.Id];
        
        ContentWorkspaceDoc cwd = new ContentWorkspaceDoc();
        cwd.ContentDocumentId = contentToUpload.ContentDocumentId;
        cwd.ContentWorkspaceId = CWList.get(0).Id;
        insert cwd;
        
        PageReference pg = new PageReference('/' + memberId);
        pg.setRedirect(true);
        return pg;
    }
}