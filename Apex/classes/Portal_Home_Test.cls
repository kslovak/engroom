@isTest
private class Portal_Home_Test {

    static String username = 'testuser@example.ashland.com';
    static String password = 'testpasswd';
    static Portal_Home_Controller phc;
    static Account testAcc;
    static Contact testCon;
    
    static void createUser() {
        testAcc = new Account(Name='testAcc');
        insert testAcc;
        
        testCon = new Contact(LastName='testCon');
        testCon.AccountId = testAcc.Id;
        insert testCon;

        String profileName = Cummins_Portal_Controller.PROFILE_NAME;
        Profile p;
        try {p = [SELECT Id FROM Profile WHERE Name = :profileName];}
        catch(Exception e) {return;}
        
        User u = new User(Alias = 'testuser', Email = username,
                            EmailEncodingKey = 'UTF-8', LastName = 'testuser',
                            LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                            ProfileId = p.Id, ContactId = testCon.Id, 
                            Ashland_Employee_Number__c = username,
                            TimeZoneSidKey = 'America/Los_Angeles', 
                            Username = username);
        try {insert u;} catch(Exception e) {return;}
    }
    
    static void setup() {
        createUser();
        phc = new Portal_Home_Controller();
    }
    
    static testMethod void test01() {
        setup(); Test.startTest();
        phc.initAction(); phc.addInfoMsg('test');
    }
}