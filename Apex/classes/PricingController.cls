public class PricingController {

    private static final String OBJECT_NAME = 'Pricing__c';
    private static final String APPROVED = 'Approved';
    private static final String ENTERED_IN_SAP = 'Entered in SAP';
    private static final String COMMA = ',';
    
    private static PageReference pageRef;
    
    private String pricingId;
    
    public Pricing__c pricing {get; set;}
    
    public List<PricingFunctions.Pricing> pricingPendingApproval {get; set;}

    public Integer pageBlockNumber {get; set;}
    public Boolean renderSendEmailButton {get; set;}
    public Boolean emailCcOnly {get; set;}
    public String emailCc {get; set;}

    public Boolean isApproved {get; set;}

    public PricingController() {}
    
    public PricingController(ApexPages.Standardcontroller sc) {
        try {
            pricingId = ApexPages.currentPage().getParameters().get('Id');
            if (pricingId != null) {
                pricing = PricingFunctions.getPricing(pricingId);
            } else {
                pricing = new Pricing__c();
            }
            isApproved = (pricing.Status__c == APPROVED);
            System.debug(pricing);
        } catch(Exception e) {}
    }
    
    public PageReference submitForApproval() {
        PricingFunctions.upsertAndSubmitForApproval(pricing);
        pageRef = new PageReference('/'+pricing.Id);
        return pageRef;
    }
    
    public PageReference updatePricing() {
        if (pricing.Status__c == APPROVED) {
        	pricing.Status__c = ENTERED_IN_SAP; pricing.Approvers__c = '';
        }
        update pricing;
        return gotoPricingPage();
    }
    
    public PageReference gotoPricingPage() {
        pageRef = new PageReference('/'+pricingId);
        pageRef.setRedirect(true);
        System.debug(pageRef);
        return pageRef;
    }
    
    public void setPricingPendingApproval() {
        pricingPendingApproval = PricingFunctions.getPricingPendingApproval();
        pageBlockNumber = (pricingPendingApproval != null && 
                           pricingPendingApproval.size() > 0)? 2 : 1;
        renderSendEmailButton = false;
        if (Pricing_Security__c.getInstance() != null) {
            renderSendEmailButton = Pricing_Security__c.getInstance().Show_Send_PC_Approvals_Email_Button__c;
        }
    }
    
    public PageReference gotoPageBlock3() {
        pageBlockNumber = 3;
        return null;
    }

    public PageReference sendEmails() {
        List<Pricing__c> pcs = PricingFunctions.getSubmittedPricings();
        if (pcs == null || pcs.isEmpty()) {
            return null;
        }
        List<String> ccIds = null;
        if (emailCc != null && emailCc.trim().length() > 0) {
            String s = emailCc.trim();
            s = s.replaceAll('\n', '');
            ccIds = s.split(COMMA);
        } else if (emailCcOnly) {
            return null;
        }
        PricingFunctions.sendApprovalPendingMails(pcs, emailCcOnly, ccIds);
        pageBlockNumber = 4;
        return null;
    }
    
    public class Pricing {
        public Pricing__c pc        {get; set;}
        public Boolean selected     {get; set;}
        public String accountNumber {get; set;}
        public String notesCount    {get; set;}
        public Pricing(Pricing__c p) {
        	pc = p; notesCount = ''; selected = false;
        	accountNumber = String_Functions.removeLeadingZeros(p.Account_Number__c);
        	if (p.NotesAndAttachments != null && !p.NotesAndAttachments.isEmpty()) {
        		notesCount = ''+p.NotesAndAttachments.size();
        	}
        }
    }
    
    List<Pricing> plist;
    List<Pricing__c> slist;
    
    public String userComments {get; set;} 
    public Boolean allSelected {get; set;}
    
    private void init() {
        plist = new List<Pricing>();
        slist = new List<Pricing__c>();
        allSelected = false;
        userComments = '';
    } 
    
    private Boolean isNull(String s) {
        return (s == null || s.trim().length() == 0);
    }

    public List<Pricing> getPricings(Set<String> pids) {
        String qry = PricingFunctions.QRY1 + 
                   ' where Id in :pids order by ' +
                   ' Account_SAP_Sales_District_Desc__c, ' +
                   ' Account_SAP_Sales_Office_Desc__c, ' +
                   ' Account_SAP_Sales_Group_Desc__c, ' +
                   ' SAP_Customer_Group_Desc__c, Account__r.Name ' +
                   ' limit 1000';
        List<Pricing__c> pcs = Database.query(qry);
        if (pcs == null || pcs.isEmpty()) {return null;}
        init();
        for (Pricing__c p : pcs) {Pricing pc = new Pricing(p); plist.add(pc);}
        return plist;
    }
    
    public List<Pricing> getPricingsPendingApproval() {
        Set<String> pids = Custom_Approval_Functions.getPendingApprovalObjectIds(OBJECT_NAME);
        if (pids == null || pids.isEmpty()) {return null;}
        return getPricings(pids);
    }
    
    private Set<String> getSelectedObjectIds() {
        Set<String> ss = new Set<String>();
        for (Pricing p : plist) {if (p.selected) {ss.add(p.pc.id);}}
        return ss;
    }
    
    public PageReference approveSelectedList() {
        Set<String> oids = getSelectedObjectIds();
        updateApproverComments();
        Custom_Approval_Functions.approveObjects(oids, userComments);
        return null;
    }
    
    public PageReference rejectSelectedList() {
        if (isNull(userComments)) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                         'Reject Comments are required');
            ApexPages.addMessage(msg);
            return null;
        }
        Set<String> oids = getSelectedObjectIds();
        updateApproverComments();
        Custom_Approval_Functions.rejectObjects(oids, userComments);
        return null;
    }
    
    private void updateApproverComments() {
        if (isNull(userComments)) {return;}
        String dt = Datetime.now().format(), un = UserInfo.getName();
        String cs = dt + ' : ' + un + ' : ' + userComments + '\n', ac;
        List<Pricing__c> pcs = new List<Pricing__c>();
        for (Pricing p : plist) {
            if (p.selected) {
                ac = p.pc.Approval_Rejection_Comments__c;
                if (isNull(ac)) {ac = '';} ac = cs + ac;
                p.pc.Approval_Rejection_Comments__c = ac;
                pcs.add(p.pc);
            }
        }
        if (!pcs.isEmpty()) {try {update pcs;} catch(Exception e) {}}
    }
    
}