global with sharing virtual class PageController {

    //public String a1_target { get; set; }

    private ASC_Page__c ASCPage;
    private String pid;
    
    protected List<Group> allUserGroups;
    protected List<Product__c> userViewableProducts;
    protected List<Market__c> userViewableMarkets;
    protected ArticleWrapper article1, article2, article3;

    global PageController() {
        String pageName = '';
        try {
            pageName = ApexPages.CurrentPage().getUrl();
            String[] directories = pageName.split('\\?')[0].split('/');
            pageName = directories[directories.size() - 1];
        } catch (exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'error: ' + e.getMessage()));
        }

        String requestedPid = ApexPages.currentPage().getParameters().get('pid');
        if(pageName != '') {    // TODO: prevent duplicate SOQL queries
            this.pid = requestedPid;
            try {
                // pull all fields possibly used by a page (including related fields)
                this.pid = this.getPageIdFromName(pageName);
                this.ASCPage = [select Id, Page_Title__c, Subtitle__c, Main_Text__c,
                        Market_Image__c, Product_Image__c, Experts_Image__c, Content_Left__c, Content_Right__c,
                        Article1__c, Article2__c, Article3__c,
                        Article1_Default__c, Article2_Default__c, Article3_Default__c,
                        Article1_Access__c, Article2_Access__c, Article3_Access__c,
                        Featured_Product_1__c, Featured_Product_2__c, Featured_Product_3__c,
                            Featured_Product_1__r.Alias__c, Featured_Product_1__r.Preview_Description__c, Featured_Product_1__r.Hero_Image__c,
                            Featured_Product_2__r.Alias__c, Featured_Product_2__r.Preview_Description__c, Featured_Product_2__r.Hero_Image__c,
                            Featured_Product_3__r.Alias__c, Featured_Product_3__r.Preview_Description__c, Featured_Product_3__r.Hero_Image__c
                    from ASC_Page__c
                    where ASC_Page__c.Id = :pid
                    limit 1];
            } catch(exception e) {
                this.ASCPage = new ASC_Page__c();
            }
        }
        this.allUserGroups = UserUtil.getGroupsForUser( UserInfo.getUserId() );
        this.userViewableProducts = this.retrieveUserProducts();
        this.userViewableMarkets = this.retrieveUserMarkets();
        
        this.article1 = null;
        this.article2 = null;
        this.article3 = null;
        
        /* DEBUG
        for(Group grp : UserUtil.getGroupsForUser( UserInfo.getUserId() )) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'group: ' + grp.DeveloperName));
        }*/
    }

    global ASC_Page__c getPage() {
        return this.ASCPage;
    }

    public Map<String,String> getASCPagesMap () {
        Map<Id, ASC_Page__c> idToObject = new Map<Id, ASC_Page__c>([SELECT Id, Name from ASC_Page__c]);
        Map<String, String> nameToId = new Map<String, String>();
        for (Id key : idToObject.keySet()){
            nameToId.put(idToObject.get(key).Name, key);
        }
        return nameToId;
    }

    global List<NavProduct> getNavProductList() {
        List<NavProduct> result = new List<NavProduct>();
        
        //List<Product__c> products = [select Alias__c, Id from Product__c];
        List<Product__c> products = this.userViewableProducts;
        for(Product__c product : products) {
            result.add(new NavProduct(product.Alias__c,product.Id));
        }
        
        result.sort();
        return result;
    }

    global List<NavMarket> getNavMarketList() {
        List<NavMarket> result = new List<NavMarket>();
        //List<Market__c> markets = [select Alias__c, Id from Market__c];
        List<Market__c> markets = this.retrieveUserMarkets();
        for(Market__c market : markets) {
            result.add(new NavMarket(market.Alias__c,market.Id));
        }
        
        result.sort();
        return result;
    }

    global ConnectApi.UnreadConversationCount getUnreadConversationCount() {
        return ConnectApi.ChatterMessages.getUnreadCount();
    }

    global PageReference pageAuthHandler() {
        PageReference result = null;

        if(UserInfo.getUserType() == 'Guest') {
            String startUrl = System.currentPageReference().getParameters().get('startURL');
            result = new PageReference(Site.getPathPrefix() + '/CommunitiesLogin?startURL=' + EncodingUtil.urlEncode(startURL, 'UTF-8'));
        }

        return result;
    }
    
    public ArticleWrapper getArticle1() {
        ArticleWrapper result = this.article1;
        if(result == null) {
            result = retrieveArticle( this.ASCPage.Article1__c, this.ASCPage.Article1_Default__c, this.ASCPage.Article1_Access__c );
        }
        return result;
    }
    public ArticleWrapper getArticle2() {
        ArticleWrapper result = this.article2;
        if(result == null) {
            result = retrieveArticle( this.ASCPage.Article2__c, this.ASCPage.Article2_Default__c, this.ASCPage.Article2_Access__c );
        }
        return result;
    }
    public ArticleWrapper getArticle3() {
        ArticleWrapper result = this.article3;
        if(result == null) {
            result = retrieveArticle( this.ASCPage.Article3__c, this.ASCPage.Article3_Default__c, this.ASCPage.Article3_Access__c );
        }
        return result;
    }
    
    public Boolean getCanViewFeatProd1() {
        return listProductContainsId(this.userViewableProducts, this.getPage().Featured_Product_1__c);
    }
    public Boolean getCanViewFeatProd2() {
        return listProductContainsId(this.userViewableProducts, this.getPage().Featured_Product_2__c);
    }
    public Boolean getCanViewFeatProd3() {
        return listProductContainsId(this.userViewableProducts, this.getPage().Featured_Product_3__c);
    }
    
    /*
        PROTECTED METHODS
    */
    
    protected ArticleWrapper retrieveArticle( Id articleId, Id defaultArticleId, String accessRolesString ) {
        ArticleWrapper result = null;
        ID searchId = defaultArticleId;
        
        // if access has been set, look for the "restricted" article
        if(accessRolesString != null) {
            Set<String> accessRoles = new Set<String>( accessRolesString.split('\\s*;\\s*') );
            searchId = UserUtil.userHasAccess( (Id)UserInfo.getUserId(), accessRoles ) ? articleId : defaultArticleId;
        }

        List<Community_Article__c> query = [select Id, Name, Article_Header__c, Button_text__c, Button_path__c,
            Preview__c, Tagline__c, Opens_New_Tab__c, Preview_Image__c, Expert_Position__c, Content_Link_Id__c
            from Community_Article__c
            where Id = :searchId ];
            
        if(!query.isEmpty()) {
            result = new ArticleWrapper( query.get(0) );
        }
        
        return result;
    }
    
    protected List<Market__c> retrieveUserMarkets() {
        List<Market__c> lstMkt = new List<Market__c>();
        try {
            List<Market__c> lstAllMkt = [select Id, Name, Alias__c, Image__c, Image_URL__c, Tag__C, RSS_Feeds__c from Market__c where Tag__c != null ORDER By Name ASC];
            Map<string, Market__c> mapMkt = new Map<string, Market__c>();
            
            //each product should have unique tag
            for( Market__c m : lstAllMkt){
                String keyName = m.Tag__c.replace('-','_') + '_Group';
                mapMkt.put(keyName, m);
            }
            List<Group> lstMktGrps = UserUtil.getFilteredGroupsForUser(UserInfo.getUserId(), mapMkt.keySet());
            for (Group grp: lstMktGrps){
                Market__c m = mapMkt.get(grp.DeveloperName);
                if ( m!= null){
                   lstMkt.add(m); 
                }
            }
            
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'error: ' + e.getMessage()));
        }

        return lstMkt;
    }
    
    protected List<Product__c> retrieveUserProducts() {
        List<Product__c> lstPrd = new List<Product__c>();
        
        try {
            List<Product__c> lstAllPrd = [select Alias__c, Hero_Image__c, Preview_Description__c, Product_Benefits__c, Benefits_Preview__c, Id, Tag__c from Product__c where Tag__c != null];
            Map<string, Product__c> mapPrd = new Map<string, Product__c>();
            
            //each product should have unique tag
            for( Product__c p : lstAllPrd){
                String keyName = p.Tag__c.replace('-','_') + '_Group';
                System.debug('keyName:'+ keyName);
                mapPrd.put(keyName, p);
            }
            List<Group> allUserGroups = UserUtil.getFilteredGroupsForUser( UserInfo.getUserId(), mapPrd.keySet());
            for (Group grp: allUserGroups) {
                Product__c p = mapPrd.get(grp.DeveloperName);
                if ( p!= null){
                   lstPrd.add(p);
                }
            }
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'error: ' + e.getMessage()));
        }

        return lstPrd;
    }
    
    /*
        UTILITY CLASSES
    */

    global class NavProduct implements Comparable {
        private String Name, Id;

        global NavProduct(String name, String id) {
            this.Name = name;
            this.Id = id;
        }
        global String getName() {
            return this.Name;
        }
        global String getId() {
            return this.Id;
        }
        
        global Integer compareTo(Object navProduct2) {
            return this.Name.compareTo(((NavProduct)navProduct2).getName());
        }
    }

    global class NavMarket implements Comparable {
        private String Name, Id;

        global NavMarket(String name, String id) {
            this.Name = name;
            this.Id = id;
        }
        global String getName() {
            return this.Name;
        }
        global String getId() {
            return this.Id;
        }
        
        global Integer compareTo(Object navMarket2) {
            return this.Name.compareTo(((NavMarket )navMarket2).getName());
        }
    }

    /**
        PRIVATE
    **/
    
    private Boolean listProductContainsId(list<Product__c> products, Id prodId) {
        for(Product__c prod : products) {
            if( prodId == prod.Id ) {
                return true;
            }
        }
        return false;
    }

    private String getPageIdFromName(String pageName) {
        String result = '';

        try {
            ASC_Page__c[] queryResult = [select Id from ASC_Page__c where ASC_Page__c.Visualforce_Page_Link__c = :pageName limit 1];
            if(queryResult.size() > 0) {
                result = (String)queryResult[0].Id;
            }
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'error: ' + e.getMessage()));
        }

        return result;
    }
    
    private List<String> retrieveUserGroups(Set<String> lstName) {
        List<String> lstGrpName = new List<String>();
        
        try {
            List<GroupMember> lstGM = [select UserOrGroupId, GroupId, Group.Name, Group.DeveloperName from GroupMember where UserOrGroupId = :UserInfo.getUserId()
                                      AND Group.DeveloperName in: lstName];
            for (GroupMember gm : lstGM){
                lstGrpName.add(gm.Group.DeveloperName);
            }
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'error: ' + e.getMessage()));
        }

        return lstGrpName;
    }

}