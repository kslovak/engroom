@isTest
private class ART_Iteration_Developer_Trigger_T {
/****************************************************************************
 * Test Class ART_Iteration_Developer_Trigger_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - ART_Iteration_Developer_Trigger Apex Trigger
 ****************************************************************************/
 
    //Test Data
    public static User user;
    public static ART_Release__c release;
    public static ART_Iteration__c iteration;
    public static ART_Iteration_Developer__c iterationDeveloper;
    
    //Test Settings
    
    
    private static testMethod void unitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
        user = TestObjects.newUser();
        
        release = new ART_Release__c();
        release.Name = 'Test Release';
        insert release;
        
        iteration = new ART_Iteration__c();
        iteration.Name = 'Test Iteration';
        iteration.Start_Date__c = Date.today();
        iteration.End_Date__c = Date.today();
        iteration.End_Date__c = iteration.End_Date__c.addMonths(1);
        iteration.Release__c = release.Id;
        insert iteration;
    }
  
    private static void executeTest01() {
        // Execute Tests
        iterationDeveloper = new ART_Iteration_Developer__c();
        iterationDeveloper.Iteration__c = iteration.Id;
        iterationDeveloper.Developer__c = user.Id;
        insert iterationDeveloper;
    }
}