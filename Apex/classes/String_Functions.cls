public class String_Functions {

    public static final String CR = '\r';
    public static final String NL = '\n';
    public static final String CRNL = CR + NL;
    
    @TestVisible
    private static void debug(String s) {System.debug(LoggingLevel.INFO, s);}
    
    public static Boolean isNull(String s) {
    	return (s == null || s.trim().length() == 0);
    }
    
    public static String convertCRtoBR(String StringIn) {
        String result = '';
        if ((StringIn != null) && (StringIn != '')) {    
            List<String> StringLines = StringIn.split('\n',0);
            for (String StringLine : StringLines) {
                if ((result != '') && (StringLine != null)) {result += '<BR>';}
                if (StringLine != null) {result += StringLine;}
            }
        }  
        return result;
    }

    public static String convertBRtoCR(String StringIn) {
        String result = '';
        if ((StringIn != null) && (StringIn != '')) {    
            List<String> StringLines = StringIn.split('<BR>',0);
            for (String StringLine : StringLines) {
                if ((result != '') && (StringLine != null)) {result += '\n';}
                if (StringLine != null) {result += StringLine;}
            }
        }
        return result;
    }

    public static String removeLeadingZeros(String str) {
        String result = null, newStr = '';
        Boolean stopProcessing = false;
        if (str != null) {
            newStr = str.trim();
            while ((stopProcessing == false) && (newStr.length() > 1)) {
                if (newStr.startsWith('0') == true) {
                    newStr = newStr.subString(1);                
                } else {stopProcessing = true;}
            }
            result = newStr;  
        }
        return result;
    }


    public static List<String> trimAllStringsinListString(List<String> lines) {
        List<String> newList = new List<String>();
        String trimmedString = '';
        for (String line:lines) {
            trimmedString = line.trim();
            newList.add(trimmedString);
        }
        return newList;  
    }

    public static String format_ListString_into_HTML_List(String dataIn, String prefix, String suffix) {
        String result = '';
        if ((dataIn != null) && (dataIn != '')) {
            List<String> Strings = new List<String>();
            Strings.Add(dataIn);
            result = Format_ListString_into_HTML_List(Strings,prefix,suffix);
        }
        return result;    
    } 

    public static String format_ListString_into_HTML_List(List<String> dataIn, String prefix, String suffix) {
        String result = '';
        if (dataIn.size() > 0) {
            result = '<ul type=circle>';
            for(String s:dataIn) {
                result += '<li>';
                if (!isNull(s)) {result += prefix + s + suffix;}
            }
            result += '</ul>';
        }
        return result;
    }

    public static String right(String str, Integer n) {
        if (n <= 0) {return '';}
        else if (n > str.length()) {return str;}
        else {
            Integer iLen = str.length();
            return str.substring(iLen - n,iLen );
        }
    }

    public static string beforeParenthesis(String s) {
        if(s.contains('(') & s.contains(')')) {
            return s.substring(0, s.lastindexof('(')).trim();
        } else {return s;}
	}

    public static string inParenthesis(String s) {
        if(s.contains('(') & s.contains(')')) {
            return s.substring(s.lastindexof('(')+1, s.length()-1).trim();
        } else {return s;}
    }

    public static String getZeroPaddedString(String s, Integer n) {
        if (s == null) {s = '';} s = s.trim();
        if (n > s.length() && (s == '' || s.isNumeric())) {
            s = ('0'.repeat(n) + s).right(n);
        } 
        return s;
    }

    public static List<String> getLines(String inStr) {
        return split(inStr, CRNL);
    }

    public static List<String> split(String inStr, String delim) {
        List<String> slist = new List<String>();
        if (String.isBlank(inStr)) {return slist;}
        Integer strLength = inStr.length();
        if (strLength > 999999) {return slist;}
        if (delim != null && delim == CRNL && 
           !inStr.endsWith(CRNL)) {inStr += CRNL;}
        strLength = inStr.length();
		Integer n = 0, strEnd = 0, strStart = 0; String str;
		//slist = inStr.split(delim); // throws Regex too complicated error
		Matcher m = Pattern.compile(delim).matcher(inStr);
		while (!m.hitEnd()) {
		    if (m.find()) {
		        n++; strEnd = m.start();
		        str = inStr.subString(strStart, strEnd);
		        strStart = m.end(); m.region(strStart, strLength);
		    } else {
		        str = inStr.subString(strStart);
		    }
		    slist.add(str);
		}
        return slist;
    }

}