global class ASI_Call_Report_Batchable implements Database.Batchable<sObject> {

    public static final String CREATE_ASI_CALL_REPORTS  = 'CREATE_ASI_CALL_REPORTS';
    public static final String CREATE_NOTES_ATTACHMENTS = 'CREATE_NOTES_ATTACHMENTS';

    public static final String QRY_DEF = 'select Id from Contact limit 1';
    public static final String QRY_ACR = 'select Id from ASI_Call_Report__c';
    public static final String QRY_OCR = 'select Id from Call_Report__c';
    
    static final Set<String> PROCESS_STEPS = new Set<String>{
        CREATE_ASI_CALL_REPORTS, CREATE_NOTES_ATTACHMENTS
    };
    
    static final Map<String, Integer> BATCH_SIZES = new Map<String, Integer>{
        CREATE_ASI_CALL_REPORTS => 100, CREATE_NOTES_ATTACHMENTS => 1 
    };
    
    static final Map<String, String> QRY_STRINGS = new Map<String, String>{
        CREATE_ASI_CALL_REPORTS => QRY_OCR, CREATE_NOTES_ATTACHMENTS => QRY_ACR 
    };
    
    final String userEmail = User_Functions.getLoggedInUserEmail();

    final String procStep, qryString; final Set<Id> recIds; final Integer recLimit;
    final String REC_FILTR = ' where Id in :recIds', REC_LIMIT = ' limit :recLimit';
    
    private void debug(String s) {System.debug(LoggingLevel.INFO, s);}

    private Boolean procStepUnknown() {
        return (String.isBlank(procStep) || !PROCESS_STEPS.contains(procStep));
    }
    
    global ASI_Call_Report_Batchable() {this('', null, 0);}
    
    global ASI_Call_Report_Batchable(String procStep) {this(procStep, null, 0);}
    
    global ASI_Call_Report_Batchable(String pstep, Set<Id> recIds, Integer recLimit) {
        if (String.isBlank(pstep)) {pstep = '';}
        this.procStep = pstep; this.recIds = recIds; this.recLimit = recLimit;
        qryString = getQueryString();
    }
    
    global Database.Querylocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(qryString);
    }

    global void execute(Database.BatchableContext bc, List<SObject> alist){
        if (procStepUnknown()) {return;}
        if (CREATE_ASI_CALL_REPORTS.equals(procStep)) {
            ASI_Call_Report_Loader.createASICallReports((List<Call_Report__c>)alist);
        } else
        if (CREATE_NOTES_ATTACHMENTS.equals(procStep)) {
            ASI_Call_Report_Loader.createAttachments((List<ASI_Call_Report__c>)alist);
        }
    }
    
    global void finish(Database.BatchableContext bc) {sendEmail(bc);}        

    private String getQueryString() {
        String q = QRY_STRINGS.get(procStep); 
        if (String.isBlank(q)) {q = QRY_DEF; return q;}
        if (recIds != null && !recIds.isEmpty()) {q += REC_FILTR;}
        if (recLimit != null && recLimit > 0)    {q += REC_LIMIT;}
        debug('q = ' + q); return q;
    }
    
    public void sendEmail(Database.BatchableContext bc) {
        String s = 'Apex Batch Job - ASI-Call-Reports - ' + procStep + ' - '; String b = s;
        if (bc != null) {
	        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
	                                 TotalJobItems, CreatedBy.Email
	                            from AsyncApexJob where Id = :bc.getJobId()];
	        s += a.Status + ' - ' + a.TotalJobItems + ' batches - ' + a.NumberOfErrors + ' failures';
	        b = s + ' - Job Id - ' + a.Id;
        }
        b += '\n\n' + 'Query : ' + qryString;
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {userEmail});
        mail.setReplyTo(userEmail); mail.setSenderDisplayName('SysAdmin');
        mail.setSubject(s); mail.setPlainTextBody(b);
        Messaging.SingleEmailMessage[] mails = new Messaging.SingleEmailMessage[]{mail};
        if (!Test.isRunningTest()) {Messaging.sendEmail(mails);}
    }
    
    public static String submitBatchJob(String procStep, List<Id> alist, Integer recLimit) {
        Set<Id> aset = new Set<Id>(); if (alist != null && !alist.isEmpty()) {aset = new Set<Id>(alist);}
        ASI_Call_Report_Batchable b = new ASI_Call_Report_Batchable(procStep, aset, recLimit);
        String msg; Integer batchSize = 1;
        if (BATCH_SIZES.containsKey(procStep)) {batchSize = BATCH_SIZES.get(procStep);}
        if (!Test.isRunningTest()) {
            try {msg = Database.executeBatch(b, batchSize);} 
            catch(Exception e) {msg = e.getMessage(); b.debug(msg);}
        }
        return msg;
    }

    Webservice static String createASICallReports(List<Id> alist, Integer recLimit) {
        String procStep = ASI_Call_Report_Batchable.CREATE_ASI_CALL_REPORTS;
        return submitBatchJob(procStep, alist, recLimit);
    }

    Webservice static String createAttachments(List<Id> alist, Integer recLimit) {
        String procStep = ASI_Call_Report_Batchable.CREATE_NOTES_ATTACHMENTS;
        return submitBatchJob(procStep, alist, recLimit);
    }

}