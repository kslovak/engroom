@isTest
private class Role_Detail_Test {

    static testMethod void test1() {
    	Test.startTest();
    	List<UserRole> roles = [
    		select Id, Name, ParentRoleId, PortalAccountId, 
			       PortalRole, PortalType
			  from UserRole limit 20];
    	Role_Detail_Methods.createRoleDetails(roles);
    }
}