public with sharing class ProductController extends PageController {
    
    private ApexPages.StandardController controller;
    private Product__c product;    // regular object
    private WebProduct wp;         // web-customized object wrapper

    public string emailContentId { get; set; }
    public string subscribeContentId { get; set; }
    public boolean subscribeSelected { get; set; }
    public string emailResponse { get; set; }

    private List<ContentVersionWrapper> literature;
    private List<ContentVersionWrapper> datasheets;
    
    public ProductController(ApexPages.StandardController sc) {
        this.controller = sc;
        if (!Test.isRunningTest()) {
            this.controller.addFields(new List<String>{
                'Alias__c', 'Benefits_Preview__c', 'Logo_Image__c', 'Hero_Image__c', 
                'Main_Text__c', 'Market_Applications__c', 'Preview_Description__c',
                'Product_Benefits__c', 'Tag__c', 'Highlight_Description__c',
                'Article1__c', 'Article1_Default__c', 'Article1_Access__c'
            });
        }
        this.product = (Product__c)this.controller.getRecord();
        this.wp = new WebProduct(this.product);
        getDocumentation();
    }
    
    public ProductController() {
        this.controller = null;
        this.product = null;
        this.wp = null;
    }
    
    public Product__c getProduct() {
        return this.product;
    }
    
    public WebProduct getWebProduct() {
        return this.wp;
    }
    
    public void getDocumentation() {
        this.literature = this.getContentByTags(new String[]{'APP-Collateral-Literature', this.product.Tag__c});
        this.dataSheets= this.getContentByTags(new String[]{'APP-Collateral-DataSheets', this.product.Tag__c});
    }
    
    public List<Product__c> getAllProducts(){
        //return [select Alias__c, Hero_Image__c, Preview_Description__c, Product_Benefits__c, Benefits_Preview__c from Product__c];
        return this.userViewableProducts;
    }
    
    public List<WebProduct> getAllWebProducts() {
        List<WebProduct> result = new List<WebProduct>();
        for(Product__c p : this.getAllProducts()) {
            result.add(new WebProduct(p));
        }
        return result;
    }

    /*
    public void getDocumentation() {
        this.literature = this.getContentByTags(new String[]{'Product-Literature', this.product.Tag__c});
        this.datasheets = this.getContentByTags(new String[]{'Product-DataSheet', this.product.Tag__c});
    } /* */
    
    public List<ContentVersionWrapper> getLiterature() {
        return this.literature;//getContentByTags(new String[]{'Product-Literature', this.product.Tag__c});
    }
    public List<ContentVersionWrapper> getDataSheets() {
        return this.dataSheets;//getContentByTags(new String[]{'Product-DataSheet', this.product.Tag__c});
    }
    
    public PageReference saveProduct() {
        try {
            if(this.product != null) {
                update this.product;
            }
        } catch (DMLException e) {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error saving product info.'));
          return null;
        }
        return null;
    }
    
    public ArticleWrapper getProductArticle1() {
        return retrieveArticle( this.product.Article1__c, this.product.Article1_Default__c, this.product.Article1_Access__c );
    }
    
    public PageReference setSubscription() {
        try {
            List<EntitySubscription> subscription = [SELECT Id FROM EntitySubscription WHERE ParentId =: this.subscribeContentId AND SubscriberId =: UserInfo.getUserId() LIMIT 1000];
            
            if(this.subscribeSelected != null) {
                if (this.subscribeSelected && subscription.size() == 0) {
                    EntitySubscription es = new EntitySubscription();
                    es.ParentId = this.subscribeContentId;
                    es.SubscriberId = UserInfo.getUserId();
                    insert es;
                } else if (!this.subscribeSelected && subscription.size() > 0) {
                    delete subscription;
                }
            }
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
        
        return null;
    }
    
    public PageReference sendEmail() {
        emailResponse ='';
        String emailMsg = Apexpages.currentPage().getParameters().get('emailMsg');
        String emailid = Apexpages.currentPage().getParameters().get('emailid');
        String contentid = Apexpages.currentPage().getParameters().get('contentid');
        try {
        emailResponse = EmailUtil.SendEmail(emailMsg, emailid, contentid);
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, resp));
        return null;
    }
    
    /*
        PRIVATE
    */
    
    private List<ContentVersionWrapper> getContentByTags(String[] tags) {
        List<ContentVersionWrapper> result = new List<ContentVersionWrapper>();
        Set<Id> subscriptionIds = new Set<Id>();
        Set<Id> contentIds = new Set<Id>();
        
        List<ContentVersion> content = [select Id, ContentDocumentId, Title, TagCsv, ContentModifiedDate, ContentUrl, ContentSize from ContentVersion where isLatest = true];
        
        for (ContentVersion c : content) {
            contentIds.add(c.ContentDocumentId);
        }
        
        if (contentIds.size() > 0) {
            Set<EntitySubscription> subscriptions = new Set<EntitySubscription>([SELECT Id, ParentId FROM EntitySubscription WHERE ParentId IN : contentIds AND SubscriberId =: UserInfo.getUserId() LIMIT 1000]);
            for (EntitySubscription subscription : subscriptions) { 
                subscriptionIds.add(subscription.ParentId);
            }
        }
        
        for(ContentVersion c : content) {
            boolean canAdd = true;
            
            for(String t : tags) {
                if(t == null || c.TagCsv == null) {
                    canAdd = false;
                    break;
                }
                canAdd = canAdd && (c.TagCsv.contains(t));
            }
            
            if(canAdd) {
                Boolean subscribed = subscriptionIds.contains(c.ContentDocumentId);
                ContentVersionWrapper w = new ContentVersionWrapper(c);
                w.Id = c.Id;
                w.ContentDocumentId = c.ContentDocumentId;
                w.Title = c.Title;
                w.TagCsv = c.TagCsv;
                w.ContentModifiedDate = c.ContentModifiedDate;
                w.ContentUrl = c.ContentUrl;
                w.ContentSize = c.ContentSize;
                w.IsSubscribed = subscribed;
                result.add(w);
            }
        }
        
        return result;
    }
    
}