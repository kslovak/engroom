@isTest
private class Custom_Approval_Functions_Test {

    public static final Set<String> PC_REC_STATUS_SET = PricingConditionApproversFunctions.PC_REC_STATUS_SET;

    static testMethod void testMe() {
        Test.startTest();
        String userName = 'sysadmin@cisproduction.com';
        List<User> usrs = [select id from user where username = :userName];
        if (usrs == null || usrs.isEmpty()) {return;}
        User u = usrs[0];
        System.runAs(u) {
            test02();
            test03();
            test04();
            test05();
            test06();
        }
        Test.stopTest();
    }
/*
    static void test01() {
        Set<String> oid1 = new Set<String>{'a3hT0000000005GIAQ'};
        Set<String> oid2 = new Set<String>{'a3hT0000000005LIAQ'};
        //System.debug(getPendingApprovalObjectNames());
        Custom_Approval_Functions.approveObjects(oid1, 'okokok');
        Custom_Approval_Functions.rejectObjects(oid2, 'notok');
    }
*/
    static void test02() {
        Set<String> oids = Custom_Approval_Functions.getPendingApprovalObjectIds('Price_Book__c');
        System.debug(oids);
    }

    static void test03() {
        PendingApprovalsController pac = new PendingApprovalsController();
        pac.getObjectSelectionList();
        pac.getPendingApprovals();
        pac.pageAction();
    }

    static void test04() {
        Account a = new Account(Name='ShipTo', Account_Number__c='AccountNumber');
        insert a;
        Pricing__c p = new Pricing__c();
        p.Account__c = a.Id;
        p.Pricing_Type__c = 'Ship and Bill pricing';
        p.Status__c = 'Submitted';
        p.Net_Deal_Rating__c = 'test';
        p.Number_of_Locations_Serve__c = 2;
        p.Promotions__c = 'test';
        insert p;
        Set<String> oids = new Set<String>{p.Id};
        Map<Id, Set<String>> approvers;
        approvers = Custom_Approval_Functions.getApproverNames(oids);
        System.debug(approvers);
        Custom_Approval_Functions.submitForApproval(oids, '');
        approvers = Custom_Approval_Functions.getApproverNames(oids);
        System.debug(approvers);
    }

    static void test05() {
        String q = PricingConditionApproversFunctions.QRY1;
        q += ' limit 5';
        try {
            List<Pricing_Condition__c> pcs = Database.query(q);
            System.debug(LoggingLevel.INFO, pcs);
            List<String> ccIds = new List<String>{'kvadlamudi@ashland.com'};
            PricingConditionApproversFunctions.sendNotifications(pcs, true, ccIds);
        } catch(Exception e) {
            System.debug(LoggingLevel.INFO, e);
        }
    }

    static void test06() {
        List<ProcessInstanceStep> ps = [select Actor.Name, Comments, CreatedDate,
                                               ProcessInstance.TargetObjectId
                                          from ProcessInstanceStep
                                      order by ProcessInstance.TargetObjectId
                                         limit 100];
        if (ps.isEmpty()) {return;}
        Set<String> objIds = new Set<String>();
        for (ProcessInstanceStep p : ps) {
            objIds.add(p.ProcessInstance.TargetObjectId);
        }
        Custom_Approval_Functions.getApprovalComments(objIds);
        Custom_Approval_Functions.getApproverNames(objIds);
    }

}