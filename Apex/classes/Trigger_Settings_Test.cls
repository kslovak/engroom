@isTest
private class Trigger_Settings_Test {

    static Apex_Trigger_Settings__c ats;
    static Apex_Trigger_Groups__c atg;
    static Account acc;

    static void debug(String s) {System.debug(LoggingLevel.INFO, s);}

    static void setup() {
        ats = new Apex_Trigger_Settings__c();
        insert ats;
        atg = new Apex_Trigger_Groups__c();
        atg.Name = 'G001_N001';
        atg.Trigger_Group__c = 'G001__c';
        atg.Trigger_Name__c = 'N001__c';
        insert atg;

        atg = new Apex_Trigger_Groups__c();
        atg.Name = 'Pricing_Account';
        atg.Trigger_Group__c = 'Pricing__c';
        atg.Trigger_Name__c = 'Account__c';
        insert atg;
    }

    static void createAccount() {
        acc = new Account(Name='test'); insert acc;
        acc = [select Id, Name, Owner__c, Owner_Name__c
                from Account where Id = :acc.Id];
        debug('****************************************************');
        debug('acc = ' + acc);
        debug('****************************************************');
    }

    static testMethod void test01() {
        setup();
        Trigger_Settings.doNotRun('test');
        Trigger_Settings.doNotRun('N001');

        // Test for Object level setting
        ats.All_Triggers__c = false; ats.Pricing__c = false; ats.Account__c = true; update ats;
        createAccount();

        // Test for Group level setting
        ats.All_Triggers__c = false; ats.Pricing__c = true; ats.Account__c = false; update ats;
        createAccount();

        // Test for All triggers setting
        ats.All_Triggers__c = true; ats.Pricing__c = false; ats.Account__c = false; update ats;
        createAccount();

        // Test for Normal trigger execution
        ats.All_Triggers__c = false; ats.Pricing__c = false; ats.Account__c = false; update ats;
        createAccount();
    }
}