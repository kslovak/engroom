public class CSV_Data_Methods {
    
    public interface Processor {
        void processCsvData(List<String> slist, String csvConfigId);
    }
    
    public static final String CRNL = '\r\n';
    public static final String CSV_DATA_ADMIN = 'CSV_Data_Admin';
    
    public static void debug(String s) {System.debug(LoggingLevel.INFO, s);}

    public static Boolean isCsvDataAdmin() {
        return CustomPermission_Methods.isPermitted(CSV_DATA_ADMIN);
    }
    
    public static List<SelectOption> getCsvProcessNames() {
        List<SelectOption> sopts = new List<SelectOption>();
        sopts.add(new SelectOption('', '--None--'));
        Set<String> aset = new Set<String>();
        for (CSV_Data_Config__c c : [
            select Id, Name from CSV_Data_Config__c 
             where Active__c = true order by Name]) {
            if (!aset.contains(c.Name)) {
                aset.add(c.Name);
                sopts.add(new SelectOption(c.Id, c.Name));
            }
        }
        return sopts;
    }
    
    public static CSV_Data_Config__c getCsvDataConfig(String csvConfigId) {
        CSV_Data_Config__c a = new CSV_Data_Config__c();
        if (String.isBlank(csvConfigId)) {return a;}
        List<CSV_Data_Config__c> alist = [
            select Id, Name, Processor_Class__c, Record_Layout__c 
              from CSV_Data_Config__c where Id = :csvConfigId];
        if (alist != null && !alist.isEmpty()) {a = alist[0];}
        return a;
    }
    
    public static String getCsvConfigId(String k) {
        String s = ''; if (String.isBlank(k)) {return s;}
        List<CSV_Data_Config__c> alist = [
            select Id from CSV_Data_Config__c 
             where Key__c = :k and Active__c = true limit 1];
        if (alist != null && !alist.isEmpty()) {s = alist[0].Id;}
        debug('config Id = ' + s); return s;
    }
    
    public static void processCsvData(List<String> slist, String csvConfigId) {
        if (slist == null || slist.isEmpty()) {return;}
        CSV_Data_Config__c cnfg = getCsvDataConfig(csvConfigId);
        if (cnfg.Id == null) {return;}
        try {
            Type t = Type.forName(cnfg.Processor_Class__c); if (t == null) {return;}
            Processor p = (Processor)t.newInstance(); if (p == null) {return;}
            p.processCsvData(slist, csvConfigId);
        } catch(Exception e) {debug(e.getMessage()); throw(e);}
    }
    
    public static List<String> getFieldLabels(CSV_Data_Config__c c) {
        List<String> slist = new List<String>();
        if (!String.isBlank(c.Record_Layout__c)) {
            slist = c.Record_Layout__c.trim().split(CRNL); 
        }
        return slist;
    }
    
}