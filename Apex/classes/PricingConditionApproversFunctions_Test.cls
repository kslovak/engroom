@isTest
private class PricingConditionApproversFunctions_Test {

    public static final Set<String> PC_REC_STATUS_SET = PricingConditionApproversFunctions.PC_REC_STATUS_SET;

    static testMethod void test01() {
        Test.startTest();
    	String q = PricingConditionApproversFunctions.QRY1;
    	q += ' limit 5';
        try {
            List<Pricing_Condition__c> pcs = Database.query(q);
            System.debug(LoggingLevel.INFO, pcs);
            List<String> ccIds = new List<String>{'kvadlamudi@ashland.com'};
            List<Pricing_Condition> submittedPcs = PricingConditionFunctions.getSubmittedPricingConditions(pcs);
            PricingConditionApproversFunctions.sendNotifications(submittedPcs, true, ccIds);
        } catch(Exception e) {
            System.debug(LoggingLevel.INFO, e);
        }
    }
    
}