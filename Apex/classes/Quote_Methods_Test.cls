@isTest
private class Quote_Methods_Test {
    
    static final String testAcc = 'test account';
    static final String testOpp = 'test opportunity';
    static final String testPbk = 'test price book';
    static final String testMat = 'test material';
    static final String testQte = 'test quote';

    static final String STD_PB = Quote_Methods.STD_PRICE_BOOK;
    static final String distChan = '20';
    static final String salesOrg = '1000';

    static Account acc;
    static Opportunity opp;
    static PriceBook2 pbk;
    static Quote qte;
    
    static List<Material_Sales_Data2__c> msd2s;
    
    static void setup() {
        acc = new Account(Name=testAcc, SAP_DistChannel__c=distChan, SAP_Sales_Org__c=salesOrg);
        try {insert acc;} catch(Exception e) {}
        
        opp = new Opportunity(Name=testOpp, AccountId=acc.Id);
        opp.StageName = 'stage1'; opp.CloseDate = Date.today().addDays(30);
        try {insert opp;} catch(Exception e) {}
        
        pbk = new PriceBook2(Name=testPbk);
        try {insert pbk;} catch(Exception e) {}
        
        msd2s = new List<Material_Sales_Data2__c>(); String matName, matNum; Material_Sales_Data2__c msd2;
        for (Integer n = 1; n < 10; n++) {
            matName = testMat + ' ' + n; matNum = '00000000' + n;
            msd2 = new Material_Sales_Data2__c(Name=matName, Active_Material__c='Y', 
                                               Material_Number__c=matNum, Record_Key__c=matNum, 
                                               Dist_Channel_Code__c=distChan, Sales_Org_Code__c=salesOrg);
            msd2s.add(msd2);
        } 
        try {insert msd2s;} catch(Exception e) {}
        
        qte = new Quote(Name=testQte, OpportunityId=opp.Id);
        try {insert qte;} catch(Exception e) {}
    }

    static testMethod void test01() {
        Id stdPbId = Test.getStandardPricebookId();
        setup(); Quote_Methods.updatePriceBook(stdPbId, distChan, salesOrg, testPbk);
    }
}