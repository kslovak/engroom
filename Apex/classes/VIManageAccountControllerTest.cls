/**
 * Contains test methods for the VIManageAccountController class.
 */
@IsTest
private class VIManageAccountControllerTest {
    /**
    * Run as a partner portal user to ensure that the user is redirected to the current user's
    * account detail page.
    */
    static testmethod void runAsPartnerPortalUser() {
        // Create the account
        Account account = new Account();
        account.Name = 'Test Partner Account';
        insert account;

        // Create a contact to represent our portal user
        Contact contact = new Contact();
        contact.AccountId = account.Id;
        contact.LastName = 'Test';
        insert contact;

        // Create a partner portal user to run the test
        Profile p;
        try {
            p = [SELECT Id FROM Profile WHERE Name = :VIUtils.PARTNER_PORTAL_NAME];
        } catch(Exception e) {return;}
        String s = 'x' + p.id + '@example.com';
        User u = new User(Alias = 'standt', Email = s,
                EmailEncodingKey = 'UTF-8', LastName = 'Testing',
                LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                ProfileId = p.Id, ContactId = contact.Id, Ashland_Employee_Number__c = 'zzzTest10',
                TimeZoneSidKey = 'America/Los_Angeles', Username = s);
        System.runAs(u) {
            // Create and initialize the controller
            VIManageAccountController controller = new VIManageAccountController();

            PageReference redirect = controller.init();
            System.assertNotEquals(null, redirect);
        }
    }
}