@isTest
private class Recursive_Check_V2_T {
/****************************************************************************
 * Test Class Recursive_Check_V2_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - Recursive_Check_V2 Apex Class
 ****************************************************************************/
 
    //Test Data
    
    
    //Test Settings
    
    
    private static testMethod void unitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
        // No Test Data Needed
    }
  
    private static void executeTest01() {
        // Execute Tests
        System.assertEquals(false,Recursive_Check_V2.hasAlreadyRan('Test'));
        Recursive_Check_V2.setAlreadyRan('Test');
        System.assertEquals(true,Recursive_Check_V2.hasAlreadyRan('Test'));
        Recursive_Check_V2.reset('Test');
        System.assertEquals(false,Recursive_Check_V2.hasAlreadyRan('Test'));

        // Verify Outcome
    }
}