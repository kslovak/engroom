public class FolderMethods {

    public static final String TYPE_DASHBOARD = 'Dashboard';
    public static final String TYPE_DOCUMENT  = 'Document';
    public static final String TYPE_EMAIL     = 'Email';
    public static final String TYPE_REPORT    = 'Report';

    private static void debug(String s) {
    	System.debug(LoggingLevel.INFO, 'FolderMethods : ' + s);
    }
    
    private static Folder getFolder(String ftype, String fname) {
    	debug('getFolder : Type = ' + ftype + ' : Name = ' + fname);
        List<Folder> fs = [select Id, Name from Folder 
                            where Type = :ftype and Name = :fname];
        if (fs == null || fs.isEmpty()) {return null;}
        debug('Folder = ' + fs[0]); return fs[0];
    }

    public static Folder getDashboardFolder(String folderName) {
        return getFolder(TYPE_DASHBOARD, folderName);
    }
         
    public static Folder getDocumentFolder(String folderName) {
        return getFolder(TYPE_DOCUMENT, folderName);
    }
         
    public static Folder getEmailFolder(String folderName) {
        return getFolder(TYPE_EMAIL, folderName);
    }
         
    public static Folder getReportFolder(String folderName) {
        return getFolder(TYPE_REPORT, folderName);
    }
         
}