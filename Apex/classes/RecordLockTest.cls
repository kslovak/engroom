@isTest
private class RecordLockTest {

    static Account acc;
    static Express_Care_Project__c ecp;
    static RecordLockController rlc; 
    
    static void createTestData() {
    	acc = new Account(Name='TestAccount');
    	insert acc;
    	
    	ecp = new Express_Care_Project__c();
    	ecp.Customer_Prospect_Account__c = acc.Id;
    	insert ecp;
    	
    	ApexPages.StandardController sc = new ApexPages.StandardController(ecp);
    	rlc = new RecordLockController(sc);
    }
    
    static testMethod void test01() {
    	createTestData();
    	rlc.lockRecord(); rlc.returnToView(); rlc.unlockRecord();
    }
    
}