@isTest
private class Lead_Functions_Test_Methods {

    static Account taccount;
    static Contact tcontact;
    static Lead tlead;
    
    static void debug(String s) {System.debug(LoggingLevel.INFO, s);}
    
    static void setup() {
    	taccount = new Account(Name='testAcc');
    	insert taccount;
    	
    	tcontact = new Contact(LastName='lname');
    	tcontact.AccountId = taccount.Id;
    	insert tcontact;
    	
        tlead = new Lead();
        tlead.FirstName = 'fname';
        tlead.LastName = 'lname';
        tlead.Company = 'company';
        tlead.DoNotCall = true;
        insert tlead;
        debug('tlead : ' + tlead);
    }

//*    
    static testMethod void myUnitTest() {
        Test.startTest();
        Map<String, Schema.RecordTypeInfo> RTMap = Lead.sObjectType.getDescribe().getRecordTypeInfosByName();
        Lead newLead = new Lead();
        newLead.FirstName = 'Jon';
        newLead.LastName = 'Keener';
        newLead.Street = 'Street'; newLead.City = 'City'; 
        newLead.State = 'OH'; newLead.PostalCode = '99999';     
        AshlandWebSiteLeadInsertResults result1 = Lead_Functions.createAshlandWebSiteLead(newLead);

        newLead.Company = 'ABC Company';
        AshlandWebSiteLeadInsertResults result2 = Lead_Functions.createAshlandWebSiteLead(newLead);
        
        System.assertEquals(result1.result,false);  
        System.assertEquals(result2.result,true);
        
        List<RecordType> RTList = [select id, name, BusinessProcessId from RecordType where name in :RTMap.keyset() and isactive = true];
        List<Lead> LeadList = new List<Lead>();
        
        for(RecordType r:RTList)
        {
        Lead NewLead2 = NewLead.clone(false);
        newLead2.recordtypeid = RTMap.get(r.name).getrecordtypeid();   
        system.debug('RecordType: ' + r.name + ' - ' + RTMap.get(r.name).getrecordtypeid());
        LeadList.add(NewLead2);
        }
        
        Lead_Trigger_Functions.processBeforeInsert(LeadList);

        List<LeadStatus> convertedstatuses= [select id, MasterLabel from LeadStatus where IsConverted = true limit 2];
        insert LeadList;
        
        Map<String, Schema.RecordTypeInfo> RTMapCon = Consumption__c.sObjectType.getDescribe().getRecordTypeInfosByName();

       Lead_Trigger_Functions.updateLeadConvertedObjects(leadlist);
       
        for(Lead l:leadlist){
          for(LeadStatus LS:convertedstatuses){
            Try{       
             Database.LeadConvert lc = new database.LeadConvert();
             lc.setLeadId(l.id);
             lc.setConvertedStatus(LS.MasterLabel);
             Database.LeadConvertResult lcr = Database.convertLead(lc);
            }  
            catch(Exception e) { system.debug('Error: ' + e);}
          }
       }
       
       //Test.stopTest();
   }
   
    static testMethod void test02() {
   	    setup();
   	    String page1 = '/apex/leadconversionprocess';
   	    PageReference pr1 = new PageReference(page1);
   	    Test.setCurrentPageReference(pr1);
   	    Map<String, String> pmap = ApexPages.currentPage().getParameters();
   	    pmap.put('id', tlead.id);
   	    leadconversionprocessclass c = new leadconversionprocessclass();
   	    c.lsource = true; c.createtask = true;
   	    c.app = taccount.Id; c.convalue = tcontact.Id;
   	    c.getcontact(); c.ConvertToAccount(); c.BackToLead();
    }
//*/
   
    static testMethod void test03() {
        setup();
        String page1 = '/apex/leadprocessextn';
        PageReference pr1 = new PageReference(page1);
        Test.setCurrentPageReference(pr1);
        Map<String, String> pmap = ApexPages.currentPage().getParameters();
        pmap.put('id', taccount.id);
        SalesDogUpdationClass c = new SalesDogUpdationClass();
        c.RedirecttoAccount(); c.dosave();
    }
}