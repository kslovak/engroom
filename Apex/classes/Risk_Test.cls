@isTest
public class Risk_Test {
    
    static Account                 acc;
    static Competitor__c           cmp;
    static Material_Sales_Data2__c mat;
    static Product_Sales_Plan__c   psp;
    static Risk__c                 rsk;
    static Risk_Detail__c          rdc;
    static List<Risk_Detail__c>  rdlist;

    static void debug(String s) {
        System.debug(LoggingLevel.INFO, '>>>>>>>>> ' + s);
    }
    
    static void createTestData() {
        cmp = new Competitor__c();
        cmp.Name = 'Test';
        insert cmp;
        
        psp = Sales_Plan_Test.getProductSalesPlan();
        debug('psp : ' + psp);
        acc = Risk_Functions.getAccount(psp.Account__c);
        rsk = new Risk__c();
        rsk.Account__c = acc.Id;
        rsk.Competitor__c = cmp.Id;
        rsk.Probability_of_Risk_CY__c = 10;
        rsk.Probability_of_Risk_NY__c = 20;
        rsk.Record_Status__c = Risk_Functions.ACTIVE;
        rsk.Stage__c = 'Test'; rsk.Reason__c = 'Test';
        rsk.Region__c = 'Test'; rsk.SBU__c = 'Test';
        rsk.Business_Segment__c = 'Test'; rsk.Business_Unit__c = 'Test';
        //insert rsk;
        
        rdc = new Risk_Detail__c();
        rdc.Material__c = psp.Material__c;
        //rdc.Risk__c = rsk.Id;
        rdc.Price_CY__c = 10; 
        rdc.Price_NY__c = 20;
        rdc.Volume_at_Risk_CY__c = 1000;
        rdc.Volume_at_Risk_NY__c = 2000;
        //insert rdc;
        
        rdlist = new List<Risk_Detail__c>{rdc};
        Risk_Functions.saveRisk(rsk, rdlist);
        
        rsk = Risk_Functions.getRisk(rsk.Id);
        debug('rsk : ' + rsk);
    }
    
    static testMethod void test01() {
        Risk_Functions.compare1('a', 'b'); Risk_Functions.compare2('a', 'b');
        createTestData();
        Test.startTest();
        PaginationController pc1 = Risk_Functions.getProductPlansPC(acc.Id, 20);
        rsk.Key_Risk_To_Plan__c = true;
        Risk_Functions.saveRisk(rsk, rdlist);
        Risk_Functions.isApproved(rsk); 
        Risk_Functions.isClosed(rsk); Risk_Functions.isDraft(rsk);
        List<Risk__c> tlist = new List<Risk__c>{rsk};
        tlist = Risk_Functions.createNextYearRisks(tlist);
        Risk_Functions.activateNextYearRisks(tlist);
    }
    
    static testMethod void test02() {
        createTestData();
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(rsk);
        Risk_Controller c = new Risk_Controller(sc);
        c.cancel(); c.deleteMaterials(); c.addAndReturn(); c.onAccountChange(); c.saveRisk();
        c.pc1Frst(); c.pc1Next(); c.pc1Prev(); c.pc1Last();
        c.gotoRiskPage1();
    }
    
}