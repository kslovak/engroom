@isTest
private class VIDeleteEnrollmentControllerTest {

    static void debug(String s) {
        System.debug(LoggingLevel.INFO, '>>>>>>>>>>>> ' + s);
    }

    static testmethod void deleteEnrollments() {

        Boolean b;

        VI_Promotion__c promotion = new VI_Promotion__c();
        promotion.Name = 'Test Promotion';
        promotion.Program_Name__c = 'Test Promotion';
        promotion.AVI__c = 'No';
        promotion.Channel__c = 'ASC';
        promotion.Active__c = true;
        insert promotion;

        Account facility = new Account();
        facility.Name = 'Test Facility';
        facility.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_FACILITY;
        facility.Inactive_Account__c = false;
        insert facility;

        Contact contact = new Contact();
        contact.AccountId = facility.Id;
        contact.LastName = 'Test';
        insert contact;

        RecordType rt = [SELECT Id from RecordType
                          where SobjectType = 'Account'
                            and Name = 'Valvoline DIFM Prospect'];

        VI_Enrollment__c enrollment = new VI_Enrollment__c();
        enrollment.Facility__c = facility.Id;
        enrollment.Promotion__c = promotion.Id;
        insert enrollment;

        Account location;
        List<Account> locations = new List<Account>();
        for (Integer i = 0; i < 1; i++) {
            location = new Account();
            location.VI_Parent_Account__c = facility.Id;
            location.Name = 'Test Location ' + String.valueOf(i + 1);
            location.RecordTypeId = rt.Id;
            locations.add(location);
        }
        insert locations;

        List<VI_Enrollment_Location__c> enrollmentLocations = new List<VI_Enrollment_Location__c>();
        for (Account loc : locations) {
            VI_Enrollment_Location__c enrollmentLocation = new VI_Enrollment_Location__c();
            enrollmentLocation.Enrollment__c = enrollment.Id;
            enrollmentLocation.Location__c = loc.Id;
            enrollmentLocation.Approval_Status__c = VIUtils.ENROLLMENT_STATUS_APPROVED;

            enrollmentLocations.add(enrollmentLocation);
        }
        insert enrollmentLocations;

        ApexPages.currentPage().getParameters().put('promotion', promotion.Id);
        ApexPages.currentPage().getParameters().put('location', location.Id);
        ApexPages.currentPage().getParameters().put('retURL', '/home/home.jsp');

        update location;

        Test.startTest();

        debug(''+location);

        VIDeleteEnrollmentController controller = new VIDeleteEnrollmentController();
        controller.init();
        List<VIEnrollmentLocation> lineItems = controller.getLineItems();
        b = lineItems != null && lineItems.size()==locations.size();
        debug(''+lineItems);

        for(VIEnrollmentLocation lineItem : lineItems){
            lineItem.selected = true;
        }
        controller.getPromotion();
        controller.cancel();

        controller.submit();
        lineItems = controller.getLineItems();
        b = lineItems != null && lineItems.size()==0;
        debug(''+lineItems);

        Test.stopTest();

    }
}