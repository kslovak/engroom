/**
 * Contains test methods for the VIPortalAVIApplicationController class.
 */
@IsTest
private class VIPortalAVIApplicationControllerTest {
	/**
	 * Submit an AVI application.
	 */
	static testmethod void submitApplication() {
		// Create a facility
		Account facility = new Account();
		facility.Name = 'Test Facility';
		facility.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_FACILITY;
		facility.VI_Channel__c = VIUtils.CHANNEL_ASC;
		insert facility;
		
		// Create a location
		Account location = new Account();
		location.VI_Parent_Account__c = facility.Id;
		location.Name = 'Test Location';
		location.VI_Account_Type__c = VIUtils.ACCOUNT_TYPE_LOCATION;
		location.SAP_Street__c = location.BillingStreet = '123 Main St';
		location.SAP_City__c = location.BillingCity = 'Dallas';
		location.SAP_State__c = location.BillingState = 'TX';
		location.SAP_Zip_Code__c = location.BillingPostalCode = '75243';
		location.Phone = '8005551234';
		location.Fax = '8005554321';
		location.Website = 'http://www.google.com';
		insert location;
		
		// Set page parameters
		PageReference thePage = Page.VIPortalAVIApplication;
		thePage.getParameters().put(VIPortalAVIApplicationController.PARAM_ID, location.Id);
        thePage.getParameters().put(VIPortalAVIApplicationController.PARAM_TYPE, VIUtils.ACCOUNT_TYPE_LOCATION);
		
		// Start the test
		Test.setCurrentPage(thePage);
		Test.startTest();
		
		// Create and initialize the controller
		VIPortalAVIApplicationController controller = new VIPortalAVIApplicationController();
		controller.init();
		
		// Ensure that the location was loaded
		System.assertNotEquals(null, controller.getLocation());
		System.assertEquals(location.Id, controller.getLocation().Id);
		System.assertEquals(location.BillingStreet, controller.getStreet().getValue());
		System.assertEquals(location.BillingCity, controller.getCity().getValue());
		System.assertEquals(location.BillingState, controller.getState().getValue());
		System.assertEquals(location.BillingPostalCode, controller.getPostalCode().getValue());
		System.assertEquals(location.Phone, controller.getPhone().getValue());
		System.assertEquals(location.Fax, controller.getFax().getValue());
		System.assertEquals(location.Website, controller.getWebsite().getValue());
		System.assertEquals(facility.Name, controller.getFacilityName());
		System.assertEquals(facility.VI_Channel__c, controller.getFacilityChannel());
		
		// Make sure we have a new application
		System.assertNotEquals(null, controller.getApplication());
		System.assertEquals(null, controller.getApplication().Id);
		
		// Try to submit without accepting the terms and conditions. Submit should fail
		controller.setUserAccepted(false);
		System.assertEquals(false, controller.getUserAccepted());
		
		System.assertEquals(null, controller.submit());
		controller.setUserAccepted(true);
		
		// Try to submit with missing required fields. Submit should fail
		controller.getStreet().setValue(null);
		System.assertEquals(null, controller.getStreet().getError());
		
		System.assertEquals(null, controller.submit());
		System.assertNotEquals(null, controller.getStreet().getError());
		
		controller.getStreet().setValue(location.BillingStreet);
		
		// Correct the errors and submit again. Submit should succeed
		System.assertNotEquals(null, controller.submit());
		
		// Call cancel to for code coverate credit
		controller.cancel();
		
		// Stop the test
		Test.stopTest();
	}
}