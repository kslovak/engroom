public with sharing class MessageController extends PageController {

    private final Id communityId = Network.getNetworkId();
    
    public String messageBody {get; set;}
    public String[] messageRecipients {get; set;}
    public String messageRecipient {get; set;}
    public String conversationQuery {get; set;}
    public String messageQuery {get; set;}
    public String userQuery {get; set;}
    
    private String conversationId;
    private List<ConnectApi.UserDetail> connections;

    public MessageController() {
        this.conversationId = ApexPages.currentPage().getParameters().get('cid');
        this.connections = null;
        this.messageRecipients = new List<String>();
        this.messageRecipient = '';
    }
    
    public ConnectApi.UserDetail getMy() {
        return ConnectApi.ChatterUsers.getUser(communityId,'me');
    }
    
    public List<ConnectApi.ChatterConversationSummary> getConversations() {
        List<ConnectApi.ChatterConversationSummary> result;
        
        if(isValidSearchQuery(this.conversationQuery)) {
            result = ConnectApi.ChatterMessages.searchConversations(conversationQuery).conversations;
        } else {
            result = ConnectApi.ChatterMessages.getConversations().conversations;
        }
        
        return result;
    }
    
    public ConnectApi.ChatterConversation getConversationDetail() {
        ConnectApi.ChatterConversation result = null;
        try {
            result = ConnectApi.ChatterMessages.getConversation(this.conversationId);
        } catch(Exception e) {
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'error: ' + e.getMessage()));
        }
        return result;
    }
    
    public List<ConnectApi.ChatterMessage> getConversationDetailMessages() {
        List<ConnectApi.ChatterMessage> result = null;
        
        if(isValidSearchQuery(this.messageQuery)) {
            result = ConnectApi.ChatterMessages.searchConversation(this.conversationId, this.messageQuery).messages.messages;
        } else if(this.getConversationDetail() != null) {
            result = this.getConversationDetail().messages.messages;
        }
        
        // reverse result (descending by date)
        reverseList(result);
        
        return result;
    }
    
    public List<ConnectApi.UserDetail> getConnections() {
        return this.getConnections(this.userQuery, null);
    }
    
    /*public List<SelectOption> getConnectionOptions() {
        List<SelectOption> options = new List<SelectOption>();
        
        for(ConnectApi.UserDetail user : this.connections) {
            options.add(new SelectOption(user.id,user.name));
        }

        return options;
    }*/
    
    public PageReference sendMessage(){
        // sanitize recipients field
        //String recipients = this.sanitizeRecipients(this.messageRecipients);
        String recipients = this.messageRecipient;
        
        try {
            ConnectApi.ChatterMessages.sendMessage(this.messageBody, recipients);
        } catch(Exception e) {
          //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'messageBody: \"' + this.messageBody + '\"'));
          //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'recipients: \"' + recipients + '\"'));
          //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'error: ' + e.getMessage()));
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'There was an error sending your message.'));
        }
        return null;
    }
    
    public PageReference getConversationView() {
        this.conversationId = ApexPages.currentPage().getParameters().get('cid');
        
        if(this.conversationId != null) {
            ConnectApi.ChatterMessages.markConversationRead(this.conversationId,true);
        }
        return null;
    }
    
    public PageReference conversationSearch() {
        return null;
    }
    
    public PageReference messageSearch() {
        return null;
    }
    
    public PageReference userSearch() {
        return null;
    }
    
    public PageReference replyToMessage(){
        String replyId = ApexPages.currentPage().getParameters().get('replyId');
        String messageReply= ApexPages.currentPage().getParameters().get('messageReply');
        
        try {
            ConnectApi.ChatterMessages.replyToMessage(messageReply, replyId);
        } catch(Exception e) {
          //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'message: \"' + messageReply + '\"'));
          //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'replyId: \"' + replyId + '\"'));
          //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'error: ' + e.getMessage()));
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'There was an error sending your message.'));
        }
        return null;
    }
    
    // JSON for auto-complete
    public String getJSON() {
        String userNameSearch = Apexpages.currentPage().getParameters().get('userNameSearch');
        List<UserSearchWrapper> results = new List<UserSearchWrapper>();
        
        for(ConnectApi.UserDetail connection : this.getConnections(userNameSearch, 7)) {
            if(connection.id != UserInfo.getUserId()) {
                results.add(new UserSearchWrapper(connection.displayName,connection.id));
            }
        }
        
        return  JSON.serialize(results);
    }
    
    /**
    **    PRIVATE METHODS
    **/
    
    private boolean isValidSearchQuery(String query) {
        boolean result = true;
        if(query == null) {
            return false;
        }
        else if(query.length() < 2) {
            return false;
        }
        return result;
    }
    
    private List<ConnectApi.UserDetail> getConnections(String query, Integer numResults) {
        if (isValidSearchQuery(query)) {
            this.connections = ConnectApi.ChatterUsers.searchUsers(this.communityId,query,0,numResults).users;
        }
        else {
            this.connections = ConnectApi.ChatterUsers.getUsers(this.communityId).users;
        }
        return this.connections;
    }
    
    private String sanitizeRecipients(String[] recipients) {
        String result = '';
        
            // max 9 recipients            
        for(Integer i = 0; i < recipients.size() && i < 9; i++) {
            if(recipients[i] != ''){
                if(i != 0) {
                    result += ',';
                }
                result += recipients[i];
            }
        }
        
        return result;
    }
    
    private List<ConnectApi.ChatterMessage> reverseList(List<ConnectApi.ChatterMessage> listToReverse) {
        if(listToReverse == null) {
            return listToReverse;
        }
        for(Integer i = 0; i < listToReverse.size() / 2; i++) {
            ConnectApi.ChatterMessage temp = listToReverse[i];
            listToReverse[i] = listToReverse[(listToReverse.size() - 1) - i];
            listToReverse[(listToReverse.size() - 1) - i] = temp;
        }
        
        return listToReverse;
    }
    
    /**
        Utility classes
    **/
    
    private class UserSearchWrapper {
        String name, id;
        public UserSearchWrapper(String name, String id){
            this.name = name;
            this.id = id;
        }
    }

}