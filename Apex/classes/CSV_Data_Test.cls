@isTest
private class CSV_Data_Test {

    static CSV_Data_Config__c cnfg;
    static CSV_Data_Controller c;
    
    static void setup() {
        cnfg = new CSV_Data_Config__c(Name='Test');
        cnfg.Key__c = 'supclm';
        cnfg.Processor_Class__c = 'Supplemental_Claim';
        cnfg.Record_Layout__c = 'test';
        insert cnfg;
        
        c = new CSV_Data_Controller();
        c.csvConfigId = cnfg.Id; c.changeProcess();
    }
    
    static testMethod void test01() {
        setup(); Test.startTest();
        c.csvFileBlob = Blob.valueof('test');
        c.processData(); c.addErrMsg('test');
        CSV_Data_Methods.getCsvConfigId(cnfg.Key__c);
        List<String> slist = new List<String>{'test'};
        CSV_Data_Methods.processCsvData(slist, cnfg.Id);
    }
}