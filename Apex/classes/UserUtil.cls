global class UserUtil {
    
    public static Boolean userHasAccess( Id thisUserId, Set<String> groupNameSet ) {
        for(Group grp : getGroupsForUser( thisUserId )) {
            for(String role : groupNameSet ) {
                if( grp.Name == role ) {
                    return true;
                }
            }
        }
        // only reached when no roles match
        return false;
    }
    
    public static Boolean userHasAccess( Id thisUserId, List<String> groupNameList ) {
        return userHasAccess( thisUserId, new Set<String>( groupNameList ) );
    }
    
    // return list of all groups the user belongs to via direct or indirect membership
    public static List<Group> getGroupsForUser(Id userId) {
        return getFilteredGroupsForUser( userId, null);
    }
    
    public static Boolean groupListContains( String groupName, List<Group> groupList ) {
        for( Group grp : groupList ) {
            if(grp.Name == groupName) {
                return true;
            }
        }
        return false;
    }
        
    /**
        Returns groups for which this user has access, restricted by a predefined list of groups
    **/
    public static List<Group> getFilteredGroupsForUser( Id thisUserId, Set<String> lstName) {
        Set<Id> groupIds = getGroupsForIds(new Set<Id>{thisUserId});
        if( lstName == null ) {
            return [
                select Id, Name, DeveloperName
                from Group
                where Id IN: groupIds];
        } else {
            return [
                select Id, Name, DeveloperName
                from Group
                where Id IN: groupIds
                    and DeveloperName IN :lstName];
        }
    }
    
    /*
        PRIVATE METHODS
    */
    
    // return all ids the user belongs to via direct or indirect membership
    private static Set<Id> getGroupsForIds(Set<Id> userOrGroupIds) {
        Set<Id> output = new Set<Id>();
    
        // only query actual groups and not roles and queues
        list<GroupMember> records = [
            select id
                 , GroupId
                 , UserOrGroupId
              from GroupMember
             where UserOrGroupId in: userOrGroupIds
            and UserOrGroupId != null
               and Group.Type = 'Regular'
               ];
        for (GroupMember record : records) {
            // found a group, remember for traversal
            output.add(record.GroupId);
        }
    
        // call self to get nested groups we found
        if (output.size() > 0) {
            output.addAll(getGroupsForIds(output));
        }
    
        return output;
    }

}