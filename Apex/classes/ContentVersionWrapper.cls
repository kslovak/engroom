public class ContentVersionWrapper {
    public string Id { get; set; }
    public string ContentDocumentId { get; set; } 
    public string Title { get; set; } 
    public string TagCsv { get; set; } 
    public DateTime ContentModifiedDate { get; set; } 
    public string ContentUrl { get; set; } 
    public Integer ContentSize { get; set; } 
    public Boolean IsSubscribed { get; set; }
    public List<String> documentTags {get; set;}
    
    //private Boolean isNew;
    private ContentVersion content;
    
    public ContentVersionWrapper(ContentVersion cv) {
        this.content = cv;
        //this.isNew = getIsNew();
    }
    
    public Boolean getIsNew() {
        //try {
        return this.content.ContentModifiedDate.date().daysBetween(Date.today()) <= 7;
        
        /*} catch (Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
            return false;
        }*/
    }
}