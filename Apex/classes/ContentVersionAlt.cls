public with sharing class ContentVersionAlt {

    public ContentVersion contentToUpload {get;set;}
    Id memberId;
    public Blob fileContent {get;set;}
//ApexPages.StandardController controller
    public ContentVersionAlt() {
        //memberId = controller.getId();
        contentToUpload = new ContentVersion();
    }
    

    // list crm content
    public List<ContentVersion> getContentVersions() {
        return [select id, Title, TagCsv, Description, FileType,
        Owner.Name, VersionNumber from ContentVersion
        Where IsLatest = true];
    }
    
    // attach user info
    public User getCurrentUser() {
        return null; //[select Name, Email, UserRole from User where User.Username = 'rcerami']; //database.query(sql);
    }
    
    // upload crm content
    
    public PageReference uploadContents() {
        List<ContentWorkSpace> CWList = [SELECT Id, Name From ContentWorkspace WHERE Name = 'Trellist Test'];
        contentToUpload.VersionData = fileContent;
        insert contentToUpload;
        
        contentToUpload = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentToUpload.Id];
        
        ContentWorkspaceDoc cwd = new ContentWorkspaceDoc();
        cwd.ContentDocumentId = contentToUpload.ContentDocumentId;
        cwd.ContentWorkspaceId = CWList.get(0).Id;
        insert cwd;
        
        PageReference pg = new PageReference('/' + memberId);
        pg.setRedirect(true);
        return pg;
    }
}