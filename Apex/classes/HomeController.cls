public with sharing class HomeController extends PageController {

    public Featured_Expert_Article__c getFeaturedArticle() {
        List<Featured_Expert_Article__c> articles = 
            [select Article__r.Name, Article__r.Tagline__c, Article__r.Position__c, Article__r.Article__c, Article__r.Image__c
                from Featured_Expert_Article__c
            ];
        // always return only one featured article
        Featured_Expert_Article__c result = articles.get(0);
        result.Article__r.Article__c = result.Article__r.Article__c.substring(0,255) + '...';
        return result;
    }
}