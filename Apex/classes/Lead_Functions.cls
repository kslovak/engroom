global with sharing class Lead_Functions {

	webservice static AshlandWebSiteLeadInsertResults createAshlandWebSiteLead(Lead newLead) {
    
	    AshlandWebSiteLeadInsertResults result = new AshlandWebSiteLeadInsertResults();
    
	    result.result = false;    
    
	    String ashlandWebSiteRecordType = RecordType_Functions.LookupRecordTypeId('Ashland Website Lead', 'Lead');    
    
	    newLead.RecordTypeId = ashlandWebSiteRecordType;
	    newLead.LeadSource = 'Ashland Website';
	    
	    try {
	        insert newLead;
	        result.result = true;
	        result.newId = newLead.Id;
		} catch(Exception e) {
	        result.result = false;
	        result.errorMessage = e.getMessage();
	        writeErrorLog(newLead, e.getMessage());
		}
	        
	    return result;
	}
	
	private static void writeErrorLog(Lead newLead, String msg) {
		String applicationName = 'Lead Creation', versionInfo = '', 
		       logType = 'Error', logCategory = 'Error', recordKey = '', 
		       logMessage = 'Error = ' + msg + ' : Lead = ' + newLead;
        AppLog.write(applicationName, versionInfo, logType, logCategory, recordKey, logMessage);
	}

}