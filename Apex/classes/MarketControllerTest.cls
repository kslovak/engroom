@isTest
class MarketControllerTest {
    
    @isTest
    static void getMarketTest() {
        MarketController mc = new MarketController();
        System.assertEquals( null, mc.getMarket() );
    }
    
    @isTest
    static void getWebMarketTest() {
        MarketController mc = new MarketController();
        System.assertEquals( null, mc.getWebMarket() );
    }
    
    @isTest
    static void getAllMarketsTest() {
        MarketController mc = new MarketController();
        System.assertNotEquals( null, mc.getAllMarkets() );
    }
    
    /**
    @isTest(SeeAllData=true)
    static void getMarketEventsTest() {
        // no events
        MarketController testClass = new MarketController();
        System.assertEquals( null, testClass.getMarketEvents() );
        
        // test events
        Market__c testMarket = new Market__c(Name='testMarketName',Alias__c='testMarketAlias',Tag__c='testMarketTag');
        Market_Event__c testMarketEvent = new Market_Event__c(Name='testMarketEvent',Link__c='testLink');
        testMarketEvent.Market__c = testMarket.Id;
        upsert testMarketEvent;
        ApexPages.StandardController sc = new ApexPages.StandardController(testMarket);
        testClass = new MarketController(sc);
        System.assertEquals( 1, testClass.getMarketEvents().size() );        
    }
    /**/
    
    @isTest(SeeAllData=true)
    static void getMarketArticle1Test() {
        // no events
        MarketController testClass = new MarketController();
        System.assertEquals( null, testClass.getMarketArticle1() );
        
        // test events
        Community_Article__c testArticle = new Community_Article__c(Name='testArticle');
        insert testArticle;
        Market__c testMarket = new Market__c(Name='testMarketName',Alias__c='testMarketAlias', Article1_Default__c = testArticle.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(testMarket);
        testClass = new MarketController(sc);
        System.assertNotEquals( null, testClass.getMarketArticle1() );        
    }
    
    @isTest(SeeAllData=true)
    static void getMarketArticle2Test() {
        // no events
        MarketController testClass = new MarketController();
        System.assertEquals( null, testClass.getMarketArticle2() );
        
        // test events
        Community_Article__c testArticle = new Community_Article__c(Name='testArticle');
        insert testArticle;
        Market__c testMarket = new Market__c(Name='testMarketName',Alias__c='testMarketAlias', Article2_Default__c = testArticle.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(testMarket);
        testClass = new MarketController(sc);
        System.assertNotEquals( null, testClass.getMarketArticle2() );        
    }
    
    
    
    @isTest(SeeAllData=true)
    static void getDocumentationTest() {
        MarketController testClass = new MarketController();
        
         // null content
        testClass.getDocumentation();
        System.assertNotEquals( null, testClass.literature );
        System.assertNotEquals( null, testClass.training );
        System.assertNotEquals( null, testClass.presentations );
        
        // real content
        ContentVersion testContent = new ContentVersion(Title='testContentTitle',contentUrl='url',TagCsv='test;APP-Collateral-Literature;test;testMarketTag;test');
        insert testContent;
        
        Market__c testMarket = new Market__c(Name='testMarketName',Alias__c='testMarketAlias',Tag__c='testMarketTag');
        ApexPages.StandardController sc = new ApexPages.StandardController(testMarket);
        testClass = new MarketController(sc);
        testClass.getDocumentation();
        System.assertNotEquals( null, testClass.literature );
        System.assertEquals( 'testContentTitle', testClass.literature[0].Title );
    }
    
    @isTest
    static void getProductApplicationsTest() {
        // try empty
        MarketController testClass = new MarketController();
        System.assertEquals( null, testClass.getProductApplications() );
        
        // try with associated products
        Market__c testMarket = new Market__c(Name='testName',Alias__c='testAlias',Product_Applications__c='test;testProduct');
        ApexPages.StandardController sc = new ApexPages.StandardController(testMarket);
        testClass = new MarketController(sc);
        
        Product__c testProduct = new Product__c(Name='testProduct',Alias__c='testAlias');
        insert testProduct;
        
        System.assertEquals( 'testProduct', testClass.getProductApplications()[0].Name );
    }
    
    @isTest
    static void getAllWebMarketsTest() {
        MarketController mc = new MarketController();
        System.assertNotEquals( null, mc.getAllWebMarkets() );
    }
    
    @isTest
    static void getMarketsDetailPageTest() {
        MarketController mc = new MarketController();
        
        upsert new ASC_Page__c(name='MarketsDetail',Visualforce_Page_Link__c='blank');
        System.assertNotEquals( null, mc.getMarketsDetailPage() );
    }
    
    @isTest
    static void setSubscriptionTest() {
        MarketController mc = new MarketController();
        System.assertEquals( null, mc.setSubscription() );
    }
    
    @isTest
    static void sendEmailTest() {
        MarketController mc = new MarketController();
        System.assertEquals( null, mc.sendEmail() );
    }
    
    @isTest
    static void getNewsItemsTest() {
        // try null RSS
        Market__c testMarket = new Market__c(Name='testName',Alias__c='testAlias');
        ApexPages.StandardController sc = new ApexPages.StandardController(testMarket);
        MarketController testClass = new MarketController(sc);
        System.assertEquals( new List<NewsController.RSSItemWrapper>(), testClass.getNewsItems() );
        
        // try blank RSS
        testMarket.RSS_Feeds__c = '';
        System.assertEquals( new List<NewsController.RSSItemWrapper>(), testClass.getNewsItems() );
        
        // try valid RSS
        testMarket.RSS_Feeds__c = 'http://compositebuild.com/feed/;';
        System.assertNotEquals( new List<NewsController.RSSItemWrapper>(), testClass.getNewsItems() );
    }
    
    public static testMethod void testgetAllMarkets() {
        Profile prof = [Select Id, Name From Profile Where Name = 'System Administrator'];

        User u = new User();
        u.FirstName = 'TestFest';
        u.LastName = 'User123';
        u.alias = 'tuser123';
        u.Username = 'Test@testengineroom.com.test';
        u.Email = 'Test@testengineroom.com.test';
        u.emailencodingkey = 'UTF-8';
        u.languagelocalekey = 'en_US';
        u.localesidkey = 'en_US';
        u.profileId = prof.Id;
        u.timezonesidkey = 'America/Los_Angeles';
        insert u;
        
       System.runAs(u){
        
            Group grp = new Group();
            grp.Name = 'Market_TestMarket_Group';
            insert grp;
            
            GroupMember gm = new GroupMember();
            gm.UserOrGroupId = u.id;
            gm.GroupId =grp.id;
            insert gm;
            //add market
            Market__C mkt = new Market__C();
            mkt.name = 'Test Market';
            mkt.Alias__c = 'Test Market';
            mkt.Tag__c = 'Market-TestMarket';
            insert mkt;

            test.startTest();
            MarketController mc = new MarketController();
            List<Market__c> m = mc.getAllMarkets();
            System.assertEquals(m.size(), 1);
            test.stopTest();
            
        }
      
     }
}