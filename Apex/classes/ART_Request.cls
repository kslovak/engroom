public with sharing class ART_Request {
  
    public ART_Request__c rec {get;set;}
  
    public Id id {get {return rec.Id;} set {rec.Id = value;}}
    
    public String developerName {get;set;}
    
    public String releaseName {get;set;}
    public String releaseNumber {get;set;}
    
    public String projectName {get;set;}
    public String projectNumber {get;set;}
    
    public Boolean recordChanged {get;set;}
  
    public Integer hoursLeft {get;set;}
	public String hoursLeftStyle {get;set;}

	public String expectedEffort {
		get {
			String result = '';
			result += rec.Expected_Effort_in_Current_Iteration__c + '';
			if (rec.Expected_Effort_in_Current_Iteration__c > 0 && rec.Expected_Effort_in_Current_Iteration__c != rec.Estimated_Effort__c) {
				result += '<BR/><BR/>' + rec.Estimated_Effort__c + ' Total';
				if (rec.Effort_Completed_in_Previous_Iterations__c > 0) {
					result += '<BR/>' + rec.Effort_Completed_in_Previous_Iterations__c + ' Completed';
					Double remainingEffort = rec.Estimated_Effort__c - (rec.Effort_Completed_in_Previous_Iterations__c + rec.Expected_Effort_in_Current_Iteration__c);
					if (remainingEffort > 0 ) {
						result += '<BR/>' + Integer.valueOf(remainingEffort) + ' Remaining';
					}
				}

			}


			return result;
		}
	}


    public ART_Request(ART_Request__c recIn) {
        rec = recIn;
        recordChanged = false;
        developerName = '';
        releaseName = '';
        projectName = '';
        
        try {if (rec.Developer_Assigned__r.Name != null) {developerName = rec.Developer_Assigned__r.Name;}} catch (Exception e) {} 
        try {if (rec.Release__r.Name != null) {releaseName = rec.Release__r.Name;}} catch (Exception e) {}
        try {if (rec.Release__r.Release_Number__c != null) {releaseNumber = rec.Release__r.Release_Number__c;}} catch (Exception e) {}
        try {if (rec.Project__r.Name != null) {projectName = rec.Project__r.Name;}} catch (Exception e) {}
        try {if (rec.Project__r.Project_Number__c != null) {projectNumber = rec.Project__r.Project_Number__c;}} catch (Exception e) {}
    }
  
    public static Map<String,String> StatusToRecordTypeMap = new Map<String,String> {
        '1 - New' => 'New',
        '2 - Authorized' => 'Authorized',
        '3a - Requirements Definition' => 'Requirements Definition',
        '3b - Technical Design' => 'Technical Design',
        '4 - Ready to Schedule' => 'Ready to Schedule',
        '5 - Scheduled' => 'Scheduled',
        '6 - Development' => 'Development',
        '7 - Testing' => 'Testing',
        '8 - Requesting Approval for Release' => 'Requesting Approval for Release',
        '9 - Approved for Release' => 'Approved for Release',
        '10 - Completed' => 'Completed',
        '10A - Rejected' => 'Rejected',
        '10B - Cancelled' => 'Cancelled'
    };  
  
    public static Map<String,Integer> StatusToStatusNumberMap = new Map<String,Integer> {
        '1 - New' => 1,
        '2 - Authorized' => 2,
        '3a - Requirements Definition' => 3,
        '3b - Technical Design' => 3,
        '4 - Ready to Schedule' => 4,
        '5 - Scheduled' => 5,
        '6 - Development' => 6,
        '7 - Testing' => 7,
        '8 - Requesting Approval for Release' => 8,
        '9 - Approved for Release' => 9,
        '10 - Completed' => 10,
        '10A - Rejected' => 10,
        '10B - Cancelled' => 10
    };  
  
    private static Map<String,Id> StatusToRecordTypeIdMap = new Map<String,Id>();

    public static Id retrieveRecordTypeIdForStatus(String status) {
        String recordTypeName = StatusToRecordTypeMap.get(status);
        
        Id recordTypeId = null;
        
        if (StatusToRecordTypeIdMap.containsKey(recordTypeName)) {
            recordTypeId = StatusToRecordTypeIdMap.get(recordTypeName);
        }
        else {
            recordTypeId = RecordType_Functions.LookupRecordTypeId(recordTypeName,'ART_Request__c');
            StatusToRecordTypeIdMap.put(recordTypeName,recordTypeId);
        }
        
        return recordTypeId;
    }
    
    public static void UpdateRecordTypesBasedOnStatus(List<ART_Request__c> requests) {
        for (ART_Request__c request : requests) {
            Id newRecordTypeId = retrieveRecordTypeIdForStatus(request.Status__c);
            
            if (newRecordTypeId != null) {
                if (request.RecordTypeId != newRecordTypeId) {
                    request.RecordTypeId = newRecordTypeId;
                }
            }
            
            Integer statusNumber = StatusToStatusNumberMap.get(request.Status__c);
            if (statusNumber != null) {
                request.Status_Number__c = statusNumber;
            }
        }   
    }
    
    public static List<SelectOption> availableStatuses {
        get {
            List<SelectOption> options = new List<SelectOption>();
            
            Set<String> statusesToInclude = new Set<String>();
            statusesToInclude.add('1 - New');
            statusesToInclude.add('2 - Authorized');
            statusesToInclude.add('3a - Requirements Definition');
            statusesToInclude.add('3b - Technical Design');
            statusesToInclude.add('4 - Ready to Schedule');
            statusesToInclude.add('5 - Scheduled');
            statusesToInclude.add('6 - Development');
            statusesToInclude.add('7 - Testing');
            statusesToInclude.add('8 - Requesting Approval for Release');
            statusesToInclude.add('9 - Approved for Release');
            statusesToInclude.add('10 - Completed');
            statusesToInclude.add('10A - Rejected');
            statusesToInclude.add('10B - Cancelled');
            
            Schema.DescribeFieldResult fieldResult = ART_Request__c.Status__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for(Schema.PicklistEntry f : ple) {
                if (f.isActive()) {
                    if (statusesToInclude.contains(f.getValue())) {
                        options.add(new SelectOption(f.getValue(), f.getValue()));
                    }
                }
            }
                   
            return options;
        }
    }
  
    public static List<AshError> updateRequestStatus(Id requestId, String newStatus) {
        List<AshError> errors = new List<AshError>();
        
        if (IdMethods.isId(requestId)) {
            ART_Request__c request = new ART_Request__c();
            request.Id = requestId;
            request.Status__c = newStatus;

            errors = ART_Request.updateRequest(request);
        }
        else {
            AshError error = new AshError('Error, ' + requestId + ' Request Id is not a valid Id',AshError.SEVERITY_ERROR);
            errors.add(error);
        }
        
        return errors;
    }
  
    public static List<AshError> updateRequestRelease(Id requestId, String newReleaseIdIn) {
        List<AshError> errors = new List<AshError>();
        
        Id newReleaseId = null;
        if (IdMethods.isId(newReleaseIdIn)) {newReleaseId = newReleaseIdIn;}        

        if (IdMethods.isId(requestId)) {
            ART_Request__c request = new ART_Request__c();
            request.Id = requestId;
            request.Release__c = newReleaseId;

            errors = ART_Request.updateRequest(request);
        }
        else {
            AshError error = new AshError('Error, ' + requestId + ' Request Id is not a valid Id',AshError.SEVERITY_ERROR);
            errors.add(error);
        }
        
        return errors;
    }

    public static List<AshError> updateRequestEstimatedEffort(Id requestId, Integer newEstimatedEffortIn, Integer newEstimatedEffortinIterationIn, Integer newEffortCompletedinPreviousIterations) {
        List<AshError> errors = new List<AshError>();
        
        ART_Request__c request = new ART_Request__c();
        request.Id = requestId;
		if (newEstimatedEffortIn != 0) {request.Estimated_Effort__c = newEstimatedEffortIn;} else {request.Estimated_Effort__c = null;}
        if (newEstimatedEffortinIterationIn != 0) {request.Estimated_Effort_in_Iteration__c = newEstimatedEffortinIterationIn;} else {request.Estimated_Effort_in_Iteration__c = null;}
        if (newEffortCompletedinPreviousIterations != 0) {request.Effort_Completed_in_Previous_Iterations__c = newEffortCompletedinPreviousIterations;} else {request.Effort_Completed_in_Previous_Iterations__c = null;}

        errors = ART_Request.updateRequest(request);
        
        return errors;
    }

    public static List<AshError> updateRequest(ART_Request__c request) {
        List<AshError> errors = new List<AshError>();
        
        try {
            update request;
        }
        catch (Exception e) {
            AshError error = new AshError(e.getMessage(),AshError.SEVERITY_ERROR);
            errors.add(error);
        }
        
        return errors;
    }
/*  
    public class ByDeveloper {
        public Id devUserId {get;set;}
        public String devName {get;set;}
        public Boolean visible {get;set;}
        public List<ART_Request> requests {get;set;} 
        public List<SelectOption> moveOptions {get;set;}
        
        public ByDeveloper() {
            requests = new List<ART_Request>();
            visible = true;
        }
    }

    public class RequestsByDevelopers {
        
        
        public RequestsByDevelopers() {
            developers = new List<ByDeveloper>();
        }
    }
*/

/*
    public static List<ART_Request.ByDeveloper> retrieveRequestsByDevelopers() {
        refreshAllRequests();
        refreshDevRequestsMap();
        
        // Update Map with values we want to persist
        if (byDevelopersInternal != null) {
            for (ART_Request.ByDeveloper byDeveloper : byDevelopersInternal) {
                ART_Request.ByDeveloper itemFromMap = byDevelopersMap.get(byDeveloper.devUserId);
                if (itemFromMap != null) {itemFromMap.visible = byDeveloper.visible;}
            }
        }
        
        byDevelopersInternal = new List<ART_Request.ByDeveloper>(byDevelopersMap.values());
        refreshMoveOptionsForEachDevRequest();
        
        if (firstRun) {
            firstRun = false;
            if (urlParameters.useStartingDevUserId) {
                if (devExists(urlParameters.startingDevUserId)) {
                    hideAllDevs();
                    showDev(urlParameters.startingDevUserId);
                } 
            }
        }
        
        return byDevelopersInternal;
    }

    public static List<ART_Request> retrieveRequests(List<String> statusesToQuery,
                                                     List<String> projectIdsToQuery,
                                                     List<String> releaseIdsToQuery) {

        List<ART_Request> results = new List<ART_Request>();                                                
        List<ART_Request__c> requests;
         
        try {                                                   
            List<ART_Request__c> requests = [ 
                    SELECT Id, 
                           Name, 
                           Request_Number__c,
                           Status__c, 
                           Status_Formatted__c,
                           Developer_Assigned__c, 
                           Developer_Assigned__r.Name, 
                           Estimated_Effort__c, 
                           Priority__c,
                           Need_By_Date__c,
                           Developer_Priority__c,
                           Project__c,
                           Project__r.Name,
                           Project__r.Project_Number__c,
                           Release__c,
                           Release__r.Name,
                           Release__r.Release_Number__c
                     FROM ART_Request__c
                    WHERE (Developer_Assigned__c IN :developerIds OR Developer_Assigned__c = NULL)
                      AND Status__c IN :statusesToQuery
                      AND Release__c IN :releaseIdsToQuery  
                      AND Project__c IN :projectIdsToQuery 
                    ORDER BY Developer_Assigned__r.Name,Developer_Priority__c
                    LIMIT :maxRowsParam
            ];
        } catch (Exception e) {requests = new List<ART_Request__c>();}

        for (ART_Request__c request : requests) {results.add(new ART_Request(request));}
        
        return results;
    } 

    public static Map<Id,ByDeveloper> createByDeveloperMap(List<ART_Request> requests) {
        ART_Request.ByDeveloper byDeveloper;
        byDevelopersMap = new Map<Id, ART_Request.ByDeveloper>(); 

        // Create a Holder for Unassigned 
        byDeveloper = new ART_Request.ByDeveloper();
        byDeveloper.devUserId = null;
        byDeveloper.devName = 'Unassigned';
        byDeveloper.visible = false;
        byDevelopersMap.put(null,byDeveloper);

        for (ART_Request request : allRequests) {
            byDeveloper = byDevelopersMap.get(request.rec.Developer_Assigned__c);
            
            if (byDeveloper != null) {
                byDeveloper.requests.add(request);
            }       
            else {
                byDeveloper = new ART_Request.ByDeveloper();
                byDeveloper.devUserId = request.rec.Developer_Assigned__c;
                byDeveloper.devName = request.rec.Developer_Assigned__r.Name;
                byDeveloper.requests.add(request);
                byDeveloper.visible = true;
                byDevelopersMap.put(byDeveloper.devUserId,byDeveloper);
            }
        }
    }


*/

  
    public static void developerPriority_MoveUp(Id id) {developerPriority_MoveUp(id, false);}
    public static void developerPriority_MoveUp(Id id, Boolean cleanPrioritiesAfterMove) {
        ART_Request artRequest = retrieveART_RequestForDeveloperPriorityChange(id);
        if (artRequest != null) {
            artRequest.developerPriority_MoveUp(cleanPrioritiesAfterMove);
        }
    }
    
    public static void developerPriority_MoveDown(Id id) {developerPriority_MoveDown(id, false);}
    public static void developerPriority_MoveDown(Id id, Boolean cleanPrioritiesAfterMove) {
        ART_Request artRequest = retrieveART_RequestForDeveloperPriorityChange(id);
        if (artRequest != null) {
            artRequest.developerPriority_MoveDown(cleanPrioritiesAfterMove);
        }
    }
    
    public static void developerPriority_MoveTop(Id id) {developerPriority_MoveTop(id, false);}
    public static void developerPriority_MoveTop(Id id, Boolean cleanPrioritiesAfterMove) {
        ART_Request artRequest = retrieveART_RequestForDeveloperPriorityChange(id);
        if (artRequest != null) {
            artRequest.developerPriority_MoveTop(cleanPrioritiesAfterMove);
        }
    }
    
    public static void developerPriority_MoveBottom(Id id) {developerPriority_MoveBottom(id, false);}
    public static void developerPriority_MoveBottom(Id id, Boolean cleanPrioritiesAfterMove) {
        ART_Request artRequest = retrieveART_RequestForDeveloperPriorityChange(id);
        if (artRequest != null) {
            artRequest.developerPriority_MoveBottom(cleanPrioritiesAfterMove);
        }
    }
    
    public static void developerPriority_MoveBefore(Id id, Id relativeToARTRequestId) {developerPriority_MoveBefore(id, relativeToARTRequestId, false);}
    public static void developerPriority_MoveBefore(Id id, Id relativeToARTRequestId, Boolean cleanPrioritiesAfterMove) {
        ART_Request artRequest = retrieveART_RequestForDeveloperPriorityChange(id);
        if (artRequest != null) {
            artRequest.developerPriority_MoveBefore(relativeToARTRequestId, cleanPrioritiesAfterMove);
        }
    }
    
    public static void developerPriority_MoveAfter(Id id, Id relativeToARTRequestId) {developerPriority_MoveAfter(id, relativeToARTRequestId, false);}
    public static void developerPriority_MoveAfter(Id id, Id relativeToARTRequestId, Boolean cleanPrioritiesAfterMove) {
        ART_Request artRequest = retrieveART_RequestForDeveloperPriorityChange(id);
        if (artRequest != null) {
            artRequest.developerPriority_MoveAfter(relativeToARTRequestId, cleanPrioritiesAfterMove);
        }
    }
    
    public static void developerPriority_MoveToDeveloper(Id id, Id newDeveloperUserId) {developerPriority_MoveToDeveloper(id, newDeveloperUserId, false);}
    public static void developerPriority_MoveToDeveloper(Id id, Id newDeveloperUserId, Boolean cleanPrioritiesAfterMove) {
        ART_Request artRequest = retrieveART_RequestForDeveloperPriorityChange(id);
        if (artRequest != null) {
            artRequest.developerPriority_MoveToDeveloper(newDeveloperUserId, cleanPrioritiesAfterMove);
        }
    }
    
    public static void developerPriority_MoveToUnassigned(Id id) {developerPriority_MoveToUnassigned(id, false);}
    public static void developerPriority_MoveToUnassigned(Id id, Boolean cleanPrioritiesAfterMove) {
        ART_Request artRequest = retrieveART_RequestForDeveloperPriorityChange(id);
        if (artRequest != null) {
            artRequest.developerPriority_MoveToUnassigned(cleanPrioritiesAfterMove);
        }
    }
     
    
    private static ART_Request retrieveART_RequestForDeveloperPriorityChange(Id id) {
        ART_Request result;
        ART_Request__c request; 
        try {
            request = [SELECT Id, Name, Developer_Assigned__c, Developer_Priority__c FROM ART_Request__c WHERE Id = :id];
        }
        catch (Exception e) {}
        if (request != null) {
            result = new ART_Request(request);
        }
        return result;
    }
    
    public void developerPriority_MoveUp() {developerPriority_MoveUp(false);}
    public void developerPriority_MoveUp(Boolean cleanPrioritiesAfterMove) {
        ART_Request_DeveloperPriorityMethods.MoveInstruction instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction.action = ART_Request_DeveloperPriorityMethods.MOVE_UP;
        instruction.request = this.rec;
        instruction.cleanPrioritiesAfterMove = cleanPrioritiesAfterMove;
    
        ART_Request_DeveloperPriorityMethods.move(instruction); 
    }
    
    public void developerPriority_MoveDown() {developerPriority_MoveDown(false);}
    public void developerPriority_MoveDown(Boolean cleanPrioritiesAfterMove) {
        ART_Request_DeveloperPriorityMethods.MoveInstruction instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction.action = ART_Request_DeveloperPriorityMethods.MOVE_DOWN;
        instruction.request = this.rec;
        instruction.cleanPrioritiesAfterMove = cleanPrioritiesAfterMove;
    
        ART_Request_DeveloperPriorityMethods.move(instruction);
    }

    public void developerPriority_MoveTop() {developerPriority_MoveTop(false);}
    public void developerPriority_MoveTop(Boolean cleanPrioritiesAfterMove) {
        ART_Request_DeveloperPriorityMethods.MoveInstruction instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction.action = ART_Request_DeveloperPriorityMethods.MOVE_TOP;
        instruction.request = this.rec;
        instruction.cleanPrioritiesAfterMove = cleanPrioritiesAfterMove;
    
        ART_Request_DeveloperPriorityMethods.move(instruction);
    }

    public void developerPriority_MoveBottom() {developerPriority_MoveBottom(false);}
    public void developerPriority_MoveBottom(Boolean cleanPrioritiesAfterMove) {
        ART_Request_DeveloperPriorityMethods.MoveInstruction instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction.action = ART_Request_DeveloperPriorityMethods.MOVE_BOTTOM;
        instruction.request = this.rec;
        instruction.cleanPrioritiesAfterMove = cleanPrioritiesAfterMove;
    
        ART_Request_DeveloperPriorityMethods.move(instruction);
    }

    public void developerPriority_MoveBefore(Id relativeToARTRequestId) {developerPriority_MoveBefore(relativeToARTRequestId,false);}
    public void developerPriority_MoveBefore(Id relativeToARTRequestId, Boolean cleanPrioritiesAfterMove) {
        ART_Request_DeveloperPriorityMethods.MoveInstruction instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction.relativeRequestId = relativeToARTRequestId;
        instruction.action = ART_Request_DeveloperPriorityMethods.MOVE_BEFORE;
        instruction.request = this.rec;
        instruction.cleanPrioritiesAfterMove = cleanPrioritiesAfterMove;
    
        ART_Request_DeveloperPriorityMethods.move(instruction);
    }

    public void developerPriority_MoveAfter(Id relativeToARTRequestId) {developerPriority_MoveAfter(relativeToARTRequestId,false);}
    public void developerPriority_MoveAfter(Id relativeToARTRequestId, Boolean cleanPrioritiesAfterMove) {
        ART_Request_DeveloperPriorityMethods.MoveInstruction instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction.relativeRequestId = relativeToARTRequestId;
        instruction.action = ART_Request_DeveloperPriorityMethods.MOVE_AFTER;
        instruction.request = this.rec;
        instruction.cleanPrioritiesAfterMove = cleanPrioritiesAfterMove;
    
        ART_Request_DeveloperPriorityMethods.move(instruction);
    }

    public void developerPriority_MoveToDeveloper(Id newDeveloperUserId) {developerPriority_MoveToDeveloper(newDeveloperUserId,false);}
    public void developerPriority_MoveToDeveloper(Id newDeveloperUserId, Boolean cleanPrioritiesAfterMove) {
        ART_Request_DeveloperPriorityMethods.MoveInstruction instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction.developerUserId = newDeveloperUserId;
        instruction.action = ART_Request_DeveloperPriorityMethods.MOVE_TO_DEVELOPER;
        instruction.request = this.rec;
        instruction.cleanPrioritiesAfterMove = cleanPrioritiesAfterMove;
    
        ART_Request_DeveloperPriorityMethods.move(instruction); 
    }
    
    public void developerPriority_MoveToUnassigned() {developerPriority_MoveToUnassigned(false);}
    public void developerPriority_MoveToUnassigned(Boolean cleanPrioritiesAfterMove) {
        ART_Request_DeveloperPriorityMethods.MoveInstruction instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction = new ART_Request_DeveloperPriorityMethods.MoveInstruction();
        instruction.action = ART_Request_DeveloperPriorityMethods.MOVE_TO_UNASSIGNED;
        instruction.request = this.rec;
        instruction.cleanPrioritiesAfterMove = cleanPrioritiesAfterMove;
    
        ART_Request_DeveloperPriorityMethods.move(instruction);
    }

  
}