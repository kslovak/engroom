@isTest
class MessageControllerTest {

    @isTest(SeeAllData=true)
    static void getMyTest() {
        MessageController testClass = new MessageController();
        
        System.assertNotEquals( null, testClass.getMy() );
    }
    
    @isTest(SeeAllData=true)
    static void getConversationsTest() {
        MessageController testClass = new MessageController();
        
        System.assertNotEquals( null, testClass.getConversations() );
    }
    
    @isTest
    static void getConversationDetailTest() {
        MessageController testClass = new MessageController();
        
        String cid = ApexPages.currentPage().getParameters().get('cid');
        System.assert( (cid == null || cid == '') == (testClass.getConversationDetail() == null) );
    }
    
    @isTest
    static void getConversationDetailMessages() {
        MessageController testClass = new MessageController();
        
        if(testClass.getConversationDetail() != null) {
            System.assertNotEquals( null, testClass.getConversationDetailMessages() );
        } else {
            System.assertEquals( null, testClass.getConversationDetailMessages() );
        }
    }
    
    @isTest(SeeAllData=true)
    static void getConnectionsTest() {
        MessageController testClass = new MessageController();
        
        testClass.userQuery = '';
        System.assertNotEquals( null, testClass.getConnections() );
    }
    
    @isTest
    static void sendMessageTest() {
        MessageController testClass = new MessageController();
        
        System.assertEquals( null, testClass.sendMessage() );
    }
    
    @isTest
    static void getConversationViewTest() {
        MessageController testClass = new MessageController();
        
        System.assertEquals( null, testClass.getConversationView() );
    }
    
    @isTest
    static void conversationSearchTest() {
        MessageController testClass = new MessageController();
        
        System.assertEquals( null, testClass.conversationSearch() );
    }
    
    @isTest
    static void messageSearchTest() {
        MessageController testClass = new MessageController();
        
        System.assertEquals( null, testClass.messageSearch() );
    }
    
    @isTest
    static void userSearchTest() {
        MessageController testClass = new MessageController();
        
        System.assertEquals( null, testClass.userSearch() );
    }
    
    @isTest
    static void replyToMessageTest() {
        MessageController testClass = new MessageController();
        
        System.assertEquals( null, testClass.replyToMessage() );
    }
    
    @isTest(SeeAllData=true)
    static void getJSONTest() {
        MessageController testClass = new MessageController();
        
        System.assertNotEquals( null, testClass.getJSON() );
    }
}