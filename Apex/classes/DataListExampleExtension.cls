public class DataListExampleExtension{

    private final Market__c market;
    public List<ContentVersion> literature { get; set; }
    public String valueToRemove { get; set; }
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public DataListExampleExtension(ApexPages.StandardController stdController) {
        this.market = (Market__c)stdController.getRecord();
        getDocumentation();
    }

    public void getDocumentation() {
        this.literature = [SELECT Id FROM ContentVersion Where IsLatest=true LIMIT 5];
    }
    
    public PageReference removeFromArray() {
        return null;
    }
}