public class SAP_LISD_Controller {

    static final String ORDERBY_NAME = 'User__r.Name';
    static final String ORDERBY_NUM  = 'Name';

    static final String ARROW_UP = '&#9650;';
    static final String ARROW_DN = '&#9660;';
    
    Boolean enableAcc, updAllowed; SAP_LISD sapLisd; Id accountId;
    public List<SAP_LISD> sapLisds {get; private set;}
    public List<SAP_Seller> sapSellers {get; private set;} 
    
    public String industry   {get {return sapLisd.industryValue;}   set {sapLisd.industryValue = value;}}
    public String endUseCust {get {return sapLisd.endUseCustValue;} set {sapLisd.endUseCustValue = value;}}
    public String salesDst   {get {return sapLisd.salesDstValue;}   set {sapLisd.salesDstValue = value;}}
    public String salesOfc   {get {return sapLisd.salesOfcValue;}   set {sapLisd.salesOfcValue = value;}}
    public String salesGrp   {get {return sapLisd.salesGrpValue;}   set {sapLisd.salesGrpValue = value;}}
    public String custGroup  {get {return sapLisd.custGroupValue;}  set {sapLisd.custGroupValue = value;}}
    public String sellerNum  {get {return sapLisd.sellerValue;}     set {sapLisd.sellerValue = value;}}
    
    public String sdhelp     {get {return sapLisd.helpText_sd;}  private set;}
    public String sohelp     {get {return sapLisd.helpText_so;}  private set;}
    public String sghelp     {get {return sapLisd.helpText_sg;}  private set;}
    public String cghelp     {get {return sapLisd.helpText_cg;}  private set;}

    public List<SelectOption> industries  {get {return sapLisd.industries;}  private set;}
    public List<SelectOption> endUseCusts {get {return sapLisd.endUseCusts;} private set;}
    public List<SelectOption> salesDsts   {get {return sapLisd.salesDsts;}   private set;}
    public List<SelectOption> salesOfcs   {get {return sapLisd.salesOfcs;}   private set;}
    public List<SelectOption> salesGrps   {get {return sapLisd.salesGrps;}   private set;}
    public List<SelectOption> custGroups  {get {return sapLisd.custGroups;}  private set;}
    public List<SelectOption> sellerOpts  {get {return sapLisd.sellerOpts;}  private set;}
    
    public Boolean readOnlyAC             {get {return sapLisd.readOnlyAC;} private set;}
    public Boolean readOnlyL1             {get {return sapLisd.readOnlyL1;} private set;}
    public Boolean readOnlyEC             {get {return sapLisd.readOnlyEC;} private set;}
    public Boolean readOnlySD             {get {return sapLisd.readOnlySD;} private set;}
    public Boolean readOnlySO             {get {return sapLisd.readOnlySO;} private set;}
    public Boolean readOnlySG             {get {return sapLisd.readOnlySG;} private set;}
    public Boolean readOnlyCG             {get {return sapLisd.readOnlyCG;} private set;}
    public Boolean readOnlySL             {get {return sapLisd.readOnlySL;} private set;}

    public String  accsarea               {get {return sapLisd.salesArea;} private set;}
    public Account acc                    {get {return sapLisd.acc;} private set;}
    public User seller                    {get {return sapLisd.seller;} private set;}

    public Contact con                    {get; private set;}
    public SAP_Seller__c sapSeller        {get; private set;}
    public String  message1               {get; private set;}

    public Boolean renderLisds            {get; private set;}
    public Boolean renderMsg1             {get; private set;}
    public Boolean renderSellers          {get; private set;}
    public Boolean renderUpdate1          {get; private set;}

    public Boolean descByName             {get; private set;}
    public Boolean descByNum              {get; private set;}
    public Boolean isDesc                 {get; private set;}
    public String  orderBy                {get; private set;}
    public String  sortImgName            {get; private set;}
    public String  sortImgNum             {get; private set;}

    public Boolean allRecs                {get; set;}
    public String  srchStr                {get; set;}
    public String  sapSellerId            {get; set;}

    public SAP_LISD_Controller() {this(null);}
    
    public SAP_LISD_Controller(ApexPages.StandardController sc) {
    	debug('Constructor : begins');
    	OrgSettings__c orgSettings = OrgSettings__c.getInstance();
    	updAllowed = orgSettings.Allowed_to_Update_LISD__c;
        con = new Contact(); 
        PageReference pr = ApexPages.currentPage();
        if (pr != null) {
        	debug('pr.getParameters() : ' + pr.getParameters());
        	con.AccountId = pr.getParameters().get('accountId');
        }
        init1(); init2();
    }
    
    private void debug(String s) {System.debug(LoggingLevel.INFO, 'SAP_LISD_Controller : ' + s);}
    
    private Boolean isNull(String s) {return String.isBlank(s);}
    
    private void init1() {
        accountId = con.AccountId; sapLisds = null; 
        renderLisds = false; renderMsg1 = false; 
        renderSellers = false; renderUpdate1 = false;
        debug('init1 : accountId = ' + accountId);
    }
    
    private void init2() {
        descByName = false; descByNum = true; isDesc = false; 
        orderBy = ORDERBY_NAME; sortImgName = ARROW_UP; sortImgNum = '';
    }
    
    public void initAction1() {
        setEnableAcc(false); setSapLisd(null);
    	if (isNull(accountId)) {return;} setSapLisds();
    }
    
    public SAP_LISD getSapLisd() {return sapLisd;}
    public void setSapLisd(SAP_LISD sl) {
        debug('setSapLisd :      sl = ' + sl); 
        debug('setSapLisd : sapLisd = ' + sapLisd);
        debug('setSapLisd :     con = ' + con);
        init1(); if (sapLisd != null) {return;}
    	if (sl == null) {
    		sapLisd = new SAP_LISD(); setIndustries();
    	} else {sapLisd = sl;}
    }
    
    public Boolean getEnableAcc() {return enableAcc;}
    public void setEnableAcc(Boolean b) {
    	if (enableAcc != null) {return;}
    	if (b == null) {enableAcc = true;} else {enableAcc = b;}
    }

    public void setIndustries() {
        init1(); if (sapLisd == null) {return;}
        sapLisd.setIndustries(accountId, !enableAcc);
    }
    
    public void setEndUseCusts() {sapLisd.setEndUseCusts();}
    
    public void setSalesDsts()   {sapLisd.setSalesDsts();}
    
    public void setSalesOfcs()   {sapLisd.setSalesOfcs();}
    
    public void setSalesGrps()   {sapLisd.setSalesGrps();}

    public void setCustGroups()  {sapLisd.setCustGroups();}

    public void setSellerOpts()  {sapLisd.setSellerOpts();}
    
    public void setSeller()      {sapLisd.setSeller();}
    
    private void setRenderFlags() {
        allRecs = false; renderMsg1 = false; renderSellers = false; 
        renderLisds = (sapLisds != null && !sapLisds.isEmpty());
        renderUpdate1 = (updAllowed && renderLisds && sapSeller != null); 
        if (!renderLisds) {
            //message1 = 'No LISDs found for the selected Account'; renderMsg1 = true;
        }
    }
    
    private void setSapLisds() {
        sapLisds = SAP_LISD_Functions.getAccountLisds(accountId);
        setRenderFlags();
    }
    
    public void searchSellers() {
        renderMsg1 = false;  
    	sapSellers = SAP_Seller_Functions.getSellers(srchStr, orderBy, isDesc, 1000);
    	renderSellers = !sapSellers.isEmpty(); renderLisds = !renderSellers;
    }
    
    public void orderByName() {
    	descByName = !descByName; isDesc = descByName; descByNum = true; sortImgNum = '';
        sortImgName = descByName ? ARROW_DN : ARROW_UP;
    	orderBy = ORDERBY_NAME; searchSellers();
    }
    
    public void orderByNum() {
        descByNum = !descByNum; isDesc = descByNum; descByName = true; sortImgName = '';
        sortImgNum = descByNum ? ARROW_DN : ARROW_UP;
        orderBy = ORDERBY_NUM; searchSellers();
    }
    
    public void selectSeller() {
        debug('selectSeller : sapSellerId = ' + sapSellerId);
        if (isNull(sapSellerId)) {return;}
    	sapSeller = SAP_Seller_Functions.getSeller(sapSellerId);
    	setRenderFlags(); 
    }

    public void updateSellers() {
    	Boolean b = SAP_LISD_Functions.updateSapLisds(sapLisds, sapSeller); 
    	if (b) {setSapLisds();}
    }
    
}