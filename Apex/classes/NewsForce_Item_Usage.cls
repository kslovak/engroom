public without sharing class NewsForce_Item_Usage {

	public NewsForce_Item_Usage(Id newsItemId, Id userId) {
		this.rec = new NewsForce_Item_Usage__c();
		this.rec.NewsForce_Item__c = newsItemId;
		this.rec.User__c = userId;
	}


	public NewsForce_Item_Usage(NewsForce_Item_Usage__c rec) {
		this.rec = rec;
	}
 
	public NewsForce_Item_Usage__c rec {get;private set;}

	public void userHasViewedItem() {
		rec.Viewed__c = true;
		if (rec.View_Count__c == null) {rec.View_Count__c = 0;}
		rec.View_Count__c++;
		rec.Last_Viewed_On__c = System.Now();
		upsert rec;
	}

	public void userHasDismissedItem() {
		rec.Dismissed__c = true;
		rec.Dismissed_On__c = System.Now();
		upsert rec;
	}
}