public class ASI_Call_Report_Distribution {

    public ASI_Call_Report_Distribution__c crd {get; private set;}

    public Integer                      recnum {get; set;}
    public Boolean                    selected {get; set;}
    public Boolean             industryDefault {get; set;}
    public Boolean               senderDefault {get; set;}
    public Boolean              sendCallReport {get; set;}

    private void debug(String s) {
        System.debug(LoggingLevel.INFO, 'ASI_Call_Report_Distribution : ' + s);
    }

    public ASI_Call_Report_Distribution(User u, Integer n) {
        crd = new ASI_Call_Report_Distribution__c(); 
        crd.Receiver__r = u; crd.Receiver__c = u.Id; init(crd, n);
    }
    
    public ASI_Call_Report_Distribution(ASI_Call_Report_Config__c c) {this(c, 1);}

    public ASI_Call_Report_Distribution(ASI_Call_Report_Config__c c, Integer n) {
        crd = new ASI_Call_Report_Distribution__c(); 
        crd.ASI_Call_Report_Config__c = c.Id;
        crd.Receiver__r = c.Receiver__r; crd.Receiver__c = c.Receiver__r.Id;
        init(crd, n);
    }
    
    public ASI_Call_Report_Distribution(ASI_Call_Report_Distribution__c d) {this(d, 1);}

    public ASI_Call_Report_Distribution(ASI_Call_Report_Distribution__c d, Integer n) {init(d, n);}
    
    private void init(ASI_Call_Report_Distribution__c d, Integer n) {
        crd = d; recnum = n; selected = false;
        industryDefault = false; senderDefault = false;
    }

/*    
    private static final String CHECKMARK = '&#x2714;';
    private static final String EMPTYBOX = '&#x2610;';
    public String confidential {
        private set; get {
            confidential = crd != null && crd.Confidential__c ? CHECKMARK : EMPTYBOX;
            return confidential;
        }
    }
    
    public ASI_Call_Report_Distribution(ApexPages.StandardController sc) {
        debug('Constructor(sc)');
        init1(sc.getRecord());
    }
    
    private void init1(SObject sobj) {
        PageReference pr = System.currentPageReference(); String s;
        Map<String, String> params = pr.getParameters(); 
        debug('params = ' + params);
    }
    
    public PageReference initAction1() {
        debug('initAction1');
        return null;
    }
*/    
}