public class VISharingUtils {

    private static final String ROLE_AND_SUBORDINATES = 'RoleAndSubordinates';
    private static final Set<String> PORTAL_ROLES = new Set<String>{'Worker'};
    private static final Set<String> PORTAL_TYPES = VIUtils.portalTypes;
    private static final String DELIM = ':';

    private static void debug(String s) {
        System.debug(LoggingLevel.INFO, 'VISharingUtils : ' + s);
    }

    public static void accountsCreated(List<Account> accounts) {
        Set<Id> accountIds = getAccountIds(accounts);
        createAccountShares(accountIds);
    }

    private static Set<Id> getAccountIds(List<Account> accounts) {
        Set<Id> accountIds = new Set<Id>();
        for (Account a : accounts) {accountIds.add(a.Id);}
        return accountIds;
    }

    private static List<Account> getAccounts(Set<Id> accountIds) {
        List<Account> accounts = new List<Account>();
        if (accountIds == null || accountIds.isEmpty()) {return accounts;}
        accounts = [select Id, VI_Parent_Account__c, 
                           VI_Parent_Account__r.VI_Parent_Account__c 
                      from Account where Id in :accountIds];
        return accounts;
    }
    
    private static Map<Id, Account> getParents(Set<Id> parentIds) {
        Map<Id, Account> parents = new Map<Id, Account>();
        if (parentIds == null || parentIds.isEmpty()) {return parents;}
        Set<Id> parentIds2 = new Set<Id>(parentIds);
        for (Account a : [SELECT VI_Parent_Account__c 
                            FROM Account WHERE Id IN :parentIds]) {
            if (a.VI_Parent_Account__c != null) {
                parentIds2.add(a.VI_Parent_Account__c);
            }
        }
        parents = new Map<Id, Account>([
            select Id from Account where Id in :parentIds2]);
                  //AND (IsCustomerPortal = TRUE or IsPartner = TRUE)]);
        debug('parents : ' + parents);
        return parents;
    }
    
    private static void addChildIds(Set<Id> accountIds) {
        if (accountIds == null || accountIds.isEmpty()) {return;}
        Set<Id> childIds = new Set<Id>();
        for (Account a : [SELECT Id FROM Account 
                           WHERE VI_Parent_Account__c IN :accountIds]) {
            childIds.add(a.Id);
        }
        if (!childIds.isEmpty()) {accountIds.addAll(childIds);}
    }
    
    private static Map<Id, UserRole> getUserRoles(Set<Id> accountIds) {
        Map<Id, UserRole> userRoles = new Map<Id, UserRole>();
        if (accountIds == null || accountIds.isEmpty()) {return userRoles;}
        userRoles = new Map<Id, UserRole>([
            SELECT Id, PortalAccountId
              FROM UserRole
             WHERE PortalAccountId IN :accountIds
               AND PortalType in :PORTAL_TYPES
               AND PortalRole in :PORTAL_ROLES]);
        debug('userRoles : ' + userRoles);
        return userRoles;
    }
    
    private static Map<Id, List<Group>> getGroups(Map<Id, UserRole> userRoles) {
        Map<Id, List<Group>> gmap = new Map<Id, List<Group>>();
        if (userRoles == null || userRoles.isEmpty()) {return gmap;}
        List<Group> glist; Id portalAccountId; UserRole userRole;
        for (Group g : [SELECT Id, RelatedId FROM Group
                         WHERE RelatedId IN :userRoles.keySet()
                           AND Type = :ROLE_AND_SUBORDINATES]) {
            userRole = userRoles.get(g.RelatedId);
            if (userRole != null) {
                portalAccountId = userRole.PortalAccountId;
                if (gmap.containsKey(portalAccountId)) {
                    glist = gmap.get(portalAccountId);
                } else {glist = new List<Group>();}
                glist.add(g); gmap.put(portalAccountId, glist);
            }
        }
        debug('gmap : ' + gmap); 
        return gmap;
    }

    private static void addAccountShares(Account a, List<Group> glist, 
                                         Map<String, AccountShare> accountShares) {
        if (glist == null || glist.isEmpty()) {return;}
        for (Group g : glist) {
            accountShares.put(a.Id + DELIM + g.Id,
                new AccountShare(AccountId = a.Id,
                    UserOrGroupId = g.Id,
                    AccountAccessLevel = 'Edit',
                    OpportunityAccessLevel = 'None',
                    CaseAccessLevel = 'None'));
        }
    }
    
    private static Set<Id> getParentIds(List<Account> accounts) {
        Set<Id> parentIds = new Set<Id>();
        if (accounts.isEmpty()) {return parentIds;}
        for (Account a : accounts) {
            if (a.VI_Parent_Account__c != null) {
                parentIds.add(a.VI_Parent_Account__c);
                if (a.VI_Parent_Account__r != null && 
                    a.VI_Parent_Account__r.VI_Parent_Account__c != null) {
                    parentIds.add(a.VI_Parent_Account__r.VI_Parent_Account__c);
                }
            }
        }
        debug('parentIds : ' + parentIds);
        return parentIds;
    }

    public static void createAccountShares(Set<Id> accountIds) {
        List<Account> accounts = getAccounts(accountIds);
        Set<Id> parentIds = getParentIds(accounts);
        if (parentIds.isEmpty()) {return;}

        Map<Id, Account> parents = getParents(parentIds);
        if (parents.isEmpty()) {return;}

        Map<Id, UserRole> userRoles = getUserRoles(parents.keySet());
        if (userRoles.isEmpty()) {return;}

        Map<Id, List<Group>> gmap = getGroups(userRoles);
        if (gmap.isEmpty()) {return;}

        Map<String, AccountShare> accountShares = new 
        Map<String, AccountShare>(); Account p; List<Group> glist;
        for (Account a : accounts) {
            p = parents.get(a.VI_Parent_Account__c);
            if (p != null && p.Id != null && gmap.containsKey(p.Id)){
                glist = gmap.get(p.Id); addAccountShares(a, glist, accountShares);
            }

            p = parents.get(a.VI_Parent_Account__r.VI_Parent_Account__c);
            if (p != null && p.Id != null && gmap.containsKey(p.Id)){
                glist = gmap.get(p.Id); addAccountShares(a, glist, accountShares);
            }
        }

        debug('accountShares : ' + accountShares.values());
        if (!accountShares.isEmpty()) {
            Database.insert(accountShares.values(), false);
        }
    }
    
    private static Boolean isChanged(Id id1, Id id2) {
        debug('id1 = ' + id1 + ' : id2 = ' + id2);
        return (id1 == null && id2 != null) ||
               (id2 == null && id1 != null) || (id1 != id2);
    }
    
    public static void accountsUpdated(List<Account> newAccounts, Map<Id, Account> oldAccounts) {
        Set<Id> accountIds1 = new Set<Id>(), accountIds2 = new Set<Id>(); Account oldAccount;
        Set<Id> oldParentIds = new Set<Id>();
        for (Account newAccount : newAccounts) {
            oldAccount = oldAccounts.get(newAccount.Id);
            if (isChanged(newAccount.VI_Parent_Account__c, oldAccount.VI_Parent_Account__c)) {
                if (newAccount.VI_Parent_Account__c != null) {
                    accountIds1.add(newAccount.Id);
                }
                if (oldAccount.VI_Parent_Account__c != null) {
                    accountIds2.add(newAccount.Id);
                    oldParentIds.add(oldAccount.VI_Parent_Account__c);
                }
            }
        }
        addChildIds(accountIds1); addChildIds(accountIds2);
        if (!accountIds1.isEmpty()) {createAccountShares(accountIds1);}
        if (!accountIds2.isEmpty()) {deleteAccountShares(accountIds2, oldParentIds);}
    }

    public static void deleteAccountShares(Set<Id> accountIds, Set<Id> parentIds) {
        if (accountIds.isEmpty() || parentIds.isEmpty()) {return;}

        Map<Id, Account> parents = getParents(parentIds);
        if (parents.isEmpty()) {return;}

        Map<Id, UserRole> userRoles = getUserRoles(parents.keySet());
        if (userRoles.isEmpty()) {return;}

        Set<Id> groupIds = new Set<Id>();
        for (Group g : [SELECT Id FROM Group
                         WHERE RelatedId IN :userRoles.keySet()
                           AND Type = :ROLE_AND_SUBORDINATES]) {
            groupIds.add(g.Id);
        }
        if (groupIds.isEmpty()) {return;}

        List<AccountShare> accountShares = [
            SELECT Id FROM AccountShare
             WHERE AccountId IN :accountIds
               AND UserOrGroupId IN :groupIds];

        if (!accountShares.isEmpty()) {
            Database.delete(accountShares, false);
        }
    }

    @future(callout = false)
    public static void customerPortalUsersCreated(Set<Id> accountIds) {
        Set<Id> childAccountIds = new Set<Id>();
        for (Account a : [select Id from Account 
                           where VI_Parent_Account__c in :accountIds]) {
            childAccountIds.add(a.Id);
        }
        createAccountShares(childAccountIds);
    }
}