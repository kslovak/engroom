@isTest
private class Account_Trigger_Test_Class  {

//******************
//Test Method 
//******************
  static testMethod void TestMe() {
    
    Account acct1 = new Account();
    Account acct2 = new Account();
    
    List<Account> accts = new List<Account>();
    
    acct1.Name = 'Test 1';
    acct2.Name = 'Test 2';
    
    acct1.SAP_Country__c = 'FR';
    acct2.SAP_Country__c = 'US';
    
    acct1.BillingStreet = 'Test1 \n Test2 \n Test3 \n Test4';
    acct2.BillingStreet = '';
    
    acct2.SAP_Customer_Number__c = '12345';
    
    acct2.Ship_To_Customer_Number__c = '12345';
    
    acct1.RecordTypeId = RecordType_Functions.LookupRecordTypeId('Aqualon Customer','Account');
    acct2.RecordTypeId = RecordType_Functions.LookupRecordTypeId('Aqualon Prospect','Account');

    accts.add(acct1);
    accts.add(acct2);
    
    insert accts;
    update accts;
    delete accts;
  }

//The End
}