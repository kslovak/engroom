global with sharing class SAP_Ultimate_Parent_Refresh_Active_UPs implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

	/********************
	 * General Constants
	 ********************/
    public static final String CLASS_NAME = 'SAP_Ultimate_Parent_Refresh_Active_UPs';
    public static final String PROCESS_NAME = 'Refresh Active UPs';
    public static final String SUBPROCESS_NAME = 'Refresh Active UPs';
    public static final String CATEGORY_NAME = 'Ultimate Parent';
    public static final String TASK_NAME = 'Refresh';
  
	/*-*-*-*-*-*-*-*-*-*-*-*
	 *                     *
	 * Schedulable Section *
	 *                     *
	 *-*-*-*-*-*-*-*-*-*-*-*/

    /* Developer Console Code Samples - Schedulable
	SAP_Ultimate_Parent_Refresh_Active_UPs.startSchedule();
	SAP_Ultimate_Parent_Refresh_Active_UPs.startSchedule('0 0 * * * ?');
    */

	/*************************
	 * Consts for Schedulable
	 *************************/
    public static final String SCHEDULE_NAME = 'Ultimate Parent Accounts - Refresh Active';
    public static final String DEFAULT_CRON = '0 0 * * * ?';

	/***********************************
	 * Public Interface for Schedulable
	 ***********************************/
    global static String startSchedule() {
    	return startSchedule(DEFAULT_CRON);
    }

    global static String startSchedule(String cronExpression) {
    	String result = '';
    	try {result = SystemMethods.schedule(SCHEDULE_NAME,cronExpression,new SAP_Ultimate_Parent_Refresh_Active_UPs());}
    	catch(Exception e) {result = e.getMessage();System.debug(e);}
    	return result;
    }

	/*****************************
	 * Schedulable Implementation 
	 *****************************/
    private SAP_Ultimate_Parent_Refresh_Active_UPs() { 
    }

    global void execute(SchedulableContext sc) {
    	SAP_Ultimate_Parent_Refresh_Active_UPs.submitBatchJob();
    }

	/*-*-*-*-*-*-*-*-*-*-*
	 *                   *
	 * Batchable Section *
	 *                   *
	 *-*-*-*-*-*-*-*-*-*-*/

	/* Developer Console Code Samples - Batchable
	SAP_Ultimate_Parent_Refresh_Active_UPs.submitBatchJob();
	*/ 

	/***********************
	 * Consts for Batchable
	 ***********************/
    public static final Integer BATCH_SIZE = 200;

	/***************************
	 * Private Vars - Batchable
	 ***************************/
	private BatchTool batchTool;
    private Boolean isScheduled = false;

    private List<String> allErrors = new List<String>();

	/*******************************
	 * Constructor(s) for Batchable
	 *******************************/
    global SAP_Ultimate_Parent_Refresh_Active_UPs(Boolean isScheduled) {
    	this.isScheduled = isScheduled;
    }

	/*********************************
	 * Public Interface for Batchable
	 *********************************/
    global static String submitBatchJob() {
    	return submitBatch(BATCH_SIZE);
    }

    global static String submitBatchJob(Integer BatchSizeOverride) {
    	return submitBatch(BatchSizeOverride);
    }

	/***************************************
	 * Private Static Methods for Batchable
	 ***************************************/
    private static String submitBatch(Integer batchSize) {
        String msg;
    	if (!BatchTool.alreadyRunning('SAP_Ultimate_Parent_Refresh_Active_UPs')) {
	        SAP_Ultimate_Parent_Refresh_Active_UPs b = new SAP_Ultimate_Parent_Refresh_Active_UPs(System.isScheduled());
	        try {msg = Database.executeBatch(b, batchSize);} catch(Exception e) {msg = e.getMessage();System.debug(e);}
    	} 
    	else {
    		msg = 'Job is already Running';
    		AppLogV2 appLog = new AppLogV2(PROCESS_NAME,SUBPROCESS_NAME,CLASS_NAME,'');
	       	appLog.write(AppLogV2.LOGCATEGORY_BATCHPROCESS,AppLogV2.TASK_BATCHPROCESSSKIPPED,AppLogV2.LOGTYPE_INFO,AppLogV2.TASK_BATCHPROCESSSKIPPED,AppLogV2.TASK_BATCHPROCESSSKIPPED);
    	}
        return msg;
    }
	
	/***************************
	 * Batchable Implementation
	 ***************************/
    global Database.Querylocator start(Database.BatchableContext bc) {
    	batchTool = new BatchTool(bc.getJobId(), isScheduled, PROCESS_NAME, SUBPROCESS_NAME);
    	
        Database.Querylocator query;

   		Id ultimateParentRecordType = RecordType_Functions.LookupRecordTypeId('Ultimate Parent','Account');

		query = Database.getQueryLocator([SELECT Id, OwnerId, Account_Rollup__r.Account_Count__c FROM Account WHERE RecordTypeId = :ultimateParentRecordType]);
    		
        return query;
    }
 
    global void execute(Database.BatchableContext bc, List<SObject> recsIn) {
        List<Account> accountsToProcess = (List<Account>)recsIn;
        Id ownerWhenActive = SAP_Ultimate_Parent_Settings.defaultSettings().Account_Owner_Id__c;
        Id ownerWhenInactive = SAP_Ultimate_Parent_Settings.defaultSettings().Account_Owner_Id_When_Inactive__c;
        Double inactiveThreshold = SAP_Ultimate_Parent_Settings.defaultSettings().Inactive_Child_Count_Threshold__c;

		List<Account> accountsToUpdate = new List<Account>();

		for (Account acc : accountsToProcess) {
			boolean changed = false;
			Id owner;
			
			if (acc.Account_Rollup__r.Account_Count__c > inactiveThreshold) {
				owner = ownerWhenActive;
			}
			else {
				owner = ownerWhenInactive;
			}
			
			if (acc.OwnerId != owner) {
				acc.OwnerId = owner;
				accountsToUpdate.add(acc);
			}
		}
		
		if (accountsToUpdate.size() > 0) {
			update accountsToUpdate;
		}
    }
     
    global void finish(Database.BatchableContext bc) {
    	batchTool.sendNotifications(TASK_NAME,allErrors);
    }
}