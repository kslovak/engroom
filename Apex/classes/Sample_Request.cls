public class Sample_Request {

    public Sample_Request__c sampleRequest {get; set;}
    public Datetime      submittedDateTime {get; set;}
    public String            accountNumber {get; set;}
    public String                approvers {get; set;}
    public String            submittedTime {get; set;}
    public Set<String>      approverEmails {get; set;}
    
    public Sample_Request(Sample_Request__c sr) {
        sampleRequest = sr;
        accountNumber = String_Functions.removeLeadingZeros(sr.Account__r.AccountNumber);
    }

    public void setApprovers(Map<Id, Set<String>> amap) {
        String sb = '';
        String k = sampleRequest.Id;
        if (amap != null && amap.containsKey(k)) {
            Set<String> sa = amap.get(k);
            sb += '<table class="approvers">';
            for (String s : sa) {
                sb += '<tr><td>' + s + '</td></tr>';
            }
            sb += '</table>';
        }
        approvers = sb;
    }
    
}