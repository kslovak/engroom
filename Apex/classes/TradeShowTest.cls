@isTest
private class TradeShowTest {

	static Trade_Show__c tshow;
	static Country_States_List__c cslist;
	
	static void setup() {
		cslist = new Country_States_List__c();
		cslist.Name            = 'US-AL';
		cslist.Country_Code__c = 'US';
		cslist.Country_Name__c = 'USA';
		cslist.State_Code__c   = 'AL';
		cslist.State_Name__c   = 'Alabama';
		insert cslist;
		
		tshow = new Trade_Show__c();
		tshow.Name = 'TestShow';
		tshow.Business__c = 'ACM';
		tshow.Start_Date__c = Date.today();
		tshow.End_Date__c = Date.today();
		tshow.Program__c = 'TestProgram';
		tshow.Lead_Type__c = 'Valvoline DIFM Lead';
		tshow.Lead_Queue__c = 'Valvoline - Lead Queue';
		insert tshow;
	}

    static testMethod void test1() {
    	Country_Code__c cc = new Country_Code__c();
    	cc.Country__c = 'US'; cc.Country_Name__c = 'USA'; 
    	cc.Code__c = '+001'; insert cc;
    	
    	Country_Code_Functions.getCountryCodeMap();
    	Country_Code_Functions.getCountryNameCodeMap();
    }
	
    static testMethod void test2() {
    	setup(); TradeShowController c = new TradeShowController();
    	c.tradeShowName = tshow.Name;
    	c.changeTradeShow(); c.changeCountry();
    	c.newLead.FirstName        = 'FirstName';
    	c.newLead.LastName         = 'LastName';
    	c.newLead.Company          = 'Company';
    	c.newLead.City             = 'City';
    	c.newLead.Title            = 'Title';
    	c.newLead.Email            = 'test@example.com';
    	c.newLead.PostalCode       = '123456';
    	c.newLead.Lead_Supplier__c = 'Supplier';
    	c.saveRegistration(); c.newRegistration();
    }
}