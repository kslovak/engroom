@isTest
private class VI_Run_VEM_Report_Form_Extension_Test {

  public static testMethod void myUnitTest() {
        CognosConfig__c cognosConfig = new CognosConfig__c(Name='config');
        cognosConfig.Cognos_URL_Prefix__c = 'test';
        insert cognosConfig;
  
        //Use the PageReference Apex class to instantiate a page
        PageReference pageRef = Page.VI_Run_VEM_Report_Form;
          
        //In this case, the Visualforce page named 'success' is the starting point of this test method.
        Test.setCurrentPage(pageRef);
   
        Account acc = new Account(Name = 'Test Account');
        acc.SAP_Zip_Code__c = '43081';
        insert acc;

        VI_Enrollment__c enrollment = new VI_Enrollment__c(Facility__c = acc.Id);
        insert enrollment;
   
        VI_Enrollment_Location__c enrollmentLocation = new VI_Enrollment_Location__c(Enrollment__c = enrollment.Id);
        enrollmentLocation.Facility__c = acc.Id;
        insert enrollmentLocation;
   
        ApexPages.currentpage().getparameters().put('id', enrollmentLocation.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(enrollmentLocation); 
        VI_Run_VEM_Report_Form_Extension extension = new VI_Run_VEM_Report_Form_Extension(sc);

        Test.startTest();

        extension.enrollmentLocation = enrollmentLocation;
        extension.primaryAccount = acc;

        String testString = '';

        extension.InitializeForm();
        
        extension.setTabInFocus('reportParameterEntry');
        testString = extension.getTabInFocus();

        extension.runVIPromotionInstallerDataReport();
        
        extension.getInsideIntranet();
        extension.getOutsideIntranet();
        extension.getUserIPAddress();
        extension.getUserName();
        
        extension.getReportTypes();
        extension.getPromotionExists();
        extension.getInstallerStoreExists();
        extension.getSAPAccountExists();
        extension.getParentAccountExists();
        extension.getSAPCustomerGroup5DescExists();
        
        extension.updateShowProspectInput();
        
        
  }      
}