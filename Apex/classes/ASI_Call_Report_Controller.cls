public class ASI_Call_Report_Controller {

    private static final ApexPages.Severity ERROR = ApexPages.Severity.ERROR;
    private static final ApexPages.Severity INFO  = ApexPages.Severity.INFO;
    private static final ApexPages.Severity WARN  = ApexPages.Severity.WARNING;

    private static final String DELIM = '-';
    private static final String  NONE = SAP_LISD_Functions.NONE;
    private static final String ADHSV = 'Adhesives';

    private static final String PARAM_ACCID  = 'accid';
    private static final String PARAM_ACRID  = 'Id';
    private static final String PARAM_CASID  = 'casid';
    private static final String PARAM_CONID  = 'conid';
    private static final String PARAM_OPPID  = 'oppid';
    private static final String PARAM_RSKID  = 'rskid';
    private static final String PARAM_SMPID  = 'smpid';

    private static final String PARAM_INITP  = 'initp';
    private static final String PARAM_CLONE  = 'clone';
    private static final String PARAM_RETURL = 'retURL';

    public Boolean                      allAttendees {get; set;}
    public Boolean                       allContacts {get; set;}
    public Boolean                        allInnovas {get; set;}
    public Boolean                           allNpis {get; set;}
    public Boolean                      allReceivers {get; set;}
    public Boolean                   srchAllContacts {get; set;}

    public String                        userSrchStr {get; set;}
    public String                     contactSrchStr {get; set;}
    public String                      innovaSrchStr {get; set;}
    public String                         npiSrchStr {get; set;}
    public String                        roleSrchStr {get; set;}
    public String                     profileSrchStr {get; set;}

    public SAP_LISD                             lisd {get; set;}
    public Boolean                     lisdCompleted {get; private set;}
    public Boolean                endUseCustReadOnly {get; private set;}
    public Boolean                  industryReadOnly {get; private set;}
    public Boolean                        renderLisd {get; private set;}
    public Boolean                         isToClone {get; private set;}

    public Boolean                renderCallReportPB {get; private set;}
    public Boolean                 renderAttendeesPB {get; private set;}
    public Boolean                  renderContactsPB {get; private set;}
    public Boolean                 renderReceiversPB {get; private set;}
    public Boolean                   renderInnovasPB {get; private set;}
    public Boolean                      renderNpisPB {get; private set;}
    public Boolean                renderNewContactPB {get; private set;}
    public Boolean                      renderMsgsPB {get; private set;}

    public Boolean                          fromCase {get; private set;}
    public Boolean                          fromOpty {get; private set;}
    public Boolean                          fromRisk {get; private set;}
    public Boolean                          fromSamp {get; private set;}

    public String                          accountId {get; set;}
    public String                       callReportId {get; private set;}
    public String                             caseId {get; private set;}
    public String                          contactId {get; private set;}
    public String                      distributorId {get; private set;}
    public String                       endUseCustId {get; private set;}
    public String                      opportunityId {get; private set;}
    public String                             riskId {get; private set;}
    public String                    sampleRequestId {get; private set;}

    public String                        initProcess {get; private set;}
    
    public Account                       accountSrch {get; private set;}
    public User                           ashlandRep {get; private set;}

    public Account                        acrAccount {get; private set;}
    public Account                     acrEndUseCust {get; private set;}
    public Account                    acrDistributor {get; private set;}
    public Case                              acrCase {get; private set;}
    public Competitor__c               acrCompetitor {get; private set;}
    public Contact                        acrContact {get; private set;}
    public Contact                        newContact {get; private set;}
    public Opportunity                acrOpportunity {get; private set;}
    public Risk__c                           acrRisk {get; private set;}
    public Sample_Request__c        acrSampleRequest {get; private set;}
    
    public ASI_Call_Report__c             callReport {get; private set;}

    public List<ASI_Call_Report_Attendee> attendees1 {get; private set;}
    public List<ASI_Call_Report_Attendee> attendees2 {get; private set;}

    public List<ASI_Call_Report_Contact>   contacts1 {get; private set;}
    public List<ASI_Call_Report_Contact>   contacts2 {get; private set;}

    public List<ASI_Call_Report_Distribution> rcvrs1 {get; private set;}
    public List<ASI_Call_Report_Distribution> rcvrs2 {get; private set;}

    public List<ASI_NPI_Record>                npis1 {get; private set;}
    public List<ASI_NPI_Record>                npis2 {get; private set;}

    public List<ASI_Innova_Record>          innovas1 {get; private set;}
    public List<ASI_Innova_Record>          innovas2 {get; private set;}

    public List<SelectOption>               pacDescs {get; private set;}

    public Map<String, String>                params {get; private set;}

    public Boolean isValidAccount {
        get {
            return (callReport != null && callReport.Account__c != null); 
        }
        private set;
    }

    private Boolean standardPage, valid1;
    private Id contactRecTypeId;
    private SObject sobj;
    private String  retUrl;
    
    private ASI_Call_Report_Settings__c acrSettings = ASI_Call_Report_Settings__c.getInstance();

    public ASI_Call_Report_Controller() {
        debug('ASI_Call_Report_Controller()');
        init1(null);
    }
    
    public ASI_Call_Report_Controller(ApexPages.StandardController sc) {
        debug('Constructor(sc)');
        init1(sc.getRecord());
    }
    
    private void debug(String s) {
        System.debug(LoggingLevel.INFO, 'ASI_Call_Report_Controller : ' + s);
    }

    @TestVisible
    private void addErrMsgs(List<String> sa) {
        Integer n = 0; for(String s : sa) {n++; addErrMsg(n + ' : ' + s);}
    }
    
    private void addErrMsg(String s)  {addMsg(ERROR, '<b>' + s + '</b>'); valid1 = false;}

    @TestVisible
    private void addReqMsg(String s)  {addMsg(ERROR, 'Required a value for : <b>' + s + '</b>'); valid1 = false;}

    @TestVisible
    private void addInfoMsg(String s) {addMsg(INFO,  '<b>' + s + '</b>');}

    @TestVisible
    private void addWarnMsg(String s) {addMsg(WARN,  '<b>' + s + '</b>');}

    private void addMsg(ApexPages.Severity mtype, String s) {
        ApexPages.Message msg = new ApexPages.Message(mtype, s);
        ApexPages.addMessage(msg);
    }

    public PageReference gotoRetUrl() {
        debug('gotoRetUrl : retUrl = ' + retUrl); if (retUrl == null) {retUrl = '/';}
        PageReference pr = new PageReference(retUrl); pr.setRedirect(true); return pr;
    }
    
    private String getRetUrl() {
        String s1 = '&' + PARAM_RETURL + '=',
               s2 = String.isBlank(retUrl) ? '/' : retUrl;
        return s1 + s2;
    }

    @TestVisible
    private PageReference getStdPageRef() {
        String s = '/'; 
        if (!String.isBlank(callReportId)) {s += callReportId;} else {
            String p = ASI_Call_Report__c.SObjectType.getDescribe().getKeyPrefix();
            s += p;
        }
        s += '/e?nooverride=1' + getRetUrl();
        return new PageReference(s).setRedirect(true);
    }
    
    private void setRetUrl(String s) {
        if (!String.isBlank(s)) {retUrl = '/' + s;}
    } 

    private void initParams() {
        PageReference pr = System.currentPageReference(); String s;
        params = pr.getParameters(); debug('params = ' + params);

        if (String.isBlank(callReportId)) {callReportId = params.get(PARAM_ACRID);}
        setRetUrl(callReportId);
        
        accountId       = params.get(PARAM_ACCID); setRetUrl(accountId);
        caseId          = params.get(PARAM_CASID); setRetUrl(caseId);
        contactId       = params.get(PARAM_CONID); setRetUrl(contactId);
        opportunityId   = params.get(PARAM_OPPID); setRetUrl(opportunityId);
        riskId          = params.get(PARAM_RSKID); setRetUrl(riskId);
        sampleRequestId = params.get(PARAM_SMPID); setRetUrl(sampleRequestId);
        
        fromCase = !String.isBlank(caseId);
        fromOpty = !String.isBlank(opportunityId);
        fromRisk = !String.isBlank(riskId);
        fromSamp = !String.isBlank(sampleRequestId);

        s = params.get(PARAM_CLONE); isToClone = (callReportId != null && s == '1');
        debug('isToClone = ' + isToClone); 

        s = params.get(PARAM_RETURL); if (!String.isBlank(s)) {retUrl = s;}
        debug('retUrl = ' + retUrl); 

        initProcess = '';
        s = params.get(PARAM_INITP); if (!String.isBlank(s)) {initProcess = s;}
        debug('initProcess = ' + initProcess); 
    }
    
    private void init2() {
        allAttendees = false; renderCallReportPB = false; renderMsgsPB         = false; 
        allContacts  = false; renderAttendeesPB  = false; renderLisd           = false;
        allInnovas   = false; renderContactsPB   = false; renderInnovasPB      = false; 
        allNpis      = false; renderReceiversPB  = false; renderNpisPB         = false;
        allReceivers = false; renderNewContactPB = false;
    }
    
    public PageReference showCallReportPB() {init2(); renderCallReportPB = true; 
        attendees2 = null; contacts2 = null; rcvrs2 = null; innovas2 = null; npis2 = null;
        if (!String.isBlank(initProcess)) {return gotoRetUrl();} return null;
    }

    private void showLisdPanel()   {init2(); industryReadOnly = true; renderLisd = true;}

    public void showAttendeesPB()  {init2(); renderAttendeesPB = true;}

    public void showContactsPB()   {init2(); renderContactsPB = true;}

    public void showReceiversPB()  {init2(); renderReceiversPB = true;}

    public void showInnovasPB()    {init2(); renderInnovasPB = true;}

    public void showNpisPB()       {init2(); renderNpisPB = true;}

    public void showNewContactPB() {init2(); newContact = new Contact(); renderNewContactPB = true;}

    private void showMsgsPB()      {init2(); renderMsgsPB = true;}

    private void init1(SObject sobj) {
        debug('init1 : ' + sobj);
        initParams(); lisd = new SAP_LISD(); lisdCompleted = false; 
        standardPage = acrSettings != null && acrSettings.Show_Standard_Page__c;
        if (standardPage) {return;} 
        contactRecTypeId = ASI_Call_Report_Methods.getContactRecTypeId(); 
        
        endUseCustReadOnly = false; industryReadOnly = false; srchAllContacts = false;
        allAttendees = false; allContacts = false; allInnovas = false; allNpis = false;  
        userSrchStr = ''; contactSrchStr = ''; innovaSrchStr = ''; 
        npiSrchStr = ''; roleSrchStr = 'ASI'; profileSrchStr = 'ASI,Aqualon'; 
        
        if (String.isBlank(callReportId)) {
            callReportId = sobj != null ? sobj.Id : null;
        }
        
        callReport     = ASI_Call_Report_Methods.getCallReport(callReportId);
        attendees1     = ASI_Call_Report_Methods.getAttendees(callReportId);
        contacts1      = ASI_Call_Report_Methods.getContacts(callReportId);
        acrCompetitor  = ASI_Call_Report_Methods.getCompetitor(callReport.Competitor__c);
        acrDistributor = ASI_Call_Report_Methods.getAccount(callReport.Distributor__c);
        acrEndUseCust  = ASI_Call_Report_Methods.getAccount(callReport.End_Use_Customer__c);

        if (String.isBlank(accountId) && isValidAccount) {accountId = callReport.Account__c;}
        initRisk(); initSampleRequest(); initCase(); initOpportunity(); initContact();
        lisd.setIndustries(accountId); initAccount(); 
        if (callReportId != null) {
            lisd.endUseCustId = callReport.End_Use_Customer__c;
            lisd.setIndustryLabel(callReport.Industry_Name__c);
        }
        onLisdComplete(); onIndustryChange(); 
           
        ashlandRep = ASI_Call_Report_Methods.getUser(callReport.Ashland_Rep__c);
        innovas1   = ASI_Call_Report_Methods.getInnovas(callReport);
        npis1      = ASI_Call_Report_Methods.getNpis(callReport);

        if (isToClone) {
            callReport = ASI_Call_Report_Methods.clone(callReport, attendees1, contacts1, rcvrs1);
            debug('cloned Call Report : ' + callReport);
            callReportId = callReport.Id;
        }
        
        if ((callReportId == null && lisd.isMultiIndustyAcc && !isToClone) || 
            String.isBlank(accountId)) {showLisdPanel();
        } else {
            lisd.setAllIndustries(); showCallReportPB();
        }
        
        if (initProcess == 'srchatts') {searchAttendees();} else
        if (initProcess == 'srchcons') {searchContacts(); } else
        if (initProcess == 'srchdsts') {searchReceivers();}
    }
    
    private void initAccount() {
        if (acrAccount == null) {acrAccount = new Account();}
        if (!String.isBlank(accountId)) {callReport.Account__c = accountId;}
        
        accountSrch = new Account(Name='', OwnerId=UserInfo.getUserId(),
                                  SAP_DistChannel__c='50', SAP_Sales_Org__c='');
        if (!String.isBlank(callReport.Ashland_Rep__c)) {
            accountSrch.OwnerId = callReport.Ashland_Rep__c;
        }
        if (!String.isBlank(acrAccount.SAP_DistChannel__c)) {
            accountSrch.SAP_DistChannel__c = acrAccount.SAP_DistChannel__c;
        }
        if (!String.isBlank(acrAccount.SAP_Sales_Org__c)) {
            accountSrch.SAP_Sales_Org__c = acrAccount.SAP_Sales_Org__c;
        }
    }
    
    private void initRisk() {
        if (String.isBlank(riskId)) {riskId = callReport.Risk__c;}
        if (String.isBlank(riskId)) {return;}
        callReport.Risk__c = riskId;
        acrRisk = ASI_Call_Report_Methods.getRisk(riskId);
        caseId = acrRisk.Case__c; //contactId = acrRisk.Contact__c;
        opportunityId = acrRisk.Opportunity__c;
        if (String.isBlank(accountId)) {
            accountId = acrRisk.Account__c;
        }
    }
    
    private void initSampleRequest() {
        if (String.isBlank(sampleRequestId)) {sampleRequestId = callReport.Sample_Request__c;}
        if (String.isBlank(sampleRequestId)) {return;}
        callReport.Sample_Request__c = sampleRequestId;
        acrSampleRequest = ASI_Call_Report_Methods.getSampleRequest(sampleRequestId);
        //contactId = acrSampleRequest.Contact__c;
        opportunityId = acrSampleRequest.Opportunity__c;
        if (String.isBlank(accountId)) {
            accountId = acrSampleRequest.Account__c;
        }
    }
    
    private void initCase() {
        if (String.isBlank(caseId)) {caseId = callReport.Case__c;}
        if (String.isBlank(caseId)) {return;}
        callReport.Case__c = caseId;
        acrCase = ASI_Call_Report_Methods.getCase(caseId);
        //contactId = acrCase.ContactId;
        opportunityId = acrCase.Related_Opportunity__c;
        if (String.isBlank(accountId)) {
            accountId = acrCase.AccountId;
        }
    }
    
    private void initOpportunity() {
        if (String.isBlank(opportunityId)) {opportunityId = callReport.Opportunity__c;}
        if (String.isBlank(opportunityId)) {return;}
        callReport.Opportunity__c = opportunityId;
        acrOpportunity = ASI_Call_Report_Methods.getOpportunity(opportunityId);
        //contactId = acrOpportunity.Contact__c;
        if (String.isBlank(accountId)) {
            accountId = acrOpportunity.AccountId;
        }
    }
    
    private void initContact() {
        if (String.isBlank(contactId)) {return;}
        acrContact = ASI_Call_Report_Methods.getContact(contactId);
        Integer n = contacts1.size(); Set<String> ids = new Set<String>();
        for (ASI_Call_Report_Contact c : contacts1) {ids.add(c.cntact.Id);}
        if (!ids.contains(contactId)) {
            contacts1.add(new ASI_Call_Report_Contact(acrContact, ++n));
        }
        if (String.isBlank(accountId)) {
            accountId = acrContact.AccountId;
        }
    }
    
    public PageReference initAction1() {
        PageReference pr = null; if (standardPage) {pr = getStdPageRef();}
        if (ASI_Call_Report_Methods.isAPM(acrAccount)) {showMsgsPB(); pr = gotoRetUrl();}
        return pr;
    }
    
    public void onLisdComplete() {
        debug('onLisdComplete : lisd = ' + lisd.seller);
        lisdCompleted = (lisd.seller != null && lisd.seller.Id != null);
        if (lisd.acc != null) {acrAccount = lisd.acc; accountId = acrAccount.id;}
        callReport.Account__c = accountId;
        callReport.End_Use_Customer__c = lisd.endUseCustId;
        String lbl = lisd.getIndustryLabel(); if (String.isBlank(lbl) || lbl == NONE) {lbl = '';}
        if (ASI_Call_Report_Methods.isAdhesives(acrAccount)) {lbl = ADHSV;}
        callReport.Industry_Name__c = lbl;
        System.debug('lbl = ' + lbl);
        System.debug('callReport.Industry_Name__c = ' + callReport.Industry_Name__c);
        if (lisdCompleted) {
            endUseCustReadOnly = !String.isBlank(callReport.End_Use_Customer__c);
            industryReadOnly   = !String.isBlank(callReport.Industry_Name__c);
	        if (String.isBlank(callReport.Ashland_Rep__c)) {
	            accountSrch.OwnerId = lisd.seller.Id;
	            callReport.Ashland_Rep__c = lisd.seller.Id;
	        }
            onIndustryChange();
        }
    }
    
    public void onIndustryChange() {
        pacDescs = new List<SelectOption>(); String lbl, val;
        lisd.setIndustryLabel(callReport.Industry_Name__c);
        rcvrs1 = ASI_Call_Report_Methods.getReceivers(callReport);
        debug('pacIndustries : ' + lisd.pacIndustries);
        if (lisd.pacIndustries == null) {return;}
        for (SelectOption so : lisd.pacIndustries) {
            lbl = so.getLabel(); val = (lbl == NONE) ? '' : lbl;
            pacDescs.add(new SelectOption(val, lbl));
        }
    }
    
    public void onConfidentialChange() {}

//  ----------------------------------------- Email Call Report -------------------------------------    

    public PageReference sendEmailAction1() {
        Boolean confidential = callReport.Confidential__c;
        for (ASI_Call_Report_Distribution a : rcvrs1) {
            a.selected = (confidential && a.crd != null && !a.crd.Confidential__c);
        }
        rcvrs1 = ASI_Call_Report_Methods.deleteReceivers(rcvrs1);
        if (rcvrs1.isEmpty()) {return sendEmailAction2();}
        for (ASI_Call_Report_Distribution a : rcvrs1) {
            a.selected = (a.crd != null && a.crd.Email_Sent_Date__c == null);
        }
        renderReceiversPB = true; return null;
    }
        
    public PageReference sendEmailAction2() {
        Set<String> emailIds;
        try {
            emailIds = ASI_Call_Report_Methods.sendEmail(callReport, rcvrs1);
            addInfoMsg('Sent the Call Report to the following Email Ids.');
            for (String s : emailIds) {addInfoMsg(s);}
        } catch(Exception e) {
            addErrMsg('Failed to send Call Report Email : ' + e.getMessage());
        }
        renderReceiversPB = false; return null;
    }
    
//  ----------------------------------------- Call Report -------------------------------------    

    private void validate1() {
        valid1 = true; 
        if (String.isBlank(callReport.Title__c))         {addReqMsg('Title');}
        if (callReport.Date__c == null)                  {addReqMsg('Date');}
        if (String.isBlank(callReport.Industry_Name__c)) {addReqMsg('Industry');}
        if (String.isBlank(callReport.SBU__c))           {addReqMsg('SBU');}
        if (String.isBlank(callReport.Ashland_Rep__c))   {addReqMsg('Ashland Rep');}
    }
    
    public PageReference saveCallReport() {
        callReport.Ashland_Rep__c = accountSrch.OwnerId;
        validate1(); if (!valid1) {return null;}
        Set<Id> attendeeIds = ASI_Call_Report_Methods.getAttendeeIds(attendees1),
                 contactIds = ASI_Call_Report_Methods.getContactIds(contacts1);
        List<Contact> newContacts = ASI_Call_Report_Methods.getNewContacts(contacts1);
        List<String> msgs = ASI_Call_Report_Methods.saveCallReport(
            callReport, attendeeIds, contactIds, newContacts, rcvrs1);
        if (msgs != null && !msgs.isEmpty()) {addErrMsgs(msgs); return null;}
        setRetUrl(callReport.Id); return gotoRetUrl();
    }
    
//  ------------------------------------------ Attendees --------------------------------------    

    public void searchAttendees() {
        attendees2 = ASI_Call_Report_Methods.getAttendees(attendees1, userSrchStr, roleSrchStr, profileSrchStr);
        showAttendeesPB();
    }
    
    public void deleteAttendees() {
        attendees1 = ASI_Call_Report_Methods.deleteAttendees(attendees1);
        showCallReportPB();
    }
    
    public void selectAttendees() {
        ASI_Call_Report_Methods.addAttendees(attendees1, attendees2);
        showCallReportPB();
    }
    
//  ------------------------------------------ Contacts ---------------------------------------    

    public void searchContacts() {
        String accId = accountId;
        Boolean b = !String.isBlank(accountSrch.Name);
        if (srchAllContacts && b) {accId = null;}
        contacts2 = ASI_Call_Report_Methods.getContacts(contacts1, contactSrchStr, accId, accountSrch);
        showContactsPB();
    }
    
    public void deleteContacts() {
        contacts1 = ASI_Call_Report_Methods.deleteContacts(contacts1);
        showCallReportPB();
    }
    
    public void selectContacts() {
        ASI_Call_Report_Methods.addContacts(contacts1, contacts2);
        showCallReportPB();
    }
    
    public void addNewContact() {
        Contact c = newContact.clone(); c.RecordTypeId = contactRecTypeId; 
        c.AccountId = accountId;
        ASI_Call_Report_Methods.addContact(c, contacts1);
        showCallReportPB();
    }
    
//  ------------------------------------------ Distributions --------------------------------------    

    public void searchReceivers() {
        rcvrs2 = ASI_Call_Report_Methods.getReceivers(rcvrs1, userSrchStr, roleSrchStr, profileSrchStr);
        showReceiversPB();
    }
    
    public void deleteReceivers() {
        rcvrs1 = ASI_Call_Report_Methods.deleteReceivers(rcvrs1);
        showCallReportPB();
    }
    
    public void selectReceivers() {
        ASI_Call_Report_Methods.addReceivers(rcvrs1, rcvrs2);
        showCallReportPB();
    }
    
//  ------------------------------------------ Innovas --------------------------------------    

    public void searchInnovas() {
        innovas2 = ASI_Call_Report_Methods.getInnovas(callReport, innovaSrchStr);
        showInnovasPB();
    }
    
    public void deleteInnovas() {
        innovas1 = ASI_Call_Report_Methods.deleteInnovas(callReport, innovas1);
        showCallReportPB();
    }
    
    public void selectInnovas() {
        ASI_Call_Report_Methods.addInnovas(callReport, innovas1, innovas2);
        showCallReportPB();
    }
    
//  ------------------------------------------ NPIs --------------------------------------    

    public void searchNpis() {
        npis2 = ASI_Call_Report_Methods.getNpis(callReport, npiSrchStr);
        showNpisPB();
    }
    
    public void deleteNpis() {
        npis1 = ASI_Call_Report_Methods.deleteNpis(callReport, npis1);
        showCallReportPB();
    }
    
    public void selectNpis() {
        ASI_Call_Report_Methods.addNpis(callReport, npis1, npis2);
        showCallReportPB();
    }
    
}