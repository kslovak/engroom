public class TradeShowController {
	
	public static final String LEAD_TYPE = 'Trade Show';
	public static final String ACM       = 'ACM';
	
    private static final String SYSADMIN_EMAIL = User_Functions.getSysAdminEmail();

    private static final ApexPages.Severity ERROR = ApexPages.Severity.ERROR;
    private static final ApexPages.Severity INFO  = ApexPages.Severity.INFO;
    private static final ApexPages.Severity WARN  = ApexPages.Severity.WARNING;

	public Boolean renderPageBlock1         {get; private set;}
	public Boolean renderPageBlock2         {get; private set;}
	public Boolean renderPageBlock3         {get; private set;}
	public Boolean isACM                    {get; private set;}
	
	public List<SelectOption> countryOpts   {get; private set;}
	public List<SelectOption> stateOpts     {get; private set;}
	public List<SelectOption> tradeShowOpts {get; private set;}
	public Trade_Show__c tradeShow          {get; private set;}

	public String countryCode               {get; set;}
	public String stateCode                 {get; set;}
	public String tradeShowName             {get; set;}
	public Lead newLead                     {get; set;}
	
	Boolean validForm; Id leadQueueId, leadRecTypeId, emailTemplateId;
	
	public TradeShowController() {init();}

	private void init() {
		tradeShowOpts = new List<SelectOption>(); tradeShowName = ''; tradeShow = new Trade_Show__c();
		countryCode = 'US'; stateCode   = 'AL'; Date tdate = Date.today();
		countryOpts = Country_Code_Functions.getCountryOpts(); changeCountry();
		for (Trade_Show__c t : [select Name from Trade_Show__c 
		                         where Start_Date__c <= :tdate and End_Date__c >= :tdate order by Name]) {
			tradeShowOpts.add(new SelectOption(t.Name, t.Name));
		}
		showPageBlock1();
	}
	
    private void debug(String s) {System.debug(LoggingLevel.INFO, 'TradeShowController : ' + s);}

    private void addErrMsg(String s)  {validForm = false; addMsg(ERROR, '<b>' + s + '</b>');}

    private void addReqMsg(String s)  {validForm = false; addMsg(ERROR, 'Please enter : <b>' + s + '</b>');}

    private void addInfoMsg(String s) {addMsg(INFO,  '<b>' + s + '</b>');}

    private void addWarnMsg(String s) {addMsg(WARN,  '<b>' + s + '</b>');}

    private void addMsg(ApexPages.Severity mtype, String s) {
        ApexPages.Message msg = new ApexPages.Message(mtype, s);
        ApexPages.addMessage(msg);
    }
    
    private String str(String s) {return String.isBlank(s) ? '' : s.trim();}

	private void showPageBlock1() {
		renderPageBlock1 = true; renderPageBlock2 = false; renderPageBlock3 = false;
	}
	
	private void showPageBlock2() {
		renderPageBlock1 = false; renderPageBlock2 = true; renderPageBlock3 = false;
	}
	
	private void showPageBlock3() {
		renderPageBlock1 = false; renderPageBlock2 = false; renderPageBlock3 = true;
	}
	
	public void changeCountry() {
		stateOpts = Country_Code_Functions.getStateOpts(countryCode);
	}
	
	public void changeTradeShow() {
		if (!String.isBlank(tradeShowName)) {showRegistrationForm();} 
	}
	
	private void setLeadRecTypeId(String leadType) {
	    leadRecTypeId = null; if (String.isBlank(leadType)) {return;}
		Map<String, Schema.RecordTypeInfo> recTypeMap = 
		Lead.sObjectType.getDescribe().getRecordTypeInfosByName();
		if (recTypeMap.containsKey(leadType)) {               
			leadRecTypeId = recTypeMap.get(leadType).getRecordTypeID(); 
		}
	}
	
	private void setLeadQueueId(String leadQueue) {
		Map<String, String> queueNameIdMap = new Map<String, String>();
		Set<String> queues = new Set<String>(); leadQueueId = null;
		if (!String.isBlank(leadQueue)) {queues.add(leadQueue);}
		if (!queues.isEmpty()) {
			queueNameIdMap = Public_Group_Functions.getQueueNameIdMap(queues);
			if (queueNameIdMap.containsKey(leadQueue)) {
				leadQueueId = queueNameIdMap.get(leadQueue);
			}
		}
	}
	
	private Id getEmailTemplateId(String templateName) {
		if (String.isBlank(templateName)) {return null;}
		List<EmailTemplate> alist = [select Id from EmailTemplate where Name = :templateName];
		if (alist == null || alist.isEmpty()) {return null;}
		return alist[0].Id;
	}
	
	private void setEmailTemplateId(String templateName) {
		String defaultTemplate = 'Trade Show Template';
		emailTemplateId = getEmailTemplateId(templateName);
		if (emailTemplateId == null) {
			emailTemplateId = getEmailTemplateId(defaultTemplate);
		}
	}
	
	private void setTradeShow() {
		tradeShow = [select Banner__c, Business__c, Contact__r.Id, Contact__r.Email, Contact__r.Name,  
		                    Email_Template__c, Lead_Queue__c, Lead_Type__c, Program__c 
		               from Trade_Show__c where Name = :tradeShowName limit 1];
		setLeadQueueId(tradeShow.Lead_Queue__c);
		setLeadRecTypeId(tradeShow.Lead_Type__c);
		//setEmailTemplateId(tradeShow.Email_Template__c);
		isACM = (tradeShow.Business__c != null && tradeShow.Business__c == ACM);
	}
	
	private void showRegistrationForm() {
		try {
			setTradeShow(); newRegistration();
		} catch(Exception e) {}
	}
	
	public void newRegistration() {
		newLead = new Lead(); showPageBlock2();
	}

	private void validate1() {
		validForm = true;
		newLead.FirstName 		 = str(newLead.FirstName); 			if (newLead.FirstName 		 == '')	{addReqMsg('First Name');}
		newLead.LastName  		 = str(newLead.LastName); 			if (newLead.LastName  		 == '')	{addReqMsg('Last Name');}
		newLead.Company   		 = str(newLead.Company);			if (newLead.Company   		 == '')	{addReqMsg('Company Name');}
		newLead.City      		 = str(newLead.City); 				if (newLead.City      		 == '')	{addReqMsg('City');}
		newLead.Title     		 = str(newLead.Title);				if (newLead.Title     		 == '')	{addReqMsg('Job Title');}
		newLead.Country   		 = str(newLead.Country);			if (newLead.Country   		 == '')	{addReqMsg('Country');}
		newLead.Email     		 = str(newLead.Email);				if (newLead.Email     		 == '')	{addReqMsg('Email');}
        newLead.Phone            = str(newLead.Phone);              if (newLead.Phone            == '') {addReqMsg('Phone Number');}
		newLead.State     		 = str(newLead.State);				if (newLead.State     		 == '')	{addReqMsg('State/Province');}
		newLead.PostalCode 		 = str(newLead.PostalCode); 		if (newLead.PostalCode       == '') {addReqMsg('ZIP/Postal Code');}
		
        newLead.Current_Motor_Oil_Brand__c = str(newLead.Current_Motor_Oil_Brand__c);   
        if (isACM && newLead.Current_Motor_Oil_Brand__c == '') {addReqMsg('Current Motor Oil Brand');}
        
        newLead.VIOC_Store_Number__c = str(newLead.VIOC_Store_Number__c);   
        if (isACM && newLead.VIOC_Store_Number__c == '') {addReqMsg('Store Number');}
/*
		newLead.Lead_Supplier__c = str(newLead.Lead_Supplier__c); 	
		if (isACM && newLead.Lead_Supplier__c == '') {addReqMsg('Current Lubricant Supplier');}
*/
		if (!validForm) {return;}
		List<Lead> leads = [select Id from Lead where Email = :newLead.Email and LeadSource = :tradeShowName limit 1];
		if (leads != null && !leads.isEmpty()) {addErrMsg(newLead.Email + ' is already Registered.');}
	}
	
	public void saveRegistration() {
		newLead.Country = countryCode; newLead.State = stateCode;
		validate1(); if (!validForm) {return;}
		String s = 'Thank You. ' + str(newLead.FirstName) + ' ' + str(newLead.LastName) + ' is Registered now.';
		if (leadQueueId != null) {newLead.OwnerId = leadQueueId;}
		if (leadRecTypeId != null) {newLead.RecordTypeId = leadRecTypeId;}
		newLead.LeadSource = tradeShowName; newLead.Lead_Type__c = LEAD_TYPE;
		try {
			insert newLead;	
			addInfoMsg(s); showPageBlock3(); //sendRegistrationEmail();  
		} catch(Exception e) {addErrMsg(e.getMessage());}
	}
	
	private void sendRegistrationEmail() {
		if (emailTemplateId == null || newLead.Id == null) {return;}
        Contact c = tradeshow.Contact__r; if (c == null) {return;}
        Messaging.SingleEmailMessage m = new Messaging.SingleEmailMessage();
        m.setReplyTo(c.Email); m.setSaveAsActivity(false); m.setSenderDisplayName(c.Name);
        m.setTargetObjectId(newLead.Id); m.setTemplateId(emailTemplateId);
        List<String> toList = new List<String>{newLead.Email}, bcList = new List<String>();
        if (SYSADMIN_EMAIL != null && SYSADMIN_EMAIL != c.Email) {bcList.add(SYSADMIN_EMAIL);}
        if (!bcList.isEmpty()) {m.setBccAddresses(bcList);}
        List<Messaging.SingleEmailMessage> mlist = new List<Messaging.SingleEmailMessage>{m};
        List<Messaging.SendEmailResult> sers;
        try {sers = Messaging.sendEmail(mlist);}
        catch(Exception e) {debug(e.getMessage() + ' : ' + sers);}
	}
}