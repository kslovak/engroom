public class Country_Code_Functions {
	
	public static Map<String, String> getCountryCodeMap() {
		Map<String, String> cmap = new Map<String, String>();
        for(Country_Code__c c : [select Country__c, Code__c 
                                   from Country_Code__c]) {
			cmap.put(c.Country__c, c.Code__c);
		}
        return cmap;
	}
	
	public static Map<String, String> getCountryCodeNameMap() {
		Map<String, String> cmap = new Map<String, String>();
        for(Country_Code__c c : [select Country__c, Country_Name__c 
                                   from Country_Code__c]) {
			cmap.put(c.Country__c, c.Country_Name__c);
		}
        return cmap;
	}
	
	public static Map<String, String> getCountryNameCodeMap() {
		Map<String, String> cmap = new Map<String, String>();
        for(Country_Code__c c : [select Country__c, Country_Name__c 
                                   from Country_Code__c]) {
			cmap.put(c.Country_Name__c, c.Country__c);
		}
        return cmap;
	}
	
	public static List<SelectOption> getCountryOpts() {
		List<SelectOption> sopts = new List<SelectOption>();
		Map<String, Country_States_List__c> csmap = Country_States_List__c.getAll();
		if (csmap == null || csmap.isEmpty()) {return sopts;}
		Map<String, String> nameCodeMap = new Map<String, String>();
		Set<String> nameSet = new Set<String>(); Country_States_List__c cs;
		for (String s : csmap.keySet()) {
			cs = csmap.get(s); nameSet.add(cs.Country_Name__c);
			nameCodeMap.put(cs.Country_Name__c, cs.Country_Code__c);
		}
		List<String> nameList = new List<String>(nameSet); nameList.sort();
		for (String s : nameList) {sopts.add(new SelectOption(nameCodeMap.get(s), s));}
		return sopts;
	}

	public static List<SelectOption> getStateOpts(String countryCode) {
		List<SelectOption> sopts = new List<SelectOption>();
		Map<String, Country_States_List__c> csmap = Country_States_List__c.getAll();
		if (csmap == null || csmap.isEmpty()) {return sopts;}
		Map<String, String> nameCodeMap = new Map<String, String>();
		Set<String> nameSet = new Set<String>(); Country_States_List__c cs;
		for (String s : csmap.keySet()) {
			cs = csmap.get(s);
			if (cs.Country_Code__c == countryCode) { 
				nameSet.add(cs.State_Name__c);
				nameCodeMap.put(cs.State_Name__c, cs.State_Code__c);
			}
		}
		List<String> nameList = new List<String>(nameSet); nameList.sort();
		for (String s : nameList) {sopts.add(new SelectOption(nameCodeMap.get(s), s));}
		return sopts;
	}
}