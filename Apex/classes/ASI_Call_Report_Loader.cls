public class ASI_Call_Report_Loader {

    public static final Map<String, Id> RTMAP = RecordType_Functions.RetrieveRecordTypeNameMap('Call_Report__c');
    
    public static final Set<Id> RECTYPES = new Set<Id>{
        RTMAP.get('AQ Call Report Record Type'), 
        RTMAP.get('PM Call Report Record Type') 
    };
    
    public static final String QRY1 = ''
        + 'select Id, Name, Account__c, Account__r.RecordTypeId, Account__r.SAP_DistChannel__c,' 
        + '       Account__r.SAP_DivisionCode__c, RecordTypeId' 
        + '  from Call_Report__c';
        
    public static final String QRY2 = ''
        + 'select Id, Name, AQ_Attendee__c, AQ_Attendee_2__c, AQ_Attendee_3__c, AQ_Attendee_4__c,' 
        + '       Contact__c, Contact_2__c, Contact_3__c, Contact_4__c'
        + '  from Call_Report__c';
        
    public static final String QRY3 = ''
        + 'select Id, Name, CreatedById, Account__c, Account__r.RecordTypeId, Account__r.SAP_DistChannel__c,' 
        + '       Account__r.SAP_DivisionCode__c, Action_Items__c, Additional_Competitors__c, AQ_Attendees__c,' 
        + '       Ashland_Attendees__c, Ashland_Competitor__c, Ashland_Rep_Performing_Call__c, Case__c,'
        + '       Date__c, Discussion__c, Distributor_Name__c, End_Use_Customer__c, hISP_Created_Date__c,'
        + '       hISP_Last_Modified_Date__c, hISP_Record_Number__c, Opportunity__c, PAC_Description__c,'
        + '       PAC_Industry_Segment__c, Publish__c, Reason_for_Visit__c, Russia_CIS__c, SBU__c, Threat__c, Title__c' 
        + '  from Call_Report__c';
        
    private static void debug(String s) {
        DebugLog_Functions.logMessage(s, 'ASI_Call_Report_Loader');
    }
    
    private static String getRichText(String s) {
        if (String.isBlank(s)) {return s;}
        String t = s.replace('\n', '<br/>');
        return t;
    }
    
    private static String getString(String s) {
        if (String.isBlank(s)) {return '';} return s;
    }
    
    private static Boolean isAPM(Account a) {
        return ASI_Call_Report_Methods.isAPM(a);
    }
    
    private static ASI_Call_Report__c getASICallReport(Call_Report__c a) {
        ASI_Call_Report__c c = new ASI_Call_Report__c(); String s;
        
        c.Account__c                 = a.Account__c;
        c.Action_Items__c            = a.Action_Items__c;
        c.Additional_Competitors__c  = a.Additional_Competitors__c;
        
        s = getString(a.AQ_Attendees__c);
        if (!String.isBlank(a.Ashland_Attendees__c)) {
            if (!String.isBlank(s)) {s += '\n';} s += getString(a.Ashland_Attendees__c);
        }
        c.Additional_Attendees__c    = s;
        
        c.Ashland_Rep__c             = a.Ashland_Rep_Performing_Call__c;
        if (c.Ashland_Rep__c == null) {c.Ashland_Rep__c = a.CreatedById;}
        
        c.Case__c                    = a.Case__c;
        c.Competitor__c              = a.Ashland_Competitor__c;
        c.Date__c                    = a.Date__c;
        c.Discussion__c              = getRichText(a.Discussion__c);
        c.Distributor__c             = a.Distributor_Name__c;
        c.End_Use_Customer__c        = a.End_Use_Customer__c;
        c.hISP_Created_Date__c       = a.hISP_Created_Date__c;
        c.hISP_Last_Modified_Date__c = a.hISP_Last_Modified_Date__c;
        c.hISP_Record_Number__c      = a.hISP_Record_Number__c;
        c.Industry_Name__c           = a.PAC_Industry_Segment__c;
        c.Old_Call_Report__c         = a.Id;
        c.Opportunity__c             = a.Opportunity__c;
        c.PAC_Description__c         = a.PAC_Description__c;
        c.Publish__c                 = a.Publish__c;
        c.Record_Key__c              = a.Name + ':' + a.Id;
        c.Reason_for_Visit__c        = a.Reason_for_Visit__c;
        c.Russia_CIS__c              = a.Russia_CIS__c;
        c.SBU__c                     = a.SBU__c;
        c.Title__c                   = a.Title__c;
        
        return c;
    }
    
    public static void createASICallReports(List<Call_Report__c> alist) {
        if (alist == null || alist.isEmpty()) {return;}
        Set<Id> aset = new Set<Id>();
        for (Call_Report__c a : alist) {aset.add(a.Id);}
        String q = QRY1 + ' where Id in :aset and RecordTypeId in :RECTYPES';
        List<Call_Report__c> blist = Database.query(q); 
        if (blist == null || blist.isEmpty()) {return;}
        aset = new Set<Id>(); 
        for (Call_Report__c b : blist) {
            if (!isAPM(b.Account__r)) {aset.add(b.Id);}
        }
        createASICallReports(aset);
    }
    
    public static void createASICallReports(Set<Id> aset) {
        if (aset == null || aset.isEmpty()) {return;}
        List<ASI_Call_Report__c> alist = new List<ASI_Call_Report__c>();
        String q = QRY3 + ' where Id in :aset';
        for (Call_Report__c b : Database.query(q)) {alist.add(getASICallReport(b));}
        try {upsert alist Record_Key__c;} catch(Exception e) {debug(e.getMessage()); return;}
        Map<Id, Id> amap = new Map<Id, Id>(); //debug('alist : ' + alist);
        for (ASI_Call_Report__c a : alist) {amap.put(a.Old_Call_Report__c, a.Id);}
        createCallReportTeams(aset, amap);
    }
    
    private static void createCallReportTeams(Set<Id> aset, Map<Id, Id> amap) {
        String q = QRY2 + ' where Id in :aset';
        List<ASI_Call_Report_Attendee__c> alist = new List<ASI_Call_Report_Attendee__c>();
        List<ASI_Call_Report_Contact__c>  clist = new List<ASI_Call_Report_Contact__c>();
        Id acrId; Set<Id> ids1, ids2;
        for (Call_Report__c b : Database.query(q)) {
            acrId = amap.get(b.Id); if (acrId == null) {continue;}

            ids1 = new Set<Id>(); ids2 = new Set<Id>();
	        addAttendee(ids1, alist, acrId, b.Id, b.AQ_Attendee__c);
            addAttendee(ids1, alist, acrId, b.Id, b.AQ_Attendee_2__c);
            addAttendee(ids1, alist, acrId, b.Id, b.AQ_Attendee_3__c);
            addAttendee(ids1, alist, acrId, b.Id, b.AQ_Attendee_4__c);

            addContact(ids2, clist, acrId, b.Id, b.Contact__c);
            addContact(ids2, clist, acrId, b.Id, b.Contact_2__c);
            addContact(ids2, clist, acrId, b.Id, b.Contact_3__c);
            addContact(ids2, clist, acrId, b.Id, b.Contact_4__c);
        }
        if (!alist.isEmpty()) {
            try {upsert alist Record_Key__c;} catch(Exception e) {debug(e.getMessage());}
        }
        if (!clist.isEmpty()) {
            try {upsert clist Record_Key__c;} catch(Exception e) {debug(e.getMessage());}
        }
    }
    
    private static void addAttendee(Set<Id> ids, List<ASI_Call_Report_Attendee__c> alist, Id acrId, Id ocrId, Id attId) {
        if (attId == null || ids.contains(attId)) {return;}
        ASI_Call_Report_Attendee__c a = new ASI_Call_Report_Attendee__c();
        a.ASI_Call_Report__c = acrId; a.Attendee__c = attId;
        a.Old_Call_Report__c = ocrId; a.Record_Key__c = ocrId + ':' + attId;
        alist.add(a); ids.add(attId);
    } 
    
    private static void addContact(Set<Id> ids, List<ASI_Call_Report_Contact__c> alist, Id acrId, Id ocrId, Id conId) {
        if (conId == null || ids.contains(conId)) {return;}
        ASI_Call_Report_Contact__c a = new ASI_Call_Report_Contact__c();
        a.ASI_Call_Report__c = acrId; a.Contact__c = conId;
        a.Old_Call_Report__c = ocrId; a.Record_Key__c = ocrId + ':' + conId;
        alist.add(a); ids.add(conId);
    } 

    public static void createAttachments(List<ASI_Call_Report__c> alist) {
        if (alist == null || alist.isEmpty()) {return;}
        Set<Id> acrIds = new Set<Id>();
        for (ASI_Call_Report__c a : alist) {acrIds.add(a.Id);}
        createAttachments(acrIds);
    }
    
    public static void createAttachments(Set<Id> acrIds) {
        if (acrIds == null || acrIds.isEmpty()) {return;}
        Map<Id, Id> amap = new Map<Id, Id>(); // Map<ocrId, acrId>
        for (ASI_Call_Report__c a : [
            select Id, Old_Call_Report__c 
              from ASI_Call_Report__c where Id in :acrIds]) {
            if (a.Old_Call_Report__c == null) {continue;}
            amap.put(a.Old_Call_Report__c, a.Id);
        }
        createAttachments(amap);
    }

    public static void createAttachments(Map<Id, Id> imap) {
        if (imap == null || imap.isEmpty()) {return;}
        Set<Id> ocrIds = imap.keySet();
        Map<Id, Set<Id>> amap = Attachment_Functions.getAttachmentsIds(ocrIds);
        Map<Id, Set<Id>> nmap = Attachment_Functions.getNotesIds(ocrIds);
        Set<Id> aset, nset; Id pid;
        for (Id oid : ocrIds) {
            pid = imap.get(oid); aset = amap.get(oid); nset = nmap.get(oid);
            createNotes(pid, nset); createAttachments(pid, aset);
        }
    }

    public static void createNotes(Id parentId, Set<Id> docIds) {
        if (parentId == null || docIds == null || docIds.isEmpty()) {return;}
        for (Id docId : docIds) {
            Attachment_Functions.cloneNote(docId, parentId);
        }
    }

    public static void createAttachments(Id parentId, Set<Id> docIds) {
        if (parentId == null || docIds == null || docIds.isEmpty()) {return;}
        for (Id docId : docIds) {
            Attachment_Functions.cloneAttachment(docId, parentId);
        }
    }

}