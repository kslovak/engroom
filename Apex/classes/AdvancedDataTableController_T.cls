@isTest
private class AdvancedDataTableController_T {
/****************************************************************************
 * Test Class AdvancedDataTableController_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - AdvancedDataTableController Apex Class
 ****************************************************************************/
 
    //Test Data
    public static Advanced_Data_Table_Definition__c adtDef;
    public static Account a;
    public static Advanced_Data_Table_Custom_Filter__c f1;
    public static Advanced_Data_Table_Custom_Filter__c f2;
    public static Advanced_Data_Table_Column__c c1;
    
    //Test Settings
    
    
    private static testMethod void myUnitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
        a = TestObjects.NewAccount();

        adtDef = new Advanced_Data_Table_Definition__c();
        adtDef.Name = 'Test';
        adtDef.Object_Name__c = 'Account';
        adtDef.Data_Table_Label__c = 'Test';
        insert adtDef;
        
        f1 = new Advanced_Data_Table_Custom_Filter__c();
        f1.Advanced_Data_Table_Definition__c = adtDef.Id;
        f1.Filter_Type__c = 'User Selectable';
        f1.Name = 'test1';
        f1.Filter_Label__c = 'test1';
        f1.Sort_Order__c = 1;
        insert f1;

        f2 = new Advanced_Data_Table_Custom_Filter__c();
        f2.Advanced_Data_Table_Definition__c = adtDef.Id;
        f2.Filter_Type__c = 'Auto Applied';
        f2.Name = 'test2';
        f2.Filter_Label__c = 'test2';
        f2.Sort_Order__c = 1;
        insert f2;
        
        c1 = new Advanced_Data_Table_Column__c();
        c1.Advanced_Data_Table_Definition__c = adtDef.Id;
        c1.Name = 'test';
        c1.Field_Label__c = 'test';
        c1.Enabled__c = true;
        c1.Visible__c = true;
        insert c1;
    }
  
    private static void executeTest01() {
        // Execute Tests
        AdvancedDataTable adt = new AdvancedDataTable('Test'); 

        ApexPages.currentpage().getparameters().put( 'id' , a.Id);
        AdvancedDataTableController c = new AdvancedDataTableController();    

        c.dataTableDefinitionName = 'Test';
        c.pageSize = '20';

        c.startingId = a.Id;
        c.startingId = null;
        c.startingIdOverride = a.id;
        c.startingId = a.Id;
        
        c.getAdminUser();
        c.getShowActionColumn();
        c.getStartsWithFilterLetters();
        
        c.getLastRetrieveCountSOQL();
        c.getLastRetrieveRelatedRecordsSOQL();
        
        c.toggleAdminPanel();
        
        c.getRelatedRecords();
        
        c.filterByFieldSelection = 'test';
        c.applyNewDynamicFilter1();
        c.applyNewDynamicFilter2();
        c.applyNewDynamicFilter3();
        
        c.editStartsWithFilterField();
        c.startsWithFilterFieldSelection = 'test';
        c.processNewStartsWithFilterField();
        c.cancelNewStartsWithFilterField();
        c.editFilterByField();
        c.processNewFilterByField();
        c.cancelNewFilterByField();
        c.startsWithFilterButtonHandler();
        c.searchByNameFilterButtonHandler();
        c.handleNewCustomFilterSelected();
        
        c.gotoFirstPage();
        c.gotoPrevPage();
        c.gotoNextPage();
        c.gotoLastPage();
        c.getPagingNavMessage();
        c.pageSizeChangeHandler();
        c.sortTableButtonHandler();

        c.idToDelete = a.Id;
        c.deleteId();
        
        c.refresh();

        //String testString;
        //testString = c.filterByFieldSelectionLabel;
        //testString = c.startsWithFilterFieldSelectionLabel;
        
        //c.applyNewStartingId();
        
        AdvancedDataTableController.ColumnSortSetting css = new AdvancedDataTableController.ColumnSortSetting('test',AdvancedDataTableColumn.SORT_ASCENDING);
        
    }
}