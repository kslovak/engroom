public class VI_FusionData_Controller {

	private Boolean showMsgs;

	private String fusionUrl, tokenEnc, tokenTxt;
	
	public VI_FusionData_Controller() {}
	
	private void debug(String s) {System.debug(LoggingLevel.INFO, s);}
	
	public Boolean getShowMsgs() {return VIUtils.SHOW_DEBUG_MSGS;}
	
    public String getFusionUrl() {return VIUtils.FUSION_URL;}
    
	private void setTokenTxt() {
        String s = '';
        s += '{ "email":"';
        s += UserInfo.getUserEmail();
        s += '", "salesforceId":"';
        s += UserInfo.getUserId();
        s += '", "program":"revup", "timestamp":"';
        s += DateTime.now().formatGmt('MMddyyyyHHmm');
        s += '" }';
        tokenTxt = s; debug('setTokenTxt : ' + tokenTxt);
	}
	
    public String getTokenTxt() {setTokenTxt(); return tokenTxt;}
    
	private void setTokenEnc() {
	    String k = VIUtils.FUSION_KEY, v = VIUtils.FUSION_IV;
        setTokenTxt(); tokenEnc = VIUtils.getEncrypted256(k, v, tokenTxt);
	}
	
	public String getTokenEnc() {setTokenEnc(); return tokenEnc;}
	
}