@isTest
private class String_Functions_Test {
    
    static void debug(String s) {String_Functions.debug(s);}

//*
    static testMethod void test01() {
	    String result = '';
	    
	    result = String_Functions.convertCRtoBR('Test1\nTest2\nTest3');
	    System.assertEquals('Test1<BR>Test2<BR>Test3',result);
	
	    result = String_Functions.convertBRtoCR('Test1<BR>Test2<BR>Test3');
	    System.assertEquals('Test1\nTest2\nTest3',result);
	
	    List<String> test = new List<String>();
	    test.add('Line 1 ');
	    test.add('Line 2 ');
	    
	    debug(String_Functions.Format_ListString_into_HTML_List(test, 'prefix:', ':suffix'));
	
	    debug(String_Functions.Format_ListString_into_HTML_List('test', 'prefix:', ':suffix'));
	
	    test = String_Functions.trimAllStringsinListString(test);
	
	    debug(String_Functions.Format_ListString_into_HTML_List(test, 'prefix:', ':suffix'));
	
	    System.assertEquals('12345', String_Functions.removeLeadingZeros('0000000000000000012345'));
	    System.assertEquals('0', String_Functions.removeLeadingZeros('000000000000000000'));
	    System.assertEquals('0', String_Functions.removeLeadingZeros('0'));
	    System.assertEquals(null, String_Functions.removeLeadingZeros(null));
	
	    System.assertEquals('', String_Functions.right('',10));
	    System.assertEquals('0000012345', String_Functions.right('000000000012345',10));
	    System.assertEquals('12345', String_Functions.right('9999912345',5));
	    
        System.assertEquals('test', String_Functions.beforeParenthesis('test'));
        System.assertEquals('test', String_Functions.beforeParenthesis('test(123)'));

        System.assertEquals('123', String_Functions.inParenthesis('123'));
        System.assertEquals('123', String_Functions.inParenthesis('test(123)'));
	    
    }
//*/    
    static testMethod void test02() {
        String d = String_Functions.CRNL; List<String> slist;
        String s = ('1234567890'.repeat(100) + d).repeat(998) + '12345';
        
        // This throws System.LimitException: Regex too complicated
        // slist = s.split(d);
        
        slist = String_Functions.getLines(s);
        debug('slist size : ' + slist.size());
    }
}