public class VI_CSV_Data_Controller {

    public static final String PARAM_ACCOUNT_ID = 'id';

    public String   accountId {get; private set;}
    public String accountName {get; private set;}

    private Account acc;

    public VI_CSV_Data_Controller() {init();}
    
    private void debug(String s) {
        System.debug(LoggingLevel.INFO, 'VI_CSV_Data_Controller : ' + s);
    }
    
    private void init() {
        Map<String, String> params = ApexPages.currentPage().getParameters();
        debug('init : params : ' + params);
        accountId = params.get(PARAM_ACCOUNT_ID); accountName = '';
        acc = VIUtils.getAccount(accountId);
        if (acc != null) {accountName = acc.Name + ' - ' + acc.AccountNumber;}
    }

}