@isTest
private class CaseApprovals_Test {

    static Account acc;
    static Case cas;
    static Id dzTypeId = RecordType_Functions.LookupRecordTypeId('AHWT DZ', 'Case');

    static void debug(String s) {
        System.debug(LoggingLevel.INFO, s);
    }

    static void setup() {
        acc = new Account(Name='TestAcc');
        acc.SAP_Sales_Org__c = '1020';
        acc.SAP_DistChannel__c = '50';
        acc.SAP_DivisionCode__c = '50';
        insert acc; debug('Account : ' + acc);

        cas = new Case();
        cas.AccountId = acc.Id;
        cas.DZ_Number__c = '123456';
        cas.RecordTypeId = dzTypeId;
        cas.Subject = 'test case';
        cas.Submit_on_Save__c = false;
        insert cas;
        cas = [select Id, CreatedDate, Account.Name, Account.AccountNumber,
                      DevEx_Transfer_Status__c, DZ_Number__c, Subject,
                      Submit_on_Save__c
                 from Case where Id = :cas.Id];
    }

    static testMethod void test01() {
        setup();
        Boolean b = CaseCustomSettings__c.getInstance().Lab_Work_Approver__c;
        debug('Lab_Work_Approver = ' + b);
        CaseApprovals.Rec r = new CaseApprovals.Rec(cas, 1);
        CaseApprovals c = new CaseApprovals();
        c.debug(''); c.getRecs(); c.init1(); c.init2(); c.initActionTSR();
        c.pc1Frst(); c.pc1Next(); c.pc1Prev(); c.pc1Last();
        try {c.pc1Size();} catch(Exception e){}
        c.onCreateDevexProj(); c.onCreateDevexProjAll(); c.addErrorMessage('');
        c.approveSelectedList(); c.rejectSelectedList();
        c.sortByAccName(); c.sortByCaseNumber(); c.sortByDevexPM(); c.sortByLabTech();
        c.sortByPriority(); c.sortByReason(); c.sortBySubject(); c.sortByTechRisk();
    }
}