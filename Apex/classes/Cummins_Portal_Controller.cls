public class Cummins_Portal_Controller {
    
    public static final String PORTAL_TYPE  = 'Partner';
    public static final String LEAD_QUEUE   = 'Valvoline - Cummins - Leads';
    public static final String LEAD_RTYPE   = 'Valvoline Cummins Lead';
    public static final String LEAD_SOURCE  = 'Cummins';
    public static final String PROFILE_NAME = 'Cummins Partner Portal';
    
    private static final String BASE_URL = URL.getSalesforceBaseUrl().toExternalForm();
    private static final String SYSADMIN_EMAIL = User_Functions.getSysAdminEmail();

    private static final ApexPages.Severity ERROR = ApexPages.Severity.ERROR;
    private static final ApexPages.Severity INFO  = ApexPages.Severity.INFO;
    private static final ApexPages.Severity WARN  = ApexPages.Severity.WARNING;

    public Boolean renderPageBlock1         {get; private set;}
    public Boolean renderPageBlock2         {get; private set;}
    public Boolean renderPageBlock3         {get; private set;}
    public Boolean renderPageBlock4         {get; private set;}
    public Boolean renderPageBlock5         {get; private set;}
    public Boolean resetpwdSuccess          {get; private set;}
    
    public List<SelectOption> countryOpts   {get; private set;}
    public List<SelectOption> stateOpts     {get; private set;}

    public transient String username        {get; set;}
    public transient String password        {get; set;}

    public String countryCode               {get; set;}
    public String stateCode                 {get; set;}
    public Lead newLead                     {get; set;}
    
    Boolean validForm, validUser; Id leadQueueId, leadRecTypeId, emailTemplateId;
    
    public Cummins_Portal_Controller() {init();}

    public void debug(String s) {System.debug(LoggingLevel.INFO, 'Cummins_Portal_Controller : ' + s);}

    public void addErrMsg(String s)  {validForm = false; addMsg(ERROR, '<b>' + s + '</b>');}

    public void addReqMsg(String s)  {validForm = false; addMsg(ERROR, 'Required : <b>' + s + '</b>');}

    public void addInfoMsg(String s) {addMsg(INFO,  '<b>' + s + '</b>');}

    public void addWarnMsg(String s) {addMsg(WARN,  '<b>' + s + '</b>');}

    private void addMsg(ApexPages.Severity mtype, String s) {
        ApexPages.Message msg = new ApexPages.Message(mtype, s);
        ApexPages.addMessage(msg);
    }
    
    private String str(String s) {return String.isBlank(s) ? '' : s.trim();}

    private void init2() {
        renderPageBlock1 = false; renderPageBlock2 = false; 
        renderPageBlock3 = false; renderPageBlock4 = false;
        renderPageBlock5 = false;
    }
    
    public  void showPageBlock1() {init2(); renderPageBlock1 = true;}
    
    private void showPageBlock2() {init2(); renderPageBlock2 = true;}
    
    private void showPageBlock3() {init2(); renderPageBlock3 = true;}

    private void showPageBlock4() {init2(); renderPageBlock4 = true;}

    private void showPageBlock5() {init2(); renderPageBlock5 = true;}

    private void init() {
        initPageBlock(); if (renderPageBlock1) {return;}
        countryCode = 'US'; stateCode = 'AL'; Date tdate = Date.today();
        countryOpts = Country_Code_Functions.getCountryOpts(); changeCountry();
        setLeadRecTypeId(); setLeadQueueId();
    }
    
    private void initPageBlock() {
        initLead(); String userType = UserInfo.getUserType();
        if ('Guest'.equalsIgnoreCase(userType)) {showPageBlock1();}
    }
    
    public void initLead() {
        newLead = new Lead(); showPageBlock2();
    }
    
    public void changeCountry() {
        stateOpts = Country_Code_Functions.getStateOpts(countryCode);
    }
    
    private void setLeadRecTypeId() {
        leadRecTypeId = null;
        Map<String, Schema.RecordTypeInfo> recTypeMap = 
        Lead.sObjectType.getDescribe().getRecordTypeInfosByName();
        if (recTypeMap.containsKey(LEAD_RTYPE)) {               
            leadRecTypeId = recTypeMap.get(LEAD_RTYPE).getRecordTypeID(); 
        }
    }
    
    private void setLeadQueueId() {
        leadQueueId = null;
        Map<String, String> queueNameIdMap = new Map<String, String>();
        Set<String> queues = new Set<String>{LEAD_QUEUE}; leadQueueId = null;
        queueNameIdMap = Public_Group_Functions.getQueueNameIdMap(queues);
        if (queueNameIdMap.containsKey(LEAD_QUEUE)) {
            leadQueueId = queueNameIdMap.get(LEAD_QUEUE);
        }
    }
    
    private void validateUser() {
        username = str(username);
        User u = User_Functions.getActiveUserWithRole(username);
        validUser = (u != null && u.Profile != null && u.UserRole != null &&
                     PROFILE_NAME.equals(u.Profile.Name) &&
                     PORTAL_TYPE.equals(u.UserRole.PortalType));
        if (!validUser) {addErrMsg('Username [' + username + '] not found');}
    }
    
    private void validate1() {
        validForm = true;
        if (String.isBlank(username)) {addReqMsg('Username');}
        if (String.isBlank(password)) {addReqMsg('Password');}
        if (!validForm) {return;}
    }

    private void validate2() {
        validForm = true;
        newLead.Company             = str(newLead.Company);             if (newLead.Company             == '') {addReqMsg('Company Name');}
        newLead.Location_Name__c    = str(newLead.Location_Name__c);    if (newLead.Location_Name__c    == '') {addReqMsg('Location Name');}
        newLead.Street              = str(newLead.Street);              if (newLead.Street              == '') {addReqMsg('Street');}
        newLead.City                = str(newLead.City);                if (newLead.City                == '') {addReqMsg('City');}
        newLead.State               = str(newLead.State);               if (newLead.State               == '') {addReqMsg('State/Province');}
        newLead.PostalCode          = str(newLead.PostalCode);          if (newLead.PostalCode          == '') {addReqMsg('ZIP/Postal Code');}
        newLead.Country             = str(newLead.Country);             if (newLead.Country             == '') {addReqMsg('Country');}
        newLead.Phone               = str(newLead.Phone);               if (newLead.Phone               == '') {addReqMsg('Phone Number');}
        newLead.Primary_Industry__c = str(newLead.Primary_Industry__c); if (newLead.Primary_Industry__c == '') {addReqMsg('Primary Industry');}

        newLead.Contract_Description__c = str(newLead.Contract_Description__c); 
        if ('Other'.equalsIgnoreCase(newLead.Contract_Type__c) && 
            newLead.Contract_Description__c == '') {addReqMsg('Contract Description');}
        
        if (!validForm) {return;}
        List<Lead> leads = [select Id from Lead 
                             where LeadSource = :LEAD_SOURCE and Company = :newLead.Company
                               and Location_Name__c = :newLead.Location_Name__c limit 1];
        if (leads != null && !leads.isEmpty()) {addErrMsg(newLead.Company + 
            ' at Location ' + newLead.Location_Name__c + ' is already Entered.');}
    }

    public PageReference login() {
        PageReference pr = null;
        //username = 'cummins1@cummins.com.qa';
        //password = 'ashland';
        validate1();    if (!validForm) {return pr;}
        validateUser(); if (!validUser) {return pr;}
        pr = Site.login(username, password, null);
        return pr;
    }
    
    public PageReference logout() {
        return new PageReference('/secur/logout.jsp');
    }
    
    public void forgotPassword() {showPageBlock4();}

    public void resetPassword() {
        //username = 'cummins1@cummins.com.qa';
        validateUser(); if (!validUser) {return;}
        resetpwdSuccess = Site.forgotPassword(username);
        if (!resetpwdSuccess) {addErrMsg('Failed to Reset Password');}
        showPageBlock5();
    }

    public void saveLead() {
        newLead.Country = countryCode; newLead.State = stateCode;
        validate2(); if (!validForm) {return;}
        String s = str(newLead.Company) + ' : Lead Created Successfully.';
        if (leadQueueId != null) {newLead.OwnerId = leadQueueId;}
        if (leadRecTypeId != null) {newLead.RecordTypeId = leadRecTypeId;}
        newLead.LeadSource = LEAD_SOURCE; newLead.Lead_Type__c = LEAD_SOURCE;
        newLead.LastName = newLead.Company; newLead.Category_1__c = 'Lubricants';
        try {
            insert newLead; 
            addInfoMsg(s); showPageBlock3();  
        } catch(Exception e) {addErrMsg(e.getMessage());}
    }
}