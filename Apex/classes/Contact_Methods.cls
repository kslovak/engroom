public class Contact_Methods {
    
    private static void debug(String s) {
        System.debug(LoggingLevel.INFO, 'Contact_Methods : ' + s);
    }
    
    public static void doBeforeUpsert(List<Contact> clist) {
        Set<Id> accIds = new Set<Id>();
        for (Contact c : clist) {
            if (c.AccountId != null) {accIds.add(c.AccountId);}
        }
        setOwnerId(clist, accIds);
    }
    
    private static void setOwnerId(List<Contact> clist, Set<Id> accIds) {
        if (accIds == null || accIds.isEmpty()) {return;}
        Map<Id, Id> accOwners = new Map<Id, Id>(); Id accOwnerId;
        for (Account a : [select Id, OwnerId from Account where Id in :accIds]) {
            accOwners.put(a.Id, a.OwnerId);
        }
        for (Contact c : clist) {
            if (c.AccountId != null) {
	            accOwnerId = accOwners.get(c.AccountId);
	            if (c.OwnerId != accOwnerId) {c.OwnerId = accOwnerId;}
            }
        }
    }

    public static void updateOwnerId(Map<Id, Id> accOwners) {
        if (accOwners == null || accOwners.isEmpty()) {return;}
        Set<Id> accIds = accOwners.keySet(); Id accOwnerId;
        Map<Id, Contact> cmap = new Map<Id, Contact>();
        for (Contact c : [select Id, AccountId, OwnerId from Contact 
                           where AccountId in :accIds]) {
            accOwnerId = accOwners.get(c.AccountId);
            if (c.OwnerId != accOwnerId) {
                c.OwnerId = accOwnerId; cmap.put(c.Id, c);
            }
        }
        if (cmap.isEmpty()) {return;}
        try {update cmap.values();} catch(Exception e) {debug(e.getMessage());} 
    }

}