public with sharing class WebContentVersionWrapper {
    
    private ContentVersion content;
    
    public WebContentVersionWrapper(ContentVersion cv) {
        this.content = cv;
    }
    
    public boolean isNew() {
        return this.content.ContentModifiedDate.date().daysBetween(Date.today()) < 7;
    }

}