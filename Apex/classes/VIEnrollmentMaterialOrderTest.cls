@isTest
private class VIEnrollmentMaterialOrderTest {
	
	static Account a;
	static VI_Promotion__c p;
	static VI_Promotion_Material__c pm;
	static VI_Enrollment__c e;
	static VI_Enrollment_Location__c el;
	static VI_Enrollment_Location_Material__c elm;
	static VI_Enrollment_Material_Order__c emo;

    static void createTestData() {
        a = new Account(Name='Test Account', AccountNumber='TestAcct');
        insert a;
        
        p = new VI_Promotion__c();
        p.Program_Code__c = 'PC1001';
        p.Channel__c = 'Test Channel';
        p.Program_Name__c = 'Test Promotion';
        insert p;
        
        pm = new VI_Promotion_Material__c();
        pm.Cost__c = 1.0; pm.Material__c = 'Tear Pads'; pm.Promotion__c = p.Id;
        insert pm;
        
        e = new VI_Enrollment__c();
        e.Facility__c = a.Id;
        e.Promotion__c = p.Id;
        insert e;
        
        el = new VI_Enrollment_Location__c();
        el.Approval_Status__c = VIUtils.ENROLLMENT_STATUS_SUBMITTED;
        el.Enrollment__c = e.Id;
        el.Location__c = a.Id;
        el.Promotion__c = p.Id;
        insert el;
        
        elm = new VI_Enrollment_Location_Material__c();
        elm.Enrollment_Location__c = el.Id;
        elm.Promotion_Material__c = pm.Id;
        elm.Key__c = el.Id + ':' + pm.Id;
        insert elm;
        
        emo = new VI_Enrollment_Material_Order__c();
        emo.Enrollment_Location_Material__c = elm.Id;
        emo.Quantity__c = 1.0;
        insert emo;
    }
    
    static testMethod void test01() {
        Test.startTest();
        createTestData();
        VIEnrollmentMaterialOrderController c = new VIEnrollmentMaterialOrderController();
        c.updateShippingData();
        List<String> smsgs = new List<String>(), emsgs = new List<String>();
        String s = emo.Id + ',05/22/2012,1Z4489020375189250,,6';
        VIEnrollmentMaterialOrderFunctions.updateShippingData(s, smsgs, emsgs);
    }
}