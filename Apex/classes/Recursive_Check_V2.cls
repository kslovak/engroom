public class Recursive_Check_V2 {

    // Static variables are local to the context of a Web request
    // (or testMethod during a runTests call)
    // Therefore, this variable will be initialized as false
    // at the beginning of each Web request which accesses it.

    private static Map<String,Boolean> alreadyRanMap = new Map<String,Boolean>();
    
    public static boolean hasAlreadyRan(String name) {
        Boolean alreadyRan = false;
        if (alreadyRanMap.containsKey(name)) {
            alreadyRan = alreadyRanMap.get(name);
        }
        return alreadyRan;
    }

    // By setting the variable to true, it maintains this
    // new value throughout the duration of the request
    // (or testMethod)

    public static void setAlreadyRan(String name) {
        alreadyRanMap.put(name,true);
    }

	public static void reset(String name) {
        alreadyRanMap.put(name,false);
	}
}