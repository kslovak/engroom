@isTest
private class Material_Sales_Data2_Controller_Test {

    static String matnum = '000000000000123456';
    static String plant  = '1234';
    static String sorg   = '1234';
    static String dchan  = '12';
    static String lang   = 'E';
    
    static Country_Code__c cc;
    static SAP_Valuation_Area__c va; 
    static SAP_Plant__c sp;
    static SAP_Sales_Org__c so;
    static SAP_Distribution_Channel__c dc;
    static Material_Valuation__c mv;   
    static Material_General_Data2__c mgd2;
    static Material_Plant2__c mp2;
    static Material_Sales_Data2__c msd2;
    static Product_Hierarchy2__c pherky2;
    
    static ApexPages.Standardcontroller sc;
    static Material_Sales_Data2_Controller mc;
    
    static void setup() {
        cc = new Country_Code__c();
        cc.Country__c = 'US'; cc.Code__c = '001';
        cc.Country_Name__c = 'United States';
        insert cc;
        
        va = new SAP_Valuation_Area__c();
        va.Company_Code__c = sorg;
        va.Valuation_Area__c = plant;
        insert va;
        
        sp = new SAP_Plant__c();
        sp.Plant_Code__c = plant;
        insert sp;

        so = new SAP_Sales_Org__c();
        so.Sales_Org_Code__c = sorg;
        so.Language_Code__c = lang;
        insert so;
        
        dc = new SAP_Distribution_Channel__c();
        dc.Distribution_Channel_Code__c = dchan;
        dc.Language_Code__c = lang;
        insert dc;

        mv = new Material_Valuation__c();
        mv.Material_Number__c = matnum;
        mv.Price_Control_Indicator__c = 'S';
        mv.Price_Unit__c = 100.0;
        mv.Record_Key__c = matnum + plant;
        mv.Standard_Price__c = 1234.56;
        mv.Valuation_Area__c = plant;
        insert mv;
        List<String> mvKeys = new List<String> {matnum + plant};

        mgd2 = new Material_General_Data2__c();
        mgd2.Material_Number__c = matnum;
        insert mgd2;
        
        setTestMp2(); setTestMsd2();
    }
    
    static void setTestMp2() {
        mp2 = new Material_Plant2__c();
        mp2.Material_Number__c = matnum;
        mp2.Plant_Code__c = plant;
        mp2.SalesOrg_Code__c = sorg;
        mp2.Record_Key__c = matnum + plant + sorg;
        mp2.Material_Origin_Country__c = cc.Country__c;
        mp2.Material_General_Data__c = mgd2.Id;
        mp2.SAP_Plant__c = sp.Id;
        mp2.SAP_Sales_Org__c = so.Id;
        insert mp2;
    }
    
    static void setTestMsd2() {
        setTestProdHerky2();
        
        msd2 = new Material_Sales_Data2__c();
        msd2.Material_Number__c = matnum;
        msd2.Sales_Org_Code__c = sorg;
        msd2.Dist_Channel_Code__c = dchan;
        msd2.Record_Key__c = matnum + sorg + dchan;
        msd2.SalesOrg_Code__c = so.Id;
        msd2.DistChannel_Code__c = dc.Id;
        msd2.Material_General_Data__c = mgd2.Id;
        msd2.Product_Hierarchy__c = pherky2.Id;
        insert msd2;
    }
    
    static Product_Hierarchy__c getPH(String phcode) {
        Product_Hierarchy__c ph = new Product_Hierarchy__c();
        ph.Language_Code__c = 'E'; ph.Product_Hierarchy_Code__c = phcode;
        insert ph; return ph;
    }
    
    static void setTestProdHerky2() {
        Product_Hierarchy__c ph1 = getPH('1'),
                             ph2 = getPH('123'),
                             ph3 = getPH('123456'),
                             ph4 = getPH('123456789'),
                             ph5 = getPH('123456789123'),
                             ph6 = getPH('123456789123456'),
                             ph7 = getPH('123456789123456789');

        pherky2 = new Product_Hierarchy2__c();
        pherky2.Product_Hierarchy__c  = ph7.Product_Hierarchy_Code__c;
        pherky2.Product_Hierarchy1__c = ph1.Id;
        pherky2.Product_Hierarchy2__c = ph2.Id;
        pherky2.Product_Hierarchy3__c = ph3.Id;
        pherky2.Product_Hierarchy4__c = ph4.Id;
        pherky2.Product_Hierarchy5__c = ph5.Id;
        pherky2.Product_Hierarchy6__c = ph6.Id;
        pherky2.Product_Hierarchy7__c = ph7.Id;
        insert pherky2;
    }
    
    public static testMethod void test1() {
        setup(); Test.startTest();
        sc = new ApexPages.Standardcontroller(msd2);
        mc = new Material_Sales_Data2_Controller(sc);
    }
    
}