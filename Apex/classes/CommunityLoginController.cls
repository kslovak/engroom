global with sharing class CommunityLoginController extends PageController {

/**
 * An apex page controller that exposes the site login functionality
 */
    global String username{get;set;}
    global String password{get;set;}
    global CommunityLoginController () {}
    global PageReference forwardToCustomAuthPage() {
        return null;//Page.CommunityLogin;
    }
    global PageReference login() {
        return Site.login(username, password, null);
    }
}