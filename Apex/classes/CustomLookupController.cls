public with sharing class CustomLookupController {

    public String objectName {get;set;}
    public String columnsToShow {get;set;}
    public String allowEmptySearch {get;set;}
    public String additionalSearchFields {get;set;}
    public String runFirst {get;set;}
    public String orderBy {get;set;}
    public String recordLimit {get;set;}
    public String minSearchBoxLength {get;set;}
    public String linkFieldName {get;set;}

    public String filter1 {get;set;}
    public String filter1AsEntered {get;set;}
    public String filter1Label {get;set;}
    public String filter1OffByDefault {get;set;}
    public String filter1Optional {get;set;}
    public String filter1Disabled {get;set;}
    public String filter1ColumnsToAppendIfRemoved {get;set;}
    public String filter1ForceSearchBoxIfRemoved {get;set;}
    public String filter1AdditionalSearchFieldsIfRemoved {get;set;}
    public String filter1OrderByPrefixIfRemoved {get;set;}
    public String filter1OrderBySuffixIfRemoved {get;set;}
    public String filter1MinSearchBoxLengthIfRemoved {get;set;}

    public String filter2 {get;set;}
    public String filter2AsEntered {get;set;}
    public String filter2Label {get;set;}
    public String filter2OffByDefault {get;set;}
    public String filter2Optional {get;set;}
    public String filter2Disabled {get;set;}
    public String filter2ColumnsToAppendIfRemoved {get;set;}
    public String filter2ForceSearchBoxIfRemoved {get;set;}
    public String filter2AdditionalSearchFieldsIfRemoved {get;set;}
    public String filter2OrderByPrefixIfRemoved {get;set;}
    public String filter2OrderBySuffixIfRemoved {get;set;}
    public String filter2MinSearchBoxLengthIfRemoved {get;set;}

    public String filter3 {get;set;}
    public String filter3AsEntered {get;set;}
    public String filter3Label {get;set;}
    public String filter3OffByDefault {get;set;}
    public String filter3Optional {get;set;}
    public String filter3Disabled {get;set;}
    public String filter3ColumnsToAppendIfRemoved {get;set;}
    public String filter3ForceSearchBoxIfRemoved {get;set;}
    public String filter3AdditionalSearchFieldsIfRemoved {get;set;}
    public String filter3OrderByPrefixIfRemoved {get;set;}
    public String filter3OrderBySuffixIfRemoved {get;set;}
    public String filter3MinSearchBoxLengthIfRemoved {get;set;}

    public Boolean searchPanelVisible {get;set;}
    
    public PageReference UpdateSettings() {
        searchPanelVisible = true;
        return null;
    }
    
}