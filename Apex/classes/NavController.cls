global with sharing class NavController {

    global ConnectApi.UnreadConversationCount getUnreadConversationCount() {
        return ConnectApi.ChatterMessages.getUnreadCount();
    }
}