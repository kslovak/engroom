@isTest
private class AccountRollupTeamSectionController_T {
/****************************************************************************
 * Test Class AccountRollupTeamSectionController_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - AccountRollupTeamSectionController Apex Class
 ****************************************************************************/
 
    //Test Data
    
    
    //Test Settings
    
    
    private static testMethod void myUnitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
        // No Test Data Needed
    }
  
    private static void executeTest01() {
        // Execute Tests
        Account a = TestObjects.NewAccount();
        user u1 = TestObjects.newUser();
        user u2 = TestObjects.newUser();
        AccountTeamMember m1 = TestObjects.NewAccountTeamMember(new Map<String,String>{'AccountId'=>a.Id,
        	'UserId'=>u1.Id,
        	'TeamMemberRole'=>'Team Member'});
        
        AccountTeamMember m2 = TestObjects.NewAccountTeamMember(new Map<String,String>{'AccountId'=>a.Id,
        	'UserId'=>u2.Id,
        	'TeamMemberRole'=>'Advisory Board Team Member'});
        
        Contact contact = TestObjects.NewContact(new Map<String,String>{'AccountId'=>a.Id});
        
        ApexPages.currentpage().getparameters().put( 'id' , a.Id);
        AccountRollupTeamSectionController c = new AccountRollupTeamSectionController();  
        
        c.getTeamMembersExist();
        c.updateTeamMembers();
        c.beginEditingTeamMembers();
        c.newTeamMember();
        c.cancelEditOfTeamMembers();
        c.idToDelete = m1.Id;
        c.deleteTeamMemberBasedOnId();  
        
        c.newTeamMember();
        c.updateTeamMembers();

        c.updateAdvisoryBoardTeamMembers();
        c.beginEditingAdvisoryBoardTeamMembers();
        c.newAdvisoryBoardTeamMember();
        c.cancelEditOfAdvisoryBoardTeamMembers();
        c.idToDelete = m2.Id;
        c.deleteAdvisoryBoardTeamMemberBasedOnId();

        c.newAdvisoryBoardTeamMember();
        c.updateAdvisoryBoardTeamMembers();
        
        c.updateCustomerTeamMembers();
        c.beginEditingCustomerTeamMembers();
        c.newCustomerTeamMember();
        c.cancelEditOfCustomerTeamMembers();
        c.idToDelete = contact.Id;
        c.deleteCustomerTeamMemberBasedOnId();
        
        c.newCustomerTeamMember();
        c.updateCustomerTeamMembers();

        c.beginEditingStrategicAccountManager();
        c.updateStrategicAccountManager();
        c.cancelEditOfStrategicAccountManager();
        
        c.beginEditingExecutiveSponsor();
        c.updateExecutiveSponsor();
        c.cancelEditOfExecutiveSponsor();

        Account a2 = TestObjects.NewAccount();
        ApexPages.currentpage().getparameters().put( 'id' , a2.Id);
        AccountRollupTeamSectionController c2 = new AccountRollupTeamSectionController();
        
        c2.idToDelete = '001000000000000';
        c2.deleteTeamMemberBasedOnId();
        
          
        
    }
}