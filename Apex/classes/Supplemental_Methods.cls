public class Supplemental_Methods {

    private static final String SORG_US = '1000';  
    private static final String VALV_DC = '40';  
    private static final String COLON   = ':';  
    private static final String COMMA   = ',';  
    
    private static void debug(String s) {System.debug(LoggingLevel.INFO, s);}

    private static void debug(Exception e) {
        String a = 'Supplemental_Methods', s = e.getMessage(); debug(s); 
        //AppLog.write(a, '', '', '', '', s);
    }

    private static String getString(String s) {
        if (String.isBlank(s)) {return '';} else {return s.trim();}
    }
        
    private static String getString(Date d) {
        if (d == null) {return '';}
        Datetime dt = Datetime.newInstance(d.year(), d.month(), d.day());
        return dt.format('yyyyMMdd');
    }
    
    private static Date getDate(String s) {
        try {return Date.parse(s);} catch(Exception e) {debug(e); return null;}
    }
    
    private static Decimal getDecimal(String s) {
        try {return Decimal.valueOf(s);} catch(Exception e) {debug(e); return null;}
    }
    
    private static Boolean equals(String s1, String s2) {
        return (s1 != null && s2 != null && s1.trim() == s2.trim());
    }
    
    private static Boolean within(Date d1, Date d2, Date d3) {
        return (d1 != null && d2 != null && d3 != null && d1 >= d2 && d1 <= d3);
    }
    
    private static Boolean isNumeric(String s) {
        return (s != null && s.trim().isNumeric());
    }
    
    private static String getAccNum(String s) {
        s = String_Functions.getZeroPaddedString(s, 10);
        if (isNumeric(s)) {s += SORG_US + VALV_DC + VALV_DC;} return s;
    }
    
    private static String getMatKey(String s) {
        s = String_Functions.getZeroPaddedString(s, 18);
        s += SORG_US + VALV_DC; return s;
    }
    
    private static Map<String, Id> getAccIdMap(Set<String> accNums) {
        Map<String, Id> amap = new Map<String, Id>();
        if (accNums.isEmpty()) {return amap;} String k; 
        for (Account a : [
            select Id, AccountNumber, Account_Number__c
              from Account
             where AccountNumber     in :accNums
                or Account_Number__c in :accNums]) {
            k = a.AccountNumber; if (isNumeric(k)) {k = a.Account_Number__c;}
            if (!String.isBlank(k)) {amap.put(k, a.Id);}
        }
        return amap;
    }

    private static Map<String, Id> getMatIdMap(Set<String> matKeys) {
        Map<String, Id> amap = new Map<String, Id>();
        if (matKeys.isEmpty()) {return amap;} String k; 
        for (Material_Sales_Data2__c a : [
            select Id, Record_Key__c
              from Material_Sales_Data2__c
             where Record_Key__c in :matKeys]) {
            amap.put(a.Record_Key__c, a.Id);
        }
        return amap;
    }

// --------------------------------------- Supplemental Rates ---------------------------------------------
    
    public static void doBeforeUpsert(List<Supplemental_Rate__c> alist, Boolean isInsert) {
        Set<String> accNums = new Set<String>(), matKeys = new Set<String>(); String s, k;
        for (Supplemental_Rate__c a : alist) {
            s = getAccNum(a.Account_Number__c);     accNums.add(s);
            s = getAccNum(a.Distributor_Number__c); accNums.add(s);
            s = getMatKey(a.Material_Number__c);    matKeys.add(s);
        }
        Map<String, Id> accIds = getAccIdMap(accNums);
        Map<String, Id> matIds = getMatIdMap(matKeys);
        for (Supplemental_Rate__c a : alist) {
	        s = getAccNum(a.Account_Number__c);     a.Account__c     = accIds.get(s); 
	        s = getAccNum(a.Distributor_Number__c); a.Distributor__c = accIds.get(s);
            s = getMatKey(a.Material_Number__c);    a.Material__c    = matIds.get(s);
            if (!isInsert) {setRecordKey(a);}
        }
    }
    
    private static void setRecordKey(Supplemental_Rate__c a) {
        String k, s;
        s = getAccNum(a.Account_Number__c);     k = s;    
        s = getAccNum(a.Distributor_Number__c); k += COLON + s;
        s = getString(a.Material_Number__c);    k += COLON + s;
        s = getString(a.Begin_Date__c);         k += COLON + s;
        s = getString(a.End_Date__c);           k += COLON + s;
        a.Record_Key__c = k; 
    }
    
    private static Boolean validate1(Supplemental_Rate__c a) {
        Boolean b1;
        b1 = !String.isBlank(a.Distributor_Number__c) &&
             !String.isBlank(a.Account_Number__c) &&
             !String.isBlank(a.Material_Number__c) &&
             a.Begin_Date__c != null && a.End_Date__c != null &&
             a.Selling_Price__c != null && a.Supplemental_Price__c != null;
        return b1;
    }
    
    private static Supplemental_Rate__c getRate(String s, CSV_Data_Config__c c) {
        if (String.isBlank(s)) {return null;}
        Supplemental_Rate__c a = new Supplemental_Rate__c();
        String[] sa = s.split(COMMA); Integer n = sa.size();
        if (n > 0) {a.Distributor_Number__c = getString( sa[0]);}
        if (n > 1) {a.Account_Number__c     = getString( sa[1]);}
        if (n > 2) {a.Material_Number__c    = getString( sa[2]);}
        if (n > 3) {a.Distributor_Part__c   = getString( sa[3]);}
        if (n > 4) {a.Begin_Date__c         = getDate(   sa[4]);}
        if (n > 5) {a.End_Date__c           = getDate(   sa[5]);}
        if (n > 6) {a.Selling_Price__c      = getDecimal(sa[6]);}
        if (n > 7) {a.Supplemental_Price__c = getDecimal(sa[7]);}
        setRecordKey(a); if (validate1(a)) {return a;} return null;
    }
    
    public static void upsertRates(List<String> slist, String csvConfigId) {
        if (slist == null || slist.isEmpty()) {return;}
        List<Supplemental_Rate__c> alist = new 
        List<Supplemental_Rate__c>(); Supplemental_Rate__c a;
        CSV_Data_Config__c c = CSV_Data_Methods.getCsvDataConfig(csvConfigId);
        for (String s : slist) {
            a = getRate(s, c); if (a != null) {alist.add(a);}
        }
        if (alist.isEmpty()) {return;}
        try {upsert alist Record_Key__c;} catch(Exception e) {debug(e);}
    }
    
// --------------------------------------- Supplemental Claims ---------------------------------------------
    
    public static void doBeforeUpsert(List<Supplemental_Claim__c> alist, Boolean isInsert) {
        Set<String> accNums = new Set<String>(), matKeys = new Set<String>(); String s, k;
        for (Supplemental_Claim__c a : alist) {
            s = getAccNum(a.Account_Number__c);     accNums.add(s);
            s = getAccNum(a.Distributor_Number__c); accNums.add(s);
            s = getMatKey(a.Material_Number__c);    matKeys.add(s);
        }
        Map<String, Id> accIds = getAccIdMap(accNums);
        Map<String, Id> matIds = getMatIdMap(matKeys);
        for (Supplemental_Claim__c a : alist) {
            s = getAccNum(a.Account_Number__c);     a.Account__c     = accIds.get(s); 
            s = getAccNum(a.Distributor_Number__c); a.Distributor__c = accIds.get(s);
            s = getMatKey(a.Material_Number__c);    a.Material__c    = matIds.get(s);
            if (!isInsert) {setRecordKey(a);}
        }
        setRateIds(alist);
    }
    
    private static void setRateIds(List<Supplemental_Claim__c> clist) {
        if (clist == null || clist.isEmpty()) {return;}
        Set<String> accIds = new Set<String>(), dstIds = new Set<String>(),
                    matIds = new Set<String>();
        Date minDate = null, maxDate = null;
        for (Supplemental_Claim__c c : clist) {
            c.Supplemental_Rate__c = null;
            if (c.Account__c     != null) {accIds.add(c.Account__c);}
            if (c.Distributor__c != null) {dstIds.add(c.Distributor__c);}
            if (c.Material__c    != null) {matIds.add(c.Material__c);}
            if (c.Invoice_Date__c != null) {
                if (minDate == null || c.Invoice_Date__c < minDate) {
                    minDate = c.Invoice_Date__c;
                }
                if (maxDate == null || c.Invoice_Date__c > maxDate) {
                    maxDate = c.Invoice_Date__c;
                }
            }
        }
        if (accIds.isEmpty() || dstIds.isEmpty() || matIds.isEmpty() || 
            minDate == null  || maxDate == null) {return;}
        List<Supplemental_Rate__c> rlist = [
            select Id, Account__c, Begin_Date__c, Distributor__c, 
                   End_Date__c, Material__c
              from Supplemental_Rate__c
             where Account__c in :accIds
               and Distributor__c in :dstIds
               and Material__c in :matIds
               and Begin_Date__c <= :minDate
               and End_Date__c >= :maxDate
          order by Account__c, Distributor__c, Material__c, 
                   Begin_Date__c, End_Date__c];
        if (rlist == null || rlist.isEmpty()) {return;}
        for (Supplemental_Claim__c c : clist) {
            for (Supplemental_Rate__c r : rlist) {
                if (compare(c, r)) {
                    debug(c.Id + ' : ' + r.Id);
                    c.Supplemental_Rate__c = r.Id; break;
                }
            }
        }
    }
    
    private static Boolean compare(Supplemental_Claim__c c, Supplemental_Rate__c r) {
        Boolean b = true;
        b &= equals(c.Account__c, r.Account__c);                        if (!b) {return b;}
        b &= equals(c.Distributor__c, r.Distributor__c);                if (!b) {return b;}
        b &= equals(c.Material__c, r.Material__c);                      if (!b) {return b;}
        b &= within(c.Invoice_Date__c, r.Begin_Date__c, r.End_Date__c); if (!b) {return b;}
        return b;
    }
    
    private static void setRecordKey(Supplemental_Claim__c a) {
        String k, s;
        s = getAccNum(a.Account_Number__c);        k = s;    
        s = getAccNum(a.Distributor_Number__c);    k += COLON + s;
        s = getString(a.Material_Number__c); k += COLON + s;
        s = getString(a.Invoice_Date__c);          k += COLON + s;
        s = getString(a.Invoice_Number__c);        k += COLON + s;
        a.Record_Key__c = k; 
    }
    
    private static Boolean validate1(Supplemental_Claim__c a) {
        Boolean b1;
        b1 = !String.isBlank(a.Distributor_Number__c) &&
             !String.isBlank(a.Account_Number__c) &&
             !String.isBlank(a.Material_Number__c) &&
             a.Invoice_Date__c != null && a.Invoice_Number__c != null &&
             a.Gallons_Sold__c != null &&
             a.Selling_Price__c != null && a.Supplemental_Price__c != null;
        return b1;
    }
    
    private static Supplemental_Claim__c getClaim(String s, CSV_Data_Config__c c) {
        if (String.isBlank(s)) {return null;}
        Supplemental_Claim__c a = new Supplemental_Claim__c();
        String[] sa = s.split(COMMA);  Integer n = sa.size();
        if (n > 0) {a.Distributor_Number__c      = getString( sa[0]);}
        if (n > 1) {a.Account_Number__c          = getString( sa[1]);}
        if (n > 2) {a.Material_Number__c   = getString( sa[2]);}
        if (n > 3) {a.Distributor_Part__c = getString( sa[3]);}
        if (n > 4) {a.Invoice_Date__c            = getDate(   sa[4]);}
        if (n > 5) {a.Invoice_Number__c          = getString( sa[5]);}
        if (n > 6) {a.Gallons_Sold__c            = getDecimal(sa[6]);}
        if (n > 7) {a.Selling_Price__c           = getDecimal(sa[7]);}
        if (n > 8) {a.Supplemental_Price__c      = getDecimal(sa[8]);}
        setRecordKey(a); if (validate1(a)) {return a;} return null;
    }
    
    public static void upsertClaims(List<String> slist, String csvConfigId) {
        if (slist == null || slist.isEmpty()) {return;}
        List<Supplemental_Claim__c> alist = new 
        List<Supplemental_Claim__c>(); Supplemental_Claim__c a;
        CSV_Data_Config__c c = CSV_Data_Methods.getCsvDataConfig(csvConfigId);
        for (String s : slist) {
            a = getClaim(s, c); if (a != null) {alist.add(a);}
        }
        if (alist.isEmpty()) {return;}
        try {upsert alist Record_Key__c;} catch(Exception e) {debug(e);}
    }
    
}