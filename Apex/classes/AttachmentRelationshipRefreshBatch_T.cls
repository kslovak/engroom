@isTest
private class AttachmentRelationshipRefreshBatch_T {
/****************************************************************************
 * Test Class AttachmentRelationshipRefreshBatch_T
 * --------------------------------------------------------------------------
 * Responsible for Testing:
 *   - AttachmentRelationshipRefreshBatch Apex Class
 ****************************************************************************/
 
    //Test Data
    public static Account a;
    public static Attachment aa;
    
    //Test Settings
    
    
    private static testMethod void myUnitTest() {
        // Create Test Data       
        createTestData();

        // Execute Tests
        Test.startTest();
        executeTest01();
        Test.stopTest();
    }
  
    private static void createTestData() {
    	a = TestObjects.NewAccount();
        Blob b = Blob.valueOf('Test');
		aa = TestObjects.newAttachment(new Map<String,String>{'Name'=>'Test','ParentId'=>a.Id},false);

		aa.body = b;
		insert aa;
    }
  
    private static void executeTest01() {
        // Execute Tests

	    AttachmentRelationshipRefreshBatch.submitBatchJob();
    }
}