@isTest
private class Label_Request_Functions_Test {

    static Account acc;
    static Contact con;
    static Label_Request__c lr;
    static Label_Vendor__c lv;
    static SAP_Distribution_Channel__c dc;
    static SAP_Sales_Org__c so;
    static String responseText;
    static String distChanCode = '10';
    static String salesOrgCode = '1020';
    static String lang = 'E';
    
    static testMethod void myUnitTest() {
        Test.startTest();
        createTestData();
        test01();
        Test.stopTest();
    }
    
    static void createTestData() {
         
        dc = getDistChan(distChanCode);
        
        so = getSalesOrg(salesOrgCode);
        
        acc = new Account(Name = 'Test Account');
        acc.Account_Number__c = 'TestAccount';
        acc.SAP_DistChannel__c = distChanCode;
        acc.SAP_Sales_Org__c = salesOrgCode;
        insert acc;
        con = new Contact();
        con.LastName = 'LastName';
        con.Email = 'emailid1@ashland.com';
        insert con;
        lv = new Label_Vendor__c();
        lv.Contact__c = con.Id;
        lv.SAP_Distribution_Channel__c = dc.Id;
        lv.SAP_Sales_Org__c = so.Id;
        lv.Additional_Emails__c = 'emailid2@ashland.com';
        insert lv;
        lr = new Label_Request__c();
        lr.Contact__c = con.Id;
        lr.Customer_Prospect__c = acc.Id;
        lr.Status__c = 'Submitted';
        insert lr;
        responseText = '\n';
        responseText += 'Almatek Replies:\n';
        responseText += 'Date Shipped (MM/DD/YYYY) : 01/01/2010\n';
        responseText += 'Quantity Shipped : 1234\n';
        responseText += 'Tracking Number : ABCDEFGHIJKL\n';
        responseText += 'Label Request Reference # : ';
        responseText += lr.Id;
        responseText += '\n';
    }
    
    static SAP_Sales_Org__c getSalesOrg(String sorg) {
        so = null;
        try {
            so = [select Sales_Org_Code__c 
                    from SAP_Sales_Org__c
                   where Sales_Org_Code__c = :sorg
                   limit 1];
        } catch(Exception e) {}
        if (so == null) {
            so = new SAP_Sales_Org__c();
            so.Sales_Org_Code__c = sorg;
            so.Language_Code__c = lang;
            insert so;
        }
        return so;
    }
    
    static SAP_Distribution_Channel__c getDistChan(String dchan) {
        dc = null;
        try {
            dc = [select Distribution_Channel_Code__c 
                    from SAP_Distribution_Channel__c
                   where Distribution_Channel_Code__c = :dchan
                   limit 1];
        } catch(Exception e) {}
        if (dc == null) {
            dc = new SAP_Distribution_Channel__c();
            dc.Distribution_Channel_Code__c = dchan;
            dc.Language_Code__c = lang;
            insert dc;
        }
        return dc;
    }
    
    static void test01() {
        List<Label_Request__c> lrs = new List<Label_Request__c>{lr};
        System.debug(lr);
        Label_Request_Functions.updateLabelRequestResponse(responseText);
        System.debug(lr);
    }
    
}