public class IRF_Unit {

    public IRF_Unit__c sobj {get; set;}
    public Boolean selected {get; set;}
    public Integer   recNum {get; set;}

    public String   scEdate {get; set;}
    public String   scPrice {get; set;}
    public String   scUdesc {get; set;}
    
    public IRF_Unit() {this(new IRF_Unit__c());}
          
    public IRF_Unit(IRF_Unit__c u) {this(u, 1);}
    
    public IRF_Unit(IRF_Unit__c u, Integer n) {
    	recNum = n; selected = false; sobj = u;
    	debug('sobj = ' + sobj);
    }
        
    private void debug(String s) {System.debug(LoggingLevel.INFO, 'IRF_Unit : ' + s);}
    
}