@isTest
class NewsControllerTest {
    
    @isTest
    static void getMarketFilterOptionsTest() {
        NewsController testClass = new NewsController();
        
        // no markets
        System.assertEquals( null, testClass.getMarketFilterOptions() );
        
        // markets, but null RSS data
        Market__c testMarket = new Market__c(Name='testMarketName',Alias__c='testMarketAlias',Tag__c='APP-TestMarket');
        insert testMarket;
        System.assertEquals( null, testClass.getMarketFilterOptions() );
        
        // markets, but empty RSS data
        testMarket.RSS_Feeds__c = '';
        upsert testMarket;
        System.assertEquals( null, testClass.getMarketFilterOptions() );
        
        // Run as user with access to test market
        Profile prof = [Select Id, Name From Profile Where Name = 'System Administrator'];

        User u = new User();
        u.FirstName = 'TestFest';
        u.LastName = 'User123';
        u.alias = 'tuser123';
        u.Username = 'Test@testengineroom.com.test';
        u.Email = 'Test@testengineroom.com.test';
        u.emailencodingkey = 'UTF-8';
        u.languagelocalekey = 'en_US';
        u.localesidkey = 'en_US';
        u.profileId = prof.Id;
        u.timezonesidkey = 'America/Los_Angeles';
        insert u;
        
        System.runAs(u) {
            // test group
            Group grp = new Group();
            grp.Name = 'APP-TestMarket-Group';
            grp.DeveloperName = 'APP_TestMarket_Group';
            insert grp;
            // test groupMember
            GroupMember gm = new GroupMember();
            gm.UserOrGroupId = u.id;
            gm.GroupId = grp.id;
            insert gm;
            
            // markets with some RSS data
            testMarket.RSS_Feeds__c = 'TestFeed';
            upsert testMarket;
            System.assertEquals( 'TestFeed', testClass.getMarketFilterOptions()[0].RSS_Feeds__c );
        }
    }
    
    @isTest
    static void setMarketFilterTest() {
        NewsController testClass = new NewsController();
        Market__c testMarket = new Market__c(Name='testMarketName',Alias__c='testMarketAlias');
        
            // taken from Market__c content: building & construction
        testMarket.RSS_Feeds__c = 'http://compositebuild.com/feed/;http://compositespavilion.com/?feed=rss2';
        insert testMarket;
        
        // add market filter
        System.assertEquals( null, testClass.setMarketFilter() );        
        testClass.filterMarketId = testMarket.Id;
        System.assertEquals( testMarket.Id, testClass.filterMarketId );
        testClass.addNewMarketFilters = true;
        System.assertEquals( true, testClass.addNewMarketFilters );
        
        // remove only market filter
        testClass.addNewMarketFilters = false;
        System.assertEquals( null, testClass.setMarketFilter() );
        System.assertEquals( null, testClass.getRSSItems() );
    }
    
    @isTest
    static void getRSSItemsTest() {
        NewsController testClass = new NewsController();
        
        system.assertEquals( null, testClass.getRSSItems() );
        
        Market__c testMarket = new Market__c(Name='testMarketName',Alias__c='testMarketAlias');
            // taken from Market__c content: building & construction
        testMarket.RSS_Feeds__c = 'http://compositebuild.com/feed/;http://compositespavilion.com/?feed=rss2';
        insert testMarket;
        testClass.filterMarketId = testMarket.Id;
        testClass.addNewMarketFilters = true;
        testClass.setMarketFilter();
        
        System.assertNotEquals( null, testClass.getRSSItems() );
        System.assert( testClass.getRSSItems().size() > 0 );
    }
}