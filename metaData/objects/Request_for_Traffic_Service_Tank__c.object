<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Request for Traffic Service - Tanks</relationshipLabel>
        <relationshipName>R00N300000016qH0EAI</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Approximate_Dimensions__c</fullName>
        <externalId>false</externalId>
        <label>Approximate Dimensions (Diameter/Height)</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Approximate_Weight__c</fullName>
        <externalId>false</externalId>
        <label>Approximate Weight</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Asset_Number__c</fullName>
        <externalId>false</externalId>
        <label>Asset Number</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CIS_Materials__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Last Contents</label>
        <referenceTo>CIS_Materials__c</referenceTo>
        <relationshipLabel>Request for Traffic Service - Tanks</relationshipLabel>
        <relationshipName>R00N300000016qHUEAY</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Capital_Asset_Notify_Equipment_Service__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Notify Equipment Services for Transfer of Asset.</inlineHelpText>
        <label>Capital Asset - Notify Equipment Service</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Last_Contained_Other__c</fullName>
        <externalId>false</externalId>
        <label>Last Contained Other</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <externalId>false</externalId>
        <label>Quantity (Number)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Request_for_Traffic_Service__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>RTS Number</label>
        <referenceTo>Request_for_Traffic_Service__c</referenceTo>
        <relationshipLabel>Tanks</relationshipLabel>
        <relationshipName>R00N300000016qHAEAY</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Special_Instructions_Comments__c</fullName>
        <externalId>false</externalId>
        <label>Special Instructions/Comments</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Tank_has_been_emptied_and_rinsed__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Tank has been emptied and rinsed?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Type_of_Tank__c</fullName>
        <externalId>false</externalId>
        <label>Type of Tank</label>
        <picklist>
            <picklistValues>
                <fullName>Bulk Tank</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Tank w/Basin</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>U/S Tank</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>U/S Tank w/Basin</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Volume_Gallons__c</fullName>
        <externalId>false</externalId>
        <label>Volume (Gallons)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Volume__c</fullName>
        <externalId>false</externalId>
        <label>Volume</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Request for Traffic Service - Tank</label>
    <nameField>
        <displayFormat>TANK-{00000000}</displayFormat>
        <label>RTS Tank #</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Request for Traffic Service - Tanks</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Quantity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Type_of_Tank__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Asset_Number__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CIS_Materials__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Volume_Gallons__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Approximate_Dimensions__c</lookupDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
