<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Distribution object - Customer has inquired about trading documents electronically with Ashland (EDI, XML, B2B).</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Annual_Potential_Amount_Sales__c</fullName>
        <externalId>false</externalId>
        <label>Annual (Potential) Amount Sales</label>
        <precision>14</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Annual_Potential_GP__c</fullName>
        <externalId>false</externalId>
        <label>Annual (Potential) GP%</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Annual_Potential_Sales_Volume__c</fullName>
        <externalId>false</externalId>
        <label>Annual (Potential) Sales Volume</label>
        <precision>12</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Average_Line_Items_Per_Order__c</fullName>
        <externalId>false</externalId>
        <label>Average Line Items Per Order</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Average_Orders_Per_Month__c</fullName>
        <externalId>false</externalId>
        <label>Average Orders Per Month</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Avg_Days_Order_Placed_Before_Ship_Date__c</fullName>
        <description>Number of days the customer places the order before Ashland needs to ship the order.</description>
        <externalId>false</externalId>
        <label>Avg. Days Order Placed Before Ship Date</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Electronic Connection Requests</relationshipLabel>
        <relationshipName>R00N30000000zWuyEAE</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Documents_To_Trade_Electronically__c</fullName>
        <externalId>false</externalId>
        <label>Documents To Trade Electronically</label>
        <picklist>
            <picklistValues>
                <fullName>Place Orders</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Send Planning Schedules</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Send Shipping Schedules</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Receive Order Conformation</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Receive Advance Ship Notification</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Receive Invoices</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Pay Invoices</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Electronic Fund Transfers</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BarCoding on Labels</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>How_ADC_Receives_Customers_Orders__c</fullName>
        <externalId>false</externalId>
        <label>How ADC Receives Customer&apos;s Orders</label>
        <picklist>
            <picklistValues>
                <fullName>Phone</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Fax</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>E-Mail</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>3</visibleLines>
    </fields>
    <label>Electronic Connection Request</label>
    <nameField>
        <displayFormat>E-Request-{000000}</displayFormat>
        <label>Electronic Connection Request Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Electronic Connection Requests</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Average_Orders_Per_Month__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Avg_Days_Order_Placed_Before_Ship_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATED_DATE</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>OWNER.FIRST_NAME</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>OWNER.LAST_NAME</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>UPDATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>LAST_ACTIVITY</lookupDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
