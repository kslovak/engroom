<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Material Plants</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>COGS_Currency__c</fullName>
        <externalId>false</externalId>
        <label>COGS Currency</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>COGS_String__c</fullName>
        <externalId>false</externalId>
        <label>COGS String</label>
        <length>25</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>COGS__c</fullName>
        <externalId>false</externalId>
        <label>COGS</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Commodity_Code__c</fullName>
        <externalId>false</externalId>
        <label>Commodity Code</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Deletion_Flag__c</fullName>
        <description>MARC / LVORM</description>
        <externalId>false</externalId>
        <inlineHelpText>The material and plant combination has been marked for deletion in SAP.  Contact Product Manager for Questions.</inlineHelpText>
        <label>Deletion Flag</label>
        <length>1</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Delivery_Time_in_Days__c</fullName>
        <description>MARC-PLIFZ</description>
        <externalId>false</externalId>
        <label>Delivery Time in Days</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Lead_Time_in_Days__c</fullName>
        <description>MARC-WZEIT</description>
        <externalId>false</externalId>
        <label>Lead Time in Days</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Material_General_Data__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Material General Data</label>
        <referenceTo>Material_General_Data2__c</referenceTo>
        <relationshipLabel>Material Plants</relationshipLabel>
        <relationshipName>Material_Plants</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Material_Number__c</fullName>
        <description>MARC / MATNR</description>
        <externalId>true</externalId>
        <label>Material Number</label>
        <length>20</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Material_Origin_Country__c</fullName>
        <description>MARC / HERKL</description>
        <externalId>false</externalId>
        <label>Material Origin Country</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Plant_Address__c</fullName>
        <externalId>false</externalId>
        <formula>SAP_Plant__r.City_Name__c &amp; &apos;, &apos; &amp;  SAP_Plant__r.Region__c &amp; &apos; &apos; &amp;  SAP_Plant__r.Postal_Code__c</formula>
        <label>Plant Address</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Plant_Code__c</fullName>
        <description>MARC / WERKS</description>
        <externalId>true</externalId>
        <label>Plant Code</label>
        <length>4</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Plant_Name__c</fullName>
        <externalId>false</externalId>
        <formula>SAP_Plant__r.Plant_Name__c</formula>
        <label>Plant Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Record_Key__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>MATNR + WERKS + VKORG</description>
        <externalId>true</externalId>
        <label>Record Key</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Required_Lead_Time__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Lead time is in calendar days and does NOT include transit time. The lead time is Order to Ship.</inlineHelpText>
        <label>Required Lead Time</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SAP_Plant__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>SAP Plant</label>
        <referenceTo>SAP_Plant__c</referenceTo>
        <relationshipLabel>Material Plants</relationshipLabel>
        <relationshipName>Material_Plants</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SAP_Sales_Org__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>SAP Sales Org</label>
        <referenceTo>SAP_Sales_Org__c</referenceTo>
        <relationshipLabel>Material Plant2</relationshipLabel>
        <relationshipName>Material_Plant2</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SalesOrg_Code__c</fullName>
        <externalId>true</externalId>
        <label>SalesOrg Code</label>
        <length>4</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SalesOrg_Desc__c</fullName>
        <externalId>false</externalId>
        <formula>SAP_Sales_Org__r.Sales_Org_Description__c</formula>
        <label>SalesOrg Desc</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Third_Party_Plant__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Third Party Plant?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Material Plant2</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Material_Number__c</columns>
        <columns>Plant_Code__c</columns>
        <columns>Plant_Name__c</columns>
        <columns>SalesOrg_Code__c</columns>
        <columns>SalesOrg_Desc__c</columns>
        <columns>Deletion_Flag__c</columns>
        <columns>Record_Key__c</columns>
        <columns>Lead_Time_in_Days__c</columns>
        <columns>Required_Lead_Time__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>MP2-{0000000}</displayFormat>
        <label>MP2 ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Material Plant2</pluralLabel>
    <searchLayouts/>
    <sharingModel>Private</sharingModel>
    <sharingReasons>
        <fullName>SalesAreaGroup__c</fullName>
        <label>SalesAreaGroup</label>
    </sharingReasons>
    <sharingRecalculations>
        <className>Material_Plant_SharingRecalc</className>
    </sharingRecalculations>
</CustomObject>
