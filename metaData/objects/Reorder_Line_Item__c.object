<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Material2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup to Material Sales Data 2</description>
        <externalId>false</externalId>
        <label>Material</label>
        <referenceTo>Material_Sales_Data2__c</referenceTo>
        <relationshipLabel>Reorder Line Items</relationshipLabel>
        <relationshipName>Reorder_Line_Items1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Material_Description__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK(&apos;/&apos;&amp; Id, Material2__r.Material_Desc__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Material Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Material__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISNUMBER(Material2__r.Material_Number__c), TEXT(VALUE(Material2__r.Material_Number__c)), Material2__r.Material_Number__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Material</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Plant_Name__c</fullName>
        <externalId>false</externalId>
        <label>Plant Name</label>
        <length>150</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <description>Quantity of requested material</description>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>12</precision>
        <required>true</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reorder__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup to the master reorder record.</description>
        <externalId>false</externalId>
        <label>Reorder</label>
        <referenceTo>Reorder__c</referenceTo>
        <relationshipLabel>Reorder Line Items</relationshipLabel>
        <relationshipName>Reorder_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Required_Lead_Time__c</fullName>
        <externalId>false</externalId>
        <label>Required Lead Time</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Tank_Number__c</fullName>
        <externalId>false</externalId>
        <label>Tank Number</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UOM_Code__c</fullName>
        <externalId>false</externalId>
        <label>UOM Code</label>
        <length>4</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UOM__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Material2__r.Sales_UOM_Desc__c==null,Material2__r.Material_General_Data__r.Base_UOM_Desc__c,Material2__r.Sales_UOM_Desc__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Unit of Measurement</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Reorder Line Item</label>
    <nameField>
        <displayFormat>RLI-{000000}</displayFormat>
        <label>Reorder Line Item Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Reorder Line Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
