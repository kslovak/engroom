<apex:component >
    
    
    <apex:attribute name="articleLeft" description="Left-hand article"
                        type="ArticleWrapper" required="false" />                        
    <apex:attribute name="articleRight" description="Right-hand article"
                        type="ArticleWrapper" required="false" />
    
    
    <apex:outputPanel rendered="{!articleLeft != null || articleRight != null}">
        <section class="news-spotlight bg_gray">
            <div class="container no-padding-x">
                <div class="row">
                    <!-- Article1 -->
                    <apex:outputPanel rendered="{!articleLeft != null}">
                        <apex:variable value="blank" var="a1_target" rendered="{!ArticleLeft.article.Opens_New_Tab__c && (ArticleLeft.article.Content_Link_Id__c == null || ArticleLeft.article.Content_Link_Id__c == '')}" />
                        <div class="col-sm-6 spotlight">
                            <h2 class="lowercase">
                                <apex:outputText value="{!ArticleLeft.article.Article_header__c}" />
                            </h2>
                            <div class="spotlight-content">
                                <div class="profile-image">
                                    <apex:outputText value="{!ArticleLeft.article.Preview_Image__c}" escape="false" />
                                </div>
                                <div class="profile-text">
                                    <apex:outputPanel rendered="{!ArticleLeft.article.Expert_Position__c != ''}">
                                        <p class="subtitle">
                                            <apex:outputText value="{!ArticleLeft.article.Expert_Position__c}" />
                                        </p>
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!ArticleLeft.article.Tagline__c != ''}">
                                        <p class="subtitle">
                                            <apex:outputText value="{!ArticleLeft.article.Tagline__c}" />
                                        </p>
                                    </apex:outputPanel>
                                    <p class="description">
                                        <apex:outputText value="{!ArticleLeft.article.Preview__c}" escape="true" />
                                    </p>
                                </div>
                            </div>
                            <apex:outputPanel rendered="{!articleLeft.ctaLink != ''}">
                                <div class="cta hide-desktop">
                                    <div class="button-container cta-left">
                                        <a class="cta-btn" href="{!articleLeft.ctaLink}" title="{!articleLeft.article.Button_text__c}" target="{!a1_target}">
                                            <apex:outputText value="{!articleLeft.article.Button_text__c}" escape="true" />
                                        </a>
                                    </div>
                                </div>
                            </apex:outputPanel>
                        </div>
                    </apex:outputPanel>
                    <!-- /ArticleLeft -->
                    <!-- ArticleRight -->
                    <apex:outputPanel rendered="{!articleRight != null}">
                        <apex:variable value="blank" var="a2_target" rendered="{!ArticleRight.article.Opens_New_Tab__c && (ArticleRight.article.Content_Link_Id__c == null || ArticleRight.article.Content_Link_Id__c == '')}" />
                        <div class="col-sm-6 spotlight">
                            <h2 class="lowercase">
                                <apex:outputText value="{!ArticleRight.article.Article_header__c}" />
                            </h2>
                            <div class="row spotlight-content">
                                <div class="profile-image">
                                    <apex:outputText value="{!ArticleRight.article.Preview_Image__c}" escape="false" />
                                </div>
                                <div class="profile-text">
                                    <apex:outputPanel rendered="{!ArticleRight.article.Expert_Position__c != ''}">
                                        <p class="subtitle">
                                            <apex:outputText value="{!ArticleRight.article.Expert_Position__c}" />
                                        </p>
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!ArticleRight.article.Tagline__c != ''}">
                                        <p class="subtitle">
                                            <apex:outputText value="{!ArticleRight.article.Tagline__c}" />
                                        </p>
                                    </apex:outputPanel>
                                    <p class="description">
                                        <apex:outputText value="{!ArticleRight.article.Preview__c}" escape="true" />
                                    </p>
                                </div>
                            </div>
                            <apex:outputPanel rendered="{!articleRight.ctaLink != ''}">
                                <div class="cta hide-desktop">
                                    <div class="button-container cta-left">
                                        <a class="cta-btn" href="{!articleRight.ctaLink}" title="{!ArticleRight.article.Button_text__c}" target="{!a2_target}">
                                            <apex:outputText value="{!ArticleRight.article.Button_text__c}" escape="true" />
                                        </a>
                                    </div>
                                </div>
                            </apex:outputPanel>
                        </div>
                    </apex:outputPanel>
                    <!-- /ArticleRight -->
                </div>
                <apex:outputPanel rendered="{!articleLeft.ctaLink != '' || articleRight.ctaLink != ''}">
                    <div class="row cta">
                        <!-- ArticleLeft -->
                        <apex:outputPanel rendered="{!articleLeft.ctaLink != ''}">
                            <apex:variable value="blank" var="a1_target" rendered="{!ArticleLeft.article.Opens_New_Tab__c && (ArticleLeft.article.Content_Link_Id__c == null || ArticleLeft.article.Content_Link_Id__c == '')}" />
                            <div class="col-sm-6 hide-mobile">
                                <div class="button-container cta-right">
                                    <a class="cta-btn" href="{!articleLeft.ctaLink}" title="{!ArticleLeft.article.Button_text__c}" target="{!a1_target}">
                                        <apex:outputText value="{!ArticleLeft.article.Button_text__c}" escape="true" />
                                    </a>
                                </div>
                            </div>
                        </apex:outputPanel>
                        <!-- /ArticleLeft -->
                        <!-- ArticleRight -->
                        <apex:outputPanel rendered="{!articleRight.ctaLink != ''}">
                            <apex:variable value="blank" var="a2_target" rendered="{!ArticleRight.article.Opens_New_Tab__c && (ArticleRight.article.Content_Link_Id__c == null || ArticleRight.article.Content_Link_Id__c == '')}" />
                            <div class="col-sm-6">
                                <div class="button-container cta-right">
                                    <a class="cta-btn" href="{!articleRight.ctaLink}" title="{!ArticleRight.article.Button_text__c}" target="{!a2_target}">
                                        <apex:outputText value="{!ArticleRight.article.Button_text__c}" escape="true" />
                                    </a>
                                </div>
                            </div>
                        </apex:outputPanel>
                        <!-- /ArticleRight -->
                    </div>
                </apex:outputPanel>
            </div>
        </section>
    </apex:outputPanel>
</apex:component>