<apex:page standardController="VI_Product_Template__c" 
           extensions="VI_Product_Template_Controller"
           showHeader="false" standardStylesheets="true"
           action="{!initAction}">

    <apex:stylesheet value="{!URLFOR($Resource.VIResources, 'css/main.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.VIResources, 'css/login.css')}" />

    <apex:includeScript value="{!URLFOR($Resource.VIResources, 'js/jquery-1.4.2.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.VIResources, 'js/utils.js')}" />
    <script type="text/javascript">
    
    jQuery.noConflict();
    
    jQuery(function() {
        disableButtonsOnSubmit("{!$Component.formPanel}");
    });
    
    </script>

    <apex:pageBlock id="block0" rendered="{!renderBlock0}">
        <table><tr><td>
        <apex:outputLink value="{!$Page.VISiteHome}">
        <apex:image url="{!URLFOR($Resource.VIResources, 'img/logo-banner.jpg')}"
                    height="95"/>
        </apex:outputLink></td></tr>
        </table>
    </apex:pageBlock>
    
    <apex:form >
    <apex:outputPanel id="formPanel">
    <apex:pageMessages />
    <apex:pageBlock id="block1" rendered="{!renderBlock1}">
        <table class="prodtbl">
            <tr>
                <!--
                <td class="title" width="60%"><apex:outputField value="{!vpt.Name}"/></td>
                -->
                <td colspan="2" height="60px">
	        <apex:actionStatus id="productStat" rendered="{!renderProducts}">
	            <apex:facet name="start">
	                <apex:outputText styleClass="bold" value=" Please wait ...">
	                    <apex:image value="{!$Resource.spinner}"/>
	                </apex:outputText>
	            </apex:facet>
	            <apex:facet name="stop">
	            <apex:outputPanel >
	            <!--
	            <apex:outputPanel styleClass="title">Product</apex:outputPanel>&nbsp;
	            -->
	            <apex:selectList styleClass="prodlist" value="{!vptId}" size="1">
	                <apex:actionSupport event="onchange" action="{!setProduct}" 
	                                rerender="formPanel" status="productStat"/>
	                <apex:selectOptions value="{!productNames}"/>
	            </apex:selectList>&nbsp;&nbsp;

                <apex:outputPanel rendered="{!renderPrevBtn}">&nbsp;&nbsp;</apex:outputPanel>
	            <apex:commandLink action="{!prevProduct}"
	                              title="Previous Product"
	                              rendered="{!renderPrevBtn}"
	                              rerender="formPanel" status="productStat">
                    <apex:image url="{!URLFOR($Resource.VIResources, 'img/control_start.png')}"/>
	            </apex:commandLink>
                <apex:outputPanel rendered="{!renderPlayBtn}">&nbsp;&nbsp;</apex:outputPanel>
                <apex:commandLink action="{!enablePoller}"
                                  title="Auto Refresh On"
                                  rendered="{!renderPlayBtn}"
                                  rerender="formPanel" status="productStat">
                    <apex:image url="{!URLFOR($Resource.VIResources, 'img/control_play.png')}"/>
                </apex:commandLink>
                <apex:outputPanel rendered="{!renderStopBtn}">&nbsp;&nbsp;</apex:outputPanel>
                <apex:commandLink action="{!disablePoller}"
                                  title="Auto Refresh Off"
                                  rendered="{!renderStopBtn}"
                                  rerender="formPanel" status="productStat">
                    <apex:image url="{!URLFOR($Resource.VIResources, 'img/control_pause.png')}"/>
                </apex:commandLink>
                <apex:outputPanel rendered="{!renderNextBtn}">&nbsp;&nbsp;</apex:outputPanel>
                <apex:commandLink action="{!nextProduct}"
                                  title="Next Product"
                                  rendered="{!renderNextBtn}"
                                  rerender="formPanel" status="productStat">
                    <apex:image url="{!URLFOR($Resource.VIResources, 'img/control_end.png')}"/>
                </apex:commandLink>

	            <br/><br/>
	            For more information about this product &nbsp;
                <apex:commandLink action="{!showLeadForm}" 
                                  value="click here" styleClass="alink"
                                  rerender="formPanel" status="productStat"/>&nbsp;
                                  to enter your Contact Info
	            </apex:outputPanel>
	            </apex:facet>
	        </apex:actionStatus></td></tr>

            <tr><td class="prodcol1">&nbsp;</td>
                <td rowspan="3" class="prodcol2">
                <apex:outputField value="{!vpt.Product_Image__c}"/></td></tr>
            <tr><td class="prodcol1">
                <apex:outputField value="{!vpt.Product_Features__c}"/></td></tr>
            <tr><td class="prodcol1">
                <apex:outputField value="{!vpt.Product_Benefits__c}"/></td></tr>
        </table><br/>
        
    </apex:pageBlock>

    <apex:actionPoller action="{!nextProduct}" rerender="block1"
                       enabled="{!pollerEnabled}" interval="5" 
                       status="productStat"/>
    
    <apex:pageBlock id="block2" mode="edit" rendered="{!renderBlock2}">
         <apex:pageBlockButtons location="bottom">
            <apex:actionStatus id="b2BtnStat">
                <apex:facet name="start">
                    <apex:outputText styleClass="bold" value=" Please wait ...">
                        <apex:image value="{!$Resource.spinner}"/>
                    </apex:outputText>
                </apex:facet>
                <apex:facet name="stop"><apex:outputPanel >
                    <apex:commandButton value="Submit" action="{!submit}" 
                                        rerender="formPanel" status="b2BtnStat"/>
                    <apex:commandButton value="Cancel" action="{!setProduct}"
                                        rerender="formPanel" status="b2BtnStat"/>
                </apex:outputPanel></apex:facet></apex:actionStatus>
         </apex:pageBlockButtons>
            
        <apex:pageBlockSection columns="1">
            <apex:pageBlockSectionItem dataStyleClass="b2td1" labelStyleClass="b2td1">
                <apex:outputLabel value="Product"/>
                <apex:outputText value="{!vpt.Name}"/>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel for="company" value="Company" />
                <apex:outputPanel layout="block" styleClass="requiredInput">
                    <div class="requiredBlock"></div>
                    <apex:inputText id="company" value="{!company.value}" 
                                    styleClass="{!IF(NOT(ISBLANK(company.error)), 'error', '')}" 
                                    size="40" maxlength="40" />
                    <apex:outputPanel styleClass="errorMsg" layout="block" 
                                      rendered="{!NOT(ISBLANK(company.error))}">
                        <strong>Error:</strong>&nbsp;
                        <apex:outputText value="{!company.error}" />
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel for="firstName" value="First Name" />
                <apex:outputPanel layout="block" styleClass="requiredInput">
                    <div class="requiredBlock"></div>
                    <apex:inputText id="firstName" value="{!firstName.value}" 
                                    styleClass="{!IF(NOT(ISBLANK(firstName.error)), 'error', '')}" 
                                    size="40" maxlength="40" />
                    <apex:outputPanel styleClass="errorMsg" layout="block" 
                                      rendered="{!NOT(ISBLANK(firstName.error))}">
                        <strong>Error:</strong>&nbsp;
                        <apex:outputText value="{!firstName.error}" />
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel for="lastName" value="Last Name" />
                <apex:outputPanel layout="block" styleClass="requiredInput">
                    <div class="requiredBlock"></div>
                    <apex:inputText id="lastName" value="{!lastName.value}" 
                                    styleClass="{!IF(NOT(ISBLANK(lastName.error)), 'error', '')}" 
                                    size="40" maxlength="40" />
                    <apex:outputPanel styleClass="errorMsg" layout="block" 
                                      rendered="{!NOT(ISBLANK(lastName.error))}">
                        <strong>Error:</strong>&nbsp;
                        <apex:outputText value="{!lastName.error}" />
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel for="email" value="Email" />
                <apex:outputPanel layout="block" styleClass="requiredInput">
                    <div class="requiredBlock"></div>
                    <apex:inputText id="email" value="{!email.value}" 
                                    styleClass="{!IF(NOT(ISBLANK(email.error)), 'error', '')}" 
                                    size="40" maxlength="40" />
                    <apex:outputPanel styleClass="errorMsg" layout="block" 
                                      rendered="{!NOT(ISBLANK(email.error))}">
                        <strong>Error:</strong>&nbsp;
                        <apex:outputText value="{!email.error}" />
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel for="phone" value="Phone" />
                <apex:outputPanel layout="block" styleClass="requiredInput">
                    <div class="requiredBlock"></div>
                    <apex:inputText id="phone" value="{!phone.value}" 
                                    styleClass="{!IF(NOT(ISBLANK(phone.error)), 'error', '')}" 
                                    size="40" maxlength="40" />
                    <apex:outputPanel styleClass="errorMsg" layout="block" 
                                      rendered="{!NOT(ISBLANK(phone.error))}">
                        <strong>Error:</strong>&nbsp;
                        <apex:outputText value="{!phone.error}" />
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel for="street" value="Address : Street" />
                <apex:outputPanel layout="block" styleClass="requiredInput">
                    <div class="requiredBlock"></div>
                    <apex:inputText id="street" value="{!street.value}" 
                                    styleClass="{!IF(NOT(ISBLANK(street.error)), 'error', '')}" 
                                    size="40" maxlength="40" />
                    <apex:outputPanel styleClass="errorMsg" layout="block" 
                                      rendered="{!NOT(ISBLANK(street.error))}">
                        <strong>Error:</strong>&nbsp;
                        <apex:outputText value="{!street.error}" />
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel for="city" value="City" />
                <apex:outputPanel layout="block" styleClass="requiredInput">
                    <div class="requiredBlock"></div>
                    <apex:inputText id="city" value="{!city.value}" 
                                    styleClass="{!IF(NOT(ISBLANK(city.error)), 'error', '')}" 
                                    size="40" maxlength="40" />
                    <apex:outputPanel styleClass="errorMsg" layout="block" 
                                      rendered="{!NOT(ISBLANK(city.error))}">
                        <strong>Error:</strong>&nbsp;
                        <apex:outputText value="{!city.error}" />
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel for="state" value="State" />
                <apex:outputPanel layout="block" styleClass="requiredInput">
                    <div class="requiredBlock"></div>
                    <apex:inputText id="state" value="{!state.value}" 
                                    styleClass="{!IF(NOT(ISBLANK(state.error)), 'error', '')}" 
                                    size="40" maxlength="40" />
                    <apex:outputPanel styleClass="errorMsg" layout="block" 
                                      rendered="{!NOT(ISBLANK(state.error))}">
                        <strong>Error:</strong>&nbsp;
                        <apex:outputText value="{!state.error}" />
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel for="postalCode" value="Postal Code" />
                <apex:outputPanel layout="block" styleClass="requiredInput">
                    <div class="requiredBlock"></div>
                    <apex:inputText id="postalCode" value="{!postalCode.value}" 
                                    styleClass="{!IF(NOT(ISBLANK(postalCode.error)), 'error', '')}" 
                                    size="40" maxlength="40" />
                    <apex:outputPanel styleClass="errorMsg" layout="block" 
                                      rendered="{!NOT(ISBLANK(postalCode.error))}">
                        <strong>Error:</strong>&nbsp;
                        <apex:outputText value="{!postalCode.error}" />
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel for="description" value="Comments" />
                <apex:inputTextArea id="description" value="{!description.value}" 
                                    cols="80" rows="6" />
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>                
    </apex:pageBlock>
    
    <apex:pageBlock id="block3" rendered="{!renderBlock3}">
        <apex:outputPanel layout="block" styleClass="center">
            <b>Thank you for your interest in {!vpt.Name}.<br/><br/> 
               You will hear back from us soon.</b><br/><br/>
            <apex:commandButton value="Home" action="{!goHome}" immediate="true" />
        </apex:outputPanel>
<!--   
        <apex:outputText escape="false" value="{!lead1Txt}"/><br/><br/>
-->
    </apex:pageBlock>
    </apex:outputPanel>
    </apex:form>

    <style>
        .bold {font-weight: bold;}
        .center {text-align: center;}
        .nowrap {white-space: nowrap;}
        .alink {font-weight: bold; text-decoration: underline;}
        .title {font-weight: bold; text-decoration: none;}
        .prodtbl  {width: 770px;}
        .prodcol1 {width: 500px;}
        .prodcol2 {width: 250px;}
        .prodlist {font-weight: bold; width: 400px;}
        .prodtbl td {vertical-align: top;}
        .prodtbl td img {vertical-align: top; padding: 2px;}
        .b2td1 {font-weight: bold; padding-bottom: 6px; vertical-align: middle;}
    </style>
    
</apex:page>