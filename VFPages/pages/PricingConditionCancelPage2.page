<apex:page standardController="Pricing_Condition__c" 
           extensions="PricingConditionListController"
           recordSetVar="pricingConditions"
           sidebar="false">

    <apex:includeScript value="{!URLFOR($Resource.jQuery, 'js/jquery-1.6.2.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.jQuery, 'js/jquery-ui-1.8.16.custom.min.js')}" />
    <style type="text/css">
        @import "{!URLFOR($Resource.jQuery, 'css/redmond/jquery-ui-1.8.16.custom.css')}";
    </style>

    <apex:form id="actionForm">
        <apex:pageMessages escape="false"/>
        <apex:pageBlock Title="Pricing Conditions to Cancel" mode="edit">

            <apex:pageBlockButtons location="top">
                <apex:commandButton value="Submit" action="{!cancelPricingConditions}"
                                    disabled="{!disableSubmit}"
                                    onclick="openWaitMsg();"/>
                <apex:commandButton value="Go Back" action="{!gotoListPage1}" 
                                    onclick="openWaitMsg();"/>
            </apex:pageBlockButtons>

            <apex:pageBlockSection columns="1">
                <apex:pageBlockSectionItem dataStyle="text-align: center; font-weight: bold;">
                    <apex:outputText value="Review the following Prices before submitting to Cancel."/>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection><br/>

            <apex:pageBlockSection columns="1">
                <apex:pageBlockSectionItem >
                    <apex:outputPanel ><table class="tbl2" width="100%"><tr>
                        <td class="td1">Reasons to Cancel PCNs : </td>
                        <td class="td2">
                            <apex:outputText value="{!userComments}"/></td>
                        <td class="td1"><apex:outputPanel >
                            Cancel the PCNs From Date : </apex:outputPanel></td>
                        <td class="td2">
                            <apex:outputPanel >
                            <apex:outputField id="cancelFromDate" value="{!pc.Valid_From_Date__c}"/>
                            </apex:outputPanel></td>
                    </tr></table></apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection columns="1">
                <apex:pageBlockSectionItem >
                    <apex:outputText value=""/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:pageBlockTable value="{!selectedPricingConditions}" var="p">
                        <apex:column >
                            <apex:facet name="header">Account Name<br/>and Number<br/>Customer Group</apex:facet>
                            <apex:outputLink value="/{!p.pricingCondition.Account__r.Id}"> 
                                <apex:outputText value="{!p.pricingCondition.Account__r.Name}"/>
                            </apex:outputLink>
                            <br/><apex:outputText value="{!p.accountNumber}"/>
                            <br/><apex:outputText value="{!p.pricingCondition.Account__r.SAP_Customer_Group_1_Desc__c}"/>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">Material Name<br/>and Number</apex:facet>
                            <apex:outputLink value="/{!p.pricingCondition.Material2__r.Id}"> 
                                <apex:outputText value="{!p.pricingCondition.Material2__r.Material_Desc__c}"/>
                            </apex:outputLink>
                            <br/><apex:outputText value="{!p.materialNumber}"/>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">Invoice<br/>Price</apex:facet>
                            <apex:outputLink value="/{!p.pricingCondition.Id}">
                                <apex:outputField value="{!p.pricingCondition.Invoice_Price__c}"/>
                            </apex:outputLink>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">Valid From<br/>and To</apex:facet>
                            <apex:outputField value="{!p.pricingCondition.Valid_From_Date__c}"/><br/>
                            <apex:outputField value="{!p.pricingCondition.Valid_To_Date__c}"/>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">Record<br/>Status</apex:facet>
                            <apex:outputField value="{!p.pricingCondition.Record_Status__c}"/>
                        </apex:column>
                    </apex:pageBlockTable>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>

        </apex:pageBlock>

        <div id="waitMsgDiv">
            <apex:image styleClass="spinner" value="{!$Resource.spinner}"/> Please wait ...
        </div>

    </apex:form>
    <script type="text/javascript">
        var $j = jQuery.noConflict();

        function log(msg) {if(window.console){console.log(msg);}}

        function setfocus(fid) {
            var fld = document.getElementById(fid);
            if (fld != null) {fld.focus();}
        }

        function onClick_AllChkbox(rowCbClass, allChkbox) {
            $j("."+rowCbClass).each(function() {
                if (this.disabled == false) {
                    this.checked = allChkbox.checked;
                }
            });
        }

        function onClick_RowChkbox(rowCbClass, allCbClass) {
            var allChkbox = $j("."+allCbClass).get(0);
            allChkbox.checked = true;
            $j("."+rowCbClass).each(function() {
                if (!this.checked) {
                    allChkbox.checked = false;
                    return false;
                }
            });
        }

        $j("#waitMsgDiv").dialog({
            modal: true, autoOpen: false, dialogClass: "no-title",
            height: 30, width: 40, resizable: false
        });

        function openWaitMsg() {$j("#waitMsgDiv").dialog("open");}

        function closeWaitMsg() {$j("#waitMsgDiv").dialog("close");}

        function getConfirm(q) {
            openWaitMsg(); answer = confirm(q);
            if (answer == 0) {closeWaitMsg(); return false;}
            return true;
        }

    </script>

    <style>
        #waitMsgDiv {display: none; font-weight: bold; height: 20px;
                     text-align: center; vertical-align: middle;}
        .no-title .ui-dialog-titlebar {display: none;}
        .spinner    {margin-bottom: -3px;}

        .bold   {font-weight: bold;}
        .nowrap {white-space: nowrap;}
        .right  {text-align: right;}

        .tbl2 .td1 {width: 15%; font-size: 91%; font-weight: bold; text-align: right; white-space: nowrap; color: #4A4A56;}
        .tbl2 .td2 {width: 35%;}
    </style>
</apex:page>