<apex:page controller="VIMassEnrollmentController" sidebar="false">

    <apex:includeScript value="{!URLFOR($Resource.VIResources, 'js/jquery-1.4.2.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.VIResources, 'js/utils.js')}" />
    
    <style>
        .list     {border-collapse: collapse; empty-cells: show; width: 100%}
        .msg      {font-weight: normal; font-style: italic; text-align: left;}
        .nowrap   {white-space: nowrap;}
        .center   {text-align: center;}
        .right    {text-align: right;}
        .tbl1 td  {white-space: nowrap; padding: 4px;}
    </style>

    <script type="text/javascript">

        jQuery.noConflict();
        
        function toggleAllRows(step, cb) {
            var checkboxes = jQuery('.row'+step);
            var checked = cb.checked;
            for (var i = 0; i < checkboxes.length; i++) {
                if (!checkboxes[i].disabled) {
                    checkboxes[i].checked = checked;
                    hiOnChecked(checkboxes[i]);
                }
            }
        }
        
        function toggleCurrentRow(step) {
            var checkboxes = jQuery('.row'+step);
            var allChecked = true;
            for (var i = 0; i < checkboxes.length; i++) {
                hiOnChecked(checkboxes[i]);
                if (!checkboxes[i].checked) {
                   allChecked = false;
                }
            }
            var allCheckBox = jQuery('.allRows'+step)[0];
            allCheckBox.checked = allChecked;
        }
        
        function hiOnChecked(cb) {
            var checked = cb.checked;
            var row = cb.parentNode.parentNode;
            row.style.fontWeight="normal";
            row.bgColor="#f3f3ec";
            if (checked) {
                row.style.fontWeight="bold";
                row.bgColor="#eeecd1";
            }
        }
    
        function uncheckAllOther(step, cb) {
            var checkboxes = jQuery('.row'+step);
            var checked = cb.checked;
            for (var i = 0; i < checkboxes.length; i++) {
                checkboxes[i].checked = false;
                hiOnChecked(checkboxes[i]);
            }
            cb.checked = checked;
            hiOnChecked(cb);
        }
        
        function isPromoSelected(step) {
            var b = false;
            var checkboxes = jQuery('.row'+step);
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    b = true; break;
                }
            }
            if (!b) {
                alert('Select a Promotion to enroll');
                return false;
            }
            return true;
        }
        
    </script>
    
    <apex:form id="theForm">

        <apex:pageBlock title="Step 1 of 5" mode="edit" 
                        id="step0" rendered="{!currStep = 0}"> 
        
            <apex:pageBlockButtons style="padding: 0.3em; text-align: left;" location="top">

                <apex:actionStatus id="step0status">
                    <apex:facet name="start">
                        <apex:outputText escape="false" style="font-weight: bold;" 
                                         value=" Please wait ... ">
                            <apex:image value="{!$Resource.spinner}"/>
                        </apex:outputText>
                    </apex:facet>
                    <apex:facet name="stop">
                        <apex:outputPanel style="font-weight: bold; white-space: nowrap;">
                            <apex:panelGrid columns="3">
                            <apex:outputPanel >
                                <apex:outputLabel for="sorglist" value=" Sales Org : "/>
                                <apex:selectList id="sorglist" value="{!salesOrg}" size="1">
                                    <apex:selectOptions value="{!salesOrgs}"/>
                                </apex:selectList>
                            </apex:outputPanel>
    
                            <apex:outputPanel >
                                <apex:outputLabel for="cgslist" value=" Customer Group 5 : "/>
                                <apex:selectList id="cgslist" value="{!custGroup5}" size="1">
                                    <apex:selectOptions value="{!custGroup5s}"/>
                                    <apex:actionSupport event="onchange" 
                                                        action="{!getPromotions}"
                                                        rerender="theForm" 
                                                        status="step0status"/>
                                </apex:selectList>
                            </apex:outputPanel>
                            
                            <apex:outputPanel >
                            <apex:outputPanel styleClass="btn">
                                <apex:outputText value=" Select Promotion » " />
                                <apex:actionSupport event="onclick" 
                                                    action="{!getPromotions}"
                                                    rerender="theForm" 
                                                    status="step0status"/>
                            </apex:outputPanel></apex:outputPanel>
                            </apex:panelGrid>
                        </apex:outputPanel>

                    </apex:facet>
                </apex:actionStatus>

            </apex:pageBlockButtons>

            <apex:pageMessages />
        
            <apex:outputPanel layout="block" styleClass="msg">
                <apex:outputText value="Select a Customer Group or click on Select Promotion to continue"/>
            </apex:outputPanel>

        </apex:pageBlock>
        
        <apex:pageBlock title="Step 2 of 5" mode="edit" 
                        id="step1" rendered="{!currStep = 1}"> 
        
            <script>

                jQuery(function() {
                    toggleCurrentRow(1);
                });

            </script>
        
            <apex:pageBlockButtons style="padding: 0.3em; text-align: left;" location="top">

                <apex:actionStatus id="step1status">
                    <apex:facet name="start">
                        <apex:outputText escape="false" style="font-weight: bold;" 
                                         value=" Please wait ... ">
                            <apex:image value="{!$Resource.spinner}"/>
                        </apex:outputText>
                    </apex:facet>
                    <apex:facet name="stop">
                        <apex:outputPanel style="font-weight: bold; white-space: nowrap;">
             
                            <table class="tbl1"><tr>
                            <td><apex:outputPanel >
                                <apex:outputLabel for="sorglist" value="Sales Org : "/>
                                <apex:selectList id="sorglist" value="{!salesOrg}" size="1">
                                    <apex:selectOptions value="{!salesOrgs}"/>
                                </apex:selectList>
                            </apex:outputPanel></td>
    
                            <td><apex:outputPanel >
                                <apex:outputLabel for="cgslist" value="Customer Group 5 : "/>
                                <apex:selectList id="cgslist" value="{!custGroup5}" size="1">
                                    <apex:selectOptions value="{!custGroup5s}"/>
                                    <apex:actionSupport event="onchange" 
                                                        action="{!getPromotions}"
                                                        rerender="theForm" 
                                                        status="step1status"/>
                                </apex:selectList>
                            </apex:outputPanel></td>
                            
                            <td><apex:outputPanel >Installer :&nbsp; 
                                <apex:inputText value="{!facilitySearch}" maxlength="30" size="15"/>
                            </apex:outputPanel></td>
                            
                            </tr><tr>
                            
                            <td><apex:outputPanel >
                                <apex:outputLabel value="Show Enrolled also?"/>
                                <apex:inputCheckbox value="{!showEnrolled}" />
                            </apex:outputPanel></td>

                            <td><apex:outputPanel styleClass="btn">
                                <apex:outputText value=" Select Installers to Enroll » " />
                                <apex:actionSupport event="onclick" 
                                                    action="{!getInstallers}"
                                                    rerender="theForm" 
                                                    status="step1status"/>
                            </apex:outputPanel>&nbsp;&nbsp;

                            <apex:outputPanel styleClass="btn">
                                <apex:outputText value=" Edit Submitted Enrollments » " />
                                <apex:actionSupport event="onclick" 
                                                    action="{!editEnrollments}"
                                                    rerender="theForm" 
                                                    status="step1status"/>
                            </apex:outputPanel></td>

                            <td><apex:outputPanel >
                                <apex:outputLabel for="maxLimit" value="# of Installers : "/>
                                <apex:selectList id="maxLimit" value="{!maxLimit}" size="1">
                                    <apex:selectOption itemValue="100" itemLabel="100"/>
                                    <apex:selectOption itemValue="200" itemLabel="200"/>
                                    <apex:selectOption itemValue="300" itemLabel="300"/>
                                    <apex:selectOption itemValue="400" itemLabel="400"/>
                                    <apex:selectOption itemValue="500" itemLabel="500"/>
                                </apex:selectList>
                            </apex:outputPanel></td>
                            
                            </tr></table>

                        </apex:outputPanel>

                    </apex:facet>
                </apex:actionStatus>

            </apex:pageBlockButtons>

            <apex:pageMessages />
        
            <apex:outputPanel layout="block" styleClass="msg">
                <apex:outputText value="Select a Promotion and click on Select Installers to continue"/>
            </apex:outputPanel>

            <apex:outputPanel layout="none">
                <table class="list">
                    <thead class="dr-table-head">
                        <tr class="headerRow">
                            <th scope="col">Row#</th>
                            <th scope="col">&nbsp;</th>
                            <th scope="col">Promotion Name</th>
                            <th scope="col">Promotion Description</th>
                            <th scope="col">Group</th>
                            <th scope="col">Start Date</th>
                            <th scope="col">End Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <apex:repeat value="{!promos}" var="p">
                            <apex:variable var="v" value="{!p.variable}"/>
                            <tr class="dataRow">
                                <td class="dataCell right" width="2%">
                                    <apex:outputText value="{!p.recordNumber}"/>
                                </td>
                                <td class="dataCell actionColumn right" width="4%">
                                    <apex:inputCheckbox id="sel" value="{!p.selected}" 
                                                        styleClass="row1" 
                                                        onclick="uncheckAllOther(1, this);" />
                                </td>
                                <td class="dataCell" width="20%">
                                    <apex:outputText value="{!p.promoName}"/>
                                </td>
                                <td class="dataCell" width="45%">
                                    <apex:outputText value="{!p.promoDesc}"/>
                                </td>
                                <td class="dataCell center" width="3%">
                                    <apex:outputField value="{!p.promo.Promotional_Group__c}"/>
                                </td>
                                <td class="dataCell" width="7%">
                                    <apex:outputField value="{!p.promo.Start_Date__c}"/>
                                </td>
                                <td class="dataCell" width="7%">
                                    <apex:outputField value="{!p.promo.End_Date__c}"/>
                                </td>
                            </tr>
                        </apex:repeat>
                    </tbody>
                </table>
            </apex:outputPanel>

        </apex:pageBlock>
        
        <apex:pageBlock title="Step 3 of 5" mode="edit"
                        id="step2" rendered="{!currStep = 2}"> 
        
            <script>

                jQuery(function() {
                    toggleCurrentRow(2);
                });

            </script>
        
            <apex:pageBlockButtons style="padding: 0.3em;" location="top">

                <apex:actionStatus id="step2status">
                    <apex:facet name="start">
                        <apex:outputText escape="false" style="font-weight: bold;" 
                                         value=" Please wait ... ">
                            <apex:image value="{!$Resource.spinner}"/>
                        </apex:outputText>
                    </apex:facet>
                    <apex:facet name="stop">
                        <apex:outputPanel style="font-weight: bold; white-space: nowrap;">
             
                        <table class="tbl1"><tr>
                            <td><apex:outputPanel >
                                <apex:outputLabel for="sorglist" value=" Sales Org : "/>
                                <apex:selectList id="sorglist" value="{!salesOrg}" size="1">
                                    <apex:selectOptions value="{!salesOrgs}"/>
                                </apex:selectList>
                            </apex:outputPanel></td>

                            <td><apex:outputPanel >
                                <apex:outputLabel for="cgslist" value=" Customer Group 5 : "/>
                                <apex:selectList id="cgslist" value="{!custGroup5}" size="1">
                                    <apex:selectOptions value="{!custGroup5s}"/>
                                    <apex:actionSupport event="onchange" 
                                                        action="{!getPromotions}"
                                                        rerender="theForm" 
                                                        status="step2status"/>
                                </apex:selectList>
                            </apex:outputPanel></td>

                            <td><apex:outputPanel >Installer :&nbsp; 
                                <apex:inputText value="{!facilitySearch}" disabled="true" size="15"/>
                            </apex:outputPanel></td>

                            <td><apex:outputPanel styleClass="btn">
                                <apex:outputText value=" « Back " />
                                <apex:actionSupport event="onclick" 
                                                    action="{!cancel}"
                                                    rerender="theForm" 
                                                    status="step2status"/>
                            </apex:outputPanel></td>

                            <td><apex:outputPanel styleClass="btn" 
                                              rendered="{!renderAccountList}">
                                <apex:outputText value=" Select Promotion Materials » " />
                                <apex:actionSupport event="onclick" 
                                                    action="{!selectEnrollmentMaterials}"
                                                    rerender="theForm" 
                                                    status="step2status"/>
                            </apex:outputPanel></td>
                        </tr></table>
                            
                        </apex:outputPanel>

                    </apex:facet>
                </apex:actionStatus>

            </apex:pageBlockButtons>

            <apex:pageMessages />
        
            <apex:outputPanel layout="block" styleClass="msg">
                <apex:outputText value="Select Installers to enroll in Promotion : {!selectedPromo.promoName}" 
                                 rendered="{!renderAccountList}"/>
            </apex:outputPanel>

            <apex:outputPanel layout="none" rendered="{!renderAccountList}">
                <table class="list">
                    <thead class="dr-table-head">
                        <tr class="headerRow">
                            <th scope="col">Row #</th>
                            <th scope="col">Facility Name</th>
                            <th scope="col">Location Name</th>
                            <th scope="col">Partner Types</th>
                            <th scope="col">Account Owner</th>
                            <th scope="col">Address</th>
                            <th scope="col" class="actionColumn right"
                                style="padding-right: 5px;">All
                                <apex:inputCheckbox id="selAll" styleClass="allRows2" 
                                                    onclick="toggleAllRows(2, this);" />
                            </th>
                        </tr>
                    </thead>
                    <tbody>
        
                        <apex:repeat value="{!enrolls}" var="e">
                            <tr class="dataRow">
                                <td class="dataCell right" width="4%">
                                    <apex:outputText value="{!e.recordNumber}"/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td class="dataCell" width="20%">
                                    <apex:outputLink value="/{!e.viacc.fac.Id}">
                                        <apex:outputText value="{!e.viacc.fac.Name}"/><br/>
                                        <apex:outputText value="{!e.viacc.fac.AccountNumber}" />
                                    </apex:outputLink>
                                </td>
                                <td class="dataCell" width="20%">
                                    <apex:outputLink value="/{!e.viacc.loc.Id}">
                                        <apex:outputText value="{!e.viacc.loc.Name}"/><br/>
                                        <apex:outputText value="{!e.viacc.loc.AccountNumber}" />
                                    </apex:outputLink>
                                </td>
                                <td class="dataCell" width="15%">
                                    <apex:outputField value="{!e.viacc.acc.SAP_Partner_Functions__c}" />
                                </td>
                                <td class="dataCell" width="10%">
                                    <apex:outputField value="{!e.viacc.acc.Owner.Name}" />
                                </td>
                                <td class="dataCell" width="25%">
                                    <apex:outputLabel for="sel">
                                        <apex:outputText value="{!e.viacc.acc.BillingStreet}" /><br/>
                                        <apex:outputText value="{0}, {1} {2}">
                                            <apex:param value="{!e.viacc.acc.BillingCity}" />
                                            <apex:param value="{!e.viacc.acc.BillingState}" />
                                            <apex:param value="{!e.viacc.acc.BillingPostalCode}" />
                                        </apex:outputText>
                                    </apex:outputLabel>
                                </td>
                                <td class="dataCell actionColumn right" width="6%">
                                    <apex:inputCheckbox id="sel" 
                                                        disabled="{!e.isEnrolled}" 
                                                        value="{!e.selected}" 
                                                        styleClass="row2" 
                                                        onclick="toggleCurrentRow(2);" />
                                </td>
                            </tr>
                        </apex:repeat>
                    </tbody>
                </table>
            </apex:outputPanel>

        </apex:pageBlock>
        
        <apex:pageBlock title="Step 4 of 5" mode="edit"
                        id="step3" rendered="{!currStep = 3}">

            <apex:pageBlockButtons >
                <apex:commandButton value=" « Back " action="{!goback}" immediate="true" />
                <apex:commandButton value=" Review Materials » " action="{!reviewEnrollmentMaterials}" />
                <apex:commandButton value="Cancel" action="{!cancel}" immediate="true" />
            </apex:pageBlockButtons>

            <apex:pageMessages />
            
            <apex:pageBlockSection title="Promotion Information" columns="1">
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Program Name" />
                    <apex:outputPanel layout="none">
                        <apex:outputText value="{!selectedPromo.promoName}" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Start Date" />
                    <apex:outputField value="{!selectedPromo.promo.Start_Date__c}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputText value="End Date" />
                    <apex:outputField value="{!selectedPromo.promo.End_Date__c}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Description" />
                    <apex:outputPanel layout="none">
                        <apex:outputText value="{!selectedPromo.promoDesc}" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection title="Default Suggested Quantities">
                <apex:facet name="body">
                    <apex:outputPanel layout="none">
                        <script type="text/javascript">
                        
                        function confirmApplyQuantities() {
                            var count = jQuery(".lineItemQuantity[value!='']").length;
                            if (count > 0) {
                                return confirm("This will overwrite any previously entered quantities. Are you sure you wish to continue?");
                            }
                            return true;
                        }
                        
                        </script>
                        
                        <apex:outputPanel layout="none">
                            <table class="list" border="0" cellspacing="0" cellpadding="0">
                                <thead class="dr-table-head">
                                    <tr class="headerRow" style="vertical-align: bottom;">
                                        <th scope="col" width="30%">Material Name</th>
                                        <apex:repeat value="{!defaultQuantities}" var="defaultQuantity">
                                            <th scope="col" style="white-space: normal;">
                                                <apex:outputText value="{!defaultQuantity.Material__c}" />
                                            </th>
                                        </apex:repeat>
                                        <apex:outputPanel layout="none" rendered="{!showCost}">
                                            <th scope="col" class="CurrencyElement" width="8%">&nbsp;</th>
                                        </apex:outputPanel>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="dataRow odd even first last">
                                        <td class="dataCell" width="30%">&nbsp;</td>
                                        <apex:repeat value="{!defaultQuantities}" var="defaultQuantity">
                                            <td class="dataCell">
                                                <apex:inputField id="defaultQuantity" value="{!defaultQuantity.Quantity__c}" 
                                                                 style="width: 4em;" />
                                                <input type="hidden" name="materials" 
                                                       value="{!defaultQuantity.Material__c}({!defaultQuantity.Material_SKU__c})" />
                                                <input type="hidden" 
                                                       name="Total: {!defaultQuantity.Material__c}({!defaultQuantity.Material_SKU__c})" 
                                                       value="{!$Component.defaultQuantity}" />
                                            </td>
                                        </apex:repeat>
                                        <apex:outputPanel layout="none" rendered="{!showCost}">
                                            <td class="dataCell CurrencyElement" width="8%">&nbsp;</td>
                                        </apex:outputPanel>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <div style="width: 100%; text-align: right; margin: 0; padding: 4px 0 0 0;">
                                 <apex:commandButton value="Apply Quantities"
                                                     onclick="return confirmApplyQuantities();" 
                                                     action="{!applyQuantities}"/>
                            </div>
                        </apex:outputPanel>
                    </apex:outputPanel>
                </apex:facet>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection title="Material Quantities">
                <apex:facet name="body">
                    <apex:outputPanel layout="none">
                        <table class="list" border="0" cellspacing="0" cellpadding="0">
                            <thead class="dr-table-head">
                                <tr class="headerRow" style="vertical-align: middle;">
                                    <th scope="col" width="30%">Location</th>
                                    <apex:repeat value="{!selectedPromo.promo.Promotion_Materials__r}" 
                                                 var="promotionMaterial">
                                        <th scope="col" style="white-space: normal;">
                                            <apex:outputText value="{!promotionMaterial.Material__c}" /><br/>
                                            <apex:outputText value="({!promotionMaterial.Warehouse_SKU__c})" />
                                        </th>
                                    </apex:repeat>
                                    <apex:outputPanel layout="none" rendered="{!showCost}">
                                        <th scope="col" class="CurrencyElement" width="8%">Cost</th>
                                    </apex:outputPanel>
                                </tr>
                            </thead>
                            <tbody>
                                <apex:repeat value="{!locationEnrolls}" var="e">
                                    <tr class="dataRow" onmouseover="if (window.hiOn){hiOn(this);}" 
                                                        onmouseout="if (window.hiOff){hiOff(this);}">
                                        <td class="dataCell" width="30%">
                                            <apex:outputText value="{!e.viacc.acc.Name} ({!e.viacc.accountNumber})" /><br/>
                                            <apex:outputText value="{!e.viacc.acc.BillingStreet}" /><br/>
                                            <apex:outputText value="{0}, {1} {2}">
                                                <apex:param value="{!e.viacc.acc.BillingCity}" />
                                                <apex:param value="{!e.viacc.acc.BillingState}" />
                                                <apex:param value="{!e.viacc.acc.BillingPostalCode}" />
                                            </apex:outputText>
                                        </td>
                                        <apex:repeat value="{!e.materials}" var="material">
                                            <td class="dataCell">
                                                <apex:inputField id="quantity" value="{!material.Quantity__c}" 
                                                                 styleClass="lineItemQuantity" style="width: 4em;" />
                                                <input type="hidden" name="{!material.Material__c}({!material.Material_SKU__c})" 
                                                                     value="{!$Component.quantity}" />
                                            </td>
                                        </apex:repeat>
                                        <apex:outputPanel layout="none" rendered="{!showCost}">
                                            <td class="dataCell CurrencyElement" width="8%">
                                                <apex:outputText value="{0,number,currency}">
                                                    <apex:param value="{!e.totalCost}" />
                                                </apex:outputText>
                                            </td>
                                        </apex:outputPanel>
                                    </tr>
                                </apex:repeat>
                            </tbody>
                            <apex:outputPanel layout="none" rendered="{!showCost}">
                                <tfoot>
                                    <tr class="totalRow">
                                        <th scope="col" width="30%">&nbsp;</th>
                                        <apex:repeat value="{!selectedPromo.promo.Promotion_Materials__r}" var="promotionMaterial">
                                            <th scope="col">&nbsp;</th>
                                        </apex:repeat>
                                        <th scope="col" class="CurrencyElement" width="8%">
                                            <apex:outputText value="{0,number,currency}">
                                                <apex:param value="{!totalCost}" />
                                            </apex:outputText>
                                        </th>
                                    </tr>
                                </tfoot>
                            </apex:outputPanel>
                        </table>
                        
                        <apex:outputPanel layout="block" rendered="{!showCost}"
                                          style="width: 100%; text-align: right; margin: 0; padding: 4px 0 0 0;">
                            <apex:commandButton action="{!calculateTotalCost}" 
                                                value="Calculate Total Cost" 
                                                rendered="{!showCost}" />
                        </apex:outputPanel>
                    </apex:outputPanel>
                </apex:facet>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
        <apex:pageBlock title="Step 5 of 5" mode="maindetail"
                        id="step4" rendered="{!currStep = 4}">

            <apex:pageBlockButtons >
                <apex:commandButton value=" « Back " action="{!goback}" immediate="true" />
                <apex:commandButton value=" Submit " action="{!submit}" />
                <apex:commandButton value="Cancel" action="{!cancel}" immediate="true" />
            </apex:pageBlockButtons>

            <apex:pageMessages />
            
            <apex:pageBlockSection title="Promotion Information" columns="1">
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Program Name" />
                    <apex:outputPanel layout="none">
                        <apex:outputText value="{!selectedPromo.promoName}" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Start Date" />
                    <apex:outputField value="{!selectedPromo.promo.Start_Date__c}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputText value="End Date" />
                    <apex:outputField value="{!selectedPromo.promo.End_Date__c}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Description" />
                    <apex:outputPanel layout="none">
                        <apex:outputText value="{!selectedPromo.promoDesc}" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection title="Material Quantities">
                <apex:facet name="body">
                    <apex:outputPanel layout="none">
                        <table class="list" border="0" cellspacing="0" cellpadding="0">
                            <thead class="dr-table-head">
                                <tr class="headerRow" style="vertical-align: middle;">
                                    <th scope="col" width="30%">Location</th>
                                    <apex:repeat value="{!selectedPromo.promo.Promotion_Materials__r}" 
                                                 var="promotionMaterial">
                                        <th scope="col" style="white-space: normal;">
                                            <apex:outputText value="{!promotionMaterial.Material__c}" /><br/>
                                            <apex:outputText value="({!promotionMaterial.Warehouse_SKU__c})" />
                                        </th>
                                    </apex:repeat>
                                    <apex:outputPanel layout="none" rendered="{!showCost}">
                                        <th scope="col" class="CurrencyElement" width="8%">Cost</th>
                                    </apex:outputPanel>
                                </tr>
                            </thead>
                            <tbody>
                                <apex:repeat value="{!locationEnrolls}" var="e">
                                    <tr class="dataRow" onmouseover="if (window.hiOn){hiOn(this);}" 
                                                        onmouseout="if (window.hiOff){hiOff(this);}">
                                        <td class="dataCell" width="30%">
                                            <apex:outputText value="{!e.viacc.acc.Name} ({!e.viacc.accountNumber})" /><br/>
                                            <apex:outputText value="{!e.viacc.acc.BillingStreet}" /><br/>
                                            <apex:outputText value="{0}, {1} {2}">
                                                <apex:param value="{!e.viacc.acc.BillingCity}" />
                                                <apex:param value="{!e.viacc.acc.BillingState}" />
                                                <apex:param value="{!e.viacc.acc.BillingPostalCode}" />
                                            </apex:outputText>
                                        </td>
                                        <apex:repeat value="{!e.materials}" var="material">
                                            <td class="dataCell">
                                                <apex:outputField id="quantity" value="{!material.Quantity__c}" 
                                                                  styleClass="lineItemQuantity" style="width: 4em;" />
                                            </td>
                                        </apex:repeat>
                                        <apex:outputPanel layout="none" rendered="{!showCost}">
                                            <td class="dataCell CurrencyElement" width="8%">
                                                <apex:outputText value="{0,number,currency}">
                                                    <apex:param value="{!e.totalCost}" />
                                                </apex:outputText>
                                            </td>
                                        </apex:outputPanel>
                                    </tr>
                                </apex:repeat>
                            </tbody>
                            <apex:outputPanel layout="none" rendered="{!showCost}">
                                <tfoot>
                                    <tr class="totalRow">
                                        <th scope="col" width="30%">&nbsp;</th>
                                        <apex:repeat value="{!selectedPromo.promo.Promotion_Materials__r}" var="promotionMaterial">
                                            <th scope="col">&nbsp;</th>
                                        </apex:repeat>
                                        <th scope="col" class="CurrencyElement" width="8%">
                                            <apex:outputText value="{0,number,currency}">
                                                <apex:param value="{!totalCost}" />
                                            </apex:outputText>
                                        </th>
                                    </tr>
                                </tfoot>
                            </apex:outputPanel>
                        </table>
                    </apex:outputPanel>
                </apex:facet>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
        <apex:pageBlock title="Enrollment - Message" mode="maindetail"
                        id="step9" rendered="{!currStep = 9}">

            <apex:pageBlockButtons >
                <apex:commandButton value=" « Select another Promotion" action="{!cancel}" immediate="true" />
            </apex:pageBlockButtons>

            <apex:pageMessages />
            
        </apex:pageBlock>
        
    </apex:form>
</apex:page>