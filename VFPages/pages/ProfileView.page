<apex:page showHeader="false" standardStylesheets="false" applyHtmlTag="false" applyBodyTag="false" docType="html-5.0" standardController="User" extensions="UserAccountController">

<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Ashland Partner Portal</title>
        
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
        <c:CommunityStyles />
        
        <style>
            
            /* Form state */
            
            .field-view, .field-edit {
                height:30px;
            }
            
            .user-info-container .field-view {
                display: block;
            }
            
            .user-info-container .field-edit {
                display: none;
            }
            
            .user-info-container .edit-form-init {
                display: block;
            }
            
            .user-info-container .edit-form-submit {
                display: none;
            }
            
            .user-info-container.edit-mode .field-view {
                display: none;
            }
            
            .user-info-container.edit-mode .field-edit {
                display: block;
            }
            
            .user-info-container.edit-mode .edit-form-init {
                display: none;
            }
            
            .user-info-container.edit-mode .edit-form-submit {
                display: block;
            }
            
        </style>
    
    </head>

    <body id="page-top">
        <apex:messages />
        <c:AccountInfoBar />
        <c:CommunityNav />
        <c:CommunitySearch />
        <c:Branding />
        
        <section id="content-main" style="padding-bottom:25px;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2><apex:outputText value="{!User.FirstName + ' ' + User.LastName}" /></h2>
                    </div>
                </div>
                <div class="row user-info-container">
                    <apex:form >
                    <div class="col-sm-12 col-med-6 col-lg-4">
                        <h4>Info</h4>
                    
                        <div class="col-sm-6 col-md-5 col-lg-4">
                            First Name:
                        </div>
                        <div class="col-sm-6 col-md-7 col-lg-8">
                            <div class="field-view">
                                <apex:outputText value="{!User.firstName}" />
                            </div>
                            <div class="field-edit">
                                <apex:outputText value="{!User.firstName}" />
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-5 col-lg-4">
                            Last Name:
                        </div>
                        <div class="col-sm-6 col-md-7 col-lg-8">
                            <div class="field-view">
                                <apex:outputText value="{!User.lastName}" />
                            </div>
                            <div class="field-edit">
                                <apex:outputText value="{!User.lastName}" />
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-5 col-lg-4">
                            Email:
                        </div>
                        <div class="col-sm-6 col-md-7 col-lg-8">
                            <div class="field-view">
                                <apex:outputText value="{!User.email}" />
                            </div>
                            <div class="field-edit">
                                <apex:outputText value="{!User.email}" />
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-5 col-lg-4">
                            Company:
                        </div>
                        <div class="col-sm-6 col-md-7 col-lg-8">
                            <div class="field-view">
                                <apex:outputText value="{!User.CompanyName}" />
                            </div>
                            <div class="field-edit">
                                <apex:outputText value="{!User.CompanyName}" />
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-5 col-lg-4">
                            Office Phone:
                        </div>
                        <div class="col-sm-6 col-md-7 col-lg-8">
                            <div class="field-view">
                                <apex:outputText value="{!User.phone}" />
                            </div>
                            <div class="field-edit">
                                <apex:inputField value="{!User.phone}" />
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-5 col-lg-4">
                            Mobile Phone:
                        </div>
                        <div class="col-sm-6 col-md-7 col-lg-8">
                            <div class="field-view">
                                <apex:outputText value="{!User.MobilePhone}" />
                            </div>
                            <div class="field-edit">
                                <apex:inputField value="{!User.MobilePhone}" />
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-5 col-lg-4">
                            Title:
                        </div>
                        <div class="col-sm-6 col-md-7 col-lg-8">
                            <div class="field-view">
                                <apex:outputText value="{!User.Title}" />
                            </div>
                            <div class="field-edit">
                                <apex:inputField value="{!User.Title}" />
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-5 col-lg-4">
                            Market Focus:
                        </div>
                        <div class="col-sm-6 col-md-7 col-lg-8">
                            <div class="field-view">
                                <apex:outputText value="{!User.MarketFocus__c}" />
                            </div>
                            <div class="field-edit">
                                <apex:inputField value="{!User.MarketFocus__c}" />
                            </div>
                        </div>
                        
                    </div>
                                
                        <div class="col-sm-12">
                            <apex:outputPanel rendered="{!hasEditPermission}">
                                <button class="edit-form-init">Edit</button>
                            </apex:outputPanel>
                            <apex:commandButton action="{!saveUser}" value="Submit Changes" styleClass="edit-form-submit" />
                        </div>
                    </apex:form>
                </div>
                <!--<chatter:feed entityId="{!User.Id}"/>-->
            </div>
        </section>
        <hr />
        <footer id="footer">
            <c:CommunitiesFooter />
        </footer>
        
        <c:CommunitiesScripts />
        
        <script type="text/javascript">
        
            document.querySelectorAll('.edit-form-init')[0].addEventListener('click',function(e) {
                e.preventDefault();
                document.querySelectorAll('.user-info-container')[0].className += ' edit-mode';
            });
        </script>
        
    </body>

</html>

</apex:page>